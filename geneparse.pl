#!/usr/bin/perl

#Copyright (C) 2006-2008 Keio University
#(Kris Popendorf) <comp@bio.keio.ac.jp> (2006)
#
#This file is part of Murasaki.
#
#Murasaki is free software: you can redistribute it and/or modify
#it under the terms of the GNU General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.
#
#Murasaki is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU General Public License for more details.
#
#You should have received a copy of the GNU General Public License
#along with Murasaki.  If not, see <http://www.gnu.org/licenses/>.

###############
## loads/reads generic sequences -- krisp
###############

use File::Basename;
use Getopt::Std;
use POSIX qw{pow};

$Getopt::Std::STANDARD_HELP_VERSION=true;
getopts('eclrfmhqsv'); #use -c/l
my $verbose=$opt_v;
my $ignoreBogus=getYesNo($ENV{MURASAKI_SR_IGNOREBOGUS}) if defined getYesNo($ENV{MURASAKI_SR_IGNOREBOGUS});
my $preserveIUPAC=getYesNo($ENV{MURASAKI_GP_KEEPIUPAC}) if defined getYesNo($ENV{MURASAKI_GP_KEEPIUPAC});
if($opt_h){HELP_MESSAGE();exit(0);}

($filename,$outfile)=@ARGV;
$inpath=(fileparse($filename))[1] if $filename;

($filename,$start,$stop,$rangeonly)=($1,$2-1,$3,1) if $filename=~m/^(.*?)\[(\d+)\D(\d+)\]$/ and !-e $filename;

if($filename and -e $filename){
  my $openfile=$filename=~m/\.gz$/ ? "zcat $filename|":"<$filename";
  print STDERR "Opening $openfile\n" if $verbose;
  open(INF,$openfile) or die "Couldn't read $filename";
} else {
  if(!($opt_s or $filename eq '-')){
    print STDERR "Could not find file $filename.\nUse - as filename or -s to allow input from stdin\n";
    exit(-1);
  }
  print STDERR "File $filename not found. Waiting for input from stdin.\n" unless $opt_q;
  open(INF,"-");
}
if($outfile){
  open(OUTF,">$outfile");
  $opt_c=1; #never stick a newline on files...
} else {
  open(OUTF,">-");
}
$linenum=0;

$nogenome=1 if $opt_l or $opt_e;
my $lengthCache="$filename.length";
if($opt_l && -f $lengthCache){ #shortcut route!
  if(-M $filename > -M $lengthCache){
    open(my $lenfh,$lengthCache);
    print OUTF <$lenfh>;
    exit;
  }else{
    print STDERR "Length cache is old, refreshing...\n";
  }
}

$length=sneakyLength($filename) if $opt_f;
if(!$nogenome or ($nogenome and (!$opt_f or !$length))){
  $rbytes=read(INF,$_,300);
  push(@toCheck,split(/\n/,$_)) unless (seek(INF,0,0)); #try to return to start, otherwise keep data
  if(m/^[AGTCUN]+$/i){ #its a raw
    print STDERR "Loading raw file...\n";
    push(@toCheck,$_);
    goto LINES;
  }
  if(m/(\S+)\t(\d+)\t(\d+)\t(\d+)/){ #stitch format
    my ($startdir,$startcmd)=($0=~m!^(.*)/([^/]+)!);
    $startdir=$ENV{PWD}."/".$startdir unless $startdir=~m!^/!; #make absolute
    my $start=($startcmd ? "$startdir/$startcmd":"$0");
    my $localdir=(fileparse($filename, qr{\.[^.]*}))[1];
    my $totallength=0;
    print STDERR "Stitch file --- " unless $opt_q;
    push(@lines,<INF>);
    print STDERR " containing ".($#lines+1)." entries\n" unless $opt_q;
    @files=map {my ($name,$length,$start,$stop)=split(/\s+/,$_);$name} @lines;
    @files_dat=map {my ($name,$length,$start,$stop)=split(/\s+/,$_);
		    {name=>$name,
		       length=>$length,
			 start=>$start,
			   stop=>$stop}} @lines;
    $launch_opts=" -c" if $opt_c;
    $launch_opts.=" -l" if $opt_l;
    $launch_opts.=" -f" if $opt_f;
    $launch_opts.=" -e" if $opt_e;
    $launch_opts.=" -r" if $opt_r;
    $launch_opts.=" -q" if $opt_q;
    $launch_opts.=" -s" if $opt_s;
    $remaining=$#files;
    foreach my $filei (0..$#files){
      my $file=$files[$filei];
      my $file_dat=$files_dat[$filei];
#      $file=join("",$inpath,$file) if $inpath;
      chdir $localdir unless -f $file;
      die "File $file not found in $localdir or $startdir..." unless -f $file;
      my $cmd="$start $launch_opts $file";
#      print STDERR "Read $file via: $cmd\n";
      if($opt_l){
	$totallength+=$file_dat->{length};
	$totallength+=10 if $filei!=$#files;
      }else{
	print OUTF `$cmd`;
	print OUTF join('n' x 10,@parts) if $filei!=$#files;
      }
    }
#      print OUTF "nnnnnnnnnn" if $remaining--; #stick 10 n's between chromosomes
    if($opt_l){
      cacheLength($totallength);
      print $totallength."\n";
    }else{
      print OUTF "\n" unless $opt_c
    }
    exit(0);
  }

  my $subseq=1; #doing it this way, we skip the first header here in this block
  while(1){#skip gbk/fasta header info
    while(@toCheck){
      $_=shift(@toCheck);
      goto LINES if(m/^(ORIGIN\s*[^\w]?)$|^(\>.*?)$/); #signals start of genome
    }continue{$linenum++}
    $_=<INF>;
    push(@toCheck,split(/\n/,$_));
    die "File ended before genome started?" unless defined $_;
  }

 LINES:
  print STDERR "Extracting genome starting at line $linenum\n" unless $opt_q;

  $length=0;
  { do {
    $rbytes=read(INF,$_,1024);
    push(@toCheck,split(/\n/,$_));
    while(@toCheck){
      $_=shift(@toCheck);
      goto DONE if m!^//!;
      if(m/(^>).*/){
	s!(^>).*!NNNNNNNNNN!smg if $subseq; #insert 'N' x 10 at each new subsequence (to mimic stitch files)
	$subseq++;
      }
      s!(^;).*!!smg; #erase comments
      $_=~y/uU/tT/;
      if($ignoreBogus){
	$_=~s/[^atgcn]+//ig;
      }else{
	$_=~s/[^a-z]+//ig; #any non-alphabet codes willl stil be culled (technically IUPAC is only atgcnurymkwsbdhv but, whatever. this provides some extra flexibility).
	$_=~s/[^atgcn]/n/gi if(!$preserveIUPAC);
      }
      $_=~s/[atgc]/n/g if $opt_r;
      $chars=length($_);
      if(!$nogenome){
	if($rangeonly){
	  if($length>=$start){
	    if($length+$chars<$stop){
	      print OUTF $_;
	    }else{
	      print OUTF substr($_,0,$stop-$length);
	    }
	  } elsif($length+$chars>=$start){
	    print OUTF substr($_,$start-$length,$stop-$start);
	  }
	}else{
	  print OUTF $_;
	}
      }
      $length+=$chars;
      goto DONE if  $rangeonly and $length>=$stop;
    }
  } until(!$rbytes); }
 DONE:
  close(INF);
}
cacheLength($length);
if($opt_l){
  print "$length\n";
}else{
  print OUTF "\n" if $ENV{TERM} and !$opt_c and !$opt_e;
}

if($opt_e){
  $el=int(pow($length,.3)/2.5);
  $ew=int($el*.65);
  if($opt_m){
    print "[$ew:$el]";
  }else{
    print "$ew $el";
  }
  print "\n" unless $opt_c;
}

sub cacheLength {
  return if $rangeOnly;
  my $len=pop;
  open(my $lenfh,">$lengthCache") or return;
  print $lenfh "$len\n";
  close $lenfh;
}

sub sneakyLength(){
  ($filename)=@_;
  my ($name,$path,$suffix) = fileparse($filename, qr{\.[^.]*});
  return 0 unless $suffix eq ".fa";
  open(FILE,"<$filename");
  $line1=<FILE>;
  $line2=<FILE>;
  chomp($line2);
  seek(FILE,-length($line2)*2,SEEK_END);
  while(<FILE>){
    $lastline=$_;
    chomp($lastline);
  }
  $lines=`wc $filename -l`;
  $len=length($line2)*($lines-2)+length($lastline);;
  print STDERR "Fast length detection succeeded: $len\n" unless $opt_q;
  return $len;
}

sub main::HELP_MESSAGE(){
  print <<ENDTEXT
Usage: $0 [-e [-m]] [-c] [-l] [-r] [-f] [genome file [output file] ]
  -c specifies "clean output" ie only [agtc] (no final newline)
If you don't specify a genome file, $0 reads from standard input.
  -l requests only the length of the genome (avoids wasting memory)
  -r remap "repeats" (ie: lowercase letters) as "n"
  -f fast length detection (ie: if it's an .fa file, be sneaky)
  -e estimate suitable weight/length
  -m print estimate in murasaki [<weight>:<length>] format
  -q quiet operation (no notifications at all)
  -s allow input from stdin
A neat trick is to use -m to specify a random pattern directly to Mursaki.
For example:
  ./murasaki -p`./geneparse.pl -m -f -e -c seq/humanY.fa`

Another trick: using the filename you can specify a start/stop point to
extract from. for example:
  humanX.gbk[1025,3033] -- extracts humanX.gbk from base 1025 to 3033
   (the range is inclusive and starts at 1)

Behavior regarding what to do with non-ACGT codes can be set from the MURASAKI_SR_IGNOREBOGUS
environment variable. If MURASAKI_SR_IGNOREBOGUS can be some value like "true/yes/1" or "false/no/0".
If enabled non-ACGT are removed from the output stream. Default is disabled.
ENDTEXT
    ;
}

sub main::VERSION_MESSAGE(){
}


sub getYesNo {
  my ($v)=@_;
  return 1 if($v=~m/^(true|yes|1)$/);
  return 0 if($v=~m/^(false|no|0)$/);
  return undef;
}
