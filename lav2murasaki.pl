#!/usr/bin/perl

#Copyright (C) 2006-2008 Keio University
#(Kris Popendorf) <comp@bio.keio.ac.jp> (2006)
#
#This file is part of Murasaki.
#
#Murasaki is free software: you can redistribute it and/or modify
#it under the terms of the GNU General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.
#
#Murasaki is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU General Public License for more details.
#
#You should have received a copy of the GNU General Public License
#along with Murasaki.  If not, see <http://www.gnu.org/licenses/>.

use strict;
use Getopt::Long;
use Pod::Usage;
use File::Basename;
#use Data::Dump qw{dump};

our ($inf,$ungapped,$verbose);

my ($help,$man);
GetOptions('help|?' => \$help, man => \$man, ungapped => \$ungapped, 'verbose+'=>\$verbose);
pod2usage(1) if $help or scalar(@ARGV)<1;
pod2usage(-exitstatus => 0, -verbose => 4) if $man;

$inf=shift(@ARGV);

die "File ($inf) not found.\n" unless open(my $infh,$inf);

my ($in_name,$in_path,$in_suffix) = fileparse($inf, qr{\.[^.]*});
our $basename="$in_path/$in_name";
our %outfh;
open($outfh{anchors},">$basename.anchors");
open($outfh{details},">$basename.anchors.details");
open($outfh{seqs},">$basename.seqs");
open($outfh{bitscore},">$basename.anchors.bitscore") if $ungapped;
unlink("$basename.anchors.bitscore") if !$ungapped and -f "$basename.anchors.bitscore";

our %info;
our @seqs;
our $anchorsWritten=0;

my $state=0;
while(<$infh>){
  chomp;
  next unless $_;
  next if m/^\#/ && $state ne 0;
  if($state eq 0){
    die "Not a LAV file." unless $_ eq "#:lav";
    $state++;
  }elsif($state eq 1){
    die "Expected a stanza..." unless m/(\w) \{/;
    print "Parsing a '$1' stanza...\n" if $verbose>2 or ($verbose and $1 ne 'a');;
    parseStanza($1,$infh);
  }
}

print "Writing sequence data...\n" if $verbose;
writeSeqs();

print "Done. Anchors written: $anchorsWritten\n";
exit;

sub parseStanza {
  my ($type,$infh)=@_;
  my $subline=0;
  local $_;
  my $state;
  my %alignment;
  while(<$infh>){
    chomp $_;
    if($_ eq "}"){
      writeAnchor($alignment{chunks},$alignment{score}) if($type eq 'a' and !$ungapped);
      last;
    }
    if($type eq 'd'){
      s/^\s+//;
      $info{commandline}=$_;
      foreach my $i (0..4){
	$info{matrix}.=<$infh>;
      }
      $info{vals}=<$infh>;
      chomp $info{matrix};
      chomp $info{vals};
    }elsif($type eq 's'){
      m/^\s+\"([^"]*)" (\d+) (\d+) (\d+) (\d+)/ or die "$type stanza line $subline: Doesn't parse...";
      push(@{$info{seqs}},$1);
      $seqs[$subline]={file=>$1,start=>$2,stop=>$3,strand=>$4,contig=>$5};
      $seqs[$subline]->{file}=~s/-$// if $seqs[$subline]->{strand}>0;
      warn "Warning: $type stanza line $subline: Specified file not found..." unless -f $seqs[$subline]->{file};
    }elsif($type eq 'h'){
      m/^\s+"([^"]*)"/ or die "$type stanza line $subline: Couldn't find a description...";
      push(@{$info{description}},$1);
    }elsif($type eq 'a'){
      m/^\s+([sbel]) ((?:\d+ ?)+)/ or die "$type stanza line $subline: Weird alignment line: $_";
      $alignment{score}=$2 if($1 eq 's');
      if($ungapped){
	next unless $1 eq 'l';
	my @bits=split(/\s+/,$2);
	my (@chunks,$p);
	foreach my $i (0..$#seqs) {
	  $chunks[$i]={start=>shift(@bits)};
	}
	foreach my $i (0..$#seqs) {
	  my $c=shift(@bits);
	  $chunks[$i]->{stop}=$c;
	  if($seqs[$i]->{strand}>0){
	    ($chunks[$i]->{start},$chunks[$i]->{stop})=
	      ($seqs[$i]->{stop}-$chunks[$i]->{stop},
	       $seqs[$i]->{stop}-$chunks[$i]->{start});
	  }
	}
	$p=shift(@bits);
	writeAnchor(\@chunks,$p);
      }else{
	next if $1 eq 'l';
	my $field='start' if $1 eq 'b';
	$field='stop' if $1 eq 'e';
	my @bits=split(/\s+/,$2);
	$alignment{chunks}=[] unless exists($alignment{chunks});
	foreach my $i (0..$#seqs) {
	  my $c=shift(@bits);
	  my $f=$field;
	  ($c,$f) = ($seqs[$i]->{stop}-$c,$f eq 'start' ? 'stop':'start') if $seqs[$i]->{strand}>0;
	  $alignment{chunks}[$i]={} unless ref $alignment{chunks}[$i];
	  $alignment{chunks}[$i]->{$f}=$c;
	}
      }
    }elsif($type=~m/x|m/){ #dynamically repeatmasked regions. don't care.
    }else{
      warn "Unknown stanza type $type\n";
    }
  }continue{
    $subline++;
  }
  print "Done parsing $type stanza\n" if $verbose>2 or ($verbose>1 and $type ne 'a');
}

sub writeAnchor {
  my ($chunksr,$score)=@_;
  $anchorsWritten++;
  my @dats=map {($_->{start},$_->{stop},$_->{start}<0 ? "-":"+")} @$chunksr;
  my $bitscore=(int(($score/100*chunkLength(${$chunksr}[0]))));
  print {$outfh{anchors}} join("\t",@dats)."\n";
  print {$outfh{details}} join("\t",@dats,$score)."\n";
  print {$outfh{bitscore}} $bitscore."\n" if $ungapped;
}

sub chunkLength {
  my ($chunk)=@_;
  my $len=($$chunk{stop}-$$chunk{start})+1;
  warn "negative length? what's this crap? from ($$chunk{start} ~ $$chunk{stop})" unless $len>0;
  return $len;
}

sub writeSeqs {
  foreach my $seq (@seqs){
    print {$outfh{seqs}} $seq->{file}."\n";
  }
}

__END__

=head1 NAME

lav2murasaki.pl -- convert a LAV file (ie: from Blastz) into a Murasaki set of files

=head1 SYNOPSIS

lav2murasaki.pl [options] <input file>

=head1 OPTIONS

--ungapped|-u =>
LAV files have "gapped regions" composed of "ungapped regions". Using this option
creates 1 anchor for each ungapped region (this also provides a .bitscore file, as
each ungapped region contains a percent identity field).

--verbose|-v =>
More verbose output to stdout. Can be applied up to 3 times (anything beyond 1 gets
messy though, as each anchor gets 1 line of output).

=head1 COMMENTS

You and Toadofsky make beautiful music!

=cut
