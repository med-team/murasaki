#!/usr/bin/perl

#Copyright (C) 2006-2008 Keio University
#(Kris Popendorf) <comp@bio.keio.ac.jp> (2006)
#
#This file is part of Murasaki.
#
#Murasaki is free software: you can redistribute it and/or modify
#it under the terms of the GNU General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.
#
#Murasaki is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU General Public License for more details.
#
#You should have received a copy of the GNU General Public License
#along with Murasaki.  If not, see <http://www.gnu.org/licenses/>.

use strict;
use Getopt::Long;
use Pod::Usage;
use File::Basename;
use Data::Dump qw{dump};

BEGIN {
  unshift(@INC,(fileparse($0))[1].'perlmodules');
}

use Murasaki;

my ($verbose,$matrix_file);
my ($help,$man);
GetOptions('help|?' => \$help, man => \$man, 'verbose+'=>\$verbose, 'matrix=s'=>\$matrix_file);
pod2usage(1) if $help or scalar(@ARGV)<1;
pod2usage(-exitstatus => 0, -verbose => 4) if $man;

my %scoring=grabScoreMatrix($matrix_file);

print "Using matrix:\n";
print prettyMatrix(\%scoring)."\n";

my %stats=(minMatch=>minMatching(\%scoring),
	   maxMatch=>maxMatching(\%scoring),
	   minMiss=>minMiss(\%scoring),
	   maxMiss=>maxMiss(\%scoring),
	   minAll=>min(map {values %$_} values %scoring),
	   maxAll=>max(map {values %$_} values %scoring)
	  );
print "Matrix stats:\n".prettyHash(\%stats)."\n";

foreach my $inf (@ARGV) {
  my $pat;
  
  if($inf=~m/^\[\d+\D\d+\]/){
    $pat=`$root/randpat.pl -c $inf`;
    analyzePat($pat);
  }elsif($inf=~m/[01]+$/){
    $pat=$inf;
    analyzePat($pat);
  }else{
    open(my $fh,$inf) or die "Couldn't open file $inf";
    while(<$fh>){
      chomp;
      analyzePat($_) if m/^[01]+$/;
    }
  }
}
exit;

sub analyzePat {
  my ($pat)=@_;

  my ($zeros,$ones,$length)=(zeros($pat),ones($pat),length($pat));
  print "Pattern: $pat\n" if $verbose;
  print "Analyzing [$ones,$length] ($zeros zeros) pattern.\n";
  
  my %pstats=(minMatch=>$zeros*$stats{minAll}+$ones*$stats{minMatch},
	      maxMatch=>$zeros*$stats{maxAll}+$ones*$stats{maxMatch},
	      minMiss=>max(0,$ones+$zeros-1)*$stats{minAll}+$stats{minMiss},
	      maxMiss=>max(0,$ones+$zeros-1)*$stats{maxAll}+$stats{maxMiss}
	     );
  
  print "Pattern stats:\n".prettyHash(\%pstats)."\n";
}

exit;

sub prettyHash {
  my ($hr)=@_;
  my $r;
  my @k=sort {my ($sa,$sb)=map {my $t=$_;$t=~s/^(min|max)(.*)$/$2/;$t} ($a,$b);
	      return ($sb cmp $sa) if ($sb cmp $sa);
	      return $b cmp $a} keys %$hr;
  foreach my $k (@k){
    $r.="$k: $hr->{$k}\n";
  }
  return $r;
}

sub prettyMatrix {
  my ($mr)=@_;
  my $r;
  my @k=sort keys(%$mr);
  $r.=join("\t",undef,@k)."\n";
  foreach my $row (@k){
    $r.=join("\t",$row,map {$mr->{$row}->{$_}} @k)."\n";
  }
  return $r;
}

sub zeros {
  my ($pat)=@_;
  return scalar(grep(/^0$/,split(//,$pat)));
}

sub ones {
  my ($pat)=@_;
  return scalar(grep(/^1$/,split(//,$pat)));
}

sub weight {
  return ones @_;
}

sub minMatching {
  my ($mr)=@_;
  return min(map {$mr->{$_}->{$_}} (keys %$mr));
}

sub maxMatching {
  my ($mr)=@_;
  return max(map {$mr->{$_}->{$_}} (keys %$mr));
}

sub minMiss {
  my ($mr)=@_;
  my @k=keys %$mr;
  return min(map {my $row=$_;@{$mr->{$row}}{grep{$row ne $_} @k}} (@k));
}

sub maxMiss {
  my ($mr)=@_;
  my @k=keys %$mr;
  return max(map {my $row=$_;@{$mr->{$row}}{grep{$row ne $_} @k}} (@k));
}

sub grabScoreMatrix {
  my ($file)=@_;
  my $matrix_text;
  if($file){
    open(my $fh,$file) or die "Couldn't open $file";
    for(1..8){
      $matrix_text.=<fh>;
    }
  }else{
    $matrix_text=<<ENDTEXT;
#:lav
d {
  "blahblahblah
     A    C    G    T
    91 -114  -31 -123
  -114  100 -125  -31
   -31 -125  100 -114
  -123  -31 -114   91
ENDTEXT
  }
  my ($go,%matrix,@cols,$row);
  $row=0;
  foreach(split(/\n/,$matrix_text)){
    next unless $go or m/(\s+[ACGT]){4}/;
    unless($go){
      s/^\s+//;
      @cols=split(/\s+/);
      $go=1;
      next;
    }
    s/^\s+//;
    my @row=split(/\s+/);
    foreach my $i (0..$#row){
      $matrix{$cols[$row]}->{$cols[$i]}=$row[$i];
    }
    $row++;
  }
  die "Never found a matrix?" unless $go;
  return %matrix;
}


sub slurp {
  local $/;
  open(my $fh,"@_") or return;
  return <$fh>;
}

sub min {
  my $best=$_[0];
  foreach(@_){
    $best=$_ if $_<$best;
  }
  return $best;
}

sub max {
  my $best=$_[0];
  foreach(@_){
    $best=$_ if $_>$best;
  }
  return $best;
}

sub sum {
  my $sum=0;
  grep {$sum+=$_} @_;
  return $sum;
}

sub mean {
  my $total;
  foreach(@_){
    $total+=$_;
  }
  return $total/($#_+1);
}


__END__

=head1 NAME

analyze-pat.pl -- analyzes a pattern

=head1 SYNOPSIS

analyze-pat.pl <pattern | file-containing pattern>

=head1 OPTIONS

--verbose|-v =>
More verbose output to stdout. Can be applied multiple times.

--matrix =>
Where to find the scoring matrix.

=head1 DESCRIPTION



=cut
