#!/usr/bin/perl

#Copyright (C) 2006-2008 Keio University
#(Kris Popendorf) <comp@bio.keio.ac.jp> (2006)
#
#This file is part of Murasaki.
#
#Murasaki is free software: you can redistribute it and/or modify
#it under the terms of the GNU General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.
#
#Murasaki is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU General Public License for more details.
#
#You should have received a copy of the GNU General Public License
#along with Murasaki.  If not, see <http://www.gnu.org/licenses/>.

use Getopt::Std;
use File::Basename;

BEGIN {
  unshift(@INC,(fileparse($0))[1].'perlmodules');
}

use Murasaki;

$Getopt::Std::STANDARD_HELP_VERSION=true;
getopts('chRC');
if($opt_h){HELP_MESSAGE();exit(0);}

($filename,$outfile)=@ARGV;

if($filename and -e $filename){
  $inseq=`$root/geneparse.pl -c $filename`;
  my ($basename,$path,$suffix) = fileparse($filename);
  $name="$basename-revcomp";
} else {
  print STDERR "File $filename not found. Waiting for input from stdin.\n" unless !$filename or $filename eq "-";
  while($_=<STDIN>){
    chomp;
    $inseq.=$_;
  }
  $name="stdin";
}
$outfile="-" unless $outfile;
if($opt_c){
  open(OUTF,">$outfile");
}else{
  open(OUTF,"|$root/faformat.pl --name=\"$name\" - $outfile");
}

$inseq=reverse($inseq) unless $opt_C;
$inseq=~y/agtcAGTC/tcagTCAG/ unless $opt_R;
print OUTF $inseq;
close(OUTF);

sub main::HELP_MESSAGE(){
  print <<ENDTEXT
Usage: $0 [-c] [-R] [-C] [<in file> [<out file>] ]

If you don't specify an infile or outfile, stdin/stdout is used.
 -c specifies clean output (ie: just the sequence)
 -R specifies reverse ONLY
 -C specifies complement ONLY
Now if you're wondering what mixing -R and -C do... 
ENDTEXT
    ;
}

sub main::VERSION_MESSAGE(){
}
