#!/usr/bin/perl

#Copyright (C) 2006-2008 Keio University
#(Kris Popendorf) <comp@bio.keio.ac.jp> (2006)
#
#This file is part of Murasaki.
#
#Murasaki is free software: you can redistribute it and/or modify
#it under the terms of the GNU General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.
#
#Murasaki is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU General Public License for more details.
#
#You should have received a copy of the GNU General Public License
#along with Murasaki.  If not, see <http://www.gnu.org/licenses/>.

use Getopt::Long;
use Pod::Usage;
use File::Basename;
use IO::Handle;

use strict;

BEGIN {
  unshift(@INC,(fileparse($0))[1].'perlmodules');
}
use Murasaki;

our ($help,$man);
our ($kogfile,@kogmap);
GetOptions('help|?' => \$help, man => \$man);
pod2usage(1) if $help or $#ARGV<0;
pod2usage(-exitstatus => 0, -verbose => 2) if $man;

foreach my $inf (@ARGV){
  die "Input file not found: $inf" unless -f $inf;
  my ($prefix)=$inf=~m/^(.*)\.repeats/;
  die "Invalid file name for $inf" unless $prefix;
  my $seqs="$prefix.seqs";
  die "Seqs file ($seqs) not found" unless -f $seqs;

  my $outf="$inf.anchors";
  my $seqsDat=slurp($seqs);
  our $seqCount=scalar(split(/\n/,$seqsDat));
  blit($seqsDat,"$inf.seqs"); #mmm cp
  open(my $ofh,">$outf");

  open(my $fh,$inf);
  my ($length,@locs);
  while(<$fh>){
    chomp;
    unless($_){ 
      createAnchors($ofh,$length,@locs);
      #start fresh
      undef $length;
      @locs=();
      next;
    }
    my ($seq,$dat)=m/^([^:]+): (.*)$/ or die "Weird line: $_";
    if($seq eq 'R'){
      $length=length($dat);
    }else{
      $locs[$seq]=[split(/\s/,$dat)];
    }
  }
}

sub slurp {
  local $/;
  open(my $fh,"@_") or return;
  return <$fh>;
}

sub blit {
  my ($dat,$file)=@_;
  open(my $fh,">$file");
  print $fh $dat;
}

sub createAnchors {
  my ($ofh,$len,@locs)=@_;
  my @firsts=map {$locs[$_][0]} (0..$#locs);
  foreach my $focus (0..($#locs-1)){
    #create anchors across each of these combinations
    foreach my $x (@{$locs[$focus]}){
      foreach my $y (@{$locs[$focus+1]}){
	createAnchor($ofh,$len,@firsts[0..($focus-1)],$x,$y,@firsts[($focus+2)..$#locs]);
      }
    }
  }
}

sub createAnchor {
  my ($ofh,$len,@locs)=@_;
  my @printLocs=(map {[$_,$_+$len,($_ > 0 ? '+':'-')]} @locs);
  print $ofh join("\t",(map {join("\t",@$_)} @printLocs))."\n";
}

__END__

=head1 NAME

repeats2anchors.pl -- generates a phony alignment (suitable for graphing) out of a .repeats file

=head1 SYNOPSIS

repeats2anchors.pl <existing .repeats file>

=head1 OPTIONS
No options yet.

=cut
