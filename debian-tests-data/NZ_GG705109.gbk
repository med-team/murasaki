LOCUS       NZ_GG705109              790 bp    DNA     linear   CON 09-JUN-2010
DEFINITION  Acinetobacter lwoffii SH145 genomic scaffold supercont1.55, whole
            genome shotgun sequence.
ACCESSION   NZ_GG705109 NZ_ACPN01000000
VERSION     NZ_GG705109.1  GI:262378012
DBLINK      Project: 41587
            BioProject: PRJNA41587
KEYWORDS    WGS.
SOURCE      Acinetobacter lwoffii SH145
  ORGANISM  Acinetobacter lwoffii SH145
            Bacteria; Proteobacteria; Gammaproteobacteria; Pseudomonadales;
            Moraxellaceae; Acinetobacter.
REFERENCE   1  (bases 1 to 790)
  AUTHORS   Ward,D., Feldgarden,M., Earl,A., Young,S.K., Zeng,Q., Koehrsen,M.,
            Alvarado,L., Berlin,A., Bochicchio,J., Borenstein,D., Chapman,S.,
            Chen,Z., Engels,R., Freedman,E., Gellesch,M., Goldberg,J.,
            Griggs,A., Gujja,S., Heilman,E., Heiman,D., Hepburn,T., Howarth,C.,
            Jen,D., Larson,L., Lewis,B., Mehta,T., Park,D., Pearson,M.,
            Roberts,A., Saif,S., Shea,T., Shenoy,N., Sisk,P., Stolte,C.,
            Sykes,S., Thomson,T., Walk,T., White,J., Yandava,C., Seifert,H.,
            Haas,B., Nusbaum,C. and Birren,B.
  CONSRTM   The Broad Institute Genome Sequencing Platform
  TITLE     The Genome Sequence of Acinetobacter lwoffii strain SH145
  JOURNAL   Unpublished
REFERENCE   2  (bases 1 to 790)
  AUTHORS   Ward,D., Feldgarden,M., Earl,A., Young,S.K., Zeng,Q., Koehrsen,M.,
            Alvarado,L., Berlin,A., Bochicchio,J., Borenstein,D., Chapman,S.,
            Chen,Z., Engels,R., Freedman,E., Gellesch,M., Goldberg,J.,
            Griggs,A., Gujja,S., Heilman,E., Heiman,D., Hepburn,T., Howarth,C.,
            Jen,D., Larson,L., Lewis,B., Mehta,T., Park,D., Pearson,M.,
            Roberts,A., Saif,S., Shea,T., Shenoy,N., Sisk,P., Stolte,C.,
            Sykes,S., Thomson,T., Walk,T., White,J., Yandava,C., Seifert,H.,
            Haas,B., Nusbaum,C. and Birren,B.
  CONSRTM   The Broad Institute Genome Sequencing Platform
  TITLE     Direct Submission
  JOURNAL   Submitted (06-JUL-2009) Broad Institute of MIT and Harvard, 7
            Cambridge Center, Cambridge, MA 02142, USA
COMMENT     PROVISIONAL REFSEQ: This record has not yet been subject to final
            NCBI review. The reference sequence was derived from GG705109.
            This is a reference genome for the Human Microbiome Project. This
            project is co-owned with the Human Microbiome Project DACC.
            Source DNA provided by Harald Seifert (Professor of Medical
            Microbiology and Hygiene, Institute for Medical Microbiology,
            Immunology and Hygiene, University of Cologne, Goldenfelsstrasse
            19-21, 50935 Koln, Germany).
            Bacteria provided by Harald Seifert (Professor of Medical
            Microbiology and Hygiene, Institute for Medical Microbiology,
            Immunology and Hygiene, University of Cologne, Goldenfelsstrasse
            19-21, 50935 Koln, Germany).
            Genome Coverage: 22.06x
            Sequencing Technology: 454.
FEATURES             Location/Qualifiers
     source          1..790
                     /organism="Acinetobacter lwoffii SH145"
                     /mol_type="genomic DNA"
                     /strain="SH145"
                     /db_xref="taxon:575588"
     gene            83..511
                     /locus_tag="HMPREF0017_03114"
     CDS             83..511
                     /locus_tag="HMPREF0017_03114"
                     /codon_start=1
                     /transl_table=11
                     /product="predicted protein"
                     /protein_id="ZP_06071193.1"
                     /db_xref="GI:262378013"
                     /translation="MDLKTVIDSAPTLKKPRRTFTAEFKHQLIQQCQQPDTSVAKVAM
                     QHQINANLLHKWIRQSRSMPPALKTPSIPQTDFLPVILHPTPVKQEAPLPLVPEKKAV
                     AHIRIPLHEAPSSARDQMIEIDWPVESVTELLLLIQGLTQ"
     misc_feature    122..>268
                     /locus_tag="HMPREF0017_03114"
                     /note="Transposase; Region: HTH_Tnp_1; pfam01527"
                     /db_xref="CDD:201841"
     misc_feature    125..>265
                     /locus_tag="HMPREF0017_03114"
                     /note="Transposase and inactivated derivatives [DNA
                     replication, recombination, and repair]; Region: COG2963"
                     /db_xref="CDD:32783"
     gene            508..>790
                     /locus_tag="HMPREF0017_03115"
     CDS             508..>790
                     /locus_tag="HMPREF0017_03115"
                     /note="3' partial"
                     /codon_start=1
                     /transl_table=11
                     /product="transposase"
                     /protein_id="ZP_06071194.1"
                     /db_xref="GI:262378014"
                     /translation="MIRIDEIWLSTQPMDMRAGMDTAMAQVLRAFGYIKPHCAYLFCN
                     KRGHRMKVLVHDGLGIWLCARRLEQGKFHWAKVHQGESMAISPEQLQALI"
     misc_feature    514..789
                     /locus_tag="HMPREF0017_03115"
                     /note="IS66 Orf2 like protein; Region: TnpB_IS66;
                     pfam05717"
                     /db_xref="CDD:191353"
ORIGIN      
        1 tgtaagcgtc cgctgaaccc cactttttct aactcattaa taccgcgata gtgtccactt
       61 aaatttaagt ggacacctat ttatggattt aaagacagtt atagacagtg caccaacgct
      121 aaaaaagccc cgtcgaacct tcacagctga atttaaacat caacttattc agcaatgcca
      181 gcagccagat acatcagtgg ccaaggtagc aatgcaacat cagatcaatg ccaatctgct
      241 acataaatgg attcgtcagt ccagatcaat gccccctgca ttaaaaaccc catccattcc
      301 acagactgac tttcttccgg tcattcttca cccgactcca gtcaaacaag aagcacctct
      361 accacttgtg ccagagaaga aagctgtcgc tcatatcagg attccattac atgaggcacc
      421 aagttccgca agggatcaga tgatcgagat cgactggccg gtggagtcag taactgaatt
      481 actgttgctg atccagggtt tgactcaatg attcgtattg atgagatctg gttgtctacc
      541 cagccgatgg acatgcgtgc agggatggat acagccatgg ctcaggtgct gagagccttt
      601 ggctacatca aaccgcattg cgcttacctg ttctgtaata agcgtggcca tcgtatgaaa
      661 gtgctggtgc atgatggact gggtatctgg ctgtgtgccc ggcggctgga acaggggaaa
      721 ttccactggg ctaaagttca ccagggtgaa agtatggcga tcagcccgga gcagttacag
      781 gcactgatcc
//
