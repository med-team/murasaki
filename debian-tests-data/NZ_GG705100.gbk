LOCUS       NZ_GG705100              885 bp    DNA     linear   CON 09-JUN-2010
DEFINITION  Acinetobacter lwoffii SH145 genomic scaffold supercont1.46, whole
            genome shotgun sequence.
ACCESSION   NZ_GG705100 NZ_ACPN01000000
VERSION     NZ_GG705100.1  GI:262377994
DBLINK      Project: 41587
            BioProject: PRJNA41587
KEYWORDS    WGS.
SOURCE      Acinetobacter lwoffii SH145
  ORGANISM  Acinetobacter lwoffii SH145
            Bacteria; Proteobacteria; Gammaproteobacteria; Pseudomonadales;
            Moraxellaceae; Acinetobacter.
REFERENCE   1  (bases 1 to 885)
  AUTHORS   Ward,D., Feldgarden,M., Earl,A., Young,S.K., Zeng,Q., Koehrsen,M.,
            Alvarado,L., Berlin,A., Bochicchio,J., Borenstein,D., Chapman,S.,
            Chen,Z., Engels,R., Freedman,E., Gellesch,M., Goldberg,J.,
            Griggs,A., Gujja,S., Heilman,E., Heiman,D., Hepburn,T., Howarth,C.,
            Jen,D., Larson,L., Lewis,B., Mehta,T., Park,D., Pearson,M.,
            Roberts,A., Saif,S., Shea,T., Shenoy,N., Sisk,P., Stolte,C.,
            Sykes,S., Thomson,T., Walk,T., White,J., Yandava,C., Seifert,H.,
            Haas,B., Nusbaum,C. and Birren,B.
  CONSRTM   The Broad Institute Genome Sequencing Platform
  TITLE     The Genome Sequence of Acinetobacter lwoffii strain SH145
  JOURNAL   Unpublished
REFERENCE   2  (bases 1 to 885)
  AUTHORS   Ward,D., Feldgarden,M., Earl,A., Young,S.K., Zeng,Q., Koehrsen,M.,
            Alvarado,L., Berlin,A., Bochicchio,J., Borenstein,D., Chapman,S.,
            Chen,Z., Engels,R., Freedman,E., Gellesch,M., Goldberg,J.,
            Griggs,A., Gujja,S., Heilman,E., Heiman,D., Hepburn,T., Howarth,C.,
            Jen,D., Larson,L., Lewis,B., Mehta,T., Park,D., Pearson,M.,
            Roberts,A., Saif,S., Shea,T., Shenoy,N., Sisk,P., Stolte,C.,
            Sykes,S., Thomson,T., Walk,T., White,J., Yandava,C., Seifert,H.,
            Haas,B., Nusbaum,C. and Birren,B.
  CONSRTM   The Broad Institute Genome Sequencing Platform
  TITLE     Direct Submission
  JOURNAL   Submitted (06-JUL-2009) Broad Institute of MIT and Harvard, 7
            Cambridge Center, Cambridge, MA 02142, USA
COMMENT     PROVISIONAL REFSEQ: This record has not yet been subject to final
            NCBI review. The reference sequence was derived from GG705100.
            This is a reference genome for the Human Microbiome Project. This
            project is co-owned with the Human Microbiome Project DACC.
            Source DNA provided by Harald Seifert (Professor of Medical
            Microbiology and Hygiene, Institute for Medical Microbiology,
            Immunology and Hygiene, University of Cologne, Goldenfelsstrasse
            19-21, 50935 Koln, Germany).
            Bacteria provided by Harald Seifert (Professor of Medical
            Microbiology and Hygiene, Institute for Medical Microbiology,
            Immunology and Hygiene, University of Cologne, Goldenfelsstrasse
            19-21, 50935 Koln, Germany).
            Genome Coverage: 22.06x
            Sequencing Technology: 454.
FEATURES             Location/Qualifiers
     source          1..885
                     /organism="Acinetobacter lwoffii SH145"
                     /mol_type="genomic DNA"
                     /strain="SH145"
                     /db_xref="taxon:575588"
     gene            complement(21..>762)
                     /locus_tag="HMPREF0017_03105"
     CDS             complement(21..>470)
                     /locus_tag="HMPREF0017_03105"
                     /exception="unextendable partial coding region"
                     /note="5' partial; truncated CDS"
                     /codon_start=1
                     /transl_table=11
                     /product="transposase (IS4 family)"
                     /protein_id="ZP_06071184.1"
                     /db_xref="GI:262377995"
                     /translation="NARKKNQSKFYSGKKKKHTLKAQVIYHPKSKQIIGVDISSGSQH
                     DIKLARKTVKKFKHCDYVMTDLGYYGLEQDGFKLLMPIKKKKNLPLFDAEKNYNKMIG
                     KIRVVIEHINSQLKRFRILSERYRNRRKRFGLRINLIAALVNRMNLQ"
     misc_feature    complement(39..464)
                     /locus_tag="HMPREF0017_03105"
                     /note="Transposase DDE domain; Region: DDE_Tnp_1;
                     pfam01609"
                     /db_xref="CDD:201886"
     misc_feature    complement(39..452)
                     /locus_tag="HMPREF0017_03105"
                     /note="DDE superfamily endonuclease; Region: DDE_4;
                     pfam13359"
                     /db_xref="CDD:205538"
ORIGIN      
        1 tagatctctt gcgaaagtcg ttattgcaag ttcatccggt ttaccagtgc agcgattaag
       61 ttaatgcgta aaccgaatct tttccgtcta tttcgatagc gttcacttag tattctaaat
      121 cttttcaatt gactgttaat atgttcgatt acaactcgta tttttccaat cattttattg
      181 taatttttct cagcatcaaa taagggtaaa ttcttctttt tctttattgg catcaataat
      241 ttaaagccat cttgctctaa cccatagtac cctaaatcgg tcataacata gtcacaatgt
      301 ttgaatttct taactgtttt tcttgccaat ttaatatcat gctgactgcc agacgatata
      361 tctactccta tgatttgttt gctcttcgga tgatagatca cttgggcttt taacgtatgt
      421 ttcttctttt taccactgta gaacttactc tgattttttt ttcgggcgtt ctattaaaca
      481 ttctgtcgca tcaataatta cccagttaaa ttgttcgtct gctgtggtaa tgcttcgttt
      541 tggcagggta aaacgtcttg atttcattaa tgcatcttca acctttttaa tagttcgatt
      601 cacattactt tcagcgatat ggtaatttgc cgccaattcc aattgggtgt tgtagctccg
      661 taagtaattg agtgttaata ataattgatc ctctatagct aaagtatgag gacgccctaa
      721 tttcttttta agtgattctg cttcttttaa aacttcgacc atttcactaa aaactgctcg
      781 aggtacacca accaagcgct ggaattcaga atctgaaaaa cgatttaatt tctgatattt
      841 catgaaatct atattgaggc agtttttact ttcgcaagag gtcta
//
