LOCUS       NZ_GG705122              603 bp    DNA     linear   CON 09-JUN-2010
DEFINITION  Acinetobacter lwoffii SH145 genomic scaffold supercont1.68, whole
            genome shotgun sequence.
ACCESSION   NZ_GG705122 NZ_ACPN01000000
VERSION     NZ_GG705122.1  GI:262378038
DBLINK      Project: 41587
            BioProject: PRJNA41587
KEYWORDS    WGS.
SOURCE      Acinetobacter lwoffii SH145
  ORGANISM  Acinetobacter lwoffii SH145
            Bacteria; Proteobacteria; Gammaproteobacteria; Pseudomonadales;
            Moraxellaceae; Acinetobacter.
REFERENCE   1  (bases 1 to 603)
  AUTHORS   Ward,D., Feldgarden,M., Earl,A., Young,S.K., Zeng,Q., Koehrsen,M.,
            Alvarado,L., Berlin,A., Bochicchio,J., Borenstein,D., Chapman,S.,
            Chen,Z., Engels,R., Freedman,E., Gellesch,M., Goldberg,J.,
            Griggs,A., Gujja,S., Heilman,E., Heiman,D., Hepburn,T., Howarth,C.,
            Jen,D., Larson,L., Lewis,B., Mehta,T., Park,D., Pearson,M.,
            Roberts,A., Saif,S., Shea,T., Shenoy,N., Sisk,P., Stolte,C.,
            Sykes,S., Thomson,T., Walk,T., White,J., Yandava,C., Seifert,H.,
            Haas,B., Nusbaum,C. and Birren,B.
  CONSRTM   The Broad Institute Genome Sequencing Platform
  TITLE     The Genome Sequence of Acinetobacter lwoffii strain SH145
  JOURNAL   Unpublished
REFERENCE   2  (bases 1 to 603)
  AUTHORS   Ward,D., Feldgarden,M., Earl,A., Young,S.K., Zeng,Q., Koehrsen,M.,
            Alvarado,L., Berlin,A., Bochicchio,J., Borenstein,D., Chapman,S.,
            Chen,Z., Engels,R., Freedman,E., Gellesch,M., Goldberg,J.,
            Griggs,A., Gujja,S., Heilman,E., Heiman,D., Hepburn,T., Howarth,C.,
            Jen,D., Larson,L., Lewis,B., Mehta,T., Park,D., Pearson,M.,
            Roberts,A., Saif,S., Shea,T., Shenoy,N., Sisk,P., Stolte,C.,
            Sykes,S., Thomson,T., Walk,T., White,J., Yandava,C., Seifert,H.,
            Haas,B., Nusbaum,C. and Birren,B.
  CONSRTM   The Broad Institute Genome Sequencing Platform
  TITLE     Direct Submission
  JOURNAL   Submitted (06-JUL-2009) Broad Institute of MIT and Harvard, 7
            Cambridge Center, Cambridge, MA 02142, USA
COMMENT     PROVISIONAL REFSEQ: This record has not yet been subject to final
            NCBI review. The reference sequence was derived from GG705122.
            This is a reference genome for the Human Microbiome Project. This
            project is co-owned with the Human Microbiome Project DACC.
            Source DNA provided by Harald Seifert (Professor of Medical
            Microbiology and Hygiene, Institute for Medical Microbiology,
            Immunology and Hygiene, University of Cologne, Goldenfelsstrasse
            19-21, 50935 Koln, Germany).
            Bacteria provided by Harald Seifert (Professor of Medical
            Microbiology and Hygiene, Institute for Medical Microbiology,
            Immunology and Hygiene, University of Cologne, Goldenfelsstrasse
            19-21, 50935 Koln, Germany).
            Genome Coverage: 22.06x
            Sequencing Technology: 454.
FEATURES             Location/Qualifiers
     source          1..603
                     /organism="Acinetobacter lwoffii SH145"
                     /mol_type="genomic DNA"
                     /strain="SH145"
                     /db_xref="taxon:575588"
     gene            <1..>603
                     /locus_tag="HMPREF0017_03128"
     CDS             <1..>603
                     /locus_tag="HMPREF0017_03128"
                     /note="3' partial; 5' partial"
                     /codon_start=3
                     /transl_table=11
                     /product="Co/Zn/Cd efflux system component"
                     /protein_id="ZP_06071206.1"
                     /db_xref="GI:262378039"
                     /translation="SLKVLHNQKDENITTKLEALGFGAKLVKTETYISNQQAKQTSTY
                     TIPKMDCSAEEQMVRMALADIEAVKGLTFDLPNRKLKVFHNEGSQQITAKLEGLGFGA
                     KLDETQLSSGDIPNEPDPVRQAKVLKLLLGINALLFFIEFISGIIAASTGLIADSLDM
                     FADAAVYGIALYAVGKAAKYQVKAAHFAGWIQLLLAVIVI"
     misc_feature    129..305
                     /locus_tag="HMPREF0017_03128"
                     /note="Heavy-metal-associated domain; Region: HMA;
                     pfam00403"
                     /db_xref="CDD:201210"
     misc_feature    345..>590
                     /locus_tag="HMPREF0017_03128"
                     /note="Co/Zn/Cd efflux system component [Inorganic ion
                     transport and metabolism]; Region: CzcD; COG1230"
                     /db_xref="CDD:31423"
     misc_feature    372..>599
                     /locus_tag="HMPREF0017_03128"
                     /note="Cation efflux family; Region: Cation_efflux;
                     cl00316"
                     /db_xref="CDD:213092"
ORIGIN      
        1 ggtctttaaa ggttctacat aatcaaaaag atgaaaatat aactaccaaa cttgaagctt
       61 tgggttttgg tgcaaagctt gtgaagactg aaacttatat aagcaatcaa caagctaagc
      121 aaacaagtac ctataccatt cctaaaatgg attgctctgc tgaagagcaa atggtccgta
      181 tggctcttgc agatattgaa gcagttaaag gtcttacttt tgatcttccc aatcgaaaac
      241 tgaaagtctt ccataatgaa gggagtcagc aaattacggc caaacttgaa ggactgggtt
      301 ttggggcgaa acttgatgaa acacagcttt cttcaggcga tatacctaat gaacctgatc
      361 ctgtgagaca agctaaagta cttaaactgt tattaggcat taatgctcta cttttcttta
      421 tagaattcat cagcggaatt attgctgcgt ctacaggatt gattgctgat tctctagata
      481 tgtttgccga tgcagccgtt tatggtattg cgctatatgc cgtcggaaaa gctgcgaaat
      541 atcaagtgaa agcagcccat ttcgcaggtt ggattcagtt attacttgct gtcatcgtga
      601 tta
//
