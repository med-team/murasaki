LOCUS       NZ_GG705110              789 bp    DNA     linear   CON 09-JUN-2010
DEFINITION  Acinetobacter lwoffii SH145 genomic scaffold supercont1.56, whole
            genome shotgun sequence.
ACCESSION   NZ_GG705110 NZ_ACPN01000000
VERSION     NZ_GG705110.1  GI:262378015
DBLINK      Project: 41587
            BioProject: PRJNA41587
KEYWORDS    WGS.
SOURCE      Acinetobacter lwoffii SH145
  ORGANISM  Acinetobacter lwoffii SH145
            Bacteria; Proteobacteria; Gammaproteobacteria; Pseudomonadales;
            Moraxellaceae; Acinetobacter.
REFERENCE   1  (bases 1 to 789)
  AUTHORS   Ward,D., Feldgarden,M., Earl,A., Young,S.K., Zeng,Q., Koehrsen,M.,
            Alvarado,L., Berlin,A., Bochicchio,J., Borenstein,D., Chapman,S.,
            Chen,Z., Engels,R., Freedman,E., Gellesch,M., Goldberg,J.,
            Griggs,A., Gujja,S., Heilman,E., Heiman,D., Hepburn,T., Howarth,C.,
            Jen,D., Larson,L., Lewis,B., Mehta,T., Park,D., Pearson,M.,
            Roberts,A., Saif,S., Shea,T., Shenoy,N., Sisk,P., Stolte,C.,
            Sykes,S., Thomson,T., Walk,T., White,J., Yandava,C., Seifert,H.,
            Haas,B., Nusbaum,C. and Birren,B.
  CONSRTM   The Broad Institute Genome Sequencing Platform
  TITLE     The Genome Sequence of Acinetobacter lwoffii strain SH145
  JOURNAL   Unpublished
REFERENCE   2  (bases 1 to 789)
  AUTHORS   Ward,D., Feldgarden,M., Earl,A., Young,S.K., Zeng,Q., Koehrsen,M.,
            Alvarado,L., Berlin,A., Bochicchio,J., Borenstein,D., Chapman,S.,
            Chen,Z., Engels,R., Freedman,E., Gellesch,M., Goldberg,J.,
            Griggs,A., Gujja,S., Heilman,E., Heiman,D., Hepburn,T., Howarth,C.,
            Jen,D., Larson,L., Lewis,B., Mehta,T., Park,D., Pearson,M.,
            Roberts,A., Saif,S., Shea,T., Shenoy,N., Sisk,P., Stolte,C.,
            Sykes,S., Thomson,T., Walk,T., White,J., Yandava,C., Seifert,H.,
            Haas,B., Nusbaum,C. and Birren,B.
  CONSRTM   The Broad Institute Genome Sequencing Platform
  TITLE     Direct Submission
  JOURNAL   Submitted (06-JUL-2009) Broad Institute of MIT and Harvard, 7
            Cambridge Center, Cambridge, MA 02142, USA
COMMENT     PROVISIONAL REFSEQ: This record has not yet been subject to final
            NCBI review. The reference sequence was derived from GG705110.
            This is a reference genome for the Human Microbiome Project. This
            project is co-owned with the Human Microbiome Project DACC.
            Source DNA provided by Harald Seifert (Professor of Medical
            Microbiology and Hygiene, Institute for Medical Microbiology,
            Immunology and Hygiene, University of Cologne, Goldenfelsstrasse
            19-21, 50935 Koln, Germany).
            Bacteria provided by Harald Seifert (Professor of Medical
            Microbiology and Hygiene, Institute for Medical Microbiology,
            Immunology and Hygiene, University of Cologne, Goldenfelsstrasse
            19-21, 50935 Koln, Germany).
            Genome Coverage: 22.06x
            Sequencing Technology: 454.
FEATURES             Location/Qualifiers
     source          1..789
                     /organism="Acinetobacter lwoffii SH145"
                     /mol_type="genomic DNA"
                     /strain="SH145"
                     /db_xref="taxon:575588"
     gene            complement(28..729)
                     /locus_tag="HMPREF0017_03116"
     CDS             complement(28..729)
                     /locus_tag="HMPREF0017_03116"
                     /codon_start=1
                     /transl_table=11
                     /product="IS1 transposase"
                     /protein_id="ZP_06071195.1"
                     /db_xref="GI:262378016"
                     /translation="MQITLEIKCPTCLSDSIKKNGIKVDGKQNYQCKDCKRQFIGDHA
                     LSYLGCNSGITRKILQLMVRGSGIRDIAEVERISIGKVLRTLTESAYQIQPKQSHYES
                     LEVDEFWTFVGNKNNKQWLIYAYHRETGEIVAYVWGKRDLATVQRLKTKLKQLGIHYT
                     RIASDHWDSFITAFKNCKQSIGKFFTVGIEGNNCKIRHRIRRGFRRSCNFSKKLENHF
                     KAFDLTFFYINNGFI"
     misc_feature    complement(463..>723)
                     /locus_tag="HMPREF0017_03116"
                     /note="Transposase and inactivated derivatives [DNA
                     replication, recombination, and repair]; Region: COG3677"
                     /db_xref="CDD:33474"
     misc_feature    complement(622..720)
                     /locus_tag="HMPREF0017_03116"
                     /note="InsA N-terminal domain; Region: Zn_Tnp_IS1;
                     pfam03811"
                     /db_xref="CDD:190760"
     misc_feature    complement(463..594)
                     /locus_tag="HMPREF0017_03116"
                     /note="InsA C-terminal domain; Region: HTH_Tnp_IS1;
                     pfam12759"
                     /db_xref="CDD:193235"
     misc_feature    complement(61..423)
                     /locus_tag="HMPREF0017_03116"
                     /note="Transposase and inactivated derivatives, IS1 family
                     [DNA replication, recombination, and repair]; Region:
                     InsB; COG1662"
                     /db_xref="CDD:31848"
ORIGIN      
        1 ggtggtgttt taaaaagtat gctgacatta aatgaagcca ttattgatgt aaaagaaggt
       61 taagtcgaag gctttaaaat ggttttcaag ctttttggag aaattacaac ttcttctaaa
      121 accacgcctt attcgatgcc ttattttgca attattacct tcaattccta cagtaaaaaa
      181 cttaccaata ctttgcttgc agtttttaaa agcagtgatg aaactatccc aatgatcact
      241 tgcaattcgg gtgtagtgaa tacctaattg tttaagcttt gtcttcaacc tttggactgt
      301 agctaaatct ctcttacccc aaacataagc aacaatctca cctgtttctc gatggtaggc
      361 gtaaataagc cattgtttat tatttttatt tccaacaaaa gtccagaatt catctacttc
      421 gagagattca taatgacttt gtttaggctg aatttgatag gccgattcag ttaaagtccg
      481 taagacttta ccgatactaa tgcgctcaac ttcagcgata tctcgtatac cgctgcctct
      541 gaccattaac tgtaatattt tacgagtaat gccagaatta catcctagat agctcagagc
      601 atggtcacca ataaactgac gtttgcagtc tttgcattgg tagttttgtt tcccatctac
      661 tttgatgcca tttttcttta tactgtcact gaggcaggtt ggacatttga tttctagagt
      721 tatttgcatt tctctatttt atcaaaattc aacctgcttt tttttagcat actttttgat
      781 acaccaccg
//
