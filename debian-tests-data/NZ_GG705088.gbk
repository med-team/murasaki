LOCUS       NZ_GG705088             2146 bp    DNA     linear   CON 09-JUN-2010
DEFINITION  Acinetobacter lwoffii SH145 genomic scaffold supercont1.34, whole
            genome shotgun sequence.
ACCESSION   NZ_GG705088 NZ_ACPN01000000
VERSION     NZ_GG705088.1  GI:262377963
DBLINK      Project: 41587
            BioProject: PRJNA41587
KEYWORDS    WGS.
SOURCE      Acinetobacter lwoffii SH145
  ORGANISM  Acinetobacter lwoffii SH145
            Bacteria; Proteobacteria; Gammaproteobacteria; Pseudomonadales;
            Moraxellaceae; Acinetobacter.
REFERENCE   1  (bases 1 to 2146)
  AUTHORS   Ward,D., Feldgarden,M., Earl,A., Young,S.K., Zeng,Q., Koehrsen,M.,
            Alvarado,L., Berlin,A., Bochicchio,J., Borenstein,D., Chapman,S.,
            Chen,Z., Engels,R., Freedman,E., Gellesch,M., Goldberg,J.,
            Griggs,A., Gujja,S., Heilman,E., Heiman,D., Hepburn,T., Howarth,C.,
            Jen,D., Larson,L., Lewis,B., Mehta,T., Park,D., Pearson,M.,
            Roberts,A., Saif,S., Shea,T., Shenoy,N., Sisk,P., Stolte,C.,
            Sykes,S., Thomson,T., Walk,T., White,J., Yandava,C., Seifert,H.,
            Haas,B., Nusbaum,C. and Birren,B.
  CONSRTM   The Broad Institute Genome Sequencing Platform
  TITLE     The Genome Sequence of Acinetobacter lwoffii strain SH145
  JOURNAL   Unpublished
REFERENCE   2  (bases 1 to 2146)
  AUTHORS   Ward,D., Feldgarden,M., Earl,A., Young,S.K., Zeng,Q., Koehrsen,M.,
            Alvarado,L., Berlin,A., Bochicchio,J., Borenstein,D., Chapman,S.,
            Chen,Z., Engels,R., Freedman,E., Gellesch,M., Goldberg,J.,
            Griggs,A., Gujja,S., Heilman,E., Heiman,D., Hepburn,T., Howarth,C.,
            Jen,D., Larson,L., Lewis,B., Mehta,T., Park,D., Pearson,M.,
            Roberts,A., Saif,S., Shea,T., Shenoy,N., Sisk,P., Stolte,C.,
            Sykes,S., Thomson,T., Walk,T., White,J., Yandava,C., Seifert,H.,
            Haas,B., Nusbaum,C. and Birren,B.
  CONSRTM   The Broad Institute Genome Sequencing Platform
  TITLE     Direct Submission
  JOURNAL   Submitted (06-JUL-2009) Broad Institute of MIT and Harvard, 7
            Cambridge Center, Cambridge, MA 02142, USA
COMMENT     PROVISIONAL REFSEQ: This record has not yet been subject to final
            NCBI review. The reference sequence was derived from GG705088.
            This is a reference genome for the Human Microbiome Project. This
            project is co-owned with the Human Microbiome Project DACC.
            Source DNA provided by Harald Seifert (Professor of Medical
            Microbiology and Hygiene, Institute for Medical Microbiology,
            Immunology and Hygiene, University of Cologne, Goldenfelsstrasse
            19-21, 50935 Koln, Germany).
            Bacteria provided by Harald Seifert (Professor of Medical
            Microbiology and Hygiene, Institute for Medical Microbiology,
            Immunology and Hygiene, University of Cologne, Goldenfelsstrasse
            19-21, 50935 Koln, Germany).
            Genome Coverage: 22.06x
            Sequencing Technology: 454.
FEATURES             Location/Qualifiers
     source          1..2146
                     /organism="Acinetobacter lwoffii SH145"
                     /mol_type="genomic DNA"
                     /strain="SH145"
                     /db_xref="taxon:575588"
     gene            169..378
                     /locus_tag="HMPREF0017_03086"
     CDS             169..378
                     /locus_tag="HMPREF0017_03086"
                     /codon_start=1
                     /transl_table=11
                     /product="predicted protein"
                     /protein_id="ZP_06071165.1"
                     /db_xref="GI:262377964"
                     /translation="MFNFKIFNKVSTEVLTIKNDLQLNAELQLINKYKTAISEDYKQA
                     IVLIFKERGYTRLEIGQLLGELKAS"
     gene            628..2082
                     /locus_tag="HMPREF0017_03087"
     CDS             628..2082
                     /locus_tag="HMPREF0017_03087"
                     /codon_start=1
                     /transl_table=11
                     /product="sulfate permease"
                     /protein_id="ZP_06071166.1"
                     /db_xref="GI:262377965"
                     /translation="MLSNVREQWFSNIRGDVLAGLVVGLALIPEAIAFSIIAGVDPKV
                     GLYASFCIAVVISFVGGRPAMISAATGAMALLMVTLVKEHGLEYLLAATILTGVIQIL
                     AGYLKLAKLMRFVSKSVVIGFVNALAILIFMAQLPELINVTWHVYALVALGLAIIYLF
                     PLVPKLGKLLPSPLVCIIAITLLAIALGIDVRTVGDMGSLPDTLPMFLIPNIPLNIET
                     LMIILPYSVALAAVGLLESMMTATIVDEMTDTPSDKYQECKGQGIANIASGFMGGMAG
                     CAMIGQSMINVKSGGLTRLSTFSAGIFLLILVVFISDWLKVIPMAALVAVMIMVSIST
                     FEWSSVTQLKDNPKSSNIVMIATVIVVVATHNLALGVLTGVLLSALFLANKLENDIQV
                     TASFEGTARLYDLRGQIFFSSSEKFMQGFDFKEDVKDVIIDLTHSHIWDVTSVAMLDS
                     VVNKFQKNGVNVTVRGLNEASSIMIDKYGTHARI"
     misc_feature    643..2079
                     /locus_tag="HMPREF0017_03087"
                     /note="Sulfate permease and related transporters (MFS
                     superfamily) [Inorganic ion transport and metabolism];
                     Region: SUL1; COG0659"
                     /db_xref="CDD:31004"
     misc_feature    655..870
                     /locus_tag="HMPREF0017_03087"
                     /note="Sulfate transporter N-terminal domain with GLY
                     motif; Region: Sulfate_tra_GLY; pfam13792"
                     /db_xref="CDD:205965"
     misc_feature    934..1674
                     /locus_tag="HMPREF0017_03087"
                     /note="Sulfate transporter family; Region: Sulfate_transp;
                     pfam00916"
                     /db_xref="CDD:144493"
     misc_feature    1828..2079
                     /locus_tag="HMPREF0017_03087"
                     /note="Sulphate Transporter and Anti-Sigma factor
                     antagonist domain of SulP-like sulfate transporters, plays
                     a role in the function and regulation of the transport
                     activity, proposed general NTP binding function; Region:
                     STAS_SulP_like_sulfate_transporter; cd07042"
                     /db_xref="CDD:132913"
ORIGIN      
        1 ccattatgtt aaatggatat aaatttagtg gattattcat gcttcccctt gagaaaaact
       61 tgcagatccc tttttaattt gcaattcttg ctactaaggg tgtctagcta agctaaggca
      121 ggtgttagtt gtagtcgggc cgctacaata ttgatgaaga gcacctgtat gtttaatttc
      181 aagattttta ataaagtctc tacggaagta ttgacgatca aaaatgacct tcagcttaat
      241 gctgagctgc aattaattaa taaatataaa acggctattt ctgaagatta taaacaggct
      301 attgttttga tctttaaaga acgtggctac acacgtttgg aaataggtca attacttggg
      361 gagctaaaag cttcataaaa taccgattaa acaaagcttg gatataacat ccaggcttta
      421 taaaaaagta cctagctgtt acccttgtcc tgattttttt actctaaaaa gttccttcat
      481 cttatgacac ctacgccacc ttaagtttat ttaccctctt aaatctgtaa ataactcatc
      541 actgtctagt gatgtggtgt agtcgcttgt gtgttgtata tatcacatca ctagcctatc
      601 tgaaaaatta aaaaacaggc tatgtttatg ttatctaatg tccgagaaca gtggttctct
      661 aacatccgtg gggatgtact tgccggatta gtcgtcggtc ttgcacttat tcctgaagcc
      721 atcgcatttt caattattgc aggtgtagat cctaaagtcg gtctatacgc atccttctgt
      781 attgccgtcg tgatttcttt cgtaggtggt cgtcccgcaa tgatttcagc tgccacaggg
      841 gctatggcac tgcttatggt cacattggtc aaagagcatg ggcttgaata tctgctggca
      901 gcaacaattc tgacaggcgt gattcagata ttggcaggct atttaaagct cgccaagtta
      961 atgcggttcg tatctaaatc tgtagttatt ggattcgtaa atgcgttagc gatcttaatt
     1021 ttcatggctc agttgcctga actgattaat gtgacctggc atgtatatgc tctggttgcc
     1081 ttaggtctgg cgattattta tctctttcct ctggttccca aattaggaaa attactccct
     1141 tctccacttg tctgcatcat tgcaatcaca ttactggcaa ttgctctagg aattgatgta
     1201 cgaactgtgg gtgatatggg atcgttacct gacacacttc caatgttctt gattccaaat
     1261 attcctttga atattgaaac attgatgatt attttgcctt attcggtagc actggcggct
     1321 gtaggtttac ttgaatcgat gatgacagca accatcgtgg atgaaatgac agatacgcca
     1381 agtgataaat atcaggaatg taagggacaa gggattgcga atattgcttc tggctttatg
     1441 ggtggcatgg ccggttgtgc catgattggc caatctatga ttaacgtaaa atcaggcggc
     1501 cttacacgct tatcaacatt ctctgccggt atttttcttc ttattctggt tgtatttatt
     1561 agcgactggc tcaaagtgat tccaatggct gcattggtag cggtaatgat catggtctcg
     1621 attagtactt ttgaatggag ttctgtcact cagttaaaag ataatcctaa gagtagtaat
     1681 atcgtcatga ttgcgacagt gattgtagtt gttgctacgc ataacttggc tctaggtgta
     1741 ttaacgggcg tattgctttc agccttattc ttggcgaata aacttgaaaa tgatatccag
     1801 gtgacagctt cttttgaagg aacagcccgt ttatatgatt tgagaggtca gatcttcttt
     1861 agttcttctg aaaagttcat gcaaggtttt gactttaaag aggatgtaaa ggatgtgatt
     1921 attgacttga cccattctca catttgggat gtaacatcag tcgccatgct tgattctgtt
     1981 gtaaataaat ttcaaaagaa tggtgtgaat gtcactgtgc gtggcttaaa tgaagccagc
     2041 tctattatga ttgataagta tggaacacat gccagaatct aagcgcagga gattttacgt
     2101 ttatgtagac tctctaaaat taacataata caccttatac gaaatg
//
