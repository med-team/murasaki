LOCUS       NZ_GG705099              927 bp    DNA     linear   CON 09-JUN-2010
DEFINITION  Acinetobacter lwoffii SH145 genomic scaffold supercont1.45, whole
            genome shotgun sequence.
ACCESSION   NZ_GG705099 NZ_ACPN01000000
VERSION     NZ_GG705099.1  GI:262377991
DBLINK      Project: 41587
            BioProject: PRJNA41587
KEYWORDS    WGS.
SOURCE      Acinetobacter lwoffii SH145
  ORGANISM  Acinetobacter lwoffii SH145
            Bacteria; Proteobacteria; Gammaproteobacteria; Pseudomonadales;
            Moraxellaceae; Acinetobacter.
REFERENCE   1  (bases 1 to 927)
  AUTHORS   Ward,D., Feldgarden,M., Earl,A., Young,S.K., Zeng,Q., Koehrsen,M.,
            Alvarado,L., Berlin,A., Bochicchio,J., Borenstein,D., Chapman,S.,
            Chen,Z., Engels,R., Freedman,E., Gellesch,M., Goldberg,J.,
            Griggs,A., Gujja,S., Heilman,E., Heiman,D., Hepburn,T., Howarth,C.,
            Jen,D., Larson,L., Lewis,B., Mehta,T., Park,D., Pearson,M.,
            Roberts,A., Saif,S., Shea,T., Shenoy,N., Sisk,P., Stolte,C.,
            Sykes,S., Thomson,T., Walk,T., White,J., Yandava,C., Seifert,H.,
            Haas,B., Nusbaum,C. and Birren,B.
  CONSRTM   The Broad Institute Genome Sequencing Platform
  TITLE     The Genome Sequence of Acinetobacter lwoffii strain SH145
  JOURNAL   Unpublished
REFERENCE   2  (bases 1 to 927)
  AUTHORS   Ward,D., Feldgarden,M., Earl,A., Young,S.K., Zeng,Q., Koehrsen,M.,
            Alvarado,L., Berlin,A., Bochicchio,J., Borenstein,D., Chapman,S.,
            Chen,Z., Engels,R., Freedman,E., Gellesch,M., Goldberg,J.,
            Griggs,A., Gujja,S., Heilman,E., Heiman,D., Hepburn,T., Howarth,C.,
            Jen,D., Larson,L., Lewis,B., Mehta,T., Park,D., Pearson,M.,
            Roberts,A., Saif,S., Shea,T., Shenoy,N., Sisk,P., Stolte,C.,
            Sykes,S., Thomson,T., Walk,T., White,J., Yandava,C., Seifert,H.,
            Haas,B., Nusbaum,C. and Birren,B.
  CONSRTM   The Broad Institute Genome Sequencing Platform
  TITLE     Direct Submission
  JOURNAL   Submitted (06-JUL-2009) Broad Institute of MIT and Harvard, 7
            Cambridge Center, Cambridge, MA 02142, USA
COMMENT     PROVISIONAL REFSEQ: This record has not yet been subject to final
            NCBI review. The reference sequence was derived from GG705099.
            This is a reference genome for the Human Microbiome Project. This
            project is co-owned with the Human Microbiome Project DACC.
            Source DNA provided by Harald Seifert (Professor of Medical
            Microbiology and Hygiene, Institute for Medical Microbiology,
            Immunology and Hygiene, University of Cologne, Goldenfelsstrasse
            19-21, 50935 Koln, Germany).
            Bacteria provided by Harald Seifert (Professor of Medical
            Microbiology and Hygiene, Institute for Medical Microbiology,
            Immunology and Hygiene, University of Cologne, Goldenfelsstrasse
            19-21, 50935 Koln, Germany).
            Genome Coverage: 22.06x
            Sequencing Technology: 454.
FEATURES             Location/Qualifiers
     source          1..927
                     /organism="Acinetobacter lwoffii SH145"
                     /mol_type="genomic DNA"
                     /strain="SH145"
                     /db_xref="taxon:575588"
     gene            complement(<1..225)
                     /locus_tag="HMPREF0017_03103"
     CDS             complement(<1..225)
                     /locus_tag="HMPREF0017_03103"
                     /note="3' partial"
                     /codon_start=1
                     /transl_table=11
                     /product="predicted protein"
                     /protein_id="ZP_06071182.1"
                     /db_xref="GI:262377992"
                     /translation="MSLDIQTLRDEAQALLQVEQAFLNKMLDQQLLSESDGKKLSQYD
                     QATLDKDSVKTEISVLRGEGVKLSNLEMVLA"
     gene            complement(286..732)
                     /locus_tag="HMPREF0017_03104"
     CDS             complement(286..732)
                     /locus_tag="HMPREF0017_03104"
                     /codon_start=1
                     /transl_table=11
                     /product="predicted protein"
                     /protein_id="ZP_06071183.1"
                     /db_xref="GI:262377993"
                     /translation="MSCSRTISWLYGEEKKSKKDKIKKIISEIRQIHGQIFSTLDSIL
                     FFAELYPLDIYIDKRSLDIYKRDSEKIVKKISSILINDTHTYDGIRQKMVAELQDIHL
                     NFKRIHEHNSKNMLSFNIESQLIEDFSYWQVSFKKLQQQVRLYSSK"
ORIGIN      
        1 cgctaatacc atttctaagt ttgaaagttt aactccttca ccacgtagaa cagaaatttc
       61 tgttttaaca gaatccttat ctagagtcgc ttgatcatat tgactgagct ttttaccatc
      121 agactctgat aaaagttgct gatctagcat tttattcaaa aatgcttgtt caacttgtaa
      181 taatgcctga gcttcatctc ttaacgtttg aatatctaaa gacatgtaat ctaccctttt
      241 gattttatgt aataatataa tttaataaaa atatgcgttc tgactttact tagaactata
      301 taagcgcact tgctgttgca attttttaaa agatacctgc caatatgaaa agtcctcaat
      361 caattgagac tcaatattaa aagacagcat atttttggaa ttatgttcat gtatcctttt
      421 aaaatttaaa tgtatatctt gtagctcagc caccattttt tgcctaatac catcataggt
      481 gtgtgtgtca ttaatcaata tactgctgat tttttttaca attttttcag aatctctttt
      541 atatatatct aagcttcttt tatctatata aatatctaac ggataaagtt ctgcaaagaa
      601 taaaattgaa tctaatgtag aaaagatttg accatgtatt tgtctaattt cactaatgat
      661 tttcttaatt ttatccttct tacttttttt ctcttcacca tatagccatg atattgttct
      721 agaacaactc atttctttat gaatttcttt agtcaaacga tctaaagaat taaatactcc
      781 ccgtaggtca gaatttactg agatttttga aatgaactct agaaattcaa aaaaaggttt
      841 tagctcatta ttatttggat tgagcttatc gactgcagct tctaaagcta gaattgcagc
      901 attgtcatat gaacttgcaa taatatc
//
