LOCUS       NZ_GG705086             4868 bp    DNA     linear   CON 09-JUN-2010
DEFINITION  Acinetobacter lwoffii SH145 genomic scaffold supercont1.32, whole
            genome shotgun sequence.
ACCESSION   NZ_GG705086 NZ_ACPN01000000
VERSION     NZ_GG705086.1  GI:262377950
DBLINK      Project: 41587
            BioProject: PRJNA41587
KEYWORDS    WGS.
SOURCE      Acinetobacter lwoffii SH145
  ORGANISM  Acinetobacter lwoffii SH145
            Bacteria; Proteobacteria; Gammaproteobacteria; Pseudomonadales;
            Moraxellaceae; Acinetobacter.
REFERENCE   1  (bases 1 to 4868)
  AUTHORS   Ward,D., Feldgarden,M., Earl,A., Young,S.K., Zeng,Q., Koehrsen,M.,
            Alvarado,L., Berlin,A., Bochicchio,J., Borenstein,D., Chapman,S.,
            Chen,Z., Engels,R., Freedman,E., Gellesch,M., Goldberg,J.,
            Griggs,A., Gujja,S., Heilman,E., Heiman,D., Hepburn,T., Howarth,C.,
            Jen,D., Larson,L., Lewis,B., Mehta,T., Park,D., Pearson,M.,
            Roberts,A., Saif,S., Shea,T., Shenoy,N., Sisk,P., Stolte,C.,
            Sykes,S., Thomson,T., Walk,T., White,J., Yandava,C., Seifert,H.,
            Haas,B., Nusbaum,C. and Birren,B.
  CONSRTM   The Broad Institute Genome Sequencing Platform
  TITLE     The Genome Sequence of Acinetobacter lwoffii strain SH145
  JOURNAL   Unpublished
REFERENCE   2  (bases 1 to 4868)
  AUTHORS   Ward,D., Feldgarden,M., Earl,A., Young,S.K., Zeng,Q., Koehrsen,M.,
            Alvarado,L., Berlin,A., Bochicchio,J., Borenstein,D., Chapman,S.,
            Chen,Z., Engels,R., Freedman,E., Gellesch,M., Goldberg,J.,
            Griggs,A., Gujja,S., Heilman,E., Heiman,D., Hepburn,T., Howarth,C.,
            Jen,D., Larson,L., Lewis,B., Mehta,T., Park,D., Pearson,M.,
            Roberts,A., Saif,S., Shea,T., Shenoy,N., Sisk,P., Stolte,C.,
            Sykes,S., Thomson,T., Walk,T., White,J., Yandava,C., Seifert,H.,
            Haas,B., Nusbaum,C. and Birren,B.
  CONSRTM   The Broad Institute Genome Sequencing Platform
  TITLE     Direct Submission
  JOURNAL   Submitted (06-JUL-2009) Broad Institute of MIT and Harvard, 7
            Cambridge Center, Cambridge, MA 02142, USA
COMMENT     PROVISIONAL REFSEQ: This record has not yet been subject to final
            NCBI review. The reference sequence was derived from GG705086.
            This is a reference genome for the Human Microbiome Project. This
            project is co-owned with the Human Microbiome Project DACC.
            Source DNA provided by Harald Seifert (Professor of Medical
            Microbiology and Hygiene, Institute for Medical Microbiology,
            Immunology and Hygiene, University of Cologne, Goldenfelsstrasse
            19-21, 50935 Koln, Germany).
            Bacteria provided by Harald Seifert (Professor of Medical
            Microbiology and Hygiene, Institute for Medical Microbiology,
            Immunology and Hygiene, University of Cologne, Goldenfelsstrasse
            19-21, 50935 Koln, Germany).
            Genome Coverage: 22.06x
            Sequencing Technology: 454.
FEATURES             Location/Qualifiers
     source          1..4868
                     /organism="Acinetobacter lwoffii SH145"
                     /mol_type="genomic DNA"
                     /strain="SH145"
                     /db_xref="taxon:575588"
     gene            complement(<1..1082)
                     /locus_tag="HMPREF0017_03075"
     CDS             complement(<1..1082)
                     /locus_tag="HMPREF0017_03075"
                     /note="3' partial"
                     /codon_start=1
                     /transl_table=11
                     /product="ABC-2 type transporter"
                     /protein_id="ZP_06071154.1"
                     /db_xref="GI:262377951"
                     /translation="MKSLWFYYLQSFKDIVSHGTIFTTLILSVLFYSFFYPTAYQAQQ
                     AEALPIIIVDEEQSSLSTRIIGQVAQSPNVEIQAITGNFAEAKQWMESQKADGILLLP
                     DNLSQSLRHGETGGIGLYLSTANFLVTKQIGLGLATSVEQTLADYTERFGKVSDFSPA
                     LSVHQIPLFNPLSGYGSYVFPAVAPLIIHQTILLGLSMLILVYRERKVELNANALIGM
                     CLAVFTIGCLGCFYLFGFSFWLFDYPRGGNLLGMLLAVPVFIYTIMGMTCLFASFLDL
                     PERAGHVIVFTSIPLFFLSGAAWPHAAMPTWMQVVGEILPSTQIVQMFIQLNQMGVPT
                     ALVLPKLLYLGMIGSLCFFLAYRRLIS"
     misc_feature    complement(12..>611)
                     /locus_tag="HMPREF0017_03075"
                     /note="ABC-2 family transporter protein; Region:
                     ABC2_membrane_3; pfam12698"
                     /db_xref="CDD:205027"
     misc_feature    complement(120..560)
                     /locus_tag="HMPREF0017_03075"
                     /note="ABC-2 type transporter; Region: ABC2_membrane;
                     cl11417"
                     /db_xref="CDD:213123"
     gene            complement(1079..2209)
                     /locus_tag="HMPREF0017_03076"
     CDS             complement(1079..2209)
                     /locus_tag="HMPREF0017_03076"
                     /codon_start=1
                     /transl_table=11
                     /product="conserved hypothetical protein"
                     /protein_id="ZP_06071155.1"
                     /db_xref="GI:262377952"
                     /translation="MWTGIWRELRYLLREKWDLCLVTLAPALVIILFSSMFAQGKPDH
                     LPIAIIDQDQSALSQSIYQHVALNHTLKIATVSEQTAEIERLLNQNKIWGYIHIPSGA
                     EQRLVRAQDADISIAFNQSYFSIGNTISSAMLVSTLQALAEFGGQQYLGNRLPSLEAP
                     SPHIKISPLYNPQLNYEFYLEPYVIPAILHLLLCCCVAFSVGQELKRQTLTSWISNSS
                     TWMALLSKNLVYVAIFSLWTWIWMFWLIEIRGWFVAGSLSLILLAQFLLYSAYAFLSS
                     AVVLATKDLSKSFGLIAVYGGSSLSFAGVTLPLNNAPLFTQFWANIIPFTPYAKLQTE
                     QWVIGSPVHISLQAGLMLLIYALVYALLAYLFLKKIAKGATP"
     misc_feature    complement(<1643..2149)
                     /locus_tag="HMPREF0017_03076"
                     /note="Protein of unknown function (DUF3533); Region:
                     DUF3533; pfam12051"
                     /db_xref="CDD:204818"
     misc_feature    complement(1217..>1741)
                     /locus_tag="HMPREF0017_03076"
                     /note="ABC-2 family transporter protein; Region:
                     ABC2_membrane_3; pfam12698"
                     /db_xref="CDD:205027"
     gene            complement(2209..3339)
                     /locus_tag="HMPREF0017_03077"
     CDS             complement(2209..3339)
                     /locus_tag="HMPREF0017_03077"
                     /codon_start=1
                     /transl_table=11
                     /product="HlyD family secretion family protein"
                     /protein_id="ZP_06071156.1"
                     /db_xref="GI:262377953"
                     /translation="MNEDQEPNDPVNEAASSSDTEKSPAQQVETKTEIENNATTSTEQ
                     QQAPEIAKKKKYLLLALIPVLLAVIAFGLWKSYQPAPLELQGRVEAETVQVATKVPSR
                     IEEIYVEEGQRVKKGDVLVRLNSPEIQAKKQQAAAALQSALALQSTAERGSQEENIAS
                     LYANWQSLKAQQNLTKVSYERGANLFKEGVISRQRRDELYAASQSAAQMTEAAYQQYA
                     RAKRGSTSQQKSSADAQVDIAQAALDEANALEAETQLVAPVNGTVSKTYGKVSELVAT
                     AVPVVSLIEDQMWVSLNIREDQYAPFQKMSSIEGYIPALDKTATFQIRQISAEGEFAT
                     IKTTRQTGGYDVRSFKLHLVPTPAIPELKVGMSVLFKLKETR"
     misc_feature    complement(2224..3123)
                     /locus_tag="HMPREF0017_03077"
                     /note="putative efflux pump membrane fusion protein;
                     Provisional; Region: PRK03598"
                     /db_xref="CDD:179602"
     misc_feature    complement(<2956..3069)
                     /locus_tag="HMPREF0017_03077"
                     /note="Biotin-lipoyl like; Region: Biotin_lipoyl_2;
                     pfam13533"
                     /db_xref="CDD:205711"
     misc_feature    complement(2299..2580)
                     /locus_tag="HMPREF0017_03077"
                     /note="Biotinyl_lipoyl_domains are present in
                     biotin-dependent carboxylases/decarboxylases, the
                     dihydrolipoyl acyltransferase component (E2) of 2-oxo acid
                     dehydrogenases, and the H-protein of the glycine cleavage
                     system (GCS). These domains transport CO2, acyl; Region:
                     Biotinyl_lipoyl_domains; cl11404"
                     /db_xref="CDD:213122"
     gene            complement(3351..4865)
                     /locus_tag="HMPREF0017_03078"
     CDS             complement(3351..4865)
                     /locus_tag="HMPREF0017_03078"
                     /codon_start=1
                     /transl_table=11
                     /product="outer membrane protein"
                     /protein_id="ZP_06071157.1"
                     /db_xref="GI:262377954"
                     /translation="MIYTSRITSPALNQFSDFMNLKKNKRQLVWGLNLLASMMYGSFA
                     HAQSISFQDAERQLLQNSYSSQAYQSLEQASKLEAEAVKGIGLPRVDLNVRAYAFHNE
                     LDLPLGALKNNLEQSLTNGVNNRIDELNLGGLADPLKNSLRQPIQNGVGLIPDSSHVV
                     LDDQVIRPTVSVMMPLYTGGLTSSAKEIANIQVGRSQLSNKQQQDTQRFELIQSYFNV
                     QLQKQLHAAALFNLNAMQQHYRNALKMEQQGFISKGQRMQFEVARNNAERSQQNTQAN
                     LNAALFQLNNLLQESSITELSTPLFVNKTEPQALNHLLKTFSQNSALIQKMQLDTQLA
                     QANVKAQQAAKKPNVFAFGEYSLDQNENWIVGVAARYNLFSGIDKNKNIQAAELKRSA
                     TELLTARTQQEIENLIYKSYSEALSAQQSDALLAQNLKAAQENLRIQELSFKEDMGTA
                     TQVIDAQNMLSGLQAETALNAYKYVMSLATLLQSHGSIAEFPTYIQHTNTHYIR"
     misc_feature    complement(4062..>4382)
                     /locus_tag="HMPREF0017_03078"
                     /note="Outer membrane efflux protein; Region: OEP;
                     pfam02321"
                     /db_xref="CDD:202204"
     misc_feature    complement(3417..3938)
                     /locus_tag="HMPREF0017_03078"
                     /note="Outer membrane efflux protein; Region: OEP;
                     pfam02321"
                     /db_xref="CDD:202204"
ORIGIN      
        1 gaaattaaac gtcgataagc caagaaaaaa cacaagctgc cgatcattcc aagatagagc
       61 agtttaggca agaccagtgc ggtaggcaca cccatctgat tgagttgaat aaacatctgc
      121 acaatttggg tagagggcag gatttcgcca acgacttgca tccaggtggg catggcagca
      181 tgcggccagg ccgcaccaga taaaaagaac agtggaatag aagtgaatac aatgacatgc
      241 cccgcacgtt ccggcagatc cagaaaactg gcaaataggc aggtcatgcc cattatcgtg
      301 tagataaaaa ctggaacagc cagtagcatc ccgagaaggt tgccaccgcg cggataatca
      361 aatagccaga agctaaaacc gaataaatag aaacatccca gacagccaat agtaaagacc
      421 gctaaacaca tcccaatcaa tgcatttgcg tttaattcta cttttctttc ccgatatact
      481 aaaatcagca tactcaaacc caaaagaatg gtttggtgaa taattaaagg tgcaaccgca
      541 ggaaagacat aactgccata gccggacagc ggattaaata aaggaatttg atgcacagaa
      601 agggccggtg aaaaatccga gaccttgcca aagcgttcag tatagtctgc caacgtttgc
      661 tcaaccgatg tggctaagcc cagaccaatt tgtttggtca ctaaaaagtt ggccgtgctt
      721 aaatacagtc caattccgcc tgtttcacca tgccttaagc tttgcgataa attgtccggc
      781 agcagtaaaa ttccatcggc tttttgagat tccatccatt gtttggcttc ggcaaaattt
      841 ccggtaatcg cctgaatttc gacattgggg ctttgtgcca cctgaccaat aatgcgggta
      901 cttaagctac tttgttcttc atcgacaata ataatcggta aggcttcagc ctgctgtgcc
      961 tgataagcgg tcggataaaa aaagctgtaa aacaagacgg atagaatcag cgtggtaaag
     1021 atcgtgccgt gactgacgat atctttaaag ctttgtagat agtaaaacca aagggatttc
     1081 atggtgttgc tcctttggcg atttttttaa gaaatagata agccagcaga gcgtagacta
     1141 gggcatagat caacagcatc aatccagctt gcagtgaaat atgtactgga ctaccaatca
     1201 cccattgttc agtttgtaat ttggcatagg gcgtaaatgg aataatattg gcccaaaact
     1261 gggtgaacag gggcgcatta tttaaaggta gggtcacacc ggcaaagctc aacgatgaac
     1321 ccccatagac cgcaatgagg ccgaaagatt tacttaaatc tttggtcgcc agaaccactg
     1381 cactgcttaa aaatgcatag gcgctataca gcagaaattg cgccaataga atcaggctga
     1441 gcgaaccggc aacaaaccag ccgcgaatct caatcagcca gaacatccag atccaggtcc
     1501 atagactaaa aatagccaca tagaccaggt tcttagacag caatgccatc caggtggacg
     1561 agttgctgat ccagcttgtt aaggtctgtc gcttcagttc ctgaccgacg gaaaatgcca
     1621 cacaacaaca cagcagtaaa tgcagaattg ccggaatgac atagggttcc agatagaatt
     1681 cataattcag ctgcggatta tacagcggcg atattttgat atgcggcgac ggtgcttcga
     1741 gagaaggcag ccggttgccc agatattgct gtccgccaaa ttcagccagt gcttgtaggg
     1801 tactgaccag catggcagag gagatggtat ttccgatact gaaataactc tggttaaagg
     1861 caatactgat gtcagcatcc tgggcacgaa ccagacgttg ctcggcaccg cttggaatat
     1921 ggatataacc ccaaatttta ttttgattta acaggcgctc aatttcagct gtttgttccg
     1981 acaccgtagc aattttaaga gtatgattca gcgccacatg ctgataaatg ctttgactca
     2041 gcgcactctg gtcttgatca ataatcgcaa tcggtaaatg atccggtttc ccttgagcaa
     2101 acatgctgct gaataaaata atcaccagtg caggtgcgag cgtgaccaga cacagatccc
     2161 atttttctcg gagcagatag cgcagttcgc gccatatacc cgtccacatt atcgggtctc
     2221 tttcagttta aataacacgc tcatcccaac tttcagttca ggaatcgctg gtgtgggcac
     2281 aagatgcagc ttaaaacttc gcacatcata accgccggtt tggcgggtgg ttttaatggt
     2341 ggcaaattca ccttcagcac tgatctgtct aatttgaaaa gtggcggttt tatctagggc
     2401 gggaatataa ccttcaatac tgctcatttt ctgaaatggg gcatattggt cttcacgaat
     2461 attcaggctc acccacattt gatcttcaat caggctgacc acagggactg cggtggctac
     2521 cagctccgag actttgccat aggttttaga caccgttccg tttacaggtg ctactaattg
     2581 ggtttctgcc tctaatgcat tggcttcatc cagcgcagcc tgggcaatat ctacttgtgc
     2641 atcggcagaa ctcttttgct gcgaggtgct gccacgtttg gcacgtgcgt attgctgata
     2701 cgctgcttct gtcatttgtg ccgctgattg actggctgca tacaattcat cacggcgctg
     2761 gcgtgaaatc acaccttcct taaataaatt tgcaccacgc tcataagaaa ctttggttaa
     2821 attttgctgt gctttcagcg attgccagtt ggcatacagg ctcgcgatat tttcttcttg
     2881 cgagccacgt tcagcagtcg attgtaaggc cagcgcagat tgtaaagccg ctgcggcttg
     2941 ctgttttttg gcctgaattt ccgggctgtt taagcgcacc agcacatcgc cttttttgac
     3001 ccgctgacct tcttctacat aaatttcttc aatacggctt ggcactttgg tcgccacctg
     3061 aaccgtttca gcttctacac gaccctgcaa ttccagcgga gccggttggt agcttttcca
     3121 taatccaaat gcgatgactg ccagcagaac tggaatcagg gccagcaata aatatttttt
     3181 ctttttagcg atttcaggag cttgttgttg ctcggtcgaa gtcgtcgcgt tgttttctat
     3241 ttcagttttt gtttctacct gttgcgcagg cgatttttct gtatcgctgg atgaagccgc
     3301 ttcattcaca ggatcattcg gttcttgatc ttcgttcatg tttggctcca ttagcgaata
     3361 tagtgagtgt tggtatgttg gatgtaggtc ggaaattctg caatcgagcc atggctttgt
     3421 aatagtgtgg ccagtgacat cacatatttg taagcattca aggcagtttc agcttgtagt
     3481 ccgctcagca tgttttgtgc atcgatgacc tgtgtggcgg tgcccatatc ttctttaaag
     3541 gacaattctt ggatgcgtaa attttcctgt gcggctttca agttttgtgc cagaagcgca
     3601 tcactttgct gtgcactgag tgcttcgcta taagatttat aaatcagatt ttcaatttcc
     3661 tgctgggtgc gtgcagtaag cagttctgta gcagagcgtt tgagttcggc agcctgaata
     3721 tttttatttt tatcaattcc ggaaaataag ttataacggg cagccacccc gacaatccag
     3781 ttttcatttt gatccagact atattcacca aaagcaaaaa cattcggttt ttttgctgct
     3841 tgctgggctt tgacattggc ttgagctaat tgagtatcca gctgcatttt ttgaatcagc
     3901 gcagagttct gactaaaggt tttcagcaga tgatttaagg cctgtggctc ggttttattg
     3961 acaaataaag gcgtcgaaag ttcggtgatg ctactttcct gaagcagatt attcagctgg
     4021 aacagggcag cattcaaatt ggcttgcgta ttttgctggc tgcgctcggc attattgcgt
     4081 gccacttcaa attgcatgcg ctggccttta ctgataaaac cttgctgttc catctttaag
     4141 gcattgcgat aatgctgttg catggcattc aggttaaata gtgctgctgc atgtagttgc
     4201 ttttgaagct gtacattaaa ataactttga atcagttcaa aacgctgcgt gtcttgctgc
     4261 tgtttgttgc tgagctgact acgaccgacc tgaatattgg cgatttcttt agcgctgctt
     4321 gtcagtccac cggtataaag cggcatcatg actgaaaccg tcgggcgaat cacctgatca
     4381 tccagcacga catgcgaact atcaggaatt aagccaactc cattttgaat aggttgtctt
     4441 aaactatttt ttagcgggtc agccagaccc cctaagttca gttcatcaat tctattattg
     4501 acaccattcg ttaaggattg ctctaaatta ttttttaatg cacctaaagg cagatcgagt
     4561 tcattatgaa aggcatacgc acgaacattc agatcgactc tcggcagacc tatacctttc
     4621 actgcttcag cttcaagttt tgaagcctgc tccagacttt gataagcttg gctgctataa
     4681 gaattttgca acaactgacg ttcagcgtcc tgaaagctga tgctctgcgc atgagcaaag
     4741 ctgccataca tcatcgatgc aagcagattt aaaccccaga caagctgtct tttatttttc
     4801 tttaagttca taaagtcgct aaactgattt aaagccggtg atgttatccg gctagtataa
     4861 atcaaagg
//
