LOCUS       NZ_GG705118              665 bp    DNA     linear   CON 09-JUN-2010
DEFINITION  Acinetobacter lwoffii SH145 genomic scaffold supercont1.64, whole
            genome shotgun sequence.
ACCESSION   NZ_GG705118 NZ_ACPN01000000
VERSION     NZ_GG705118.1  GI:262378031
DBLINK      Project: 41587
            BioProject: PRJNA41587
KEYWORDS    WGS.
SOURCE      Acinetobacter lwoffii SH145
  ORGANISM  Acinetobacter lwoffii SH145
            Bacteria; Proteobacteria; Gammaproteobacteria; Pseudomonadales;
            Moraxellaceae; Acinetobacter.
REFERENCE   1  (bases 1 to 665)
  AUTHORS   Ward,D., Feldgarden,M., Earl,A., Young,S.K., Zeng,Q., Koehrsen,M.,
            Alvarado,L., Berlin,A., Bochicchio,J., Borenstein,D., Chapman,S.,
            Chen,Z., Engels,R., Freedman,E., Gellesch,M., Goldberg,J.,
            Griggs,A., Gujja,S., Heilman,E., Heiman,D., Hepburn,T., Howarth,C.,
            Jen,D., Larson,L., Lewis,B., Mehta,T., Park,D., Pearson,M.,
            Roberts,A., Saif,S., Shea,T., Shenoy,N., Sisk,P., Stolte,C.,
            Sykes,S., Thomson,T., Walk,T., White,J., Yandava,C., Seifert,H.,
            Haas,B., Nusbaum,C. and Birren,B.
  CONSRTM   The Broad Institute Genome Sequencing Platform
  TITLE     The Genome Sequence of Acinetobacter lwoffii strain SH145
  JOURNAL   Unpublished
REFERENCE   2  (bases 1 to 665)
  AUTHORS   Ward,D., Feldgarden,M., Earl,A., Young,S.K., Zeng,Q., Koehrsen,M.,
            Alvarado,L., Berlin,A., Bochicchio,J., Borenstein,D., Chapman,S.,
            Chen,Z., Engels,R., Freedman,E., Gellesch,M., Goldberg,J.,
            Griggs,A., Gujja,S., Heilman,E., Heiman,D., Hepburn,T., Howarth,C.,
            Jen,D., Larson,L., Lewis,B., Mehta,T., Park,D., Pearson,M.,
            Roberts,A., Saif,S., Shea,T., Shenoy,N., Sisk,P., Stolte,C.,
            Sykes,S., Thomson,T., Walk,T., White,J., Yandava,C., Seifert,H.,
            Haas,B., Nusbaum,C. and Birren,B.
  CONSRTM   The Broad Institute Genome Sequencing Platform
  TITLE     Direct Submission
  JOURNAL   Submitted (06-JUL-2009) Broad Institute of MIT and Harvard, 7
            Cambridge Center, Cambridge, MA 02142, USA
COMMENT     PROVISIONAL REFSEQ: This record has not yet been subject to final
            NCBI review. The reference sequence was derived from GG705118.
            This is a reference genome for the Human Microbiome Project. This
            project is co-owned with the Human Microbiome Project DACC.
            Source DNA provided by Harald Seifert (Professor of Medical
            Microbiology and Hygiene, Institute for Medical Microbiology,
            Immunology and Hygiene, University of Cologne, Goldenfelsstrasse
            19-21, 50935 Koln, Germany).
            Bacteria provided by Harald Seifert (Professor of Medical
            Microbiology and Hygiene, Institute for Medical Microbiology,
            Immunology and Hygiene, University of Cologne, Goldenfelsstrasse
            19-21, 50935 Koln, Germany).
            Genome Coverage: 22.06x
            Sequencing Technology: 454.
FEATURES             Location/Qualifiers
     source          1..665
                     /organism="Acinetobacter lwoffii SH145"
                     /mol_type="genomic DNA"
                     /strain="SH145"
                     /db_xref="taxon:575588"
     gene            complement(<1..616)
                     /locus_tag="HMPREF0017_03124"
     CDS             complement(<1..616)
                     /locus_tag="HMPREF0017_03124"
                     /note="3' partial"
                     /codon_start=1
                     /transl_table=11
                     /product="integrase"
                     /protein_id="ZP_06071203.1"
                     /db_xref="GI:262378032"
                     /translation="MLITMSDKEIQRLAVLQDVRDHRITQVRAAEILNLSTRQITRLL
                     QKLNQDGVSSMAHASRGQPGHHRHDDLLKSKCLSIISEHLLGFGPTLAHEKLSSMFDL
                     NIPVETLRRWMTANDLWIPRSKRLKRPYQPRYNRDCFGELIQIDGSYHDWFEGRAAKC
                     CLLVYIDDATGKLLHLRFCEAETTFDYMLSTRAYIEQYGKPLAFY"
     misc_feature    complement(434..586)
                     /locus_tag="HMPREF0017_03124"
                     /note="Helix-turn-helix domain; Region: HTH_28; pfam13518"
                     /db_xref="CDD:205696"
     misc_feature    complement(278..583)
                     /locus_tag="HMPREF0017_03124"
                     /note="Winged helix-turn helix; Region: HTH_29; pfam13551"
                     /db_xref="CDD:205729"
     misc_feature    complement(278..502)
                     /locus_tag="HMPREF0017_03124"
                     /note="Helix-turn-helix domains; Region: HTH; cl00088"
                     /db_xref="CDD:213080"
ORIGIN      
        1 tataaaaggc cagaggctta ccgtattgtt caatgtaggc tcgggttgaa agcatatagt
       61 cgaaggtcgt ttccgcctca cagaagcgca gatgcaacag ctttccagtg gcatcatcga
      121 tataaaccag caagcagcac ttagcagcgc gtccttcgaa ccagtcatga tatgagccat
      181 caatctggat tagctcaccg aagcaatccc ggttataacg tggctgatat ggacgtttaa
      241 ggcgcttgga tcgtggaatc cagaggtcat ttgcagtcat ccagcgccga agcgtttcta
      301 ccgggatatt caggtcaaac atgctgctga gcttctcatg ggccaaggta ggtccaaatc
      361 ccagcagatg ttcagaaata atggaaagac attttgattt taataaatcg tcatggcgat
      421 gatggcccgg ttgaccacga ctggcatgcg ccatacttga aacaccatct tgattgagct
      481 tctgtaataa ccgggtaatt tgacgggttg aaagattaag gatttcagca gcacggactt
      541 gtgtaatacg atgatctcga acgtcttgca gaactgcaag acgttgaatt tctttgtcag
      601 acatagtgat cagcatctat atttatatcc agtccatgat ggtgaaaacc atcataaact
      661 agaca
//
