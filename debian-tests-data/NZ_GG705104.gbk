LOCUS       NZ_GG705104              821 bp    DNA     linear   CON 09-JUN-2010
DEFINITION  Acinetobacter lwoffii SH145 genomic scaffold supercont1.50, whole
            genome shotgun sequence.
ACCESSION   NZ_GG705104 NZ_ACPN01000000
VERSION     NZ_GG705104.1  GI:262378002
DBLINK      Project: 41587
            BioProject: PRJNA41587
KEYWORDS    WGS.
SOURCE      Acinetobacter lwoffii SH145
  ORGANISM  Acinetobacter lwoffii SH145
            Bacteria; Proteobacteria; Gammaproteobacteria; Pseudomonadales;
            Moraxellaceae; Acinetobacter.
REFERENCE   1  (bases 1 to 821)
  AUTHORS   Ward,D., Feldgarden,M., Earl,A., Young,S.K., Zeng,Q., Koehrsen,M.,
            Alvarado,L., Berlin,A., Bochicchio,J., Borenstein,D., Chapman,S.,
            Chen,Z., Engels,R., Freedman,E., Gellesch,M., Goldberg,J.,
            Griggs,A., Gujja,S., Heilman,E., Heiman,D., Hepburn,T., Howarth,C.,
            Jen,D., Larson,L., Lewis,B., Mehta,T., Park,D., Pearson,M.,
            Roberts,A., Saif,S., Shea,T., Shenoy,N., Sisk,P., Stolte,C.,
            Sykes,S., Thomson,T., Walk,T., White,J., Yandava,C., Seifert,H.,
            Haas,B., Nusbaum,C. and Birren,B.
  CONSRTM   The Broad Institute Genome Sequencing Platform
  TITLE     The Genome Sequence of Acinetobacter lwoffii strain SH145
  JOURNAL   Unpublished
REFERENCE   2  (bases 1 to 821)
  AUTHORS   Ward,D., Feldgarden,M., Earl,A., Young,S.K., Zeng,Q., Koehrsen,M.,
            Alvarado,L., Berlin,A., Bochicchio,J., Borenstein,D., Chapman,S.,
            Chen,Z., Engels,R., Freedman,E., Gellesch,M., Goldberg,J.,
            Griggs,A., Gujja,S., Heilman,E., Heiman,D., Hepburn,T., Howarth,C.,
            Jen,D., Larson,L., Lewis,B., Mehta,T., Park,D., Pearson,M.,
            Roberts,A., Saif,S., Shea,T., Shenoy,N., Sisk,P., Stolte,C.,
            Sykes,S., Thomson,T., Walk,T., White,J., Yandava,C., Seifert,H.,
            Haas,B., Nusbaum,C. and Birren,B.
  CONSRTM   The Broad Institute Genome Sequencing Platform
  TITLE     Direct Submission
  JOURNAL   Submitted (06-JUL-2009) Broad Institute of MIT and Harvard, 7
            Cambridge Center, Cambridge, MA 02142, USA
COMMENT     PROVISIONAL REFSEQ: This record has not yet been subject to final
            NCBI review. The reference sequence was derived from GG705104.
            This is a reference genome for the Human Microbiome Project. This
            project is co-owned with the Human Microbiome Project DACC.
            Source DNA provided by Harald Seifert (Professor of Medical
            Microbiology and Hygiene, Institute for Medical Microbiology,
            Immunology and Hygiene, University of Cologne, Goldenfelsstrasse
            19-21, 50935 Koln, Germany).
            Bacteria provided by Harald Seifert (Professor of Medical
            Microbiology and Hygiene, Institute for Medical Microbiology,
            Immunology and Hygiene, University of Cologne, Goldenfelsstrasse
            19-21, 50935 Koln, Germany).
            Genome Coverage: 22.06x
            Sequencing Technology: 454.
FEATURES             Location/Qualifiers
     source          1..821
                     /organism="Acinetobacter lwoffii SH145"
                     /mol_type="genomic DNA"
                     /strain="SH145"
                     /db_xref="taxon:575588"
     gene            60..807
                     /locus_tag="HMPREF0017_03109"
     CDS             166..807
                     /locus_tag="HMPREF0017_03109"
                     /note="truncated CDS"
                     /codon_start=1
                     /transl_table=11
                     /product="transposase (IS4 family)"
                     /protein_id="ZP_06071188.1"
                     /db_xref="GI:262378003"
                     /translation="MLRQYSIVIVQVFRGETYPNGSVILESYIPVLAVGLKQACGREF
                     LKCYHMTVIMNMPCWTQPLCALINTVPEKKEQAIGRSKGGLSTKIHARTDALGNPTGF
                     YLTAGQAHDLNGADVLLDLSLSQTWLMDKAYNSRDRVIDPIHKINGQVVMPSKSNAID
                     QRDYDQHLYKARHLIENFFAKLKQYRGIATRYDKLAQNFLSAIYLASIMIWLN"
     misc_feature    379..801
                     /locus_tag="HMPREF0017_03109"
                     /note="Transposase DDE domain; Region: DDE_Tnp_1;
                     pfam01609"
                     /db_xref="CDD:201886"
     misc_feature    553..801
                     /locus_tag="HMPREF0017_03109"
                     /note="Transposase DDE domain; Region: DDE_Tnp_1_2;
                     pfam13586"
                     /db_xref="CDD:205764"
ORIGIN      
        1 aatctagggc ctgtcatcaa ttagcttgag aaaatctaat ctgtcctcat tattttttca
       61 tgactaaacg ctatgcatta agagatgatc agtgggaacg gatcaaggat ctgttgcctg
      121 gacggccggg tactgttgga gtaaccgcca aagacaaccg attatttgtt gaggcaatac
      181 tctatcgtta tcgttcaggt attccgtggc gagacttacc cgaacggttc ggtgatttta
      241 gagtcgtaca tacccgtttt agccgttggg ctaaaacagg cgtgtggcag agagtttttg
      301 aagtgctatc acatgacagt gataatgaat atgccatgct ggactcaacc attgtgcgcg
      361 ctcatcaaca cagtgccgga aaaaaaagaa caggcgatcg gacgcagtaa aggtggctta
      421 agtaccaaaa ttcatgcccg tactgatgct ttaggcaacc caacaggatt ttatctcact
      481 gccgggcagg cccatgactt gaacggtgcc gatgtgttac tggacttatc gttaagccaa
      541 acatggctta tggataaagc ctacaattcc agggatcgag tgattgatcc gattcataag
      601 atcaatggtc aagtcgtgat gccatccaaa agtaacgcga ttgatcagcg tgattatgac
      661 caacatcttt ataaggcaag acatctgatc gagaatttct tcgccaagct caagcagtat
      721 cgaggtattg ctacgcgcta tgataaattg gctcaaaact tcctgtcagc gatctacctt
      781 gcctcaatca tgatatggct taattgatga cacgccctaa a
//
