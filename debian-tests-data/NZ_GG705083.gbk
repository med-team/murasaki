LOCUS       NZ_GG705083             5476 bp    DNA     linear   CON 09-JUN-2010
DEFINITION  Acinetobacter lwoffii SH145 genomic scaffold supercont1.29, whole
            genome shotgun sequence.
ACCESSION   NZ_GG705083 NZ_ACPN01000000
VERSION     NZ_GG705083.1  GI:262377932
DBLINK      Project: 41587
            BioProject: PRJNA41587
KEYWORDS    WGS.
SOURCE      Acinetobacter lwoffii SH145
  ORGANISM  Acinetobacter lwoffii SH145
            Bacteria; Proteobacteria; Gammaproteobacteria; Pseudomonadales;
            Moraxellaceae; Acinetobacter.
REFERENCE   1  (bases 1 to 5476)
  AUTHORS   Ward,D., Feldgarden,M., Earl,A., Young,S.K., Zeng,Q., Koehrsen,M.,
            Alvarado,L., Berlin,A., Bochicchio,J., Borenstein,D., Chapman,S.,
            Chen,Z., Engels,R., Freedman,E., Gellesch,M., Goldberg,J.,
            Griggs,A., Gujja,S., Heilman,E., Heiman,D., Hepburn,T., Howarth,C.,
            Jen,D., Larson,L., Lewis,B., Mehta,T., Park,D., Pearson,M.,
            Roberts,A., Saif,S., Shea,T., Shenoy,N., Sisk,P., Stolte,C.,
            Sykes,S., Thomson,T., Walk,T., White,J., Yandava,C., Seifert,H.,
            Haas,B., Nusbaum,C. and Birren,B.
  CONSRTM   The Broad Institute Genome Sequencing Platform
  TITLE     The Genome Sequence of Acinetobacter lwoffii strain SH145
  JOURNAL   Unpublished
REFERENCE   2  (bases 1 to 5476)
  AUTHORS   Ward,D., Feldgarden,M., Earl,A., Young,S.K., Zeng,Q., Koehrsen,M.,
            Alvarado,L., Berlin,A., Bochicchio,J., Borenstein,D., Chapman,S.,
            Chen,Z., Engels,R., Freedman,E., Gellesch,M., Goldberg,J.,
            Griggs,A., Gujja,S., Heilman,E., Heiman,D., Hepburn,T., Howarth,C.,
            Jen,D., Larson,L., Lewis,B., Mehta,T., Park,D., Pearson,M.,
            Roberts,A., Saif,S., Shea,T., Shenoy,N., Sisk,P., Stolte,C.,
            Sykes,S., Thomson,T., Walk,T., White,J., Yandava,C., Seifert,H.,
            Haas,B., Nusbaum,C. and Birren,B.
  CONSRTM   The Broad Institute Genome Sequencing Platform
  TITLE     Direct Submission
  JOURNAL   Submitted (06-JUL-2009) Broad Institute of MIT and Harvard, 7
            Cambridge Center, Cambridge, MA 02142, USA
COMMENT     PROVISIONAL REFSEQ: This record has not yet been subject to final
            NCBI review. The reference sequence was derived from GG705083.
            This is a reference genome for the Human Microbiome Project. This
            project is co-owned with the Human Microbiome Project DACC.
            Source DNA provided by Harald Seifert (Professor of Medical
            Microbiology and Hygiene, Institute for Medical Microbiology,
            Immunology and Hygiene, University of Cologne, Goldenfelsstrasse
            19-21, 50935 Koln, Germany).
            Bacteria provided by Harald Seifert (Professor of Medical
            Microbiology and Hygiene, Institute for Medical Microbiology,
            Immunology and Hygiene, University of Cologne, Goldenfelsstrasse
            19-21, 50935 Koln, Germany).
            Genome Coverage: 22.06x
            Sequencing Technology: 454.
FEATURES             Location/Qualifiers
     source          1..5476
                     /organism="Acinetobacter lwoffii SH145"
                     /mol_type="genomic DNA"
                     /strain="SH145"
                     /db_xref="taxon:575588"
     gene            complement(215..1156)
                     /locus_tag="HMPREF0017_03060"
     CDS             complement(215..1156)
                     /locus_tag="HMPREF0017_03060"
                     /codon_start=1
                     /transl_table=11
                     /product="DNA replication protein"
                     /protein_id="ZP_06071139.1"
                     /db_xref="GI:262377933"
                     /translation="MKNGLVVKDNALINASYNLEVTEQRLILLAIINARETQQGITSD
                     SKLEIHASDYANQFNVKKETAYEALKNAVNNLFERQFSFRETTKKGIGIVRSRWVSRI
                     KYIDDSAILEITFAPDVVPLITRLEQHFTSYQLKQVSQLTSKYAIRLYELLIAWREVG
                     KVPKIELSEFREKLGIAADEYKAMNHFKSRVLEPSIKQINEHTDITVSYEQHKTGRTI
                     TSFSFSFKQKHQPRIEKPIADKRDPNTPDFFIKLTDSQRHLFAKKMSEMPEMSKYSQG
                     TESYQQFAIRIADMLLEPEKFREVYPVLEKAGFQGNL"
     misc_feature    complement(482..1153)
                     /locus_tag="HMPREF0017_03060"
                     /note="Initiator Replication protein; Region: Rep_3;
                     pfam01051"
                     /db_xref="CDD:144588"
     gene            complement(1765..1932)
                     /locus_tag="HMPREF0017_03061"
     CDS             complement(1765..1932)
                     /locus_tag="HMPREF0017_03061"
                     /codon_start=1
                     /transl_table=11
                     /product="predicted protein"
                     /protein_id="ZP_06071140.1"
                     /db_xref="GI:262377934"
                     /translation="MSIPEIILLIESIPVYGWAVVLILAVAVCVLAVNTKMNIMINIE
                     KSDTEIPARTK"
     gene            complement(2065..2652)
                     /locus_tag="HMPREF0017_03062"
     CDS             complement(2065..2652)
                     /locus_tag="HMPREF0017_03062"
                     /codon_start=1
                     /transl_table=11
                     /product="predicted protein"
                     /protein_id="ZP_06071141.1"
                     /db_xref="GI:262377935"
                     /translation="MSYELEQGNNMSERRKAARSRKIAKTYKERRKNLWPELTDSDFW
                     SKDSKGFVCVPRSMPIIMLIMDHFSPNKPISKTYFSLWCRCFDEMVVKADNKIVLATE
                     AGFSGERKVTTWKSRIEKLEELGFIKRQETFDGDLIVLINPFKVILKLFDTDHSNALA
                     VLKDALETRVLEVNSKDLANLQQDETEEDEEVIEA"
     gene            complement(3118..3588)
                     /locus_tag="HMPREF0017_03063"
     CDS             complement(3118..3588)
                     /locus_tag="HMPREF0017_03063"
                     /codon_start=1
                     /transl_table=11
                     /product="conserved hypothetical protein"
                     /protein_id="ZP_06071142.1"
                     /db_xref="GI:262377936"
                     /translation="MSNQNDLDDQLYILLASMKEYREAIADDKKRLETFYTQVASGVL
                     DKAEKSLQETNKQAIGALKSRIQELDKATSRLNYQFIAVFASAFVALVMVLFLALFLF
                     VPSMDEIQQRRSEVNNLKNYSLDLSKCDGKTCVRIIKKQCGYGKNADYCVIDPK"
     gene            complement(3585..4538)
                     /locus_tag="HMPREF0017_03064"
     CDS             complement(3585..4538)
                     /locus_tag="HMPREF0017_03064"
                     /codon_start=1
                     /transl_table=11
                     /product="conserved hypothetical protein"
                     /protein_id="ZP_06071143.1"
                     /db_xref="GI:262377937"
                     /translation="MAIYHCTTKTVNRSSGRTAVASMAYRAGEKLTDERTGLTHDFTR
                     KEGVAYTEIISNLDTQIDRAELWNLAEKTENRKDARTAREWVIALPDELDEEQRKELA
                     KDFAKSLVDRYGVVADLAIHAPSKGGDDKNHHAHILLTTRKAELDPENKLVLTQKAEI
                     ELSNTKRKSLGMGTSQEEIKQIRATWANLANYALDKAGYEEKIDHRSYAEQGNGLQAT
                     IHEGSKVTQLRRKGIDTEVSRFNDNIKQQNSQQLQYKEPKKEKILEQGFSRVEKGFEQ
                     WKKDQEAKRLQLEHQQQLKLQQERAMKLKQRKSMNRDGPSL"
     misc_feature    complement(3846..4490)
                     /locus_tag="HMPREF0017_03064"
                     /note="MobA/MobL family; Region: MobA_MobL; pfam03389"
                     /db_xref="CDD:112214"
     gene            4752..5021
                     /locus_tag="HMPREF0017_03065"
     CDS             4752..5021
                     /locus_tag="HMPREF0017_03065"
                     /codon_start=1
                     /transl_table=11
                     /product="conserved hypothetical protein"
                     /protein_id="ZP_06071144.1"
                     /db_xref="GI:262377938"
                     /translation="MTKAAENLEKKIEAQLEKLKQLKARKQAIEAREKSKQKEQERKD
                     DTRRKILLGSYLIKKMNDNEANKEKILAELNEYLTEERDRTLFNL"
     gene            complement(5031..>5471)
                     /locus_tag="HMPREF0017_03066"
     CDS             complement(5031..>5471)
                     /locus_tag="HMPREF0017_03066"
                     /exception="unextendable partial coding region"
                     /note="5' partial"
                     /codon_start=1
                     /transl_table=11
                     /product="predicted protein"
                     /protein_id="ZP_06071145.1"
                     /db_xref="GI:262377939"
                     /translation="KPNIESYPELKKYYLDLFDKGLIFQNTEGNHENGYKPLGIRIVD
                     ARSIGFECSSKLLNLTEDELALGIRNLDNETNSEGYDSLHVIMDEINKIPPIAIVFNE
                     YKADKKITDRELCTLANMYEKGLEYQHNLTLRDLNNQTKSEVLQ"
ORIGIN      
        1 ccctttcaag ggcgttaaaa acttccattt ctttgtcgtc ttttttatca agataaaaag
       61 ctctacccac aagaattagc gtcaaacaaa taagaattaa aagtatccaa agagctattt
      121 ttttcatttt ataaattact gataaatatc aataaaatta tagtgcattt catagaaata
      181 atcagtactg tgattaatcc tataagttat tttttcataa attcccttgg aaccctgctt
      241 tttctaaaac aggataaact tctctaaatt tttcaggttc taaaagcata tccgctatac
      301 gaatagcaaa ttgttgatag ctttctgtac cttgagaata tttactcatt tctggcatct
      361 cggacatttt tttggcaaac aaatgacgtt gagaatcagt caatttgata aaaaagtcag
      421 gggtgtttgg atctctttta tcagctatcg gcttttcaat tcttggttgg tgcttctgtt
      481 taaagctgaa tgaaaagcta gtaatagtac gtccagtttt atgttgctca taacttactg
      541 taatgtcggt atgctcatta atttgtttaa tagatggttc taatacacga cttttgaagt
      601 gattcattgc tttgtattca tcagctgcaa tacctagttt ttccctgaat tctgagagtt
      661 ctatttttgg tactttgcca acttcacgcc acgcaataag aagctcatat aagcgtatag
      721 cgtacttact ggtaagctga gaaacctgct ttaactgata gctagtaaag tgttgctcaa
      781 gccttgtaat taatggcact acatcaggtg caaaagtaat ttctaatatg gctgaatcat
      841 caatatattt gattcgactt acccagcgag atcgaacaat accaattcct ttttttgttg
      901 tttctctaaa agaaaactgg cgttcaaata aattatttac agcatttttt aatgcttcat
      961 aagctgtttc tttcttcaca ttaaattggt tggcatagtc actagcatgt atctctagtt
     1021 tgctatctga cgtaattcct tgctgggttt cacgtgcatt gataattgct aaaagtatta
     1081 aacgctgctc tgttacttct aaattataac tagcgtttat caaggcatta tctttcacta
     1141 ctaatccatt cttcatattc aacacgttat ataattatgt gcataaagac acattaaatc
     1201 attatgtgtc tttatgcaag gaatctgtgt cgttatagca aggaatctgt gtcgttatag
     1261 caaggaatct gtgtcgttat agcaaggaat ctgtgtcgtt attgcctttt aaacataata
     1321 atatcaatga tttaagaagg tataaaagca tttaaaaact ttaaaatata tataaaagcc
     1381 taagcaggca taaaaacagc cctatgccct cgctaaagct cggacgtttt aacctcgctt
     1441 tgctcggttg aggggctgtt ttttcaaaga ggaattgagg atttaataaa agaaactatg
     1501 ccttcgcttc gctcagaagt tttatgcgag cttcgctcga tcttaaatca actgaagtac
     1561 agtcgctttg ctctgttaat ccctttatta agcaaaagga attaattgta atcagagagg
     1621 aaaatatagg ttttgatgct atgtgatcgc tcgtagacac tcgctcggct cttattggtc
     1681 acattaattt aatttatgat aagatttata tcgtaaacaa aaagcgagtg gttcagactc
     1741 gcttcttgtg tttaccttct acggttactt agttctggca ggaatttcag tatccgattt
     1801 ttcaatattg atcatgatgt tcatcttagt gttcacagct aagacgcata ctgctacggc
     1861 tagaatcagt actactgccc agccgtagac tggaatgctt tcgattaata aaataatttc
     1921 tggaatactc ataaattaaa tctctcttgt taaggttgcg gtttcgagct ttaacgccca
     1981 ttgtttgtaa attagtcatg cgaatctccg agactaacta gttagtaaat tcaattagtt
     2041 acgctattaa tctgttttaa attttcatgc ttcaattacc tcctcatctt cttcagtttc
     2101 atcttgttgc aagttagcca agtcctttga attgacttct aaaacgcgtg tttccagtgc
     2161 atctttcaac actgctaaag catttgaatg atcagtgtca aaaagtttta aaatcacttt
     2221 gaatggattg attaaaacaa tcagatcgcc atcaaaggtt tcttgccgtt tgataaagcc
     2281 aagttcttca agtttttcta tacgtgattt ccacgtggtc actttacgct caccagaaaa
     2341 tccggcttct gtagctagaa caattttgtt atctgcctta accaccattt catcgaagca
     2401 gcggcaccaa agtgaaaagt acgtcttaga aataggtttg ttcggtgaaa agtgatccat
     2461 gattaacatg atgatcggca tacttcttgg tacacacaca aaccccttac tgtcttttga
     2521 ccaaaaatct gagtcagtca actccggcca aagattttta cgacgctctt tataagtctt
     2581 cgctatcttt cttgatctgg ctgcttttct acgttctgac atgttgtttc cttgttctag
     2641 ttcataagac atatattatt taaaaaaatc ttacgattca aatatttagt ggtgtattaa
     2701 aaattagtga atatttaata aaaatacgtt ttaaaaatta gattgttata tttattctgt
     2761 tagatttctt gagttgtaat tttttttacg aaaatatttt tcaagtgatt gaatataacg
     2821 gagaaatatg aaaattgtgg tttcaggggt atctctggta tccctgagtt atcactgtat
     2881 aatttaatat cttggggggt gattattaag aggggggtat atttttaaaa aagcttaaaa
     2941 aaatataaaa ataattaggc agaaaagtat gaaaattggc tgttatgagt gtaaatctgg
     3001 ggggattagc tcgatgcttt gaaatatggc tttagccata atttaaagtc gagcaacttt
     3061 gcgactgtca ttgttggcag aatattgaaa gaagcagagc aaagtaccaa gcggccatta
     3121 ttttgggtca atcacacaat aatcagcatt tttcccatat ccacactgtt tttttataat
     3181 tctaacgcat gtttttccat cgcattttga taggtctaag ctgtaatttt tcaggttatt
     3241 cacttccgat ctgcgctgct ggatttcatc catacttggc acaaataaaa ataatgccaa
     3301 gaacagcacc attaccaacg ctacaaaggc actggcaaac acggcaataa attgatagtt
     3361 aagacggctg gttgctttat ctaactcctg aatacggctc tttaatgccc ctatagcctg
     3421 cttatttgtc tcctgtagcg acttttcggc tttatccaga accccactag ctacttgggt
     3481 gtaaaacgtc tctaatcgct tcttatcgtc tgctatggct tctctgtatt ccttcatgga
     3541 agccaataaa atatacaact ggtcgtctaa gtcgttttga ttgctcataa ggaaggccca
     3601 tctctattca tgcttttgcg ttgtttcagc ttcatggctc tttcttgctg tagctttaac
     3661 tgctgctgat gttccagttg taagcgttta gcttcctgat ctttcttcca ttgctcgaaa
     3721 cccttttcga cacggctgaa gccttgttct aggatttttt cctttttggg ttctttgtat
     3781 tggagctgct gactgttttg ctgtttgatg ttgtcattga aacggctaac ttcagtatct
     3841 atgcccttcc tgcgtaactg agtaaccttg ctgccttcat gtatcgtggc ttgtaggccg
     3901 ttaccttgtt cggcatagct acgatggtct attttttctt cgtaccctgc tttgtccagg
     3961 gcataatttg caaggtttgc ccaggtggct cgaatctgct tgatttcttc ctggcttgtt
     4021 cccatgccta ggctttttct tttggtgtta cttagctcga tttccgcttt ttgagtaagg
     4081 accaatttat tttcagggtc taattctgct ttgcgagttg tcagcaagat atgtgcatga
     4141 tggtttttat catctccacc tttgctgggt gcatgaatcg ctaaatctgc aactacccca
     4201 tagcgatcta ctaaggactt ggcaaagtct ttagcaagtt ccttgcgttg ttcttcatcc
     4261 aattcatcag gcaaggcaat aacccattct ctagcagttc tagcatcttt tctgttttcg
     4321 gttttttccg ccagattcca gagttcggca cgatcaattt gagtatctaa gttcgagata
     4381 atttcggtat aagccacacc ttcttttctc gtaaagtcgt gcgttaatcc tgtccgttca
     4441 tctgtcaatt tttctcctgc acgataagcc atagatgcta ccgcagttcg tcccgaactg
     4501 cgattaacgg tcttagtcgt acagtgataa attgccatgc cttagccttg tgatggtttt
     4561 gcttttgctt ttaaaaaatt gcgtagcagt tttttggggt taaggggatt ccccttacgc
     4621 aacgatgact tgaactttgt tcaagtcgta agtgcgcatt tcatgaaaaa cacgatagca
     4681 taaagcccgc ttgaattcca attcataggg ccttatgctt gagtggtttt ctatatcccg
     4741 aaatctgaat catgacaaaa gctgctgaaa atctcgaaaa gaaaatcgaa gcccagttag
     4801 aaaagctcaa acagttaaag gctcgtaaac aagctatcga agctagagaa aaatcaaagc
     4861 agaaagagca agaaagaaag gacgatactc ggcgtaaaat tttgctcggt tcttatttga
     4921 ttaaaaaaat gaatgataat gaagccaaca aagaaaaaat actcgctgaa ctcaacgagt
     4981 atttaacaga ggaaagagat agaacgttat ttaatctata agttttagac ttattgaagt
     5041 acttccgatt tagtctgatt attaagatca cgtagagtta aattatgttg atactccaga
     5101 cccttctcat acatatttgc aagagtacat agctctctat ctgtaatttt tttgtcagct
     5161 ttatattcgt taaaaacaat agcaataggt ggaattttat taatctcatc cataatgaca
     5221 tgaagtgaat cataaccctc agaattggtt tcattatcaa ggtttcttat acctaaggct
     5281 aactcatctt cagttaagtt taggagtttc gaggagcact caaaacctat agaccgagca
     5341 tctacaattc taattcctag aggcttataa ccattctcat gattaccttc tgtattttga
     5401 aaaataagac ccttgtcgaa taaatcaaga taatattttt taagttctgg atatgattca
     5461 atattcggtt taagtt
//
