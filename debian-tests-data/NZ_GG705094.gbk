LOCUS       NZ_GG705094             1153 bp    DNA     linear   CON 09-JUN-2010
DEFINITION  Acinetobacter lwoffii SH145 genomic scaffold supercont1.40, whole
            genome shotgun sequence.
ACCESSION   NZ_GG705094 NZ_ACPN01000000
VERSION     NZ_GG705094.1  GI:262377981
DBLINK      Project: 41587
            BioProject: PRJNA41587
KEYWORDS    WGS.
SOURCE      Acinetobacter lwoffii SH145
  ORGANISM  Acinetobacter lwoffii SH145
            Bacteria; Proteobacteria; Gammaproteobacteria; Pseudomonadales;
            Moraxellaceae; Acinetobacter.
REFERENCE   1  (bases 1 to 1153)
  AUTHORS   Ward,D., Feldgarden,M., Earl,A., Young,S.K., Zeng,Q., Koehrsen,M.,
            Alvarado,L., Berlin,A., Bochicchio,J., Borenstein,D., Chapman,S.,
            Chen,Z., Engels,R., Freedman,E., Gellesch,M., Goldberg,J.,
            Griggs,A., Gujja,S., Heilman,E., Heiman,D., Hepburn,T., Howarth,C.,
            Jen,D., Larson,L., Lewis,B., Mehta,T., Park,D., Pearson,M.,
            Roberts,A., Saif,S., Shea,T., Shenoy,N., Sisk,P., Stolte,C.,
            Sykes,S., Thomson,T., Walk,T., White,J., Yandava,C., Seifert,H.,
            Haas,B., Nusbaum,C. and Birren,B.
  CONSRTM   The Broad Institute Genome Sequencing Platform
  TITLE     The Genome Sequence of Acinetobacter lwoffii strain SH145
  JOURNAL   Unpublished
REFERENCE   2  (bases 1 to 1153)
  AUTHORS   Ward,D., Feldgarden,M., Earl,A., Young,S.K., Zeng,Q., Koehrsen,M.,
            Alvarado,L., Berlin,A., Bochicchio,J., Borenstein,D., Chapman,S.,
            Chen,Z., Engels,R., Freedman,E., Gellesch,M., Goldberg,J.,
            Griggs,A., Gujja,S., Heilman,E., Heiman,D., Hepburn,T., Howarth,C.,
            Jen,D., Larson,L., Lewis,B., Mehta,T., Park,D., Pearson,M.,
            Roberts,A., Saif,S., Shea,T., Shenoy,N., Sisk,P., Stolte,C.,
            Sykes,S., Thomson,T., Walk,T., White,J., Yandava,C., Seifert,H.,
            Haas,B., Nusbaum,C. and Birren,B.
  CONSRTM   The Broad Institute Genome Sequencing Platform
  TITLE     Direct Submission
  JOURNAL   Submitted (06-JUL-2009) Broad Institute of MIT and Harvard, 7
            Cambridge Center, Cambridge, MA 02142, USA
COMMENT     PROVISIONAL REFSEQ: This record has not yet been subject to final
            NCBI review. The reference sequence was derived from GG705094.
            This is a reference genome for the Human Microbiome Project. This
            project is co-owned with the Human Microbiome Project DACC.
            Source DNA provided by Harald Seifert (Professor of Medical
            Microbiology and Hygiene, Institute for Medical Microbiology,
            Immunology and Hygiene, University of Cologne, Goldenfelsstrasse
            19-21, 50935 Koln, Germany).
            Bacteria provided by Harald Seifert (Professor of Medical
            Microbiology and Hygiene, Institute for Medical Microbiology,
            Immunology and Hygiene, University of Cologne, Goldenfelsstrasse
            19-21, 50935 Koln, Germany).
            Genome Coverage: 22.06x
            Sequencing Technology: 454.
FEATURES             Location/Qualifiers
     source          1..1153
                     /organism="Acinetobacter lwoffii SH145"
                     /mol_type="genomic DNA"
                     /strain="SH145"
                     /db_xref="taxon:575588"
     gene            complement(<1..1103)
                     /locus_tag="HMPREF0017_03098"
     CDS             complement(<1..1103)
                     /locus_tag="HMPREF0017_03098"
                     /note="3' partial"
                     /codon_start=1
                     /transl_table=11
                     /product="transposase (IS4 family)"
                     /protein_id="ZP_06071177.1"
                     /db_xref="GI:262377982"
                     /translation="MYANTLIFFMTLSENLDCTLQHSLPSLSHFSELIDLNWIKESLH
                     QTGKASIRRRKLPAEHVVWLVIGLALFRDQPIGYVVEQLKLVFGTTEYCVPSAAVQAR
                     QRLGREPLNTLFSLLSQAWFEESQQQYSNFHGLSVCAVDGVVWSMPYTDENFEHFGSS
                     KGKTAAAPYPQVRATCLVNTSTHEIIDAQIGSMDQGELTLANRLCPPSHSITLFDRAY
                     FSADFLIDWQTRVEASHWLMRAKDNLRYEIIKRNSPHDFQIKMPLSARARKLNPSLGE
                     YWEARLIEVEQAGKIRRYITSLMDSKAYPLIGLAKLYAQRWEIEMCYREIKSDLQEGK
                     HLRSKQPDLIYQELWGVFIAYNILRRQMKHMAQR"
     misc_feature    complement(756..1040)
                     /locus_tag="HMPREF0017_03098"
                     /note="Insertion element 4 transposase N-terminal; Region:
                     Nterm_IS4; pfam13006"
                     /db_xref="CDD:205187"
     misc_feature    complement(33..707)
                     /locus_tag="HMPREF0017_03098"
                     /note="Transposase DDE domain; Region: DDE_Tnp_1;
                     pfam01609"
                     /db_xref="CDD:201886"
ORIGIN      
        1 cgttgagcca tatgtttcat ttgtcttctt agaatattat aggcaatgaa gaccccccat
       61 aattcttgat aaattaaatc aggttgcttg ctcctcaaat gcttgccttc ctgtaaatca
      121 cttttgattt ctcggtaaca catttctatt tcccagcgct gggcataaag cttcgccaag
      181 cctatcagtg gatatgcctt tgaatccatt aatgaagtga tataacgtct aattttccct
      241 gcctgctcaa cttcaatcaa acgcgcttcc caatattctc ctaatgatgg atttagcttc
      301 ctggctctag ctgaaagagg cattttgatt tgaaagtcat gcggggaatt acgtttaatg
      361 atctcatagc gtaaattatc ttttgctctc attaaccaat gactggcttc tactcgtgtt
      421 tgccagtcaa tcaagaaatc agcagagaaa taggctcgat caaacaaggt aatactatgc
      481 gatggaggac ataatcgatt tgccagtgta agttcaccct gatccatgct acctatttgg
      541 gcatcaatga tttcatgggt gctggtattt accaggcagg ttgctctcac ttgtggataa
      601 ggggctgcag cggttttacc cttagatgag ccaaagtgtt caaaattctc gtctgtataa
      661 ggcatagacc aaaccacccc gtcaacagca catacactaa ggccatgaaa gtttgagtat
      721 tgctgttggg attcttcaaa ccaggcttgg ctgagtagag aaaacaaggt attcaagggc
      781 tctcgtccta aacgttgtcg tgcttgcact gctgcactgg ggacacaata ttctgttgta
      841 ccaaacacaa gttttagttg ctctacgaca tacccgatcg gttgatctcg aaatagggca
      901 agcccaatta ccagccatac cacatgttcg gcaggtaatt ttcttctcct gatagaagcc
      961 ttacctgttt gatgtaggct ttctttaatc cagtttaaat caatgagttc actgaaatgg
     1021 ctgagtgaag gcaaagaatg ttgaagggtg caatctaaat tttcagataa agtcataaaa
     1081 aaaatgagcg tattggcata cactcatttt tactacattt ttctaatgtc tgcttaactg
     1141 actggcatta ctc
//
