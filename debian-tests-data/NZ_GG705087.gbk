LOCUS       NZ_GG705087             4863 bp    DNA     linear   CON 09-JUN-2010
DEFINITION  Acinetobacter lwoffii SH145 genomic scaffold supercont1.33, whole
            genome shotgun sequence.
ACCESSION   NZ_GG705087 NZ_ACPN01000000
VERSION     NZ_GG705087.1  GI:262377955
DBLINK      Project: 41587
            BioProject: PRJNA41587
KEYWORDS    WGS.
SOURCE      Acinetobacter lwoffii SH145
  ORGANISM  Acinetobacter lwoffii SH145
            Bacteria; Proteobacteria; Gammaproteobacteria; Pseudomonadales;
            Moraxellaceae; Acinetobacter.
REFERENCE   1  (bases 1 to 4863)
  AUTHORS   Ward,D., Feldgarden,M., Earl,A., Young,S.K., Zeng,Q., Koehrsen,M.,
            Alvarado,L., Berlin,A., Bochicchio,J., Borenstein,D., Chapman,S.,
            Chen,Z., Engels,R., Freedman,E., Gellesch,M., Goldberg,J.,
            Griggs,A., Gujja,S., Heilman,E., Heiman,D., Hepburn,T., Howarth,C.,
            Jen,D., Larson,L., Lewis,B., Mehta,T., Park,D., Pearson,M.,
            Roberts,A., Saif,S., Shea,T., Shenoy,N., Sisk,P., Stolte,C.,
            Sykes,S., Thomson,T., Walk,T., White,J., Yandava,C., Seifert,H.,
            Haas,B., Nusbaum,C. and Birren,B.
  CONSRTM   The Broad Institute Genome Sequencing Platform
  TITLE     The Genome Sequence of Acinetobacter lwoffii strain SH145
  JOURNAL   Unpublished
REFERENCE   2  (bases 1 to 4863)
  AUTHORS   Ward,D., Feldgarden,M., Earl,A., Young,S.K., Zeng,Q., Koehrsen,M.,
            Alvarado,L., Berlin,A., Bochicchio,J., Borenstein,D., Chapman,S.,
            Chen,Z., Engels,R., Freedman,E., Gellesch,M., Goldberg,J.,
            Griggs,A., Gujja,S., Heilman,E., Heiman,D., Hepburn,T., Howarth,C.,
            Jen,D., Larson,L., Lewis,B., Mehta,T., Park,D., Pearson,M.,
            Roberts,A., Saif,S., Shea,T., Shenoy,N., Sisk,P., Stolte,C.,
            Sykes,S., Thomson,T., Walk,T., White,J., Yandava,C., Seifert,H.,
            Haas,B., Nusbaum,C. and Birren,B.
  CONSRTM   The Broad Institute Genome Sequencing Platform
  TITLE     Direct Submission
  JOURNAL   Submitted (06-JUL-2009) Broad Institute of MIT and Harvard, 7
            Cambridge Center, Cambridge, MA 02142, USA
COMMENT     PROVISIONAL REFSEQ: This record has not yet been subject to final
            NCBI review. The reference sequence was derived from GG705087.
            This is a reference genome for the Human Microbiome Project. This
            project is co-owned with the Human Microbiome Project DACC.
            Source DNA provided by Harald Seifert (Professor of Medical
            Microbiology and Hygiene, Institute for Medical Microbiology,
            Immunology and Hygiene, University of Cologne, Goldenfelsstrasse
            19-21, 50935 Koln, Germany).
            Bacteria provided by Harald Seifert (Professor of Medical
            Microbiology and Hygiene, Institute for Medical Microbiology,
            Immunology and Hygiene, University of Cologne, Goldenfelsstrasse
            19-21, 50935 Koln, Germany).
            Genome Coverage: 22.06x
            Sequencing Technology: 454.
FEATURES             Location/Qualifiers
     source          1..4863
                     /organism="Acinetobacter lwoffii SH145"
                     /mol_type="genomic DNA"
                     /strain="SH145"
                     /db_xref="taxon:575588"
     gene            15..206
                     /locus_tag="HMPREF0017_03079"
     CDS             15..206
                     /locus_tag="HMPREF0017_03079"
                     /codon_start=1
                     /transl_table=11
                     /product="predicted protein"
                     /protein_id="ZP_06071158.1"
                     /db_xref="GI:262377956"
                     /translation="MKKISTKARSNSMQLKQVLGKIKDITFGVIAAPLIIIIMMWASI
                     LDQRAFKKERNRKKEQDKT"
     gene            206..1135
                     /locus_tag="HMPREF0017_03080"
     CDS             206..1135
                     /locus_tag="HMPREF0017_03080"
                     /codon_start=1
                     /transl_table=11
                     /product="MobA"
                     /protein_id="ZP_06071159.1"
                     /db_xref="GI:262377957"
                     /translation="MAIARLSVKVGKAGKAAPHAEYIDRDEEKKLKQEQAETDLEHSA
                     YGNMPKWAEHNPITFWQAADLYERKNGSTYREYEIALPREMNAEQRLELVEDFIQSEI
                     GSKYPYQFAIHNPKAMDGKDQPHVHLMFNERLQDGIERDPEQYFKRYNAKNPERGGAK
                     KDNTGKGYQERKTDIKDLRQRWADLCNSHLEKHQIDSRIDMRSYKEQGIEKEPEKKLL
                     PSQAKDPEIREALQQSRIAYKELEQLDLGDPKKDLKDLKNSPISDKEIKQGIESFKAD
                     FDSFKQLALEQYKQQQKLEREQQKTMKFRGMSR"
     misc_feature    341..877
                     /locus_tag="HMPREF0017_03080"
                     /note="MobA/MobL family; Region: MobA_MobL; pfam03389"
                     /db_xref="CDD:112214"
     gene            1132..1632
                     /locus_tag="HMPREF0017_03081"
     CDS             1132..1632
                     /locus_tag="HMPREF0017_03081"
                     /codon_start=1
                     /transl_table=11
                     /product="predicted protein"
                     /protein_id="ZP_06071160.1"
                     /db_xref="GI:262377958"
                     /translation="MTPDEKKLYALMAVAEQQQNLVNQAVKELQITKEHIQSTIAGQT
                     HHTISKAVNEGLQDGTAELIKTAKALGRLKNSIESANDQFSWKLITTFLGVFSVLILT
                     MTWLFASYIKPLENIPKIESLRQQGIQFDIQSCAVGEESKPCVRVMKEQCGYGKHGDL
                     CVIDPK"
     gene            1714..2529
                     /locus_tag="HMPREF0017_03082"
     CDS             1714..2529
                     /locus_tag="HMPREF0017_03082"
                     /codon_start=1
                     /transl_table=11
                     /product="conserved hypothetical protein"
                     /protein_id="ZP_06071161.1"
                     /db_xref="GI:262377959"
                     /translation="MLKQSSLERTEFKEEELAPIKKLDLNDLKVPMPKNLDQLLIWTS
                     ELAQYNYAQFLDQSEQLKLVDEDNLEEQAEISQKLRFYRGNALIENQIKDFYLKKLLD
                     QGEATVKGYIKSPTKKNTRYAVISYHDYDFVSFATPEILNKFPADLINFVQKETPKTL
                     ADIYPEEAVFLSAIKKFIKPFKEQYKKLADKNREIKEHNTVAHKIYIKIKNKEATLQK
                     PTESKPKESSPKTTENSAVPVEMKQGTIQKNMAPQEKPLTVIKKRKFALNKDK"
     gene            3431..3604
                     /locus_tag="HMPREF0017_03083"
     CDS             3431..3604
                     /locus_tag="HMPREF0017_03083"
                     /codon_start=1
                     /transl_table=11
                     /product="predicted protein"
                     /protein_id="ZP_06071162.1"
                     /db_xref="GI:262377960"
                     /translation="MFTGFKSYPNSVYPKILYDNKYDKAREYMQELKKREDNLLESLP
                     STYEYLKHLHSRK"
     misc_feature    <3452..>3589
                     /locus_tag="HMPREF0017_03083"
                     /note="RhoGAP: GTPase-activator protein (GAP) for Rho-like
                     GTPases; GAPs towards Rho/Rac/Cdc42-like small GTPases.
                     Small GTPases (G proteins) cluster into distinct families,
                     and all act as molecular switches, active in their
                     GTP-bound form but inactive when...; Region: RhoGAP;
                     cl02570"
                     /db_xref="CDD:243095"
     gene            3606..4214
                     /locus_tag="HMPREF0017_03084"
     CDS             3606..4214
                     /locus_tag="HMPREF0017_03084"
                     /codon_start=1
                     /transl_table=11
                     /product="predicted protein"
                     /protein_id="ZP_06071163.1"
                     /db_xref="GI:262377961"
                     /translation="MELYTIILYSFFCFIGAILPGPTSLLALNLGLNNSIKAVFLASI
                     GAAIADFLIICAIGFGLKQIIDDFPIIFEVIKTLGFFYLIFIAYMIWKSENSISEHKD
                     DTHNIYRLPIKGFLTAISNPKVLVFFIAFLPQFIDPNLNISYQYIVLGIASSMIDIIC
                     MTIYGLLGVKLLYFFKDKTNLLYLNRISAICMASIAMFLIIR"
     misc_feature    3606..4211
                     /locus_tag="HMPREF0017_03084"
                     /note="Putative threonine efflux protein [Amino acid
                     transport and metabolism]; Region: RhtB; COG1280"
                     /db_xref="CDD:31471"
     gene            complement(4215..4634)
                     /locus_tag="HMPREF0017_03085"
     CDS             complement(4215..4634)
                     /locus_tag="HMPREF0017_03085"
                     /codon_start=1
                     /transl_table=11
                     /product="predicted protein"
                     /protein_id="ZP_06071164.1"
                     /db_xref="GI:262377962"
                     /translation="MNNEWLFEKITYISALKNPNDAQKLLLELAKIQYRTPDQEKKIN
                     ALIKAEKAIDRANKQKVAVRKLLNAEKEAERKARTRHLIQLGALFEIANLDQRDPAEL
                     LGVLLKTAEIDPNDMKWEIWKDLGQAVLNHNKKKNRV"
     misc_feature    complement(4245..4427)
                     /locus_tag="HMPREF0017_03085"
                     /note="Conjugal transfer protein TraD; Region: TraD;
                     pfam06412"
                     /db_xref="CDD:148174"
ORIGIN      
        1 caattgaaaa ttgaatgaaa aaaataagta ccaaagctag gagcaacagc atgcaactaa
       61 aacaggtatt gggaaaaatc aaagatatta cttttggcgt gatcgcagca ccgctaatca
      121 tcatcattat gatgtgggct tcgatacttg atcaaagagc gtttaagaaa gagcgcaacc
      181 gaaaaaagga acaggataaa acctaatggc gattgctcgt ttaagtgtca aagtcggtaa
      241 ggcaggtaaa gcagcaccgc atgccgaata tatagatcgg gatgaagaaa agaagctgaa
      301 acaggaacag gcagaaactg atcttgaaca tagtgcttat ggaaacatgc cgaaatgggc
      361 tgaacataat ccgattacct tttggcaggc agccgacctt tacgagcgta aaaatggcag
      421 tacctatcga gaatatgaaa ttgccctgcc tagagagatg aatgccgagc aacgcctaga
      481 actggttgag gacttcattc agtctgagat cggctcaaaa tatccgtatc aatttgcgat
      541 tcataaccct aaagcaatgg acggcaagga tcagcctcac gttcacctca tgtttaatga
      601 acgtctgcaa gatggaatcg aacgagatcc agagcaatat ttcaaacgct ataacgccaa
      661 gaatcctgaa cgtggcggag ctaaaaaaga caatacgggc aagggttacc aggaacgaaa
      721 aactgatatt aaagacctac gccaaagatg ggctgattta tgcaacagcc atttagaaaa
      781 acaccaaatt gacagtcgta ttgatatgcg aagctacaaa gagcaaggca tagagaaaga
      841 acccgaaaag aaactattac caagccaagc taaagaccct gaaatcaggg aagccttgca
      901 acagtcacgt atagcctata aagagcttga gcaactagat ttaggcgacc ctaaaaaaga
      961 cctgaaagac ctcaaaaaca gcccaatttc agataaagaa attaaacagg gtattgagag
     1021 ctttaaggct gattttgaca gctttaaaca gcttgctttg gaacagtaca aacagcagca
     1081 gaaattagaa agagagcaac agaaaacaat gaagtttaga gggatgagcc gatgacacct
     1141 gatgaaaaaa agctatatgc actgatggca gttgcagaac aacagcagaa cctagtcaat
     1201 caggccgtta aagagcttca aatcacgaaa gaacacattc aaagcactat tgctgggcaa
     1261 acccaccaca ctatatcaaa agcggtaaat gagggcttac aggacggtac agcagaactt
     1321 attaagaccg ctaaagcgct tggacgcttg aaaaactcga ttgaatcagc caatgatcaa
     1381 ttttcatgga agttaatcac cactttctta ggtgtttttt cagttctcat actgactatg
     1441 acgtggctat ttgcatctta tattaagcct ttagagaata ttccaaagat cgagagttta
     1501 aggcagcaag gtattcagtt tgatattcaa agttgtgccg ttggagaaga gtccaagccc
     1561 tgcgtccgag tgatgaaaga gcagtgtggc tatgggaagc atggggattt atgcgttatt
     1621 gaccccaaat agctgaatct gaaaatgaag gctctaagca ctcatttata atttttatta
     1681 tgtttaaaat tagtggaagt taaggaaatt attatgttaa agcaatcaag tttagaaaga
     1741 acagagttta aggaagaaga acttgcgccc attaaaaagc ttgatttgaa tgatttgaaa
     1801 gttcctatgc ctaaaaacct tgatcagtta ctgatttgga catccgaact tgctcaatat
     1861 aattatgctc aattccttga ccaatccgaa cagctaaaac ttgttgatga agacaacctg
     1921 gaagaacagg cggaaatcag tcagaagctt aggttctata gaggtaatgc gcttatagaa
     1981 aaccaaatca aagatttcta tttgaaaaag ttacttgatc aaggggaggc aacagtcaaa
     2041 ggttatatta aaagtccgac taagaaaaat acccgatatg ctgtaatttc ttatcatgac
     2101 tatgattttg taagctttgc tactccagag attcttaata aatttccagc cgatctcatc
     2161 aatttcgtgc aaaaggaaac acctaagaca ttagcggaca tatatcctga agaagccgtg
     2221 ttcttatcag ctatcaaaaa atttataaaa ccttttaaag aacagtataa aaaattggct
     2281 gataagaaca gggaaattaa ggagcacaat acagttgctc ataaaattta tataaaaatt
     2341 aaaaataagg aagcaacttt gcaaaagcca acagagtcaa agcctaaaga atctagtcca
     2401 aagactactg aaaatagtgc tgtcccagta gagatgaagc agggaaccat tcagaaaaat
     2461 atggctccac aagaaaagcc acttactgtg attaagaagc gaaaatttgc tttaaacaag
     2521 gataagtaac ttgtaatgta aaattctaaa caaaaacccc tctaagtctt gcaacttaga
     2581 ggggttttca agtaaattca gcggtacgca gttatttgat gctacaacat caagtaacga
     2641 agatcataag gtttgacgac cgttatgatt tcaccaaaag gcttgtacct gaactctatt
     2701 ttcacaataa acagtgttta tatcaagcct caaaatgata agtcttcgtt aattcgtgtt
     2761 ttaaaccatc aaataagcat cctagatcga gctttgcccg cttctgcgtg tccgttttac
     2821 ggtcggcatg cagtacgtgt ggtaggaata agtgtcactg gcgtattgta gctatagtcg
     2881 aaaggctaaa ccgcttacag gggaacttca cataggcgga gcgttaggag tagttacata
     2941 ggacttgtta agttgcatgt tggggaaacc ctcgcagaga tacaggcaat gtaacgacag
     3001 ggagcgcgtg gagcgaggtg caagactttg agtccttgtc tgcatagtca gcaaagctat
     3061 gtgcggatac gaccgatata cgtggctccg aagccaaata tatacaggta aatcggcaat
     3121 acttagggga tatgtcattt tttcaaatgg ctgcccttag ggtgagtatt gccgaaactc
     3181 tctcaaccgt ttccaaagcc aaagcacgaa gtgcgacgct ttggaaacgg ttgagaggtt
     3241 gagttcattt gtttttcttt tatatatgag ctttagaaaa aactaacttg ccttaggcta
     3301 tttttttatt ttacttttct tgtgaaaagt tttactttat tttcctccaa ttactacaaa
     3361 tgaagaagat tattatggag actttgatat agagttttct aatttctgga caaatagcaa
     3421 ttactactgc atgtttacag ggtttaaatc ctatccaaac tcagtatatc caaaaatact
     3481 ttatgataat aaatatgata aagctcgaga atatatgcaa gagttaaaga aacgtgagga
     3541 taatctcttg gaaagtttac caagtactta tgaataccta aaacatctac acagtagaaa
     3601 ataatatgga actttataca attattttgt atagtttttt ttgctttata ggggccattt
     3661 tacctggccc tacatcttta ttggccttaa atctaggtct taataattct ataaaagctg
     3721 tttttctagc tagtattgga gcagcgatag ctgattttct gattatttgt gctattggtt
     3781 ttggactaaa acaaataatt gatgattttc ctataatttt tgaggtaata aaaactctag
     3841 gatttttcta cttaattttt atcgcgtata tgatatggaa atcagaaaac agtatttctg
     3901 aacataaaga tgatactcat aatatatata gattacctat aaaaggtttt ttaacagcaa
     3961 tttctaatcc taaagtactt gtatttttta ttgcattttt accccaattt attgatccta
     4021 atttaaatat atcttatcaa tacatagttc taggtatcgc ttcttcgatg atagatataa
     4081 tttgtatgac aatatatgga ttattgggag ttaaattatt atattttttt aaggataaaa
     4141 ctaatctact ttatcttaat cgtataagcg ctatatgtat ggctagtata gctatgttcc
     4201 tgataataag gtaactagac tctatttttt ttcttattat gatttaggac agcttgtcct
     4261 aaatccttcc aaatttccca tttcatatca tttgggtcga tttcagcagt tttaagcaga
     4321 actccaagta attctgcagg atcacgctga tctaggttag caatctcaaa aagtgcgcct
     4381 aattggataa ggtggcgtgt acgagctttt cgttcagctt ccttttctgc attcaataat
     4441 tttctaactg caactttttg cttatttgct cgatctattg ctttttcagc tttgattaag
     4501 gcattaattt ttttctcttg gtcaggtgtt ctgtactgaa ttttggctaa ttcaagtaaa
     4561 agtttttgag catcattagg gttctttaaa gcagaaatat aggtgatttt ttcaaatagc
     4621 cattcattat tcatatttaa ttccttgatt cagataccgc aaaattacag gctcaatact
     4681 caaagaacga taacaaaata tttacgttcg agtaacgtaa ttttgttaaa gtcgttctag
     4741 agattgtcgc ctgcacttcc atcaaaaatt tgatggcgcg cttagacgtt tctcaatttc
     4801 attgagaaac ctaagcacgt tagggacttc atccctaaaa ccctgaaact tatttttttc
     4861 gtt
//
