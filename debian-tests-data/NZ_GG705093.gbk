LOCUS       NZ_GG705093             1180 bp    DNA     linear   CON 09-JUN-2010
DEFINITION  Acinetobacter lwoffii SH145 genomic scaffold supercont1.39, whole
            genome shotgun sequence.
ACCESSION   NZ_GG705093 NZ_ACPN01000000
VERSION     NZ_GG705093.1  GI:262377978
DBLINK      Project: 41587
            BioProject: PRJNA41587
KEYWORDS    WGS.
SOURCE      Acinetobacter lwoffii SH145
  ORGANISM  Acinetobacter lwoffii SH145
            Bacteria; Proteobacteria; Gammaproteobacteria; Pseudomonadales;
            Moraxellaceae; Acinetobacter.
REFERENCE   1  (bases 1 to 1180)
  AUTHORS   Ward,D., Feldgarden,M., Earl,A., Young,S.K., Zeng,Q., Koehrsen,M.,
            Alvarado,L., Berlin,A., Bochicchio,J., Borenstein,D., Chapman,S.,
            Chen,Z., Engels,R., Freedman,E., Gellesch,M., Goldberg,J.,
            Griggs,A., Gujja,S., Heilman,E., Heiman,D., Hepburn,T., Howarth,C.,
            Jen,D., Larson,L., Lewis,B., Mehta,T., Park,D., Pearson,M.,
            Roberts,A., Saif,S., Shea,T., Shenoy,N., Sisk,P., Stolte,C.,
            Sykes,S., Thomson,T., Walk,T., White,J., Yandava,C., Seifert,H.,
            Haas,B., Nusbaum,C. and Birren,B.
  CONSRTM   The Broad Institute Genome Sequencing Platform
  TITLE     The Genome Sequence of Acinetobacter lwoffii strain SH145
  JOURNAL   Unpublished
REFERENCE   2  (bases 1 to 1180)
  AUTHORS   Ward,D., Feldgarden,M., Earl,A., Young,S.K., Zeng,Q., Koehrsen,M.,
            Alvarado,L., Berlin,A., Bochicchio,J., Borenstein,D., Chapman,S.,
            Chen,Z., Engels,R., Freedman,E., Gellesch,M., Goldberg,J.,
            Griggs,A., Gujja,S., Heilman,E., Heiman,D., Hepburn,T., Howarth,C.,
            Jen,D., Larson,L., Lewis,B., Mehta,T., Park,D., Pearson,M.,
            Roberts,A., Saif,S., Shea,T., Shenoy,N., Sisk,P., Stolte,C.,
            Sykes,S., Thomson,T., Walk,T., White,J., Yandava,C., Seifert,H.,
            Haas,B., Nusbaum,C. and Birren,B.
  CONSRTM   The Broad Institute Genome Sequencing Platform
  TITLE     Direct Submission
  JOURNAL   Submitted (06-JUL-2009) Broad Institute of MIT and Harvard, 7
            Cambridge Center, Cambridge, MA 02142, USA
COMMENT     PROVISIONAL REFSEQ: This record has not yet been subject to final
            NCBI review. The reference sequence was derived from GG705093.
            This is a reference genome for the Human Microbiome Project. This
            project is co-owned with the Human Microbiome Project DACC.
            Source DNA provided by Harald Seifert (Professor of Medical
            Microbiology and Hygiene, Institute for Medical Microbiology,
            Immunology and Hygiene, University of Cologne, Goldenfelsstrasse
            19-21, 50935 Koln, Germany).
            Bacteria provided by Harald Seifert (Professor of Medical
            Microbiology and Hygiene, Institute for Medical Microbiology,
            Immunology and Hygiene, University of Cologne, Goldenfelsstrasse
            19-21, 50935 Koln, Germany).
            Genome Coverage: 22.06x
            Sequencing Technology: 454.
FEATURES             Location/Qualifiers
     source          1..1180
                     /organism="Acinetobacter lwoffii SH145"
                     /mol_type="genomic DNA"
                     /strain="SH145"
                     /db_xref="taxon:575588"
     gene            72..641
                     /locus_tag="HMPREF0017_03096"
     CDS             72..641
                     /locus_tag="HMPREF0017_03096"
                     /codon_start=1
                     /transl_table=11
                     /product="transposase 1"
                     /protein_id="ZP_06071175.1"
                     /db_xref="GI:262377979"
                     /translation="MTHLNELYLILNKYLKWNKSHLKCFALIMLVIILKQTCNLSSAS
                     KALPIKCLPQSFYRRMQRFFAGQYFDYRQISQLIFNMFSFDQVQLTLDRTNWKWGKRN
                     INILMLAIVYRGIAIPILWTLLNKRGNSDTKERIALIQRFIAIFGKDRIVNVFADREF
                     IGEQWFTWLIEQDINFCIRVKKTSLSPII"
     misc_feature    348..>620
                     /locus_tag="HMPREF0017_03096"
                     /note="Transposase DDE domain; Region: DDE_Tnp_1;
                     pfam01609"
                     /db_xref="CDD:144990"
     gene            716..1162
                     /locus_tag="HMPREF0017_03097"
     CDS             716..1162
                     /locus_tag="HMPREF0017_03097"
                     /codon_start=1
                     /transl_table=11
                     /product="transposase subunit"
                     /protein_id="ZP_06071176.1"
                     /db_xref="GI:262377980"
                     /translation="MVGRVKLYISALQLENGELLLVVSPQFNANAIQDYALRWEIETL
                     FSCLKGRGFNLENTRLTDPRRVKKLIAVLAISFCWCYLTGEWQHDQKKAIKIKKHGRL
                     SMSLFRYGLDYVQMAIQRLIGFGKKEEFKEILAILRRQNPDRIRVL"
     misc_feature    <818..952
                     /locus_tag="HMPREF0017_03097"
                     /note="Transposase DDE domain; Region: DDE_Tnp_1;
                     pfam01609"
                     /db_xref="CDD:201886"
ORIGIN      
        1 ctctgtacac gacaaaaata gataactcat tgaaataatg tcataataat tgttttctaa
       61 cgacgaatac tatgacacat ctcaatgagt tatatcttat cttaaacaaa tatctaaaat
      121 ggaacaagtc acatttaaag tgctttgcgc tcatcatgct tgtgattatt ttaaagcaaa
      181 catgtaatct ttcttctgca tctaaagcct tgcccatcaa gtgcttacca caatcatttt
      241 atcgacgtat gcagcgcttc tttgcaggtc agtattttga ttatcgtcaa atttctcagt
      301 tgattttcaa tatgttttca ttcgaccaag tgcaactgac tttagataga accaattgga
      361 aatggggaaa acgaaatatt aatatcctga tgctcgcaat cgtttatcgt ggaatagcga
      421 tacctatcct ttggacattg cttaataaac gtggaaattc agatacgaaa gagcgtattg
      481 ctttgattca acgctttata gccatttttg gtaaagaccg tattgtgaat gtgttcgcag
      541 acagagagtt tatcggtgag cagtggttta catggttaat tgaacaagac atcaacttct
      601 gcattcgtgt taaaaaaact tcattgtcac caatcattta ggaaagaatc ataaaattag
      661 tgatttattt cgccatctta aagttggtca aattgaatgt cgtaaacgac ggattttggt
      721 tggtcgggtg aaactatata taagtgcact acagttagaa aatggagagc ttttactcgt
      781 cgtttctcct cagtttaatg ccaatgctat tcaggattat gcattacgct gggaaattga
      841 aaccttattc agttgtctca aaggacgcgg gtttaatctt gaaaatacgc gcttgacaga
      901 ccctagacga gtgaaaaaat tgattgcggt gttagctata agcttctgtt ggtgttactt
      961 aacgggtgaa tggcaacatg atcaaaaaaa agcgataaaa ataaagaagc atggacgact
     1021 ctcaatgagt ttatttcgct atggtttaga ctatgttcaa atggcgattc agcgtttaat
     1081 tggttttggg aaaaaagaag agtttaagga aattttggca attttaagaa ggcagaaccc
     1141 tgataggata agggttctgt gaaatttgtc gtgtacagag
//
