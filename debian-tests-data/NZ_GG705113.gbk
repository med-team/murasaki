LOCUS       NZ_GG705113              776 bp    DNA     linear   CON 09-JUN-2010
DEFINITION  Acinetobacter lwoffii SH145 genomic scaffold supercont1.59, whole
            genome shotgun sequence.
ACCESSION   NZ_GG705113 NZ_ACPN01000000
VERSION     NZ_GG705113.1  GI:262378021
DBLINK      Project: 41587
            BioProject: PRJNA41587
KEYWORDS    WGS.
SOURCE      Acinetobacter lwoffii SH145
  ORGANISM  Acinetobacter lwoffii SH145
            Bacteria; Proteobacteria; Gammaproteobacteria; Pseudomonadales;
            Moraxellaceae; Acinetobacter.
REFERENCE   1  (bases 1 to 776)
  AUTHORS   Ward,D., Feldgarden,M., Earl,A., Young,S.K., Zeng,Q., Koehrsen,M.,
            Alvarado,L., Berlin,A., Bochicchio,J., Borenstein,D., Chapman,S.,
            Chen,Z., Engels,R., Freedman,E., Gellesch,M., Goldberg,J.,
            Griggs,A., Gujja,S., Heilman,E., Heiman,D., Hepburn,T., Howarth,C.,
            Jen,D., Larson,L., Lewis,B., Mehta,T., Park,D., Pearson,M.,
            Roberts,A., Saif,S., Shea,T., Shenoy,N., Sisk,P., Stolte,C.,
            Sykes,S., Thomson,T., Walk,T., White,J., Yandava,C., Seifert,H.,
            Haas,B., Nusbaum,C. and Birren,B.
  CONSRTM   The Broad Institute Genome Sequencing Platform
  TITLE     The Genome Sequence of Acinetobacter lwoffii strain SH145
  JOURNAL   Unpublished
REFERENCE   2  (bases 1 to 776)
  AUTHORS   Ward,D., Feldgarden,M., Earl,A., Young,S.K., Zeng,Q., Koehrsen,M.,
            Alvarado,L., Berlin,A., Bochicchio,J., Borenstein,D., Chapman,S.,
            Chen,Z., Engels,R., Freedman,E., Gellesch,M., Goldberg,J.,
            Griggs,A., Gujja,S., Heilman,E., Heiman,D., Hepburn,T., Howarth,C.,
            Jen,D., Larson,L., Lewis,B., Mehta,T., Park,D., Pearson,M.,
            Roberts,A., Saif,S., Shea,T., Shenoy,N., Sisk,P., Stolte,C.,
            Sykes,S., Thomson,T., Walk,T., White,J., Yandava,C., Seifert,H.,
            Haas,B., Nusbaum,C. and Birren,B.
  CONSRTM   The Broad Institute Genome Sequencing Platform
  TITLE     Direct Submission
  JOURNAL   Submitted (06-JUL-2009) Broad Institute of MIT and Harvard, 7
            Cambridge Center, Cambridge, MA 02142, USA
COMMENT     PROVISIONAL REFSEQ: This record has not yet been subject to final
            NCBI review. The reference sequence was derived from GG705113.
            This is a reference genome for the Human Microbiome Project. This
            project is co-owned with the Human Microbiome Project DACC.
            Source DNA provided by Harald Seifert (Professor of Medical
            Microbiology and Hygiene, Institute for Medical Microbiology,
            Immunology and Hygiene, University of Cologne, Goldenfelsstrasse
            19-21, 50935 Koln, Germany).
            Bacteria provided by Harald Seifert (Professor of Medical
            Microbiology and Hygiene, Institute for Medical Microbiology,
            Immunology and Hygiene, University of Cologne, Goldenfelsstrasse
            19-21, 50935 Koln, Germany).
            Genome Coverage: 22.06x
            Sequencing Technology: 454.
FEATURES             Location/Qualifiers
     source          1..776
                     /organism="Acinetobacter lwoffii SH145"
                     /mol_type="genomic DNA"
                     /strain="SH145"
                     /db_xref="taxon:575588"
     gene            complement(<1..775)
                     /locus_tag="HMPREF0017_03119"
     CDS             complement(<1..775)
                     /locus_tag="HMPREF0017_03119"
                     /note="3' partial"
                     /codon_start=1
                     /transl_table=11
                     /product="translation elongation factor Tu"
                     /protein_id="ZP_06071198.1"
                     /db_xref="GI:262378022"
                     /translation="MAKAKFERNKPHVNVGTIGHVDHGKTTLTAAIATVCAKKFGGEA
                     KDYAAIDSAPEEKARGITINTSHVEYDSPTRHYAHVDCPGHADYVKNMITGAAQMDGA
                     ILVCAATDGPMPQTREHILLSRQVGVPYIVVFLNKCDLVDDEELLELVEMEVRELLST
                     YDFPGDDTPVIRGSALLALNGDDSQYGEPAVVALVEALDSYIPEPERAIDLPFLMPIE
                     DVFSISGRGTVVTGRVETGIVKVGESVEIVGIRDTQTTTV"
     misc_feature    complement(167..748)
                     /locus_tag="HMPREF0017_03119"
                     /note="Elongation factor Tu GTP binding domain; Region:
                     GTP_EFTU; pfam00009"
                     /db_xref="CDD:200924"
     misc_feature    complement(161..745)
                     /locus_tag="HMPREF0017_03119"
                     /note="Elongation Factor Tu (EF-Tu) GTP-binding proteins;
                     Region: EF_Tu; cd01884"
                     /db_xref="CDD:206671"
     misc_feature    complement(698..721)
                     /locus_tag="HMPREF0017_03119"
                     /note="G1 box; other site"
                     /db_xref="CDD:206671"
     misc_feature    complement(order(236..241,317..319,329..331,422..424,
                     431..442,446..451,518..523,575..580,662..664,674..679,
                     686..688,695..700,710..712,716..718))
                     /locus_tag="HMPREF0017_03119"
                     /note="GEF interaction site [polypeptide binding]; other
                     site"
                     /db_xref="CDD:206671"
     misc_feature    complement(order(248..256,359..361,365..370,635..637,
                     695..712))
                     /locus_tag="HMPREF0017_03119"
                     /note="GTP/Mg2+ binding site [chemical binding]; other
                     site"
                     /db_xref="CDD:206671"
     misc_feature    complement(578..610)
                     /locus_tag="HMPREF0017_03119"
                     /note="Switch I region; other site"
                     /db_xref="CDD:206671"
     misc_feature    complement(590..592)
                     /locus_tag="HMPREF0017_03119"
                     /note="G2 box; other site"
                     /db_xref="CDD:206671"
     misc_feature    complement(524..535)
                     /locus_tag="HMPREF0017_03119"
                     /note="G3 box; other site"
                     /db_xref="CDD:206671"
     misc_feature    complement(473..529)
                     /locus_tag="HMPREF0017_03119"
                     /note="Switch II region; other site"
                     /db_xref="CDD:206671"
     misc_feature    complement(359..370)
                     /locus_tag="HMPREF0017_03119"
                     /note="G4 box; other site"
                     /db_xref="CDD:206671"
     misc_feature    complement(248..256)
                     /locus_tag="HMPREF0017_03119"
                     /note="G5 box; other site"
                     /db_xref="CDD:206671"
     misc_feature    complement(<1..139)
                     /locus_tag="HMPREF0017_03119"
                     /note="Translation_Factor_II_like: Elongation factor Tu
                     (EF-Tu) domain II-like proteins. Elongation factor Tu
                     consists of three structural domains, this family
                     represents the second domain. Domain II adopts a beta
                     barrel structure and is involved in binding to...; Region:
                     Translation_Factor_II_like; cl02787"
                     /db_xref="CDD:207732"
ORIGIN      
        1 ttactgtagt agtttgagta tcacggatac caacgatttc tacagactca cctactttaa
       61 cgataccagt ctctacacgg ccagttacta ctgtaccacg accagagatt gagaatacgt
      121 cttcaattgg cataaggaat ggaaggtcaa ttgcacgctc tggctctgga atgtaagagt
      181 caagtgcttc aacaagcgca actaccgctg gctcgccgta ttggctgtca tcaccgttaa
      241 gcgcaagaag agcagaacca cggatcactg gagtgtcatc acccgggaag tcgtaagtag
      301 aaagaagttc acgaacttcc atttcaacta gctcaagaag ctcttcatcg tctacaaggt
      361 cgcatttgtt caagaataca acgatgtaag gtacacctac ctgacgagaa agcaggatgt
      421 gttcacgagt ctgtggcata ggaccatcag tcgcagcaca tacaaggatc gcgccatcca
      481 tctgagcagc accagtaatc atgtttttaa cataatcggc gtggcccggg caatccacgt
      541 gagcgtagtg acgagttgga gaatcgtatt ctacgtgtga agtattaatt gtaataccac
      601 gtgctttttc ttctggtgca gagtcgattg cagcgtagtc tttcgcttcg ccaccgaatt
      661 tcttcgcaca tacagttgca attgcagctg ttaaagttgt tttaccatgg tcaacgtgac
      721 caattgtgcc cacgttaacg tgtggcttat tacgttcaaa cttagcctta gccatg
//
