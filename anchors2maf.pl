#!/usr/bin/perl

#Copyright (C) 2006-2008 Keio University
#(Kris Popendorf) <comp@bio.keio.ac.jp> (2006)
#
#This file is part of Murasaki.
#
#Murasaki is free software: you can redistribute it and/or modify
#it under the terms of the GNU General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.
#
#Murasaki is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU General Public License for more details.
#
#You should have received a copy of the GNU General Public License
#along with Murasaki.  If not, see <http://www.gnu.org/licenses/>.

use strict;
use Getopt::Long;
use Pod::Usage;
use File::Basename;
use Data::Dump qw{dump};

BEGIN {
  unshift(@INC,(fileparse($0))[1].'perlmodules');
}

use Murasaki;

our ($inf,$pairs,$verbose);
local $\="\n";

my ($help,$man);
GetOptions('help|?' => \$help, man => \$man, 'pairs+' => \$pairs, 'verbose+'=>\$verbose);
pod2usage(1) if $help or scalar(@ARGV)<1;
pod2usage(-exitstatus => 0, -verbose => 4) if $man;

$inf=shift(@ARGV);

die "File ($inf) not found.\n" unless open(my $infh,$inf);

my ($in_name,$in_path,$in_suffix) = fileparse($inf, qr{\.anchors});
$in_path=~s!/$!!;
our $basename="$in_path/$in_name";
my %infiles=(anchors=>$inf);
$infiles{score}="$basename.anchors.stats.ungappedscore" if -f "$basename.anchors.stats.ungappedscore";

my @seqs=split("\n",slurp("$basename.seqs"));
die "Lack of sequences?" unless @seqs;
my %lengths=map {$_=>`$root/geneparse -l -c $_`} @seqs;

if($pairs){
  foreach my $i (0..$#seqs){
    foreach my $j (($i+1)..$#seqs){
      my $mafName="$basename.anchors.$i-$j.maf";
      makeMafFromSeqs($mafName,$i,$j);
    }
  }
}else{
  my $mafName="$basename.anchors.all.maf";
  makeMafFromSeqs($mafName,0..$#seqs);
}


sub makeMafFromSeqs {
  my ($outf,@seqi)=@_;
  open(my $outfh,">$outf");
  local $,="\n";
  print "Writing $outf for sequences: @seqi";
  print $outfh "##maf version=1 scoring=blastz program=murasaki","# murasaki";
  my %infh=map {open(my $fh,$infiles{$_}) or die "Failed to open $infiles{$_}"; $_=>$fh} keys %infiles;
  while(my %lines=getLines(%infh)){
    last unless keys(%lines);
    my @anchors=getAnchors($lines{anchors});
    my $score=$lines{score} if exists $lines{score};
    print $outfh defined $score ? "a score=$score":"a";
    foreach my $i (@seqi){
      my $src=$seqs[$i];
      my $length=$lengths{$seqs[$i]};
      my $anchor=$anchors[$i];
      my $start=$anchor->{strand} eq '+' ? ($anchor->{start}-1):($length-$anchor->{stop});
      my $extract="$root/geneparse -c $seqs[$i]\[$anchor->{start},$anchor->{stop}\]";
      my $content=$anchor->{strand} eq '+' ? `$extract`:`$extract | $root/antisense.pl -c`;
      print $outfh sprintf("s %-10s %9d %4d %1s %9d ",$seqs[$i],$start,$anchor->{length},$anchor->{strand},$length).$content;
    }
    print $outfh "";
  }
}

sub getAnchors {
  my ($line)=@_;
  my @anchors;
  my @bits=split(/\t/,$line);
  foreach my $i (map {$_*3} 0..(scalar(@bits)/3)){
    push(@anchors,($bits[$i+2] eq '+') ?
	 {start=>$bits[$i],
	  stop=>$bits[$i+1],
	  strand=>$bits[$i+2],
	  length=>$bits[$i+1]-$bits[$i]+1}
	 :{
	   start=>abs($bits[$i+1]),
	     stop=>abs($bits[$i]),
	       strand=>$bits[$i+2],
		 length=>abs($bits[$i+1]-$bits[$i])+1
	       }
	);
  }
  return @anchors;
}

sub slurp {
  local $/;
  open(my $fh,"<",@_) or die "Couldn't open @_";
  my $r=<$fh>;
  return $r;
}

sub getLines {
  my %fhs=@_;
  my %lines;
  my $ended;
  foreach my $k (keys %fhs){
    $lines{$k}=readline($fhs{$k});

    if(length $lines{$k}){
      if($ended){
	die "$ended ($infiles{$ended}) ended before $k ($infiles{$k})";
      }
      chomp $lines{$k};
    }else{
      unless($ended){
	$ended=$k;
      }
    }
  }
  return () if $ended;
  return %lines;
}
