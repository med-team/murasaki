#!/usr/bin/perl

#Copyright (C) 2006-2008 Keio University
#(Kris Popendorf) <comp@bio.keio.ac.jp> (2006)
#
#This file is part of Murasaki.
#
#Murasaki is free software: you can redistribute it and/or modify
#it under the terms of the GNU General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.
#
#Murasaki is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU General Public License for more details.
#
#You should have received a copy of the GNU General Public License
#along with Murasaki.  If not, see <http://www.gnu.org/licenses/>.

use Getopt::Long;
use Pod::Usage;
use File::Basename;
use IO::Handle;
use Data::Dump qw {dump};

use strict;

BEGIN {
  unshift(@INC,(fileparse($0))[1].'perlmodules');
}
use Murasaki;
use Murasaki::Ticker qw{resetTick tick};
use Murasaki::KOG;

our $root;
our $seqhome="$root/seq";
our $koglist;

our ($help,$man);
our ($kogfile,@kogmap,%kogpref);
GetOptions('help|?' => \$help, man => \$man,
	   'kogfile=s' => \$kogfile,
	   'kog=s%' => sub {pod2usage(-msg => "Invalid argument for --kog ($_[1] is not a valid index)")
			      unless(defined $_[1] and $_[1]>=0);
			    pod2usage(-msg => "Invalid argument for --kog $_[1] ('$_[2]' doesn't look like a kog name)")
				unless($_[2] and $_[2]=~m/^...$/);
			    $kogmap[$_[1]]=$_[2]},
	   'kogpref=s%' => \%kogpref,
	  'koglist=s' => \$koglist);
pod2usage(1) if $help or $#ARGV<0;
pod2usage(-exitstatus => 0, -verbose => 2) if $man;

die "Need to specify kogfile with --kogfile" unless $kogfile;

if($koglist){
  my $count;
  open(my $koglistfh,$koglist) or die "Couldn't open $koglist";
  $koglist={};
  while(<$koglistfh>){
    chomp;
    $koglist->{$_}=1;
    $count++;
  }
  print "\"Used members\" list loaded. $count approved locs.\n";
}

my $outfile=shift @ARGV;
my $usedSeqFile;
if($outfile=~m/\.seqs$/){ #alignment file
  my $basename=$`;
  print "Loading sequence list from $outfile\n";
  open(INF,$outfile) or die "Couldn't open $outfile";
  while(<INF>){
    chomp;
    push(@ARGV,$_);
  }
  close(INF);
  $usedSeqFile=$outfile;
  $outfile="$basename.kogaligned";
  print "Storing output to $outfile\n";
}else{
  #append a .anchors unless it already has one
  $outfile="$outfile.anchors" unless $outfile=~m/\.anchors$/;
  if(-f $outfile){
    open(my $peek,$outfile);
    my $line=<$peek>;
    unless($line=~m/^(-?\d+\t-?\d+\t[-+]\t?)+$/){
      die "$outfile already exists and doesn't look like an anchor file. I don't want to break something important...";
    }
  }
}

our (@locusLists,%knownLocs);

my ($i,$learnedKogSpecs);

LoadSeqs: foreach my $seq (@ARGV){
  unless($kogmap[$i]){
    local $_=$seq;
    my ($id)=Murasaki::KOG->guessKogMember($seq);
    unless($id or $learnedKogSpecs){
      print "$seq not identifable from basic Kog list, checking $kogfile contents...\n";
      #pre-load our kogfile and see if it knows any more species names
      Murasaki::KOG->learnKogSpecs($kogfile);
      $learnedKogSpecs;
      $id=Murasaki::KOG->guessKogMember($seq);
    }
    if($id){
      print "Identified $_ as KOG member $id\n";
      $kogmap[$i]=$id;
    }
  }
  die "$seq isn't bound to a kog. Use --kog (eg. --kog $i=MtC) to set it.\n"
    unless $kogmap[$i];

  if(!-f "$seq.cds"){
    print "CDS file not found for $seq. Generating...\n";
    my $res=system("$root/getcds.pl $seq");
    do {
      die "Generation of CDS file for $seq failed" unless -f "$seq.cds\n";
      next LoadSeqs;
    } unless -f "$seq.cds";
  }
  open(CDS,"$seq.cds") or die "Couldn't read $seq.cds";
  print "Loading annotation for $seq...";
  my ($cdscount);
 LoadCDS: while(<CDS>){
    my ($name,$start,$stop,$strand,$locus) = split(/\s/,$_);
    $locus=$name if $name && !$locus;
    next unless $locus;
    $locus=uc $locus;
    my $cd={name => $name,start=>$start,stop=>$stop,strand=>$strand,locus=>$locus};
    print "Uh oh, $locus occurs again!\n" if $locusLists[$i]->{$locus};
    $locusLists[$i]->{$locus}=$cd;
    $knownLocs{$locus}=1;
    $cdscount++;
  }
  print("$cdscount CDS's loaded.\n");
}continue{$i++}

my $kogs=KOG->kogFrom($kogfile,\@kogmap,\%knownLocs,\%kogpref);

open(OUTF,">$outfile") or die "Couldn't open $outfile for writing\n";
our $count=0;
resetTick(scalar(keys(%{$locusLists[0]})));
foreach my $locus (keys(%{$locusLists[0]})){
  my $allmembers=$kogs->memberMap($locus);
  my @bitsToPlot;
  my $i=0;
  foreach my $spec (@kogmap){
    $bitsToPlot[$i]=[grep {$allmembers->{$_} eq $spec} keys(%$allmembers)];
  }continue{$i++}
  plotBits(0,"",@bitsToPlot);
  tick();
}
close OUTF;
print "\nDone. $count psuedo-anchors.\n";

unless($usedSeqFile or $outfile eq "-"){
  my $prefix=(fileparse($outfile,qr/\.[^.]+/))[0];
  print "Writing $prefix.seqs\n";
  open(my $seqfh,">$prefix.seqs");
  print $seqfh $_."\n" foreach @ARGV
}

sub plotBits {
  my ($ite,$leaderBits,$mybits,@others)=@_;
  unless($mybits){
    print OUTF $leaderBits."\n";
    $count++;
    return;
  }
  foreach my $bit (@$mybits){
    my $nextLeader="$leaderBits\t" if $leaderBits;
    local $_=$locusLists[$ite]->{$bit};
    if($koglist){
      next unless $koglist->{$_->{locus}};
    }
    my $wasNasty=($leaderBits=~m/\t\t/ or $leaderBits=~m/\t$/);
    my @bits=@{$_}{qw{start stop}};
    die "Uh oh. Missing coordinates from: @bits from ".dump($bit)." (maybe you need --kogpref underbar=1?)" unless $bits[0] and $bits[1];
    my $nextLeader=($nextLeader.join("\t",@bits,$_->{strand}<0 ? "-":"+"));
    plotBits($ite+1,$nextLeader,@others);
  }
}

__END__

=head1 NAME

kogalign.pl -- produce (untested) alignments from cds files

=head1 SYNOPSIS

kogalign.pl --kogfile <kogdata> <output> <input1> <input2> [<input3> ...]
kogalign.pl --kogfile <kogdata> <alignment.seq>

=head1 OPTIONS
The kogalign.pl <aligment.seq> execution style writes results to
align.kogaligned

The --kogfile option is necessary.

=cut
