#!/usr/bin/perl

#Murasaki - multiple genome global alignment program
#Copyright (C) 2006-2007 Keio University
#(Kris Popendorf) <comp@bio.keio.ac.jp> (2006)
#
#This program is free software; you can redistribute it and/or
#modify it under the terms of the GNU General Public License
#as published by the Free Software Foundation; either version 2
#of the License, or (at your option) any later version.
#
#This program is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU General Public License for more details.
#
#You should have received a copy of the GNU General Public License
#along with this program; if not, write to the Free Software
#Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

use strict;
package Murasaki::KOG;

#use Data::Dump qw{dump};
use Murasaki::Ticker qw{resetTick tick};

sub knownKogs {qw{ath ath dme hsa sce spo ecu osa aga ptr cfa mmu rno mgr ncr}}
sub knownCogs {qw{Mth Mja Hbs Tac Tvo Pho Pab Afu Mka Mac Pya Sso Ape Sce Spo Ecu Aae Tma Nos Syn Dra Fnu Tpa Bbu Ctr Cpn Cgl Mtu MtC Mle Cac Sau Lin Bsu Bha Lla Spy Spn Uur Mpu Mpn Mge Pae Eco EcZ Ecs Ype Sty Buc Xfa Vch Hin Pmu Rso Nme NmA Hpy jHp Cje Ccr Atu Sme Bme Mlo Rpr Rco mga mge mpn}}

sub commonAliases {
  (human=>'hsa',rice=>'osa',mosquito=>'aga',chimp=>'ptr',dog=>'cfa',mouse=>'mmu',mus=>'mmu',rat=>'rno', #official ones
   homo=>'hsa',sapien=>'hsa', #semi-official
   rhesus=>'mac', macaq=>'mac', cow=>'bov', oposs=>'pos', chicken=>'avi', #made up by me
   mx=>'mmu',hx=>'hsa',cx=>'ptr',qx=>'mac', #also made up by me (for chromosome x special case)
   avium=>'mta', bovis=>'mtb',
   m3=>'mpn',m6=>'mge',m8=>'mga' #made up by osana
)}

sub empty {
  my ($class,$kogname)=@_;
  $kogname="**empty**" unless $kogname;
  bless {kogs=>{},kogSrc=>$kogname,locToKog=>{}},$class;
}

sub kogFrom {
  my ($class,$kogSrc,$srcspec,$knownLocusr,$kogprefr)=@_;
  my $srcre=qr/.../; #default to all
  if($srcspec){
    $srcre=join("|",@$srcspec);
    $srcre=qr/$srcre/;
  }
  my %kogs;
  open(my $infh,$kogSrc) or die "Couldn't open $kogSrc\n";
  my $state=0;
  my $kog;
  my %unknownLocs;
  my %locToKog;
  while(<$infh>){
    if($state==0){
      chomp;
      m/\[(\S+)\] (\S+) (.*)/ or die "$kogSrc: unparsable KOG description at line $.: $_";
#      print "loading kog: func $1 name $2 desc $3\n";
      $kog={};
      $kog->{func}=$1;
      $kog->{name}=$2;
      $kog->{desc}=$3;
      $state++;
    }elsif($state==1){
      goto KOGDONE if m/^$/;
      m/^  ($srcre):\s+(.+)/ or next;
      my ($spec,$allloc)=($1,uc $2);
      foreach my $loc (split(/s+/,$allloc)){
	do{$unknownLocs{$loc}++;next} unless (!$knownLocusr or exists $knownLocusr->{$loc}); #only load for known locuses
	$loc=~s/_\d+// unless $kogprefr->{underbar}; #this is some KOG crazy talk for domains or something
	$kog->{members}->{$loc}=$spec;
      }
    }
    next;
  KOGDONE:
    $state=0;
    next unless $kog->{members}; #skip unless it has some contents
    if($srcspec){ #got a set of species to require?
      my %met;
      foreach my $spec (values(%{$kog->{members}})){
	$met{$spec}=1;
      }
      next if grep {!$met{$_}} @$srcspec; #no soup if you're missing one
    }
    #good to go!
    $kogs{$kog->{name}}=$kog;
    foreach my $loc (keys(%{$kog->{members}})){
      $locToKog{$loc}=$kog;
    }
  }
  my $this=bless {kogs=>\%kogs,kogSrc=>$kogSrc,locToKog=>\%locToKog},$class;
  print "$kogSrc loaded. ".$this->summary."\n".(keys(%unknownLocs))." unknown locs\n";
  print "Unknown locs: ".join(" ",keys(%unknownLocs))."\n" if $kogprefr->{echoUnknown};
  return $this;
}

sub memberList {
  my ($self,$ref)=@_;
  my $kog=$self->isIn($ref);
  return () unless $kog;
  return keys(%{$kog->{members}});
}

sub memberMap {
  my ($self,$ref)=@_;
  my $kog=$self->isIn($ref);
  return () unless $kog;
  return $kog->{members};
}

sub isIn {
  my ($self,$ref)=@_;
  my $kog=$self->{locToKog}->{$ref};
  return undef unless $kog;
  return $self->{locToKog}->{$ref}=$self->flatten($kog);
}

sub isOrtho {
  my ($self,$l1,$l2)=@_;
  my ($k1,$k2)=map {$self->isIn($_)} ($l1,$l2);
  return undef if !$k1 && !$k2;
  return $k1==$k2;
}

sub flatten {
  my ($self,$kog)=@_;
  return $kog unless $kog->{remapped};
  return $kog->{remapped}=$self->flatten($kog->{remapped});
}

sub pushHash {
  $_[0]={} unless ref $_[0] eq "HASH";
  my $h1=shift;
  foreach my $h2 (@_){
    while(my ($k,$v)=each(%$h2)){
      $h1->{$k}=$v;
    }
  }
}

sub countKogs {
  my ($self)=@_;
  return scalar(keys(%{$self->{kogs}}));
}

sub countGenes {
  my ($self)=@_;
  return scalar(keys(%{$self->{locToKog}}));
}

sub summary {
  my ($self)=@_;
  return $self->countKogs." KOGs of ".$self->countGenes." genes";
}

sub merge {
  my ($self,$kog1,$kog2)=@_;
  return $kog1 if($kog1 && !$kog2);
  return $kog2 if($kog2 && !$kog1); #no need to merge if only 1 exists
  return $self->anonKog if(!$kog1 and !$kog2); #no kogs at all? well make one!
  return $kog1 if ($kog1 == $kog2); #if they're the same kog, pick one...
  #ok, two different kogs exist and need to be merged.
  my ($smaller,$bigger)=($kog1->{members}<$kog2->{members} ? ($kog1,$kog2):($kog2,$kog1));
  $smaller->{remapped}=$bigger;
  pushHash($bigger->{members},$smaller->{members});
  delete $self->{kogs}->{$smaller->{name}}; #dont need that anymore
  return $bigger;
}

sub anonKog {
  my ($self)=@_;
  my $kogid=$self->{nextAnonKog}++;
  my $name="ANON$kogid";
  my $anonkog={name=>$name};
  $self->{kogs}->{$name}=$anonkog;
  return $anonkog;
}

sub add {
  my ($self,$p1,$p2)=@_;
  my ($l1,$l2)=map {(keys(%$_))[0]} ($p1,$p2);
  my ($kog1,$kog2)=map {$self->isIn($_)} ($l1,$l2);
  return if $kog1==$kog2 and $kog1 and $kog2; #be thee not silly!
  my $kog3=$self->merge($kog1,$kog2);
  pushHash($kog3->{members},$p1,$p2);
  $self->{locToKog}->{$l1}=$kog3;
  $self->{locToKog}->{$l2}=$kog3;
}

sub compare {
  my ($self,$other)=@_;
  my %loclist;
  print "Comparing KOGs from $self->{kogSrc} and $other->{kogSrc}\n";
  foreach my $kog (values(%{$self->{kogs}}),values(%{$other->{kogs}})){
    foreach my $member (keys(%{$kog->{members}})){
      $loclist{$member}=1;
    }
  }
  print "Comparing KOG members...\n";
  resetTick(scalar(keys(%loclist)));
  my @unmatched;
  my %res=(in1=>0,in2=>0,inboth=>0);
  foreach my $node (keys(%loclist)){
    my %matching;
    foreach my $linked ($self->memberList($node)){
      next if $linked eq $node;
      $matching{$linked}=1;
    }
    foreach my $linked ($other->memberList($node)){
      next if $linked eq $node;
      if($matching{$linked}){
	$res{inboth}++;
	delete $matching{$linked};
      }else{
	$res{in2}++;
      }
    }
    $res{in1}+=keys(%matching);
#    push(@unmatched,map {"$node~$_"} keys(%matching));
    tick();
  }
#  print "\nUnmatched:\n".join("\n",@unmatched)."\n";
  $res{in0}=(scalar(keys %loclist)*(scalar(keys %loclist)-1))-$res{in1}-$res{in2}-$res{inboth};
  print "\n";
  return %res;
}

sub guessKogMember {
  my ($class,@src)=@_;
  our %learnedKogSpecs;
  my $kogre=join("|",knownCogs,knownKogs,keys %learnedKogSpecs);
  my $i=0;
  my %aliases=commonAliases;
  my @res;
  foreach(@src){
    my ($id)=m/($kogre)/i;
    unless($id){ #try harder...
      foreach my $alias (keys(%aliases)){
	$id=$aliases{$alias} if (m/$alias/i);
      }
    }
    return $id unless wantarray;
    push(@res,$id);
  }
  return @res;
}

sub learnKogSpecs {
  our %learnedKogSpecs;
  our %learnedFrom;
  my ($class,$kogSrc)=@_;
  return if exists $learnedFrom{$kogSrc};
  my $state=0;
  open(my $infh,$kogSrc) or die "Couldn't open $kogSrc\n";
  while(<$infh>){
    if($state==0){
      chomp;
      m/\[(\S+)\] (\S+) (.*)/ or die "$kogSrc: unparsable KOG description at line $.: $_";
      $state++;
    }elsif($state==1){
      goto KOGDONE if m/^$/;
      m/^  (\w+):\s+(.+)/ or next;
      $learnedKogSpecs{$1}->{$kogSrc}=1;
      $learnedFrom{$kogSrc}->{$1}=1;
    }
    next;
  KOGDONE:
    $state=0;
  }
}

1;
