#!/usr/bin/perl

#Murasaki - multiple genome global alignment program
#Copyright (C) 2006-2007 Keio University
#(Kris Popendorf) <comp@bio.keio.ac.jp> (2006)
#
#This program is free software; you can redistribute it and/or
#modify it under the terms of the GNU General Public License
#as published by the Free Software Foundation; either version 2
#of the License, or (at your option) any later version.
#
#This program is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU General Public License for more details.
#
#You should have received a copy of the GNU General Public License
#along with this program; if not, write to the Free Software
#Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

use strict;
package Murasaki::OrthoPairs;

#use Data::Dump qw{dump};
use Murasaki::Ticker qw{resetTick tick};
no Carp::Assert;

sub empty {
  my ($class,$name,$kogmap,$knownLocs)=@_;
  bless {orthos=>{},name=>$name,knownLocs=>$knownLocs,spec2pos=>reverseMap($kogmap)},$class;
}

sub clone {
  my ($class,$base,$newName)=@_;
  my %content=%{$base};
  $content{name}=$newName if $newName;
  bless {%content},$class;
}

sub canonicalize {
  my ($members)=@_;
  return join("~",map {keys(%$_)} (sort {(values(%$a))[0] cmp (values(%$b))[0]} @$members));
}

sub addBits {
  my ($basis,$next,@others)=@_;
  if($next){
    my @bits;
    foreach my $bit (@$next){
      push(@bits,addBits([@$basis,$bit],@others));
    }
    return @bits;
  }else{
    return $basis;
  }
}

sub getLocPairs {
  my @bits=@_;
  my @res;
  foreach my $i (0..$#bits){
    foreach my $j (($i+1)..$#bits){
      push(@res,[@{$bits[$i]},@{$bits[$j]}]);
    }
  }
  return @res;
}

sub getPairs {
  my @bits=@_;
  my @res;
  foreach my $i (0..$#bits){
    foreach my $j (($i+1)..$#bits){
      push(@res,[$bits[$i],$bits[$j]]);
    }
  }
  return @res;
}

sub isKnownLoc {
  my ($self,$loc)=@_;
  die "Bad loc: ".dump($loc) unless ref $loc eq 'HASH' and scalar(keys %$loc)==1;
  my $l=(keys %$loc)[0];
  return exists $self->{knownLocs}->{$l} and $self->{knownLocs}->{$l} eq $loc->{$l};
}

sub addAll {
  my ($self,$memr)=@_;
  my @bits=(ref $memr eq 'ARRAY') ? (@$memr):
    (values(%{reverseMap($memr)}));
  our @pairs=getLocPairs(@bits);
  foreach my $pair (@pairs){
    assert(scalar(@{$pair})==2);
    my $content={ortho=>1};
    if($self->isKnownLoc($$pair[0]) and $self->isKnownLoc($$pair[1])){
      $content->{known}=1;
      my ($p1,$p2)=map {canonicalize([$_])} @$pair;
      my $canon=canonicalize($pair);
      $self->{knownPairs}->{$p1}->{$p2}=$canon;
      $self->{knownPairs}->{$p2}->{$p1}=$canon;
      $self->{knownPairList}->{$canon}=1;
    }
    $self->{orthos}->{canonicalize($pair)}=$content;
  }
}

sub hitAllKnown {
  my ($self,@perSeqLocs)=@_;
  my @locs=map {@$_} @perSeqLocs; #flatten
  my %hits=map {canonicalize([$_])=>1} @locs; #hash;
  my %res;

  #for each loc that we hit, if it didn't meet its intended partner, we record a miss (otherwise it's a hit)
  my %counted;
  foreach my $loc (@locs){
    next unless $self->isKnownLoc($loc);
    my $p1=canonicalize([$loc]);
    foreach my $p2 (keys %{$self->{knownPairs}->{$p1}}){
      my $canon=$self->{knownPairs}->{$p1}->{$p2};
      next if $counted{$canon};
      $counted{$canon}=1;
      unless($hits{$p1} and $hits{$p2}){
	$self->{orthos}->{$canon}->{missed}++;
	$res{misses}++;
      }else{
	$self->{orthos}->{$canon}->{hit}++;
	$self->{orthos}->{$canon}->{hitonce}=1;
	$res{hits}++;
      }
    }
  }
  return %res;
}

sub reverseMap {
  my $arg=pop;
  my $ret={};
  if(ref $arg eq 'ARRAY'){
    for(0..$#$arg){
      $ret->{$$arg[$_]}=$_;
    }
  }elsif(ref $arg eq 'HASH'){
    for my $k (keys(%$arg)){
      push(@{$ret->{$arg->{$k}}},{$k=>$arg->{$k}});
    }
  }else{
    print "Non-reversable: ".dump($arg)."....\n";
    die "Non-reversable arg called on reverseMap...\n";
    $ret=$arg; #ummmm
  }
  return $ret;
}

sub toSpecList {
  my ($self,$members)=@_;
  my @ret;
  foreach my $k (keys(%$members)){
    do{print "Unknown species $k.\n"; return undef} unless exists($self->{spec2pos}->{$k});
    $ret[$self->{spec2pos}->{$k}]=$members->{$k};
  }
  return @ret;
}

sub orthosFromKog {
  my ($class,$kogs,$kogmap,$knownLocs)=@_;
  my $this=empty($class,$kogs->{kogSrc},$kogmap,$knownLocs); #this isnt C++. we can get our object up and going now ^^
  print "Filling OrthoPairs from KOGset\n";
  foreach my $kogname (keys(%{$kogs->{kogs}})){
    $this->addAll($kogs->{kogs}->{$kogname}->{members})
  }
  print $kogs->{kogSrc}." converted to OrthoPairs list. ".$this->summary."\n";
  return $this;
}

sub summary {
  my ($self)=@_;
  my $count=scalar(keys %{$self->{orthos}});
  return $count.($count==1 ? " ortholog":" orthologs");
}

sub possibleOrthosCount {
  my ($self)=@_;
  my %count;
  foreach my $s (values(%{$self->{knownLocs}})){
    $count{$s}++;
  }
  return 0 unless keys(%count);
  my $possible=1;
  foreach my $s (keys(%count)){
    $possible*=$count{$s};
  }
  return $possible;
}

sub addOrtho {
  my ($self,@set)=@_;
  foreach my $subset (addBits([],@set)){
    $self->{orthos}->{canonicalize($subset)}=1;
  }
}

sub isOrtho {
  my ($self,@set)=@_;
  my %res;
  foreach my $subset (addBits([],@set)){
    $res{($self->{orthos}->{canonicalize($subset)}) ? "yes":"no"}++;
  }
  return %res;
}

sub sensitivity {
  my ($self)=@_;
  my ($consistent);
  my $total=keys %{$self->{knownPairList}};
  return undef unless $total;
  foreach my $canonPair (keys %{$self->{knownPairList}}){
    $consistent++ if $self->{orthos}->{$canonPair}->{hit};
  }
  return $consistent/$total;
}

sub specificity {
  my ($self)=@_;
  my %counted;
  my ($conCount,$inconCount)=(0,0);
  foreach my $hitloc (keys %{$self->{hitLocs}}){
    foreach my $partner (keys %{$self->{knownPairs}->{$hitloc}}){
      my $canon=$self->{knownPairs}->{$hitloc}->{$partner};
      next if $counted{$canon}; #thou shalt not double count
      $conCount+=$self->{orthos}->{$canon}->{hit};
      $inconCount+=$self->{orthos}->{$canon}->{missed};
    }
  }
  return undef unless ($conCount+$inconCount);
  return $conCount/($conCount+$inconCount);
}

sub stats { #both of those should just be one function
  my ($self)=@_;
  my %counted;
  my ($conCount,$inconCount,$recallCount)=(0,0,0);
  foreach my $hitloc (keys %{$self->{knownPairs}}){
    foreach my $partner (keys %{$self->{knownPairs}->{$hitloc}}){
      my $canon=$self->{knownPairs}->{$hitloc}->{$partner};
      next if $counted{$canon}; #thou shalt not double count
      $conCount+=$self->{orthos}->{$canon}->{hit};
      $inconCount+=$self->{orthos}->{$canon}->{missed};
      $recallCount+=$self->{orthos}->{$canon}->{hitonce};
      $counted{$canon}=1;
    }
  }
  my $totalPairs=keys %{$self->{knownPairList}};
  my %res=(recall=>$recallCount,
	   con=>$conCount,
	   incon=>$inconCount,
	   total=>$totalPairs);
  $res{sens}=$res{recall}/$res{total} if $res{total};
  $res{spec}=$conCount/($conCount+$inconCount) if ($conCount+$inconCount);
  return %res;
}

sub compare {
  my ($self,$other)=@_;
  my %done;
  my %res=(in1=>0,in2=>0,inboth=>0);
  print "Comparing ortholist members...\n";
  @done{keys(%{$self->{orthos}})}=(1) x scalar keys(%{$self->{orthos}});
  my @inboth;
  resetTick(scalar(keys(%{$other->{orthos}})));
#  print "Done: ".join("\n",keys(%done))."\n";
  foreach my $ortho (keys(%{$other->{orthos}})){
#    print "Compare: $ortho\n";
    if($done{$ortho}){
      $res{inboth}++;
      push(@inboth,$ortho);
      delete $done{$ortho};
    }else{
      $res{in2}++;
    }
    tick;
  }
  $res{in1}+=scalar keys %done;
#  print "\nOrtho inboth:\n".join("\n+",@inboth)."\n";
#  print "\nOrtho in source:\n".join("\n*",keys %{$self->{orthos}})."\n";
  if($self->{knownLocs} == $other->{knownLocs}){#impossible to know otherwise
    $res{in0}=$self->possibleOrthosCount()-$res{in1}-$res{in2}-$res{inboth};
  }
  print "\n";
  return %res;
}

1;
