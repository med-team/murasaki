#!/usr/bin/perl

#Copyright (C) 2006-2008 Keio University
#(Kris Popendorf) <comp@bio.keio.ac.jp> (2006)
#
#This file is part of Murasaki.
#
#Murasaki is free software: you can redistribute it and/or modify
#it under the terms of the GNU General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.
#
#Murasaki is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU General Public License for more details.
#
#You should have received a copy of the GNU General Public License
#along with Murasaki.  If not, see <http://www.gnu.org/licenses/>.
###############
## chop anchors into tiny bits
## by krisp
######

use Getopt::Long;
use Pod::Usage;
use File::Basename;

use strict;

BEGIN {
  unshift(@INC,(fileparse($0))[1].'perlmodules');
}
use Murasaki;

my ($help,$man,$maxSize);

my $opt_res=GetOptions('help|?' => \$help, man => \$man,
		       'size=i' => \$maxSize);
pod2usage(-exitstatus => 0, -verbose => 2) if $man;
pod2usage(-exitstatus => 0, -verbose => 99, -section=>"SYNOPSIS|OPTIONS|DESCRIPTION") if $help;
pod2usage() if !@ARGV or !$opt_res;

my $geneparse="$root/geneparse";
$geneparse="$root/geneparse.pl" unless -x $geneparse;


foreach my $file (@ARGV){
  my ($name,$dir,$ext)=fileparse($file,qr/\.[^\.]+/);
  die "Need an .anchors file" unless $ext eq '.anchors';
  local $\="\n";
  open(my $infh,$file) or die "Couldn't open $file";
  open(my $seqinfh,"$dir$name.seqs") or die "No seqfile $dir$name.seqs";
  my @seqs=grep {m/\S/} (map {chomp;$_} <$seqinfh>);
  unless($maxSize>0){
    $maxSize=int(min(map {`$geneparse -l -c $_`/1000} @seqs));
    print "MaxSize set automatically to $maxSize";
  }
  my $prefix="$dir$name.interp-$maxSize";
  print "Writing to $prefix.anchors";
  open(my $otseqfh,">$prefix.seqs") or die "Couldn't open $prefix.seqs";
  print $otseqfh join("\n",@seqs);
  open(my $otfh,">$prefix.anchors") or die "Couldn't open $prefix.seqs";
  while(my $line=<$infh>){
    chomp $line;
    my @bits=split(/\t/,$line);
    my @meta=@bits[scalar(@seqs)*3..$#bits];
    @bits=@bits[0..(scalar(@seqs)*3-1)];
    my @anchors=toAnchors(scalar(@seqs),@bits);
    my @lens=map {ancLength($_)} @anchors;
#    print join(" ",map {ancToString($_)} @anchors)." -> ".max(@lens);
    if(max(@lens)>$maxSize){ #break into chunks
      my $factor=$maxSize/max(@lens);
#      print "Splitting @bits into ".int(1/$factor)." chunks (each: ".join(",",map {int($_*$factor)} @lens).")\n";
      my $pi=0;
      while($pi<1){
	$pi=1 if($pi>1);
	my @rescaled=(map {
	  my $len=$lens[$_];
	  my $anc=$anchors[$_];
#	  print "$pi: Seq $_: offset: ".($pi*$len)."\n";
	  {
	    start=>int($anc->{start}+$pi*$len),
	      stop=>int($anc->{start}+(min($pi+$factor,1)*$len)),
		sign=>($anc->{sign})
	      }
	} 0..$#anchors);
	foreach my $i (0..$#rescaled){
	  my $test=$rescaled[$i];
#	  die "Anchor bit $i too big ($pi+$factor):".ancToString($anchors[$i])."(".ancLength($anchors[$i]).")->".ancToString($test)."(".ancLength($test).")" if ancLength($test)>$maxSize;
#	  die "Anchor bit $i too small ($pi+$factor):".ancToString($anchors[$i])."(".ancLength($anchors[$i]).")->".ancToString($test)."(".ancLength($test).")" if ancLength($test)<=1;
	}
	print $otfh join("\t",map {(@{$_}{qw{start stop sign}})} @rescaled);
      }continue{$pi+=$factor}
    }else{ #output as is
      print $otfh join("\t",@bits,@meta);
    }
  }
}

sub ancToString {
  my ($a)=@_;
  return '['.($a->{start}.'~'.$a->{stop}).']';
}

sub ancLength {
  my ($a)=@_;
  return ($a->{stop}-$a->{start});
}


sub toAnchors {
  my ($count,@bits)=@_;
  my @res;
  foreach my $si (0..($count-1)){
    my ($start,$stop,$sign)=map {$bits[$_+$si*3]} 0..2;
    die "Not an anchor $si: (@bits) -> ($start $stop $sign)" unless $start=~m/\d+/ and $stop=~m/\d+/ and $sign=~m/^[+-]$/;
    ($start,$stop)=($stop,$start) if $stop<$start;
    push(@res,{start=>$start,stop=>$stop,sign=>$sign});
  }
  return @res;
}

sub sum {
  my $r=0;
  foreach my $i (@_){
    $r+=$i;
  }
  return $r;
}

sub max {
  my ($r,@l)=@_;
  foreach my $v (@l){
    $r=$v if $v>$r;
  }
  return $r;
}

sub min {
  my ($r,@l)=@_;
  foreach my $v (@l){
    $r=$v if $v<$r;
  }
  return $r;
}

__END__

=head1 NAME

interpolate.pl - chop anchors into arbitrarily sized tiny anchor bits,
interpolate anchor break points based on individual anchor lengths.

=head1 SYNOPSIS

interpolate.pl <input1> [input2]

=head1 OPTIONS

  --size=<N> Specify max size for an anchor. By default, it's set
    1/1000 of the longest input sequence.
