/*
Copyright (C) 2006-2008 Keio University
(Kris Popendorf) <comp@bio.keio.ac.jp> (2006)

This file is part of Murasaki.

Murasaki is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Murasaki is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Murasaki.  If not, see <http://www.gnu.org/licenses/>.
*/

//////////////
// murasaki project
// globaltypes.h
// provides global typing glue to keep other .h files sane
//////////////

#ifndef _GLOBALTYPES_H_
#define _GLOBALTYPES_H_

#ifdef LARGESEQ_SUPPORT
typedef long SeqPos;
#else
typedef int SeqPos;
#endif

//this might change on different environments...ideally would be autoconf'd
typedef unsigned long word;

//originally defined in sequence.h

#ifdef __linux__
#define WORDSIZE __WORDSIZE
#else
#if (defined(__FreeBSD__) && defined(__i386__)) || (defined (__APPLE__) && (defined (__ppc__) || defined (__i386__))) || defined(__MINGW32__)
#define WORDSIZE 32
#endif

#if (defined (__APPLE__) && (defined (__ppc64__) || defined (__x86_64__))) || (defined(__FreeBSD__) && defined(__x86_64__))
#define WORDSIZE 64
#endif
#endif

class BaseIterator; 
class BitSequence;

typedef word HashKey;
class Location;
class Sequence;
typedef Location HashVal;
typedef unsigned char SeqIdx;

//from cryptohasher
class CryptoHasher;

#endif
