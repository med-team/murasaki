/*
Copyright (C) 2006-2008 Keio University
(Kris Popendorf) <comp@bio.keio.ac.jp> (2006)

This file is part of Murasaki.

Murasaki is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Murasaki is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Murasaki.  If not, see <http://www.gnu.org/licenses/>.
*/

//////////////////
// compare some cgr images
// this is basically load, then multiply
/////////////////

#include <iostream>
#include <memory.h>
#include <getopt.h>
#include <vector>
#include <map>
#include <list>
#include <boost/lexical_cast.hpp>
#include "dinkymath.h"
#include "cgr.h"

using namespace std;

string program_help();
string program_version();

typedef vector<pair<PgmBuffer*,string> > CgrSet;

void doSubsets(CgrSet base,list<CgrSet > remaining);

void doNwise(CgrSet base,
	     CgrSet remaining,
	     unsigned n);

typedef map<string,double> Resmap;
Resmap results;
unsigned nwise=0;

int main(int argc,char** argv){
  int optc;
  while(1){
    //options struct:
    // name, has_arg, store_pointer, return_value
    static struct option long_options[] = {
      {"help",0,0,'?'},
      {"version",0,0,'v'},
      {"nwise",1,0,'n'},
      {0,0,0,0}
    };
    int longindex=0;
    optc=getopt_long(argc,argv,"?vn:",long_options,&longindex);
    if(optc==-1)break;
    switch(optc){
    case 'n':
      using boost::lexical_cast;
      using boost::bad_lexical_cast;
      try{
	nwise=lexical_cast<unsigned>(optarg);}
      catch(bad_lexical_cast& e){
	cerr << "Bad argument to --nwise."<<endl;
	cout << program_help();exit(-1);break;
      }
      break;
    case 'v': cout << program_version();exit(-1);break;
    default: cout << program_help();exit(1);break;
    }
  }

  list<CgrSet > sets;
  sets.push_back(CgrSet()); //starter set
  for(int seqi=optind;seqi<argc;seqi++){
    string inname(argv[seqi]);
    if(inname=="and" && !nwise){ //start a new set
      sets.push_back(CgrSet());
      continue;
    }
    cout << "Loading "<<inname<<"...."<<endl;
    sets.back().push_back(pair<PgmBuffer*,string>(new PgmBuffer,inname));
    try {
      sets.back().back().first->loadPGM(inname);
    } catch(PGMFileException e){
      cerr << e.reason() << endl;
      cerr << inname << " ^^File skipped^^"<<endl;
      sets.back().pop_back();
    }    
  }
  
  cout << "All images loaded..."<<endl;

  cerr << "Comparing..." << endl;
  CgrSet base;
  if(sets.size()==1)nwise=2;
  if(nwise){
    doNwise(base,sets.front(),nwise);
  }
  else
    doSubsets(base,sets);

  
  cout << "Done!"<<endl<<endl;
  for(Resmap::iterator i=results.begin();i!=results.end();i++){
    cout << i->first << "\t" << i->second << endl;
  }
}

inline void setCmp(CgrSet &base){
    CgrSet::iterator i=base.begin();
    string resname=i->second;
    PgmBuffer* home=i->first;
    vector<PgmBuffer*> others;
    i++;
    for(;i!=base.end();i++){
      others.push_back(i->first);
      resname+="-"+(i->second);
    }
    results[resname]=home->difference(others);
}

void doNwise(CgrSet base,
	     CgrSet remaining,
	     unsigned n){
  if(base.size()==n){ //base step
    setCmp(base);
  }else{ //inductive step
    if(remaining.empty())return; //insufficient to finish
    for(CgrSet::iterator i=remaining.begin();i!=remaining.end();i++){
      CgrSet temp;
      CgrSet::iterator j=i;

      j++;
      copy(j,remaining.end(),back_insert_iterator<CgrSet>(temp));

      base.push_back(*i);
      doNwise(base,temp,n);
      base.pop_back();
    }
  }
}

//muwah! beautiful code
void doSubsets(CgrSet base,
	       list<CgrSet > remaining){
  if(remaining.size()){ //inductive step
    CgrSet mine=remaining.front();
    remaining.pop_front();
    for(CgrSet::iterator i=mine.begin();
	i!=mine.end();i++){
      base.push_back(*i);
      doSubsets(base,remaining);
      base.pop_back();
    }
  }else{ //base case
    setCmp(base);
  }
}

string program_help(){
  return string("\
Usage: cgr-compare [options] <set1-cgr1 [set1-cgr2] [and set2-cgr1 ...]>\n\
\n\
Options\n\
*Takes an option (like --maxres 3 or -m3)\n\
\n\
*Toggles: (just --merge or -b)\n\
");
}

string program_version(){
  return string("0.1");
}

