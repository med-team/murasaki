/*
Copyright (C) 2006-2008 Keio University
(Kris Popendorf) <comp@bio.keio.ac.jp> (2006)

This file is part of Murasaki.

Murasaki is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Murasaki is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Murasaki.  If not, see <http://www.gnu.org/licenses/>.
*/

//////////////////
// timing tools
// timing.h
// Kris Popendorf
/////////////////

#include "timing.h"
#include <math.h>
#include <stdio.h>

bool timing_useHumanTime=true;

ostream& operator <<(ostream &os,const Timer &obj){
  return os << obj.asDouble();
}
  
double Timer::asDouble() const{
  return TIME_DOUBLE(tv);
}

double diff(const Timer &a,const Timer &b){
  return a.asDouble()-b.asDouble();
}

Timer operator*(const Timer &a,double factor){ //this had better already be derived by difference
  return Timer(TIME_DOUBLE(a.tv)*factor);
}

Timer operator*(double factor,const Timer &a){ //this had better already be derived by difference
  return Timer(factor*TIME_DOUBLE(a.tv));
}

Timer operator/(const Timer &a,double factor){ //this had better already be derived by difference
  return Timer(TIME_DOUBLE(a.tv)/factor);
}

Timer operator/(double factor,const Timer &a){ //this had better already be derived by difference
  return Timer(factor/TIME_DOUBLE(a.tv));
}

void Timer::reset(){
  gettimeofday(&tv,NULL);
}

Timer::Timer(){
  reset();
}

Timer operator- (const Timer &a,const Timer &b){
  Timer r(a);
  r-=b;
  return r;
}

Timer& Timer::operator-= (const Timer &a){
  tv.tv_sec-=a.tv.tv_sec;
  tv.tv_usec-=a.tv.tv_usec;
  if(tv.tv_usec<0){
    --tv.tv_sec;
    tv.tv_usec+=MILLION_INT;
  }
  return *this;
}

Timer operator+ (const Timer &a,const Timer &b){
  Timer r(a);
  r+=b;
  return r;
}

Timer& Timer::operator+= (const Timer &a){
  tv.tv_sec+=a.tv.tv_sec;
  tv.tv_usec+=a.tv.tv_usec;
  if(tv.tv_usec>=MILLION_INT){
    tv.tv_usec-=MILLION_INT;
    ++tv.tv_sec;
  }
  return *this;
}

Timer::Timer(double t){
  tv.tv_sec=(int)t;
  tv.tv_usec=(int)(t*MILLION_DOUBLE);
}

Timer::Timer(int sec,int usec)
{
  tv.tv_sec=sec;
  tv.tv_usec=usec;
}

string elapsed(Timer start,Timer stop){
  return humanTime((stop-start).asDouble());
}

string humanTime(double dur){
  char buf[300];
  string str;
  if(timing_useHumanTime){
    int days=(int)(dur/60/60/24);
    dur-=days*60*60*24;
    int hours=(int)(dur/60/60);
    dur-=hours*60*60;
    int mins=(int)(dur/60);
    dur-=mins*60;
    if(days){
      sprintf(buf,days==1 ? "%d day ":"%d days ",days);
      str.append(string(buf));
    }
    if(hours){
      sprintf(buf,hours==1 ? "%d hour ":"%d hours ",hours);
      str.append(string(buf));
    }
    if(mins){
      sprintf(buf,mins==1 ? "%d minute ":"%d minutes ",mins);
      str.append(string(buf));
    }
    if(dur || (!hours && !days && !mins)){
      sprintf(buf,dur==1 ? "%.3f second ":"%.3f seconds",dur);
      str.append(string(buf));
    }
    return str;
  }else{
    sprintf(buf,"%lf",dur);
    return string(buf);
  }
}
