/*
Copyright (C) 2006-2008 Keio University
(Kris Popendorf) <comp@bio.keio.ac.jp> (2006)

This file is part of Murasaki.

Murasaki is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Murasaki is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Murasaki.  If not, see <http://www.gnu.org/licenses/>.
*/

//////////////
// murasaki project
// geneparse.cc
// c++ implementation of the geneparse.pl program
//////////////

#include <iostream>
#include <fstream>
#include <assert.h>
#include "seqread.h"
#include <getopt.h>
#include <list>
#include <err.h>

#include <boost/regex.hpp>
#include <boost/lexical_cast.hpp>

using namespace std;

string program_version();
string program_help();

typedef pair<size_t,string> SubSeq;
size_t getSubSeqs(SequenceFileReader &reader,list<SubSeq> &output);
size_t getSubSeqs(SequenceFileReader &reader,list<SubSeq> &output,string filename);
void writeLenFile(ostream &os,size_t size,const list<SubSeq> &segments);

int main(int argc,char **argv){
  SequenceReader seqreader;
  bool verbose=false;
  bool useStdOut=false; //send output to stdout instead of disk

  ostream* os=&cout;
  string customOutput;

  int optc;
  while(1){
    //options struct:
    // name, has_arg, store_pointer, return_value
    static struct option long_options[] = {
      {"help",0,0,'h'},
      {"version",0,0,'V'},
      {"verbose",0,0,'v'},
      {"stdout",0,0,'c'},
      {0,0,0,0}
    };
    int longindex=0;
    string prefreq;
    optc=getopt_long(argc,argv,"hVvc",long_options,&longindex);
    if(optc==-1)break;
    switch(optc){
    case 'h': cout << program_help();exit(0);break;
    case 'V': cout << program_version();exit(0);break;
    case 'v': verbose=true;seqreader.verbose=true;break;
    case 'c': useStdOut=true;break;
    default: cerr << "Unknown option: "<<(char)optc<<endl; assert(0);break;
    }
  }


  if(argc<=optind){
    //read from stdin
    try {
      list<SubSeq> segments;
      size_t size=0;
      SequenceFileReader reader(cin);
      useStdOut=true; //only stdout when using stdin
      size=getSubSeqs(reader,segments);
      writeLenFile(cout,size,segments);
    }catch(SeqReadException e){
      cerr << "Error reading from stdin: "<<e.reason()<<endl;
    }
  }else{
    for(int i=optind;i<argc;i++){
      list<SubSeq> segments;
      size_t size=0;
      string file(argv[i]);
      string outfilename(file+".len");
      if(!useStdOut){
	os=new ofstream(outfilename.c_str());
	if(!os || !*os){
	  warn("Problem opening %s for writing. Skipping.",outfilename.c_str());
	  continue; //skip this file
	}
      }

      if(verbose)cerr << "Reading "<<file<<endl;
      try {
	SequenceFileReader reader(file,seqreader.options());
	size=getSubSeqs(reader,segments,file);
	writeLenFile(*os,size,segments);
      }catch(SeqReadException e){
	cerr << "Error reading "<<file<< ": "<<e.reason()<<endl;
	continue;
      }

      if(!useStdOut){ //cleanup filehandle
	((ofstream*)os)->close();
	delete os;
	os=&cout;
      }
    }
  }

  return 0;
}

size_t getSubSeqs(SequenceFileReader &reader,list<SubSeq> &output,string filename){
  //if we're given the filename, and it's a stitch file we can sneakily strip the
  //relevant data right out of the stitch file.
  if(reader.format()!=SEQFILE_STITCH)
    return getSubSeqs(reader,output);
  ifstream inf(filename.c_str());
  if(!inf)
    return getSubSeqs(reader,output);

  using namespace boost;
  const regex stitchLine("(\\S+)\\t(\\d+)\t(\\d+)\\t(\\d+)");
  
  string buf;
  size_t lastCoord=0;
  for(int linenum=1;inf && getline(inf,buf);linenum++){
    smatch results;
    if(!regex_match(buf,results,stitchLine)){
      warnx("Invalid stitch (%s) line %d: '%s'. Aborting stitch-read.",filename.c_str(),linenum,buf.c_str());
      output.clear();
      return getSubSeqs(reader,output);
    }
    
    try {
      output.push_back(SubSeq(lexical_cast<size_t>(results[2]),results[1]));
      lastCoord=lexical_cast<size_t>(results[4]);
    }catch(bad_lexical_cast e){
      warnx("Failed to parse stitch (%s) line %d: '%s'. %s",filename.c_str(),linenum,buf.c_str(),e.what());
      output.clear();
      return getSubSeqs(reader,output);
    }
  }
  return lastCoord;
}

size_t getSubSeqs(SequenceFileReader &reader,list<SubSeq> &output){
  size_t count=0,inSeg=0;
  long prevSubSeqId=reader.getSubSeqId();
  string prevSubSeqName=reader.getSubSeqName();

  while(reader.getc()){
    if(reader.getSubSeqId()!=prevSubSeqId){ //subseq changed!
      assert(inSeg>=1); //murasaki technically forbids 0 length sub-sequences from being recognized
      output.push_back(SubSeq(inSeg,prevSubSeqName));
      
      inSeg=-10; //this looks a bit hinky, but between every subseq there _should_ be exactly 10 Ns, so this accounts for that
      //      assert(c=='N'); //see? we're looking at an N!
      prevSubSeqId=reader.getSubSeqId();
      prevSubSeqName=reader.getSubSeqName();
    }
    count++;
    inSeg++;
  }
  assert(inSeg>=1); //murasaki technically forbids 0 length sub-sequences from being recognized
  output.push_back(SubSeq(inSeg,prevSubSeqName));
  return count;
}

void writeLenFile(ostream &os,size_t size,const list<SubSeq> &segments){
  os << size << endl;
  for(list<SubSeq>::const_iterator i=segments.begin();
      i!=segments.end();++i)
    os << i->first << "\t" << i->second << endl;
}

string program_help(){
  return string("\
Usage: getsegments [options...] [input] [input2 ...]\n\
\n\
Generates a .len file for each input sequence containing the names\n\
and lengths of each subsequence. These files aren't used by Murasaki\n\
itself, but several of the other supporting programs use/require them.\n\
\n\
Options:\n\
  --version|-V      - program version\n\
  --help|-h         - this message\n\
  --verbose|-v      - lots of extra details\n\
  --stdout|-c       - write output to stdout instead of input.len\n\
");
}
 
string program_version(){
  return string("1.0");
}
