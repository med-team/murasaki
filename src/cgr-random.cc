/*
Murasaki - multiple genome global alignment program
Copyright (C) 2006-2008 Keio University
(Kris Popendorf) <comp@bio.keio.ac.jp> (2006)

This file is part of Murasaki.

Murasaki is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Murasaki is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Murasaki.  If not, see <http://www.gnu.org/licenses/>.
*/

//////////////////
// generate a random sequence with markov probabilities derived from
// a CGR sequence
/////////////////

#include <iostream>
#include <memory.h>
#include <getopt.h>
#include <vector>
#include <map>
#include <list>
#include <boost/lexical_cast.hpp>
#include <boost/random.hpp>
#include <stdint.h> //for uint defs
#include <pstream.h>
#include <fstream>
#include <sstream>
#include "dinkymath.h"
#include "cgr.h"

using namespace std;
using boost::lexical_cast;
using boost::bad_lexical_cast;
boost::mt19937 randgen; //mersenne twister. long cycles, fast = wee

string program_help();
string program_version();
void genLots(size_t targetLength,vector<Cgr*> &mip,ostream *outs=&cout);
void genMipmap(Cgr &src,vector<Cgr*> &mip);
typedef uint64_t superpix; //should hold 4 pix

bool outRaw=false;
ostream* msg=&cout;

int main(int argc,char **argv){
  int optc;
  while(1){
    //options struct:
    // name, has_arg, store_pointer, return_value
    static struct option long_options[] = {
      {"help",0,0,'?'},
      {"version",0,0,'v'},
      {"raw",0,0,'r'}, //dont faformat
      {"quiet",0,0,'q'},
      {0,0,0,0}
    };
    int longindex=0;
    optc=getopt_long(argc,argv,"?v",long_options,&longindex);
    if(optc==-1)break;
    switch(optc){
    case 'v': cout << program_version();exit(-1);break;
    case 'r': outRaw=true;break;
    case 'q': msg=new ostringstream();break;
    default: cout << program_help();exit(1);break;
    }
  }
  
  vector<string> args;
  for(int seqi=optind;seqi<argc;seqi++){
    args.push_back(string(argv[seqi]));
  }
  if(args.size()<1){
    cerr << "You need to specify an input file."<<endl;
    cerr << program_help();exit(2);
  }
  if(args.size()<2){
    cerr << "You need to specify a length."<<endl;
    cerr << program_help();exit(2);
  }
  PgmBuffer srcPgm;
  try {
    srcPgm.loadPGM(args[0]);
  } catch(PGMFileException e){
    cerr << e.reason() << endl;
    cerr << args[0] << ": Could not load input file."<<endl;
    exit(-1);
  }
  *msg << "Inverting..."<<endl;
  srcPgm.invert();
  *msg << "Reverting to CGR..."<<endl;
  Cgr src(srcPgm);
  vector<Cgr*> mip(src.iter);
  *msg << "Generating mipmap..."<<endl;
  genMipmap(src,mip);
  size_t targetLength;
  try {
    targetLength=lexical_cast<unsigned>(args[1]);
    *msg << "Making a "<<targetLength<<"bp sequence...\n"<<endl;
  }
  catch(bad_lexical_cast& e) {
    *msg << "Invalid length: "<<args[1]<<endl;
    exit(-1);
  }
  
  ostream *outp=&cout;
  if(args.size()>2){
    *msg << "Writing output to: "<<args[2]<<endl;
    if(outRaw){
      *msg << "Format: raw\n";
      outp=new ofstream(args[2].c_str());
    }else{
      *msg << "Format: Fasta\n";
      vector<string> argv;
      argv.push_back(string("--name=random-")+args[0]);
      argv.push_back("-");
      argv.push_back(args[2]);
      outp=new redi::opstream("./faformat.pl",argv);
    }
  }
  genLots(targetLength,mip,outp);
  if(outp==&cout)
    cout << endl;
  else {
    *msg << "Done!\n";
    delete outp; //needs to flush/close for files
  }
}

void genMipmap(Cgr &src,vector<Cgr*> &mip) {
  int i=src.iter-1;
  mip.resize(i+1);
  mip[i--]=&src;
  Cgr *prev=&src;
  for(;i>=0;i--){
    Cgr *subCgr=new Cgr(*prev,true);
    pair<pix,pix> lowhigh(subCgr->findNiceRange());
    assert(cout << "Low: " << lowhigh.first << " High: "<< lowhigh.second << endl);
    mip[i]=subCgr;
    prev=subCgr;
  }
#ifndef NDEBUG
  for(unsigned i=0;i<mip.size();i++){
    assert(mip[i]);
  }
#endif
  *msg << mip.size() << " level(s) of detail."<<endl;
}

void genLots(size_t targetLength,vector<Cgr*> &mip,ostream *outs){
  string context;
  vector<string> opts(4);
  opts[0]=string("C");
  opts[1]=string("G");
  opts[2]=string("A");
  opts[3]=string("T");
  for(size_t done=0;done<targetLength;done++){
    assert(cout << "Context is: "<<context<<" (length: "<<context.length()<<")"<<endl);
    Cgr* sampler=mip[context.length()];
    superpix prob[4];
    for(int i=0;i<4;i++){
      assert(sampler);
      assert(sampler->iter==(int)context.length()+1);
      prob[i]=(*sampler)[context+opts[i]];
    }
    assert(cout <<"Probabilities: ");
#ifndef NDEBUG
    copy(prob,prob+4,ostream_iterator<superpix>(cout," "));
#endif
    assert(cout << endl);
    boost::uniform_int<superpix> range(0,prob[0]+prob[1]+prob[2]+prob[3]);
    boost::variate_generator<boost::mt19937&, boost::uniform_int<superpix> > chooser(randgen,range);
    superpix choice=chooser(); //1 random int from 0 to that sum
    assert(cout << "Rand is "<<choice<<endl);
    int chosen=3;
    for(int i=0;i<3;i++)
      if(choice<prob[i]){
	chosen=i;
	break;
      }else{
	choice-=prob[i];
      }
    assert(chosen<4);
    assert(chosen>=0);
    assert(cout << "new char: "<<opts[chosen]<<endl);
    *outs << opts[chosen];

    context+=opts[chosen];
    if(context.length()>=mip.size()){
      context.erase(0,1); //chop one off the front
    }
    assert(!(context.length()>=mip.size())); //dont have to loop (by IH)
  }
}

string program_help(){
  return string("\
Usage: cgr-random [options] <cgr-source> <length> [output]\n\
\n\
Generates a string random DNA sequence based on the markov\n\
model described by the inputted CGR.\n\
\n\
Options\n\
*Takes an option (like --maxres 3 or -m3)\n\
\n\
*Toggles: (just --merge or -b)\n\
");
}

string program_version(){
  return string("0.1");
}

