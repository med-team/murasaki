/*
Copyright (C) 2006-2008 Keio University
(Kris Popendorf) <comp@bio.keio.ac.jp> (2006)

This file is part of Murasaki.

Murasaki is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Murasaki is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Murasaki.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef _HASHING_H_
#define _HASHING_H_

#include "globaltypes.h"
#include "dinkymath.h"
#include <algorithm>
#include <assert.h>
#include <iostream>

template<typename T>
class SBox {
public:
  static const size_t SBOX_BOXBYTES=(sizeof(T));
  static const size_t SBOX_BOXSIZE=(((size_t)1)<<(SBOX_BOXBYTES*8));
  static const size_t SBOX_BOXBITS=(SBOX_BOXBYTES*8);
  //  typedef T (*BoxType_p)[SBOX_BOXSIZE]; //craziest syntax evar, eh?
  T typedef BoxType[SBOX_BOXSIZE];
protected:
  int boxCount;
  BoxType *boxes;
  //  T (*boxes)[SBOX_BOXSIZE];
public:
  SBox(int hashbits) :
    boxCount((hashbits+SBOX_BOXBITS-1)/SBOX_BOXBITS), //number of bytes rounded up
    boxes(new T[boxCount][SBOX_BOXSIZE])
  {
    using namespace std;
    
    assert(boxes);
    for(int b=boxCount-1;b>=0;b--){
      size_t boxbits=((b+1)*SBOX_BOXBITS>(size_t)hashbits) ? (hashbits%SBOX_BOXBITS):SBOX_BOXBITS;
      size_t boxsize=(boxbits==SBOX_BOXBITS) ? SBOX_BOXSIZE:(1<<boxbits);
      cout << "SBox "<<b<<" has "<<boxsize<<" x "<<sizeof(T)<<" byte entries ("<<humanMemory(boxsize*sizeof(T))<< ") for "<<boxbits<<" bits." <<endl;
      for(size_t i=0;i<(size_t)boxsize;i++)
	boxes[b][i]=i;
      random_shuffle(boxes[b],boxes[b]+boxsize);
    }
  }

  ~SBox(){
    assert(boxes);
    delete[] boxes;
    boxes=0;
  }

  word confuse(word in){
    word out=0;
    word mask=lowN(SBOX_BOXBITS);
    word shift=0; 
    for(BoxType *box=boxes;box!=boxes+boxCount;box++,shift+=SBOX_BOXBITS,mask<<=SBOX_BOXBITS){
      out|=(word)((*box)[( (in & mask) >> shift )]) << shift;
    }
    return out;
  }
};

#endif
