/*
Copyright (C) 2006-2008 Keio University
(Kris Popendorf) <comp@bio.keio.ac.jp> (2006)

This file is part of Murasaki.

Murasaki is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Murasaki is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Murasaki.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef ECOLIST__H
#define ECOLIST__H

#include "sequence.h"
#include <list>

#define MAXSUBNODES

typedef union block_t block;

union block_t {
  word w;
  block_t* p;
};

class Ecolist {
 public:
  typedef pair<SeqPos,SeqPos> val_type;
  typedef class BidirectionalEcolistIterator {
  public:
    typedef BidirectionalEcolistIterator _Self;

    block *current;
    list<block*> prev;
    SeqPos pos,ite,end;
    BidirectionalEcolistIterator(block *start,list<block*> prev,SeqPos p,SeqPos ite);
    void put(SeqPos seq,SeqPos idx) const;
    void put(const val_type &v) const;
    val_type operator*() const;
    val_type get() const; //identical to operator*, just there for symmetry with put
    _Self& operator++();
    _Self operator++(int);
    _Self& operator--();
    _Self operator--(int);
    bool operator!=(const _Self&) const;
    bool operator==(const _Self&) const;
    inline SeqPos to_i() const {SeqPos ret=pos;for(SeqPos j=ite;j>0;j--)ret+=minChunks<<(j-1);return ret;}
  } bi_iterator;

  typedef class EcolistIterator {
  public:
    typedef EcolistIterator _Self;

    block *current;
    SeqPos pos,ite,end;
    EcolistIterator(block *start,SeqPos p,SeqPos ite);
    EcolistIterator(const BidirectionalEcolistIterator&);
    void put(SeqPos seq,SeqPos idx) const;
    void put(const val_type &v) const;
    val_type operator*() const;
    val_type get() const; //identical to operator*, just there for symmetry with put
    _Self& operator++();
    _Self operator++(int);
    bool operator!=(const _Self&) const;
    bool operator==(const _Self&) const;
    inline SeqPos to_i() const {SeqPos ret=pos;for(SeqPos j=ite;j>0;j--)ret+=minChunks<<(j-1);return ret;}
  } iterator;


  //blocks are allocated in blobs (of some 2^n*initBlocks)
  //blocks are composed of chunks.
  //chunks might straddle word/block boundaries.
  static SeqPos minBlocks,minChunks;
  static unsigned seqbits,idxbits,chunkSize;
  static word idxLongest;
  static SeqPos initBlocks;
  static bool inited;
  static bool (*lessthan)(const pair<SeqPos,SeqPos>&,const pair<SeqPos,SeqPos>&);
  static SeqPos externalSortLimit;
  static size_t cost(size_t chunks);
  
  SeqPos size;
  block *blocks;

  //inity stuff
  Ecolist();
  ~Ecolist();
  static void init(SeqPos seqs,word longest,word total,SeqPos weight,bool (*_lt)(const val_type&,const val_type&),bool frugal);

  //accessy-stuff
  pair<SeqPos,SeqPos> getValAt(SeqPos idx);
  iterator begin();
  iterator end();
  bi_iterator bi_begin();
  bi_iterator bi_end();
  SeqPos capacity();
  inline SeqPos getSize(){return size;}
  iterator at(SeqPos pos);
  bi_iterator bi_at(SeqPos pos);

  //take care only to use on sorted lists...
  inline iterator lower_bound(const val_type& m){return at(int_lower_bound(m));}
  inline iterator upper_bound(const val_type& m){return at(int_upper_bound(m));}
  inline bi_iterator bi_lower_bound(const val_type& m){return bi_at(int_lower_bound(m));}
  inline bi_iterator bi_upper_bound(const val_type& m){return bi_at(int_upper_bound(m));}
  SeqPos int_lower_bound(const val_type& m); //first elem not <m
  SeqPos int_upper_bound(const val_type& m); //first elem x where m<x (or end())
  inline pair<iterator,iterator> equal_range(const val_type& m){return pair<iterator,iterator>(lower_bound(m),upper_bound(m));}

  //manipulators
  void clear(); //erase and reset
  void putValAt(SeqPos idx,SeqPos seq,SeqPos pos);
  void putValAt(SeqPos idx,const val_type&);
  void push(SeqPos seq,SeqPos pos);
  void swap(iterator &a,iterator &b);
  inline void sort(){if(size>externalSortLimit)inPlaceSort(); else externalSort();}
  void inPlaceSort(); //in place sort. might be necessary if size becomes crazy-big
  void externalSort(); //this is faster, but takes more memory

  //behind the scenes mojo
 protected:
  block* findBlockFor(SeqPos &remain);
  static void eraseAll(block* root,SeqPos ite);
  static block* allocBlob(SeqPos ite);
  static void writeBits(block* root,SeqPos loc,SeqPos offset,SeqPos size,SeqPos val);
  static SeqPos readBits(block* root,SeqPos loc,SeqPos offset,SeqPos size);

  SeqPos partition(SeqPos left,SeqPos right,SeqPos pivoti);
  void qsort(SeqPos left,SeqPos right);

  friend ostream& operator<<(ostream& os,Ecolist& a);
};

size_t operator-(const Ecolist::iterator &a,const Ecolist::iterator &b);
size_t operator-(const Ecolist::bi_iterator &a,const Ecolist::bi_iterator &b);

#endif

