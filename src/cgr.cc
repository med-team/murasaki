/*
Copyright (C) 2006-2008 Keio University
(Kris Popendorf) <comp@bio.keio.ac.jp> (2006)

This file is part of Murasaki.

Murasaki is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Murasaki is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Murasaki.  If not, see <http://www.gnu.org/licenses/>.
*/

/////////////////////
//Krisp - CGR Support Class
//////////////////

#include "cgr.h"
#include "dinkymath.h"
#include "mingw32compat.h"
#include <iostream>
#include <fstream>
#include <memory.h>
#include <iterator>
#include <utility>
#include <limits.h>
#include <stdio.h> //for fopen for libpgm
#include <vector>
#include <strings.h>
#include <sstream>

//multiple versions of spirit floating around there. annoying.
#include <boost/version.hpp>
#if BOOST_VERSION >= 103800
//use spirit "classic"
#if !defined(BOOST_SPIRIT_USE_OLD_NAMESPACE)
#define BOOST_SPIRIT_USE_OLD_NAMESPACE
#endif

#include <boost/spirit/include/classic_core.hpp>
#include <boost/spirit/include/classic_attribute.hpp>
#include <boost/spirit/include/classic_symbols.hpp>
#include <boost/spirit/include/classic_chset.hpp>
#include <boost/spirit/include/classic_escape_char.hpp>
#include <boost/spirit/include/classic_confix.hpp>
#include <boost/spirit/include/classic_iterator.hpp>
#include <boost/spirit/include/classic_exceptions.hpp>
#else
//current is "classic"
#include <boost/spirit/core.hpp>
#include <boost/spirit/attribute.hpp>
#include <boost/spirit/symbols/symbols.hpp>
#include <boost/spirit/utility/chset.hpp>
#include <boost/spirit/utility/escape_char.hpp>
#include <boost/spirit/utility/confix.hpp>
#include <boost/spirit/iterator.hpp>
#include <boost/spirit/error_handling/exceptions.hpp>
#endif

using namespace std;

int debugswitch=0;
void debugme(){debugswitch=!debugswitch;}
unsigned long seedcount;
const pix maxout=PGM_OVERALLMAXVAL;
bool fullWidthBinary=true; //because some programs suck and don't support 16-bit binary PGMs

Cgr::Cgr():
  iter(1),rez(2),data(new pix[rez*rez]),normalized(false)
{
  memset(data,0,sizeof(pix)*rez*rez);
  cout << "Making a new CGR of default rez "<<rez<<endl;
  if(!data){
    cerr << "Couldn't create pixel buffer...!"<<endl;
    exit(-2);
  }
}

Cgr::Cgr(int iter):
  iter(iter),rez(1<<iter),data(new pix[rez*rez]),normalized(false)
{
  memset(data,0,sizeof(pix)*rez*rez);
  cout << "Making a new CGR of specific rez "<<rez<<endl;
  if(!data){
    cerr << "Couldn't create pixel buffer...!"<<endl;
    exit(-2);
  }
}

Cgr::Cgr(const Cgr& a):
  iter(a.iter+1),rez(a.rez*2),data(new pix[rez*rez]),normalized(false)
{
  if(!data){
    cerr << "Couldn't create pixel buffer...!"<<endl;
    exit(-2);
  }
  //cout << "Next CGR iteration "<<iter<<" ("<<rez<<") "<<endl;
  memset(data,0,sizeof(pix)*rez*rez);
}

Cgr::Cgr(const Cgr& a,bool dohalve):
  iter(a.iter-1),rez(a.rez/2),data(new pix[rez*rez]),normalized(false)
{
  if(!data){
    cerr << "Couldn't create pixel buffer...!"<<endl;
    exit(-2);
  }
  //  cout << "Next CGR mipmap "<<iter<<" ("<<rez<<") "<<endl;
  memset(data,0,sizeof(pix)*rez*rez);
  halve(a);
}


Cgr::Cgr(const PgmBuffer& a):
  iter(ceil_log2_loop((long)a.rez)-1),rez(a.rez),data(new pix[rez*rez]),normalized(true)
{
  if(!data){
    cerr << "Couldn't create pixel buffer...!"<<endl;
    exit(-2);
  }
  //  cout << "Importing CGR from PGM. Size: "<<iter<<" ("<<rez<<") "<<endl;
  copy(a.data.begin(),a.data.end(),data);
}

Cgr::~Cgr(){
  if(data){
    delete data;
    data=0;
  }
}

void Cgr::merge(Cgr& a){
  int redist=(unsigned long)(rez*rez)<=seedcount;
  redist=0;
  cout << "Merging i"<<a.iter<<" and i"<<iter<<endl;
  a.normalize();
  normalize();

  for(pix *here=data,*there=a.data,*end=here+rez*rez;
      here<end;){
    assert(here<end);
    for(pix *rowend=there+a.rez;there!=rowend;there++){ //make one row
      pix val=*there;
      //cout << "here "<<(here-data)<<" += "<<(there-a.data)<<endl;
      *(here++)+=val;
      //cout << "here "<<(here-data)<<" += "<<(there-a.data)<<endl;
      *(here++)+=val;
    }
    there-=a.rez; //back to row start...
    for(pix *rowend=there+a.rez;there!=rowend;there++){ //make one row
      pix val=*there;
      //cout << "here "<<(here-data)<<" += "<<(there-a.data)<<endl;
      *(here++)+=val;
      //cout << "here "<<(here-data)<<" += "<<(there-a.data)<<endl;
      *(here++)+=val;
    }
  }
  normalized=false;
}

void Cgr::halve(const Cgr& a){
  //  cout << "Halving i"<<a.iter<<" to make i"<<iter<<endl;
  for(int y=0;y<rez;y++){
    for(int x=0;x<rez;x++){
      int x2=x*2,y2=y*2*a.rez;
      data[x+y*rez]=a.data[x2+y2]/4+a.data[x2+y2+1]/4+a.data[x2+y2+a.rez]/4+a.data[x2+y2+a.rez+1]/4;
    }
  }
  normalized=false;
}

pix* Cgr::operator[](int row){ //row access
  return data+rez*row;
}

string itodna(unsigned long idx,const int &iter,const int &rez){
  int x=(idx & (~(((unsigned long)-1)<<iter))),y=idx>>iter;
  string ret;
  for(int i=0;i<iter;i++){
    switch((y & 1)<<1 | (x & 1)){
    case 0:ret+="C";break;
    case 1:ret+="G";break;
    case 2:ret+="A";break;
    case 3:ret+="T";break;
    default: break;//this doesnt happen
    }
    x>>=1;y>>=1;
  }
  reverse(ret.begin(),ret.end());
  return ret;
}

unsigned long dnatoi(const char *dna,const int &iter,const int &rez){
  int x=0,y=0;
  for(int i=0;i<iter;i++){
    x<<=1;y<<=1;
    switch(dna[i]){
    case 'c':case 'C':break;
    case 'g':case 'G':x++;break;
    case 'a':case 'A':y++;break;
    case 't':case 'T':x++;y++;break;
    default:
      //      x>>=1;y>>=1; //retry!
      return ~0; //disaster!
    }
  }
  /*
  if(debugswitch){
    char buf[1024];
    strncpy(buf,dna,iter);
    buf[iter]=0;
    cout << buf << " maps to (" << x << ","<< y << ")"<<endl;
  }
  */
  return (y<<iter)+x; //oh ho! << is lower than addition!!
}

pix& Cgr::operator[](const char* str){ //point access
  return data[dnatoi(str,iter,strlen(str))];
}

pix& Cgr::operator[](const string& str){ //point access
  unsigned long idx=dnatoi(str.c_str(),iter,str.size());
  assert(idx<(unsigned long)rez*(unsigned long)rez);
  return data[idx];
}

bool Cgr::savePGM(string file,bool binary){
  normalize();
  ofstream of((file+".pgm").c_str(),ios::out | (binary ? ios::binary : ios::out));
  if(of.bad()){
    cerr << "Error opening "<<file<<endl;
    return false;
  }
  of << (binary ? "P5":"P2") <<endl
     << "#"<<file<<endl
     << rez << " " << rez << endl;

  if(binary){
    if(fullWidthBinary){
      of << maxout << endl;
      gray buf;
      pix* end=data+rez*rez;
      for(pix* i=data;i!=end;i++){
	buf=maxout-*i;
	of.write((char*)&buf,sizeof(gray));
      }
    }else{
      of << "255" << endl;
      uint8_t buf; //gimp (and many others) only support binary at 8bit
      pix* end=data+rez*rez;
      for(pix* i=data;i!=end;i++){
	buf=(maxout-*i)>>8;
	of.write((char*)&buf,1);
      }
    }
  }else{
    of << maxout << endl;
    of << *this;
  }

  return true;
}

ostream& operator<<(ostream& of,const Cgr &a){
  for(int y=0;y<a.rez;y++){
    pix* row=a.data+a.rez*y;
    for(pix *i=row,*end=row+a.rez;i!=end;i++)
      of << (maxout-*i) << " ";
    of << "\n";
  }
  return of;
}

bool Cgr::savePNG(string file){
  //  cout << "Writing uncompressed PGM version..."<<endl;
  savePGM(file);
  //  cout << "Converting PGM to "<<(file+".png")<<" with ImageMagick"<<endl;
  string command("convert "+file+".pgm "+file+".png");
  cout << "ImageMagick: " << command << endl;
  int res;
  res=system(command.c_str());
  command=string("rm "+file+".pgm");
  //  cout << "Deleting temporary PGM file..."<<endl;
  res=system(command.c_str());
  return true;
}

void Cgr::load(const char* str,size_t size){
  cout << "Computing word frequencies...\n";
  const char* end=str+size-iter+1;
  while(str<end){
    unsigned long idx=dnatoi(str,iter,rez);
    //    char tmp[500];
    //    strncpy(tmp,str,iter);
    //    tmp[iter]=0;
    //    cout << tmp << " -> " << idx <<endl; 
    if(idx!=~(unsigned long)0)
      data[idx]++;
    assert(idx<(unsigned long)(rez*rez)); //dont be dumb
    str++;
  }
  normalized=false;
}

void Cgr::normalize(){
  if(normalized)return;
  cout << "Normalizing i"<<iter<<endl;
  pair<pix,pix> lowhigh(findNiceRange());
  cout << "Low: "<<lowhigh.first<<" high: "<<lowhigh.second<<endl;
  double range=(double)(lowhigh.second-lowhigh.first);
  for(pix *i=data,*end=i+rez*rez;i<end;i++){
    long val=(unsigned)(((double)(*i-lowhigh.first)/range)*maxout);
    val=min<long>(val,maxout);
    val=max<long>(val,0);
    *i=val;
  }
  normalized=true;
}

pair<pix,pix> Cgr::sampleHighLow(double lowp,double highp){
  const unsigned size=rez*rez;
  pix *alt=new pix[size];
  if(!alt){
    cerr << "Out of memory!\n"<< endl;
    exit(-3);
  }
  memcpy(alt,data,size*sizeof(pix));
  sort(alt,alt+size-1);
  pix low=alt[(unsigned)((float)size*lowp)],high=alt[(unsigned)((float)size*highp)];

  delete alt;
  return pair<pix,pix>(low,high);
}

pair<pix,pix> Cgr::sampleHighLow(int lown,int highn){
  const unsigned size=rez*rez;
  pix *alt=new pix[size];
  if(!alt){
    cerr << "Out of memory!\n"<< endl;
    exit(-3);
  }
  memcpy(alt,data,size*sizeof(pix));
  sort(alt,alt+size-1);
  pix low=(lown>=0 ? alt[lown]:0),high=(highn>=0 ? alt[size-highn-1]:~(pix)0);

  delete alt;
  return pair<pix,pix>(low,high);
}

void Cgr::findEdges(const pair<pix,pix> &p,vector<unsigned long> *low,vector<unsigned long> *high){
  for(pix *i=data,*end=i+rez*rez;i<end;i++){
    if(*i<=p.first){
      if(low)
	low->push_back(i-data);
    }
    else if(*i>=p.second)
      if(high)
	high->push_back(i-data);
  }
}

pix Cgr::get(unsigned idx){
  return data[idx];
}

pair<pix,pix> Cgr::findNiceRange(){
  const unsigned size=rez*rez;
  pix *alt=new pix[size];
  if(!alt){
    cerr << "Out of memory!\n"<< endl;
    exit(-3);
  }
  memcpy(alt,data,size*sizeof(pix));
  sort(alt,alt+size-1);
  pix low=alt[(unsigned)((float)size*.01)],high=alt[(unsigned)((float)size*.99)];
  /*
  cout << "Source: "<<endl;
  copy(data,data+size,ostream_iterator<pix>(cout," "));
  cout << "sorted: "<<endl;
  copy(alt,alt+size,ostream_iterator<pix>(cout," "));
  */

  for(uint i=(uint)((float)size*.99);low==high && i<size;i++) //degenerate cases...
    high=alt[i];
  if(low==high)
    high++;
  delete alt;
  return pair<pix,pix>(low,high);
}

PgmBuffer::PgmBuffer()
  : data(0),rez(0)
{
  //boring
}

int PgmBuffer::loadCgr(Cgr& a){
  if(!data.empty())
    return -1; //already loaded
  rez=a.rez;
  /* in a world where netPBM worked...
  data=new gray*[rez]; //make rez rows
  for(int y=0;y<rez;y++){
    data[y]=new gray[rez];
    for(int x=0;x<rez;x++)
      data[y][x]=a[y][x];
  }
  */
  data.reserve(rez*rez);
  for(pix *i=a.data;i!=a.data+rez*rez;i++)
    data.push_back(maxout-*i);
  
  return 0;
}

int PgmBuffer::loadPGM(string file){
  using namespace boost::spirit;
  int width,height;
  if(!data.empty()){
    throw PGMFileException("I already have data thank you...");
  }
  const int maxline=102400;
  char line[maxline];
  int linenum=0;
  int state=0;
  ifstream inf(file.c_str());
  int wordwidth=1;
  if(!inf)
    PGMFileException("Couldn't read file",file,linenum);
  do{
    if(inf.bad())break; //game over
    linenum++;
    parse_info<char const*> info;
    switch(state){
    case 0: //magicnumber
      inf.read(line,2);
      line[2]=0;
      if(!strcmp("P5",line))
	return loadBinaryPGM(file);
      else if(strcmp("P2",line))
	throw PGMFileException("Not a (plain) PGM file",file,linenum);
      state++;
      inf.getline(line,maxline); //got to chomp that remaining newline
      break;
    case 1: //header - dimensions
      if(line[0]=='#')break; //comment
       info =
	parse(line, int_p[assign(width)] >> int_p[assign(height)],space_p);
      if(!info.full)
	throw PGMFileException("Couldn't parse dimensions",file,linenum);
      state++;
      if(width!=height)
	throw PGMFileException("Width!=Height???",file,linenum);
      rez=width;
      data.reserve(rez*rez);
      break;
    case 2: //header - maxval
      if(line[0]=='#')break; //comment
      info =
	parse(line, int_p[assign(maxval)], space_p);
      if(!info.full)
	throw PGMFileException("Couldn't parse maxval",file,linenum);
      if(maxval>255)
	wordwidth=2;
      state++;
      break;
    case 3: //data
      info =
	parse(line,*(uint_p[push_back_a(data)]),space_p);
      if(!info.full)
	throw PGMFileException("Couldn't read data...",file,linenum);
      break;
    }
    inf.getline(line,maxline);
  }while(inf);
  
  if(data.size()<(uint)(height*width))
    throw PGMFileException("Didn't get enough data points...",file,data.size());

  return 0;
}

int PgmBuffer::loadBinaryPGM(string file){
  using namespace boost::spirit;
  int width,height;
  gray maxval;
  if(!data.empty()){
    throw PGMFileException("I already have data thank you...");
  }
  const int maxline=1024;
  char line[maxline];
  int linenum=0;
  int state=0;
  int wordwidth=1;
  ifstream inf(file.c_str(),ios::in | ios::binary);
  if(!inf)
    PGMFileException("Couldn't read file",file,linenum);
  inf.read(line,2);
  line[2]=0;
  do{
    if(inf.bad())break; //game over
    linenum++;
    parse_info<char const*> info;
    switch(state){
    case 0: //magicnumber
      if(strcmp("P5",line))
	throw PGMFileException("Not a (binary) PGM file",file,linenum);
      state++;
      inf.getline(line,maxline); //kill that remaining last line
      break;
    case 1: //header - dimensions
      inf.getline(line,maxline);
      if(line[0]=='#')break; //comment
       info =
	parse(line, int_p[assign(width)] >> int_p[assign(height)],space_p);
      if(!info.full)
	throw PGMFileException("Couldn't parse dimensions",file,linenum);
      state++;
      if(width!=height)
	throw PGMFileException("Width!=Height???",file,linenum);
      rez=width;
      data.reserve(rez*rez);
      break;
    case 2: //header - maxval
      inf.getline(line,maxline);
      if(line[0]=='#')break; //comment
      info =
	parse(line, int_p[assign(maxval)], space_p);
      if(!info.full)
	throw PGMFileException("Couldn't parse maxval",file,linenum);
      if(maxval>255)
	wordwidth=2;
      state++;
      break;
    case 3: //data
      inf.read(line,wordwidth);
      if(inf.good())
	data.push_back(*(gray*)(line));
      switch(maxval){
      case maxout:break;
      case 255: data.back()<<=8; break; //faster
      case 127: data.back()<<=9; break;
      default: data.back()*=(maxout/maxval);break;//ugly, but it'll work
      }
      break;
    }
  }while(inf);
  
  if(data.size()<(uint)(height*width))
    throw PGMFileException("Didn't get enough data points...",file,data.size());

  return 0;
}

void PgmBuffer::invert(){
  for(vector<gray>::iterator i=data.begin();i!=data.end();i++){
    *i=(maxval-*i);
  }
}

int PgmBuffer::savePGM(string file,bool binary){
  if(data.empty()){
    cerr << "No data to write???"<<endl;
    exit(-1);
  }

  ofstream of((file+".pgm").c_str(),ios::out | (binary ? ios::binary : ios::out));
  if(of.bad()){
    cerr << "Error opening "<<file<<endl;
    return false;
  }
  of << (binary ? "P5":"P2") <<endl
     << "#"<<file<<endl
     << rez << " " << rez << endl;

  if(binary){
    if(fullWidthBinary){
      of << maxout << endl;
      for(vector<gray>::iterator i=data.begin();i!=data.end();i++){
	of.write((char*)&(*i),sizeof(gray));
      }
    }else{
      of << 255 << endl;
      uint8_t buf;
      for(vector<gray>::iterator i=data.begin();i!=data.end();i++){
	buf=(*i>>8);
	of.write((char*)&(buf),1);
      }
    }
  }else{
    vector<gray>::iterator i=data.begin();
    for(int y=0;y<rez;y++){
      for(int x=0;x<rez;x++)
	of << *i++ << " ";
      of << endl;
    }
  }

  return 0;
}

PgmBuffer::~PgmBuffer(){
}

PGMFileException::PGMFileException(const string& reason, const string& filename,
				   unsigned int line)
{
  ostringstream oss;
  oss << "Error in file '" << filename 
      << "', line " << line << ": "
      << reason;
  reason_ = oss.str();
}

gray PgmBuffer::average(){
  double sum=0;
  for(vector<gray>::iterator i=data.begin();i!=data.end();i++)
    sum+=(double)(*i);
  sum/=(double)data.size();
  return (gray)(sum);
}

double PgmBuffer::difference(vector<PgmBuffer*> others){
  double avg=0;
  double sum=0;
  double variance=0;
  if(!allSameRez(others))
    throw PGMException("Buffers differ in size");

  for(unsigned i=0;i<data.size();i++){
    avg=data[i];
    for(vector<PgmBuffer*>::iterator buf=others.begin();buf!=others.end();buf++){
      avg+=(*buf)->data[i];
    }
    avg/=others.size()+1;
    variance=sqr(data[i]-avg);
    for(vector<PgmBuffer*>::iterator buf=others.begin();buf!=others.end();buf++)
      variance+=sqr((*buf)->data[i]-avg);
    sum+=sqrt(variance);
  }
  return sum/((double)(rez*rez))/(double)maxout;
}

bool PgmBuffer::allSameRez(vector<PgmBuffer*> others){
  for(vector<PgmBuffer*>::iterator i=others.begin();i!=others.end();i++)
    if((*i)->rez!=rez)
      return false;
  return true;
}

void invert(string& str){
  for(unsigned i=0;i<str.length();i++){
    switch(str[i]){
    case 'a':case 'A':str[i]='t';break;
    case 'g':case 'G':str[i]='c';break;
    case 'c':case 'C':str[i]='g';break;
    case 't':case 'T':case 'u':case 'U':str[i]='a';break;
    default:;
    }
  }
}

