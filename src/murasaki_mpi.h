/*
Copyright (C) 2006-2008 Keio University
(Kris Popendorf) <comp@bio.keio.ac.jp> (2006)

This file is part of Murasaki.

Murasaki is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Murasaki is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Murasaki.  If not, see <http://www.gnu.org/licenses/>.
*/

//////////////
// murasaki project
// murasaki_mpi.h
// provides MPI-specific functions
//////////////

#ifdef MURASAKI_MPI
#ifndef MURASAKI_MPI_H__
#define MURASAKI_MPI_H__ 1

#include "sequence.h"
#include "dinkymath.h"
#include <mpi.h>
#include <vector>
#include <list>

using namespace std; //oof. once you start, you can't stop.

//signals
#define MURASAKI_MPI_REQ_SIZE 0
#define MURASAKI_MPI_REQ_LKUP_NOMATCH 1
#define MURASAKI_MPI_REQ_LKUP_MATCH 2
#define MURASAKI_MPI_REQ_QUIT  999

//types
enum MPI_Job {JOB_HASHER=0,JOB_STORAGE,JOB_MAX};

class MPI_HashMessage {
 public:
  word key;
  SeqIdx seqno;
  SeqPos pos;
  MPI_HashMessage(const word& key,const Location& l);
  void set(const word& key,const Location& l);
  MPI_HashMessage();
 private:
};

//have to tell MPI what these things are
extern MPI_Datatype MPI_HashMessage_type;

//for some reason large values cause large problems on mx (stops receiving after the first packet)
const int mpi_bufferSize=50000;

class MessageBlock {
 public:
  int used;
  int stored;
  MPI_HashMessage *messages;
  MPI_Request request;
  void reset();
  MessageBlock();
  ~MessageBlock();
  MessageBlock(const MessageBlock &a); //fake-copy
};

class MPI_AnchorHeader {
 public:
  word hashKey;
  word count;
  MPI_AnchorHeader(word key);
  MPI_AnchorHeader();
};

class AnchorBlock {
 public:
  vector<Location> locs;
  MPI_AnchorHeader header;
  MPI_Request headerReq,bodyReq;
  int headerStored,bodyStored;
  int owner;

  AnchorBlock();
  AnchorBlock(word hashKey);
  AnchorBlock(const AnchorBlock &a); //fake-copy
};

class GlobalHashPointIterator {
 public:
  typedef GlobalHashPointIterator _self;
  GlobalHashPointIterator(); //start at 0
  GlobalHashPointIterator(word x); //start at x
  word counted() const;
  word span() const; //size of region covered here
  pair<word,Location> operator*() const;
  _self& operator++();
  bool atLast() const;
  bool sanityCheck() const;
  void seek(word x); //goto first global region prior to "globalPos"
  string debugString() const;
 protected:

  word _count;
  unsigned _seqi;
  BitSequence* _bs;
  unsigned _regi;
};

//globals

extern int mpi_id;
extern int mpi_procs;
extern bool mpi_capable;
extern word mpi_totalMemory,mpi_totalStorage;
extern vector<double> mpi_storeShare;
extern vector<pair<word,word> > mpi_storeOwn;
extern map<word,int> mpi_storeBrk; // start of hash region -> mpi rank
extern map<word,int> mpi_storeBrkStop; // stop+1 of hash region -> id
extern word mpi_myStoreOffset;
extern int mpi_finalAssembler;

extern vector<string> mpi_hostnames;
extern map<string,int> mpi_hostLeader_byName;
extern vector<int> mpi_hostLeader;//lowest id on same host
extern bool mpi_isHostLeader,mpi_usingShm;

extern vector<long> mpi_hashCount,mpi_storeCount,mpi_extractCount,mpi_mergeCount,mpi_extractLocCount,mpi_anchorSendCount,mpi_anchorRecvCount;
extern vector<double> mpi_workTime;

extern vector<double> mpi_hashShare;
extern vector<pair<word,word> > mpi_hashOwn;
extern map<word,int> mpi_hashBrk; //start of sequence -> mpi rank
extern map<word,Location> mpi_hashPoints; //maps each hashBrk to an actual Location

extern word mpi_total_hash_size;
extern vector<word> mpi_worldMemory;
extern vector<MPI_Job> mpi_jobs;
extern vector<int> mpi_jobCount;
extern const char *MPI_jobNames[];
extern vector<int> mpi_hasherIds;
extern int mpi_myHasherId;
extern vector<int> mpi_worldId2jobId;
extern vector<int> mpi_assemblerIds;
extern int mpi_finalAssembler;


extern int mpi_sysv_projid;

extern MPI_Comm mpi_leaders_comm,mpi_localhost_comm,mpi_job_comm;
extern int mpi_myHostId,mpi_myLeaderRank,mpi_myLocalRank,mpi_myJobRank;

//functions
void mpi_init();
void mpi_initJobs();
void mpi_types_init();
void mpi_fillHashPoints();
void mpi_syncSeqCounts();

void mpi_hasher_client_mode(BitSequence *pat);
void mpi_storage_client_mode();
void mpi_storage_store(list<MessageBlock> &toStore);

void mpi_merge_client_mode();
void mpi_extract_client_mode(BitSequence *pat);
word mpi_extract_checkSentQueue(vector<list<AnchorBlock*> > &sent,vector<multimap<int,int>::iterator> &bufferItes,multimap<int,int> &buffers);
void mpi_procAnchorBlock(list<AnchorBlock*> &todo);

void mpi_hashAndStore_client_mode(BitSequence *pat);
void mpi_hashSeqLocal(BitSequence *bitseq,BitSequence *pat,int sign,Sequence *s);

void mpi_anchorMergeServer(const vector<int> &senders);
void mpi_anchorMergeClient(int mergeTarget);

void mpi_write_histogram();
#endif
#endif
