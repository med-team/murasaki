/*
Copyright (C) 2006-2008 Keio University
(Kris Popendorf) <comp@bio.keio.ac.jp> (2006)

This file is part of Murasaki.

Murasaki is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Murasaki is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Murasaki.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef SHMVECTOR__H
#define SHMVECTOR__H

#include <vector>
#include <algorithm>
#include <iterator>
#include <assert.h>
#include <string.h> //for memcpy
#include "globaltypes.h"
#include "murasaki_mpi.h"
#include "dinkymath.h"

#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <fcntl.h>

#include <errno.h>
#include <unistd.h>

#if defined(OPT_SHMVERBOSE) || !defined(NDEBUG)
#define opt_ShmVerbose (1)
#else
#define opt_ShmVerbose (0)
#endif

template <typename T>
class ShmVector {
protected:
  T* _data;
  size_t _size;
  size_t _capacity;
  size_t _byteCapacity;

  key_t sysv_key;
  int sysv_shmid;
  bool useShm;

public:
  bool hasShm(){return useShm;}

  typedef T val_type;

  class ShmVectorIterator {
  protected:
    T* _p;
  public:
    typedef ShmVectorIterator _Self;

    //required to be an "iterator"
    typedef T value_type;
    typedef int distance_type;
    typedef int difference_type;
    typedef T* pointer;
    typedef T& reference;
    typedef random_access_iterator_tag iterator_category;

    //sanity
    void put(const reference a,int idx) const {
      *_p;
    }
    void put(const val_type &v) const {
      *_p=v;
    }
    inline reference get() const { //identical to operator*, just there for symmetry with put
      return *_p;
    }

    pointer operator->() const {
      return &*_p;
    }

    //qualify as Iterator
    inline reference operator*() const {
      return *_p;
    }
    _Self& operator++(){
      ++_p;
      return *this;
    }
    _Self operator++(int){
      _Self a(*this);
      ++_p;
      return a;
    }
    _Self& operator--(){
      --_p;
      return *this;
    }
    _Self operator--(int){
      _Self a(*this);
      --a._p;
      return a;
    }
    bool operator!=(const _Self& p) const {
      return _p!=p._p;
    }
    bool operator==(const _Self& p) const {
      return _p==p._p;
    }

    //qualify as RandomAccessIterator
    bool operator<(const ShmVectorIterator &a){
      return _p<a._p;
    }
    difference_type operator-(const ShmVectorIterator &a){
      return _p-a._p;
    }
    ShmVectorIterator operator+(const distance_type n){
      ShmVectorIterator a(*this);
      a._p+=n;
      return a;
    }
    ShmVectorIterator& operator+=(const distance_type n){
      _p+=n;
      return *this;
    }
    ShmVectorIterator operator-(const distance_type n){
      ShmVectorIterator a(*this);
      a._p-=n;
      return a;
    }
    ShmVectorIterator& operator-=(const distance_type n){
      _p-=n;
      return *this;
    }

    val_type& operator[](const distance_type n){
      return *(_p+n);
    }

    ShmVectorIterator():
      _p(NULL)
    {}

    ShmVectorIterator(ShmVector* parent,int idx):
      _p(parent->_data+idx)
    {}
  };

  typedef ShmVectorIterator iterator;

  void shmInit(){
    using namespace std;
#if defined(MURASAKI_MPI) && defined(USE_SHM_SYSV)
    if(useShm){
      //mark old for deletion if it exists
      if(sysv_shmid!=-1 && shmctl(sysv_shmid,IPC_RMID,NULL)<0)
	; //that's fine.
	//	throw MurasakiException("Couldn't mark System V IPC shared memory region for deletion for sequence "+string(strerror(errno)));

      if(mpi_myLocalRank==0){
	if((sysv_shmid=shmget(IPC_PRIVATE,_byteCapacity,IPC_CREAT | 00600))==-1)
	  throw MurasakiException("ShmVector: Error creating System V IPC shared memory segment (size: "+humanMemory(_byteCapacity)+"): "+string(strerror(errno)));
	//      MPI_Bcast(&sysv_shmid,sizeof(key_t),MPI_BYTE,0,mpi_localhost_comm);
	
	assert(mpi_isHostLeader);
	if((_data=(T*)shmat(sysv_shmid,NULL,mpi_isHostLeader ? O_RDWR:(O_RDONLY | SHM_RDONLY)))==(void*)-1)
	  throw MurasakiException("ShmVector: Error attaching to System V IPC shared memory segment: "+string(strerror(errno)));
	if(opt_ShmVerbose)
	  cout << "ShmVector: Mapped a "<<humanMemory(_byteCapacity)<<" sysv chunk (key: "<<dhstring(sysv_key)<<" shmid: "<<sysv_shmid<<")"<<endl;

	if(sysv_shmid!=-1 && shmctl(sysv_shmid,IPC_RMID,NULL)<0)
	  throw MurasakiException("Couldn't mark System V IPC shared memory region for deletion for sequence "+string(strerror(errno)));
      }
      return;
    }
#endif
    
    //non SHM backwards compatability (hacky, unpleasant, but so is inheritance of iterator subclasses
    _data=new T[_byteCapacity];
  }

  void shmClean(){
    if(useShm){
      if(sysv_shmid!=-1 && shmctl(sysv_shmid,IPC_RMID,NULL)<0)
	throw MurasakiException("Couldn't mark System V IPC shared memory region for deletion for sequence "+string(strerror(errno)));

      if(_data && shmdt(_data)<0)
	throw MurasakiException("Couldn't detach ShmVector System V shared memory region: "+string(strerror(errno)));
    }else{
      delete[] _data;
    }
  }

  void shmRealloc(size_t newc){
    using namespace std;
    
    //      key_t old_sysv_key=sysv_key;
    //      int old_sysv_shmid=sysv_shmid;
    //      size_t oldCapacity=_capacity;
      size_t oldByteCapacity=_byteCapacity;
      T* old_data=_data;
      
      _capacity=newc;
      _byteCapacity=newc*sizeof(T);
      if(opt_ShmVerbose)
	cout << "ShmVector: Reallocing "<<humanMemory(oldByteCapacity)<<" shared vector into a "<<humanMemory(_byteCapacity)<<" sysv chunk..."<<endl;
      shmInit(); //simply reinit.
      
      if(_size>_capacity)
	_size=_capacity; //must always be true
      
      //copy old data into new space
      memcpy(_data,old_data,(oldByteCapacity<_byteCapacity ? oldByteCapacity:_byteCapacity));
      
    if(useShm){
      if(old_data && shmdt(old_data)<0)
	throw MurasakiException("Couldn't detach (old) ShmVector System V shared memory region: "+string(strerror(errno)));
    }else{
      delete[] old_data;      
    }
  }
public:
  ShmVector() :
    _data(NULL),
    _size(0),
    _capacity(16),
    _byteCapacity(16*sizeof(T)),
    sysv_key(0),
    sysv_shmid(-1),
#ifdef MURASAKI_MPI
    useShm(mpi_capable && mpi_usingShm)
#else
    useShm(false)
#endif
  {
    shmInit();
  }

  ShmVector(size_t size) :
    _data(NULL),
    _size(size),
    _capacity(_size),
    _byteCapacity(_capacity*sizeof(T)),
    sysv_key(0),
    sysv_shmid(-1),
#ifdef MURASAKI_MPI
    useShm(mpi_capable && mpi_usingShm)
#else
    useShm(false)
#endif
  {
    shmInit();
  }

  ~ShmVector(){
    shmClean();
  }

  size_t capacity(){
    return _capacity;
  }

  void reserve(size_t c){
    if(_size>=c) //can't reserve space smaller than that which we've already used
      return;
    shmRealloc(c);
  }

  void resize(size_t c){
    shmRealloc(c);
    _size=c;
  }

  void push_back(const T& a){
    if(_size>=_capacity)
      reserve((_capacity==0 ? 1:_capacity)*2);
    _data[_size++]=a;
  }

  iterator begin(){
    return iterator(this,0);
  }
  iterator end(){
    return iterator(this,_size);
  }
  val_type& operator[](int n){
    return _data[n];
  }

  size_t size(){
    return _size;
  }
  bool empty(){
    return !_size;
  }
  val_type& front(){
    assert(_size);
    return _data[0];
  }

#ifdef MURASAKI_MPI
  void sync(MPI_Comm shareComm,const int leader,const bool asLeader){
    std::string vtag;
    if(useShm){
      vtag=string(" ShmVector-Sync (as SHM-")+string(asLeader ? "leader":"slave")+string("): ");
      if(mpi_myLocalRank!=leader){
	if(_data){
	  cout << vtag << " dettaching old region: "<<(void*)(_data)<<" -> "<<dhstring(sysv_shmid)<<endl;
	  shmdt(_data);
	}
      }

      //      cout << vtag << "Bcast from "<<leader<<" with "<<(MPI_COMM_WORLD==shareComm ? "world":"local host")<<endl;
      MPI_Bcast(&sysv_shmid,sizeof(key_t),MPI_BYTE,leader,shareComm);
      MPI_Bcast(&sysv_key,sizeof(key_t),MPI_BYTE,leader,shareComm);
      MPI_Bcast(&_size,sizeof(key_t),MPI_BYTE,leader,shareComm);
      MPI_Bcast(&_capacity,sizeof(key_t),MPI_BYTE,leader,shareComm);
      MPI_Bcast(&_byteCapacity,sizeof(key_t),MPI_BYTE,leader,shareComm);
      cout << vtag << _size << (_size==1 ? " obj":" objs")<< " ("<<humanMemory(sizeof(T)*_size)<<") with "<<(MPI_COMM_WORLD==shareComm ? "world":"local host")<<endl;
      
      if(shareComm==MPI_COMM_WORLD){ //"special" case
	if(mpi_isHostLeader && !asLeader)
	  shmInit(); //need to create region on this host
	MPI_Bcast(&sysv_shmid,sizeof(key_t),MPI_BYTE,leader,mpi_localhost_comm);
      }

      if(!asLeader && !mpi_isHostLeader){
	cout << vtag << "Attaching new region: "<<dhstring(sysv_shmid)<<" ("<<humanMemory(_byteCapacity)<<")"<<endl;
	if((_data=(T*)shmat(sysv_shmid,NULL,mpi_isHostLeader ? O_RDWR:(O_RDONLY | SHM_RDONLY)))==(void*)-1)
	  throw MurasakiException("ShmVector: Error attaching to System V IPC shared memory segment: "+string(strerror(errno)));
      }
    }else{
      if(!mpi_capable)
	return;
      vtag=string(" ShmVector-Sync (non-SHM-")+string(leader==mpi_myLocalRank ? "leader":"slave")+string("): ");
      //      cout << vtag << "Start sync."<<endl;
      word regionCount=size();
      MPI_Bcast(&regionCount,1,MPI_UNSIGNED_LONG,leader,shareComm);
      cout << vtag << regionCount<< (regionCount==1 ? " region":" regions")<< " ("<<humanMemory(sizeof(T)*regionCount)<<")"<<endl;
      if(size()!=regionCount)
	resize(regionCount);
      MPI_Bcast(&front(),sizeof(T)*size(),MPI_BYTE,leader,shareComm);
    }
    //    cout << vtag << "sync done"<<endl;
  }

  void localSync(){
    sync(mpi_localhost_comm,0,mpi_isHostLeader);
  }
  void worldSync(){
    sync(MPI_COMM_WORLD,0,mpi_id==0);
    if(useShm && mpi_isHostLeader)
      MPI_Bcast(&front(),sizeof(T)*size(),MPI_BYTE,0,mpi_leaders_comm);
  }
#endif

  void dump(){
    int ix=0;
    for(iterator i=begin();i!=end();++i)
      cout << "data["<<ix++<<"]="<<*i << endl;
  }
};

#endif

