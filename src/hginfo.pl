#!/usr/bin/perl

use strict;

my (@hg)=`env LANG=C hg parents 2>/dev/null`;
exit 1 if $?; #some hg error? don't return anything.

my %hg=kvsplit(@hg);
my $date=parseDate($hg{date});
my @hgstatus=`hg status`;
my @modified=grep {/^M/ and !/Makefile$/ } @hgstatus;
my $modified="*".join("~",map {m!([^/ \t]+?)\s*$!; "$1"} @modified);
print join(":","r$hg{'changeset'}",$date,(@modified ? ($modified):()));

sub kvsplit {
  my %r;
  foreach my $l (@_){
    chomp $l;
    next unless $l=~m/^([^:]+):\s+(\S+.*)/;
    $r{$1}=$2;
  }
  return %r;
}

sub parseDate {
  my ($wday,$mon,$day,$time,$year,$tz)=split(/\s+/,$_[0]);
  return "$year-$mon-$day";
}
