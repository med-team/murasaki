/*
Copyright (C) 2006-2008 Keio University
(Kris Popendorf) <comp@bio.keio.ac.jp> (2006)

This file is part of Murasaki.

Murasaki is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Murasaki is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Murasaki.  If not, see <http://www.gnu.org/licenses/>.
*/

////////
// sequence defs
// Kris Popendorf
///////

#ifndef SEQUENCE__H
#define SEQUENCE__H

#include "globaltypes.h"
#include "cmultiset.h"
#include "scoring.h"
#include "binseq.hh"

#include <iostream>
#include <vector>
#include <set>
#include <map>
#include <list>
#include <limits.h>
#include <functional>
#include <assert.h>
#include "itree.h"
#include "seqread.h"
#include "shmvector.hh"

#ifdef USE_SHM_SYSV
#include <sys/types.h>
#include <sys/ipc.h>
#endif

using namespace std;
using namespace Itree;

//in my world:
// A = 00
// C = 01
// G = 10
// T = 11
// isnt that cute? ~G==C and so on (and in alphabetical order!)

#define MODWORDSIZE(a)  ((a) & (WORDSIZE-1))

extern int _wordsize,hash_max; 
extern int max_hash_bits;//hash_max limits memory usage
extern word hash_size; //actually used size, might be less than max
extern word hash_padding;
extern int mismatch_penalty,match_score,max_credits;
extern bool gappingEnabled;
typedef map<word,SeqPos> HashCount;

#define BASE_A 0
#define BASE_C 1
#define BASE_G 2
#define BASE_T 3
#define BASE_N 4
#define BASES 5
typedef int BASE_TYPE; //stupid broken enums...

#define OVERLAP_NONE 0
#define OVERLAP_START_AB 1
#define OVERLAP_START_BA 2
#define OVERLAP_STOP_AB 4
#define OVERLAP_STOP_BA 8
#define OVERLAP_AINB 16
#define OVERLAP_BINA 32
#define COLINEAR_AB 64
#define COLINEAR_BA 128
#define COLINEAR_NONE 0
typedef int OverlapSense; //doing |= on enums does bad things now apparently
typedef int ColinearSense; //to avoid semantic abuse of overlapsense with non-overlapping things
extern double globalBaseFreq[4];
extern word globalBaseCount[4],globalCounted;
enum SharedMemType {SHM_NONE=0,SHM_MMAP_RO, SHM_MMAP_RW, SHM_SYSV, SHM_END};

void finishGlobalFrequencies();

//forward declarations...
class HashMethod;
class Window;
class Anchor;
class Location;
class AnchorSet;
class UsedMap;
class Sequence;

void set_seqtable(vector<Sequence*> seqs);

void initHashParams(int bits,BitSequence *pat);
void initConstants();

inline word revComp8bit(word t);
inline word revCompWord(word t);
inline int highestBit(word t);
inline word lowN(int i);
inline word highN(int i);
inline word first2(word w){return w>>(WORDSIZE-2);}

int popCount(word w);
string baseToString(BASE_TYPE b);
string bitToString(word w);
string wordToString(word w,int bits=WORDSIZE);
string wordToMaskedString(word w,word pat, int bits=WORDSIZE);
string wordToPattern(word w,int bits=WORDSIZE);
string repString(string s,int count);

string hashCacheName(string name, BitSequence *pat);

//Sequence base pointers and meta-data
typedef pair<SeqPos,SeqPos> SeqPosPair;
typedef ShmVector<SeqPosPair> SeqPosPairArray;

//bits for sequence index table
extern map<Sequence*,SeqIdx> seq2idx;

typedef itree<SeqPos,AnchorSet*> usedItree;
typedef usedItree::iterator MapIte;
typedef Interval<SeqPos> UsedInt;

extern int next_sequence_;
extern int base_patt_len;
extern word activeHash;
extern word totalSequenceMemory;
extern word sys_pagesize;

extern CryptoHasher *cryptoHasher;

//for managing UsedInts
UsedInt UsedInt_grown(const UsedInt &a,const SeqPos amt);
UsedInt UsedInt_inverted(const UsedInt &a,const Sequence& s);
UsedInt& UsedInt_invert(UsedInt &a,const Sequence& s);
UsedInt UsedInt_coalesced(const UsedInt &a,const UsedInt &b);
void UsedInt_coalesce(UsedInt &x,const UsedInt &a);
bool UsedInt_contains(const UsedInt &outer,const UsedInt &inner);
inline bool isRev(const UsedInt &a){return a.start<0;}
string asAnchor(const UsedInt& a,const Sequence *s);
bool UsedInt_sanityCheck(const UsedInt &a);
OverlapSense UsedInt_overlaps(UsedInt a,UsedInt b);
string OverlapSense2str(OverlapSense o);
UsedInt& UsedInt_rawInvert(UsedInt &x);
SeqPos UsedInt_offset(UsedInt a,UsedInt b);
SeqPos UsedInt_distance(UsedInt a,UsedInt b); //distance between nearest ends

//inline function defs actually have to be in here if they're used in other .cc's...
const inline SeqIdx lookup_seqtable(Sequence* s) {
  assert(seq2idx.find(s)!=seq2idx.end()); //must be in there already
  return seq2idx[s];
}

inline SeqPos bitSeqCoords(SeqPos seqCoords){
  return abs(seqCoords)-1;
}

inline SeqPos seqCoords(SeqPos bitSeqCoords,int sign){
  return sign>0 ? (bitSeqCoords+1):-(bitSeqCoords+1);
}

inline UsedInt seqCoords(UsedInt bitSeqCoords,int sign){
  return UsedInt(seqCoords(bitSeqCoords.start,sign),seqCoords(bitSeqCoords.stop,sign));
}

inline word lowN(int i){
  return ~(((word)-1)<<i); //lowest n bits are 1
}

inline word highN(int i){
  return ~((~(word)0)>>i);
}

class BitSequence { //for packing dna strings into 2 bit chunks
 public:
  //accessor
  inline word getCounted() const {return counted;}
  inline const word* words() const {return _words;}
  inline word wordCount() const {return word_count;}
  inline word reverseOtf(SeqPos pos) const;
  inline SeqPos length() const {return _length;}
  int wordsize();
  string asString() const;
  string asPattern() const;
  inline int readCode(SeqPos bitpos) const;
  inline BASE_TYPE readBase(SeqPos basepos) const;
  word readWord(SeqPos wordpos) const;
  inline word readRawWord(SeqPos wordpos) const;
  inline word wordAtBase(SeqPos basepos) const;
  inline SeqPos readBitCount(){return bit_count;}
  inline int hashLength(){return _hashLength;}
  int compHashLength();
  SeqPosPair* localRegion(SeqPos pos); //find region local to pos
  SeqPosPairArray::iterator localRegionIte(SeqPos pos);
  SeqPos spaceRight(SeqPos pos); //matchable space right of pos
  SeqPos spaceLeft(SeqPos pos); //matchable space left of pos
  string genBitFilename();

  SeqPos cmpRight(SeqPos pos,BitSequence& target,SeqPos targetPos);
  SeqPos cmpLeft(SeqPos pos,BitSequence& target,SeqPos targetPos);

  //manipulate others
  void maskString(string &str) const;

  //operator
  bool equal(SeqPos pos,BitSequence& target,SeqPos targetPos,BitSequence& patt);

  //mutator
  void invert(); //flips to comp sequence
  
  //bitsequence constructors
  BitSequence(Sequence *s); //from a file. do the smart thing.
  BitSequence(const string &str); //an anonymous 0101 style pattern (as mask: ie double each bit)
  BitSequence(SequenceFileReader &reader,Sequence* s); //from either GATC or 0101 (as mask: ie double each bit)
  BitSequence(const BitSequence& a); //copy
  BitSequence(const word length,Sequence* s,bool _reverse=false);
  BitSequence();//uninited
  ~BitSequence();
  BitSequence* reverseComplement(); //generate a reverse complement version

  friend bool operator==(const BitSequence& a,const BitSequence& b);
  friend class Window;
  friend class HashMethod;
  friend void initHashParams(int bits,BitSequence *pat);
  friend ostream& operator<<(ostream &os,const BitSequence &a);

  SeqPosPairArray matchRegions; //match regions contains hashable areas (ie: no Ns)
  SeqPosPairArray subSeqs; //subSeqs contains all subsequences (contigs, chromosomes, whatever multiple sequences are combined into a single fasta or .stitch file), regions that can be anchored (might contain Ns)

  UsedInt subSeqBounds(SeqPos at);

  //mpi specific
  void mpi_distribute(); //distribute worldwide

  //shared memory specific
  SharedMemType shmUsed;
  string shmAnchor;
  void shm_distribute(); //distribute to other local nodes
  void mmap_msync();

 protected:
  //init
  void initPattern(const string &str);
  void loadReader(SequenceFileReader &reader);
  void loadBinary(const SequenceBinary &reader);
  void init(const BitSequence& a);

  void compileHashFunc();
  void randomHashFunc(int n);
  word* allocWords();

  SeqPos _length;
  word bit_count,word_count;
  int _hashLength;
  bool isPattern;
  Sequence* seq;
  word* _words;
  bool reverse;
  word counted;

  //for fast hash computation:
  HashMethod* hasher;
};

class HashMethod {
 public:
  typedef vector<pair<int,int> > InputList;
 protected:
  InputList inputs;
  static map<int,int> popmap;
  BitSequence *seq;
  void addWord();
  void removeWord();
  string srcSetToString(const set<int> &a);
 public:
  HashMethod(const HashMethod &a);
  HashMethod(const HashMethod &a,const HashMethod &b); //mmm hot function on function action
  HashMethod(BitSequence *s);
  HashMethod(BitSequence *s,int n); //random hash function for n words. bad idea, but I won't stop you.

  string prettyPrint();

  //scoring state
  vector<double> entropy;
  vector<int> used,unusedWords;
  double sources,empties,fitness;
  double entropyHi,entropyLo,entropyTotal;
  double entropyMean,entropyStd;
  double totalCorrelationPenalty;
  word active;

  inline const InputList& inputlist(){return inputs;}
  friend ostream& operator<<(ostream &os,const HashMethod &a);
  int align(int w); //propose an offset for a new input
  void finalize();
  int maxSources();
  void mutate();
  word hash(Window &w);
  double fitnessCheck();
  void removeDuplicates();
  void pruneUseless();
};

inline bool operator<(const HashMethod &a,const HashMethod &b){return a.fitness<b.fitness;}

//tool for skimming bitsequences through a pattern. binds to BitSequence*
class Window { 
 public:
  BitSequence *seq,*pat;
  SeqPos pos,frame;
  void slide(); //advance 1 base (ie: 2 bits)
  void slide(SeqPos dist); //advance dist bits. (note: bits!!! not bases!!!)
  void slideWord(); //advance 1 whole word

  bool equals(const Window &a) const;
  word eqbases(const Window &a) const;
  word hash();
  Window(BitSequence *s,SeqPos pos,BitSequence *p);
  Window(Location &v,BitSequence *p);
  Window(const UsedInt &i,const Sequence *s,BitSequence *p);
  friend ostream& operator<<(ostream &os, const Window &a);
  friend bool operator<(const Window &a,const Window &b);
  string prettyString();
  string toRawString();

 protected:
  void initslide(SeqPos dist);
  SeqPos frames;
  vector<word> buffer;
 public:
  inline word firstWord(){return buffer[0];}
};

class Sequence {
 public:
  Sequence(); //empty sequence (ie: useless)
  Sequence(string); //from a file
  Sequence(const Sequence &obj); // duplicate
  ~Sequence(); //have to cleanup fwd and rev

  string filename,name,baseFilename;
  SeqPos length() const;
  inline int getId(){return seqID;}
  inline SeqPos extCoords(SeqPos mcoords) const
    {return mcoords<0 ? -1-(rev->length()+mcoords):mcoords;}
  UsedInt getSubSeqBounds(SeqPos at);
  UsedInt growInBounds(const UsedInt &basis,SeqPos amount);

  friend bool operator ==(const Sequence&,const Sequence&);
  BitSequence *fwd,*rev;
  BaseIterator iterate(SeqPos at);
  pair<BaseIterator,BaseIterator> iterate(const UsedInt &at);

 protected:
  int seqID;

#ifdef USE_SHM_SYSV
  //sysvipc specific
  key_t sysv_key;
  int sysv_shmid;

  friend class BitSequence; //i need you to be my friend!!
#endif
};

class Location {
 public:
  SeqIdx seqno;
  SeqPos pos;

  Sequence* seq() const;
  inline int seqId() const {return seqno;} //umm this should be "return seqno" i think?
  BitSequence* bitSeq() const;
  bool equal(const Location &a,BitSequence &patt) const;
  friend bool operator==(const Location &a,const Location &b);
  friend bool operator<(const Location &a,const Location &b);
  friend ostream& operator<<(ostream &os,const Location &a);
  inline SeqPos bitSeqPos(){return (pos>0 ? pos:0-pos)-1;}
  SeqPosPair* localRegion();
  SeqPosPairArray::iterator localRegionIte(); //find region local to pos
  
  Location(Sequence*,SeqPos);
  Location(SeqIdx, SeqPos);
  Location(); //this is dangerous. be careful with it.
};

typedef list<Location> LocSubList;
typedef vector<LocSubList> LocList;
bool operator==(const HashVal &a,const HashVal &b);

class Hash {
 public:
  //abstract parts
  virtual void clear() = 0;
  virtual void add(const HashKey key,const HashVal &val) = 0;
  virtual void lookup(HashKey key,LocList &locList) = 0;
  virtual void getMatchingSets(HashKey base,list<LocList> &fulllist) = 0;
  virtual bool emptyAt(const HashKey key) = 0;
  virtual word sizeAt(const HashKey key) = 0;
  virtual word sizeAt(const HashKey key,const HashVal &val) = 0;
 
  //optional bits
  virtual void writePerformanceData(string prefix);

  //for computing memory costs
  static const word linear_cost(word);
  static const word bucket_prep_cost(word);

  // dump / load may be overloaded
  virtual void dump(ostream &os);
  virtual void load(istream &is);

  //non virtual stuff that will never change
  void writeHistogram(ostream &of);
  void writeDetailedHistogram(ostream &of);
  BitSequence* hashpat;
  bool sanityCheck();
  bool sanityCheck(word base);
  
  //the boring junk
  Hash(BitSequence *pat);
  virtual ~Hash() = 0; 
 protected:
  //nada
};

class AnchorSet;

struct ltRegion : binary_function<SeqPosPair,SeqPosPair,bool> {
  inline bool operator()(const SeqPosPair &a, const SeqPosPair &b) const {
    return a.second<b.second;
  }
};

class IntervalSet { //intermediate type used for building/modifying AnchorSets
 public:
  IntervalSet();
  IntervalSet(const AnchorSet& a);
  typedef vector<UsedInt>::iterator _IntervalIterator;
  typedef vector<pair<word,SeqPos> >::iterator _MemberIterator;
  IntervalSet(const _IntervalIterator &intStart, const _IntervalIterator &intStop,
	      const _MemberIterator &memberStart,const _MemberIterator &memberStop);
  
  string contents(string delim) const;
  inline string contents() const {return contents("\n");}
  void invert(); //flip everything from forwards<->backwards
  void coalesce(const AnchorSet& neighbor);
  bool contains(const IntervalSet &a) const;
  OverlapSense overlaps(const AnchorSet &a) const;
  bool hasGaps(const AnchorSet& a) const; //would merging with a cause gaps?
  SeqPos offset(const AnchorSet& a) const;
  pair<bool,SeqPos> gapOffset(const AnchorSet& a) const; //no sense in duplicating the above code
  ColinearSense colinear(const AnchorSet& a,SeqPos MaxDist) const;
  void add(HashVal&,SeqPos length);
  void exactExtend();
  Score fuzzyExtend();
  Score score();
  pair<double,SeqPos> entropy() const;
  
  bool sanityCheck() const;

  friend ostream& operator<<(ostream& os,const IntervalSet& a);
  vector<UsedInt> spaces;
  HashCount members;
};

class AnchorSet { //anchorsets are necessarily tied to a set of usedmap entries
 public:
  vector<MapIte> spaces;
  HashCount members;

  AnchorSet(const IntervalSet &a,UsedMap& used);
  bool contains(const IntervalSet &a) const;
  
  bool sanityCheck() const; //this is straight forward enough

  size_t hitCount() const;
  double uniqueness(UsedMap* context) const;
  SeqPosPair bitscore() const; //though this is outmoded, no reason to actually remove it...
  Score score() const;

  string asString() const;
  friend ostream& operator<<(ostream &os, const AnchorSet &a);
};

class UsedMap {
 public:
  UsedMap();

  bool sanityCheck();
  string asString();
  int count() const;
  void insert(IntervalSet& a);
  word* makeDfCount(); //for calculating tfidf scores, may return 0 if out of mem

  ostream& saveDetails(ostream &os,ostream &bitos); //make second cerr to skip
  ostream& writeOut(ostream &os);
  ostream& writeTfidf(ostream &os);
  ostream& writeScores(ostream &os);

  friend AnchorSet::AnchorSet(const IntervalSet &a,UsedMap& used);
#ifdef MURASAKI_MPI
  friend void mpi_anchorMergeClient(int mergeTarget);
  friend void mpi_anchorMergeServer(const vector<int> &senders);
#endif
 protected:
  vector<usedItree > used;

  bool alreadyExists(const IntervalSet &a);
  bool merge(IntervalSet& a);
  void add(const IntervalSet& a);
  void remove(AnchorSet *a); //deletes a!

  bool fetchOverlaps(int seq,const UsedInt &a,set<AnchorSet*> &out);
  bool fetchNearbyOverlaps(int seq,const UsedInt &a,SeqPos maxgrow,set<AnchorSet*> &out);
};

class RepeatMap {
 public:
  void add(const LocList &l);
  void writeOut(ostream &os);
  inline word size(){return clusters.size();}

  RepeatMap(int size);
  RepeatMap();
#ifdef MURASAKI_MPI
  friend void mpi_anchorMergeClient(int mergeTarget);
#endif
 protected:
  list<LocList> clusters;
};

class BaseIterator {
 protected:
  bool rev;
  BitSequence *src;
  SeqPos idx;

  SeqPosPairArray::iterator here,prev,next;
  void shiftFwd();
  void shiftBack();

 public:
  BaseIterator(Sequence *seq,SeqPos pos);
  BaseIterator(const BaseIterator&);
  
  BaseIterator& operator++(); //pre-inc
  BaseIterator& operator--(); //pre-dec
  BaseIterator operator++(int); //post-inc
  BaseIterator operator--(int); //post-dec
  bool operator==(const BaseIterator &a) const;
  bool operator!=(const BaseIterator &a) const;
  BASE_TYPE operator*() const; //spit out current base

  inline int getIdx() const {return idx;}
  inline SeqPosPair region() const {return *here;}
  string debugInfo();
};

inline bool isZero(const MapIte& i){return i.key()==UsedInt(0,0);}
inline bool isZero(const UsedInt& i){return i==UsedInt(0,0);}
UsedInt anchorCoords(const UsedInt &b,Sequence *s);
UsedInt operator-(SeqPos a,const UsedInt& b);
string SeqPosPair2string(const SeqPosPair& a);

#endif

