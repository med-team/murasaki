/*
Copyright (C) 2006-2008 Keio University
(Kris Popendorf) <comp@bio.keio.ac.jp> (2006)

This file is part of Murasaki.

Murasaki is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Murasaki is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Murasaki.  If not, see <http://www.gnu.org/licenses/>.
*/

/////////////////////
// murasaki config options manager
////////////////////

#ifndef __OPTIONS_H
#define __OPTIONS_H

#include "sequence.h"
#include "scoring.h"
#include <string>
#include <iostream>
#include <map>

using namespace std;

enum HashType {ArrayHash_t, MSetHash_t, EcoHash_t,OpenHash_t,Last_t};
enum OptSrcType {OPT_SRC_DEFAULT=0,OPT_SRC_AUTO=1,OPT_SRC_MANUAL=2};
extern int mpi_id;

class Options {
 public:
  //config options
  word verbose;
  int quick_hash,randomHashInputs;
  word hitfilter;
  int histogram;
  bool user_seed;
  int rand_seed;
  bool repeatMask;
  bool skip1to1,skipFwd,skipRev;
  bool hashOnly;
  bool hashCache;
  bool bitscore;
  int probingMethod;
  HashType hashPref;
  string alignment_name;
  string output_dir;
  string config_file;
  string cache_dir;
  
  word seedfilter,hashfilter,mergeFilter;
  Score scoreFilter;

  bool joinNeighbors;
  float joinSpec;
  int joinDist;

  int hashSkip;

  bool auto_hashbits;
  bool dumpRegions,leaveRecords;

  unsigned long targetMemory;
  bool userSetMemory;
  bool retainMembers;
  bool useHumanTime;
  bool repeatMap;
  word anchorProgressCheck;
  bool tfidf,reverseOtf,inplaceDf;
  int rifts,set_islands;
  bool fuzzyExtend,gappedAnchors;
  Score fuzzyExtendLossLimit;
  bool scoreByMinimumPair;
  bool measureHashCollisions;
  bool ecolistFrugal;
  bool hasherFairEntropy,hasherCorrelationAdjust;
  word hasherTargetGACycles;
  double hasherEntropyAgro;
  bool useSeqBinary;

  int mpi_hashers;
  bool mpi_noCake;
  bool mpi_fileDistro;
  bool mpi_anchorInSpareTime;
  word mpi_maxBuffers;
  bool mpi_bigFirst,mpi_hostbalance,mpi_memoryBalance,mpi_distMerge,mpi_distCollect;
  bool mpi_outputRedirect,mpi_keepstdoe;
  
  bool use_shm_mmap,mmap_writePerHost;
  //mmap has a potential performance hit because it requires sequence distribution via nfs/disk,
  //so we leave it as an option. sysv requires no disk backing, so if we have it, always use it.
  bool use_shm_sysv;

  //computed dynamically in solidfy()
  string prefix;
  string hashStatus_record;
  string hashHisto_record;
  string hashDetailed_record;
  string seq_record;
  string anchor_record;
  string repeat_record;
  string pat_record;
  string region_record;
  string status_record;
  string options_record;
  string anchorProgress_record;

  int seqreadOptions;

  //functions
  Options();
  void preInit();
  void loadConfig(string config);
  void solidify();
  void commandline_opts(int argc,char **argv);
  void environment_opts(string prefix);
  string hashPref_s() const;
  friend ostream& operator<<(ostream& os,const Options &a);
  int getOptSource(string name);

 protected:
  void staticInit();
  static bool staticInited;
  map<int,int> optionSource;
  static map<int,string> optionSwitch;
  static map<string,int> optionSwitchToInt;
  static struct option long_options[];
};

string program_help(bool longhelp=false);
void parseYesNo(const char* str,bool& toggle,const char *name);

template <typename T> void parseLexical(const char* str,T& ref,const char *name);


#endif
