/*
Copyright (C) 2006-2008 Keio University
(Kris Popendorf) <comp@bio.keio.ac.jp> (2006)

This file is part of Murasaki.

Murasaki is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Murasaki is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Murasaki.  If not, see <http://www.gnu.org/licenses/>.
*/

////////
// super-memory-miserly hash list storage (def.)
// Kris Popendorf
///////

#include "ecohash.h"
#include "ecolist.h"
#include "murasaki.h"
#include "options.h"

using namespace std;

EcoHash::EcoHash(BitSequence *pat) :
  Hash(pat),
  sorted(hash_size)
{
  fasta=new Ecolist[hash_size];
}

EcoHash::~EcoHash(){
  delete[] fasta;
}

void EcoHash::clear(){
  for(word i=0;i<hash_size;i++)
    fasta[i].clear();
  sorted.clear();
}

bool EcoHash::emptyAt(const HashKey key){
  return !fasta[key].getSize();
}

word EcoHash::sizeAt(const HashKey key){
  return fasta[key].getSize();
}

word EcoHash::sizeAt(const HashKey key,const HashVal &val){
  return fasta[key].int_upper_bound(loc2val(val))-fasta[key].int_lower_bound(loc2val(val));
}

void EcoHash::add(const HashKey key,const HashVal &val){
  fasta[key].push(val.seqno,val.pos);
  sorted.unset(key);
}

void EcoHash::getMatchingSets(HashKey key,list<LocList> &sets){
  if(emptyAt(key))
    return;
  if(!sorted.checkAndSet(key))
    fasta[key].sort();

  Ecolist::iterator start(fasta[key].begin()),stop(fasta[key].end());
  for(Ecolist::iterator si=start;si!=stop;){
    sets.push_back(LocList(seq_count));
    LocList &locList=sets.back();
    for(Ecolist::iterator setEnd(fasta[key].upper_bound(*si));si!=setEnd;++si){
      val_type l(*si);
      locList[l.first].push_back(val2loc(l));
    }
  }
}

void EcoHash::lookup(HashKey key,LocList &locList){
  Ecolist::iterator start(fasta[key].begin()),stop(fasta[key].end());
  for(Ecolist::iterator i=start;i!=stop;++i){
    val_type l(*i);
    locList[l.first].push_back(val2loc(l));
  }
}

bool EcoHash::lessthan(const val_type& a,const val_type& b){
  Location la(val2loc(a)),lb(val2loc(b));
  return la<lb;
}

void EcoHash::init(SeqPos _seq_count,word _longestSeq,word _totalHashLength,SeqPos _hash_bits){
  Ecolist::init(_seq_count,_longestSeq,_totalHashLength,_hash_bits,&(EcoHash::lessthan),opt.ecolistFrugal);
}
