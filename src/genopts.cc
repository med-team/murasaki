/*
Copyright (C) 2006-2008 Keio University
(Kris Popendorf) <comp@bio.keio.ac.jp> (2006)

This file is part of Murasaki.

Murasaki is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Murasaki is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Murasaki.  If not, see <http://www.gnu.org/licenses/>.
*/

//////////////
// murasaki project
// genotps.cc
// generic option parsing toys
//////////////

#include "genopts.h"
#include <err.h>
#include <strings.h>
#include <boost/lexical_cast.hpp>

bool isYes(const char* str){
  return (!strcasecmp(str,"yes") || !strcasecmp(str,"y") || !strcasecmp(str,"true") || !strcasecmp(str,"1"));
}

bool isNo(const char* str){
  return (!strcasecmp(str,"no") || !strcasecmp(str,"n") || !strcasecmp(str,"false") || !strcasecmp(str,"0"));
}

bool getYesNo(const char* str,bool &toggle,const char* name){
  if(isYes(str))toggle=true;
  else if(isNo(str))toggle=false;
  else {
    warnx("Invaild argument for %s. Please specify either yes/t/true or no/n/false",name);
    return false;
  }
  return true;
}

bool toggleYesNo(const char* str,bool &toggle,const char* name){
  if(str)
    return getYesNo(str,toggle,name);
  else {
    toggle=!toggle;
    return true;
  }
}

template<typename T>
bool getLexical(const char* str,T &ref,const char* name){
  using namespace boost;
  try {
    ref=lexical_cast<T>(optarg);
    return true;
  }catch(bad_lexical_cast& e){
    warnx("Invaild argument for %s.",name);
    return false;
  }
}
