#!/usr/bin/perl
#Quick hack to use in place of a proper autoconf-like build manager.
#While autoconf might be nice, compared to when autoconf FAILS the current Makefile system is easier to tweak, so I'll try this for now.

use strict;
local $\="\n";

my $debug=undef;
#$debug=1;

my %cfg=(HASMPI=>sub{return runtest('which mpicxx') ? "YES":"NO";},
	 HASCRYPTOPP=>sub{return testgcc(
<<ENDTEXT
#define CRYPTOPP_ENABLE_NAMESPACE_WEAK 1
#include <cryptopp/md5.h> //fastest
#include <cryptopp/sha.h> //medium
#include <cryptopp/whrlpool.h> //slow
//Non-cryptographic checksums
#include <cryptopp/crc.h>
#include <cryptopp/crc.h>
#include <cryptopp/adler32.h>

int main(){
 CryptoPP::Weak::MD5 MD5CryptoHasher;
 CryptoPP::SHA1 SHACryptoHasher;
 CryptoPP::Whirlpool WhirlpoolCryptoHasher;
 CryptoPP::CRC32 CRC32CryptoHasher;
 CryptoPP::Adler32 Adler32CryptoHasher;
 return 0;
}

ENDTEXT
, "@_ -lcryptopp -lpthread") ? "YES":"NO";}
	);


my ($test,@args)=@ARGV;
exit 2 unless $test and ref $cfg{$test};

print &{$cfg{$test}}(@args);


sub runtest {
  return !system("@_ 1>/dev/null 2>/dev/null");
}

sub testgcc {
  my ($content,$compiler)=@_;
  $compiler="gcc" unless $compiler;
  my $cmd="$compiler -x c++ -o /dev/null -";
  $cmd.=" 1>/dev/null 2>/dev/null" unless $debug;
  print STDERR "Trying to compile using $cmd" if $debug;
  open(my $gccfh,"|$cmd") or exit 1; #can't test gcc. tough potatoes.
  print $gccfh "$content";
  close $gccfh;
  return !$?; #errored?
}
