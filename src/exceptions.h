#ifndef __EXCEPTIONS_H_
#define __EXCEPTIONS_H_

#include <iostream>

class MurasakiException : public std::exception { //from std::exception
 public:
 MurasakiException(const std::string& reason):
  reason_s(std::string("Murasaki: ")+reason){}
  virtual const char* what() const throw () { return reason_s.c_str(); }
  virtual std::string reason() const throw () { return reason_s; }
  virtual ~MurasakiException() throw () {}
  
 protected:
  std::string reason_s;
};

class MurasakiDebugException : public MurasakiException {
 public:
 MurasakiDebugException(const std::string& reason):
  MurasakiException(std::string("Debug: ")+reason) //prepend debug prefix
  {}
};

class MurasakiAbortException : public MurasakiException {
 protected:
  int _status;
 public:
  int status(){return _status;};
 MurasakiAbortException(const std::string& reason,int s):
  MurasakiException(std::string("Abort: ")+reason), //prepend debug prefix
  _status(s)
  {}
};


#endif
