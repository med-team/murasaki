/*
Copyright (C) 2006-2008 Keio University
(Kris Popendorf) <comp@bio.keio.ac.jp> (2006)

This file is part of Murasaki.

Murasaki is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Murasaki is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Murasaki.  If not, see <http://www.gnu.org/licenses/>.
*/

//////////////
// murasaki project
// scoring.cc
// provides scoring functionality for working on sequences
//////////////

#include "murasaki.h"
#include "scoring.h"
#include "sequence.h"
#include "dinkymath.h"
#include "murasaki_mpi.h"
#include "options.h"

#include <iostream>
#include <fstream>
#include <limits.h>
#include <assert.h>
#include <vector>
#include <string.h>
#include <math.h>

bool scoreMatrixInited=false;
double worstCaseEntropy=-log(1.0/(double)BASES)/M_LN2;

Score ScoreMatrix[5][5]={  //borrowed from blastz, augmented with (arbitrary) match scores for matching to Ns
    {   91, -114,  -31, -123 ,    0 },
    { -114,  100, -125,  -31 ,    0 },
    {  -31, -125,  100, -114 ,    0 },
    { -123,  -31, -114,   91 ,    0 },
    {    0,    0,    0,    0 ,    0 } //this N behaviour should perform somehwat similar to the entropy scoring method
  };

Score scoreColumn(const vector<BaseIterator> &column){
  //sum of pairs score
  Score totalScore=0;
  if(column.size()<5){ //this way's plenty fast for small numbers of sequences
    for(vector<BaseIterator>::const_iterator i=column.begin();i!=column.end();++i)
      for(vector<BaseIterator>::const_iterator j=column.begin();i!=j;++j)
	totalScore+=ScoreMatrix[**i][**j];
  }else{ //this is linear time in number of sequences (as opposed to n^2 as above)
    unsigned int baseCount[BASES];
    memset(baseCount,0,sizeof(unsigned int)*BASES);
    for(vector<BaseIterator>::const_iterator i=column.begin();i!=column.end();++i)
      baseCount[**i]++;
    for(BASE_TYPE i=0;i<BASES;i++){
      if(!baseCount[i])
	continue;
      for(BASE_TYPE j=0;j<i;j++)
	totalScore+=baseCount[i]*baseCount[j]*ScoreMatrix[i][j];
      totalScore+=baseCount[i]*(baseCount[i]-1)/2*ScoreMatrix[i][i];
    }
  }
  return totalScore/(Score)(column.size()*(column.size()-1)/2);
}

void addColumnPairScores(const vector<BaseIterator> &column,vector<vector<Score> > &output){ //full set of pairs
  //there's no escaping s^2 in this case, so just go for it.
  output.resize(column.size(),vector<Score>(column.size(),0)); //in case any are uninited...
  for(size_t i=0;i<column.size();++i)
    for(size_t j=0;j<i;++j){
      output[i][j]+=ScoreMatrix[*column[i]][*column[j]];
    }
}

Score minimumPairScore(const vector<vector<Score> > &pairs){
  assert(!pairs.empty());
  assert(!pairs.front().empty());
  Score x=MAXSCORE;
  for(size_t i=0;i<pairs.size();++i)
    for(size_t j=0;j<i;++j){
      if(pairs[i][j]<x)
	x=pairs[i][j];
    }
  assert(x!=MAXSCORE);
  return x;
}

SeqPos fuzzyExtendUntil(vector<BaseIterator> column,SeqPos max,int direction,long* scoreOut){
  Score score=0;
  Score maxScore=0,maxScoreOffset=0,loss=0,scoreDelta;
  vector<vector<Score> > pairScores;
  SeqPos offset=1;
  
  for(;loss<opt.fuzzyExtendLossLimit && offset<=max;offset++){
    for(size_t i=0;i<column.size();i++){
      if(direction>0) //go-go branch prediction!
	++column[i];
      else
	--column[i];
    }

    if(opt.scoreByMinimumPair){
      addColumnPairScores(column,pairScores);
      Score prev=score;
      score=minimumPairScore(pairScores); //because pairScores is actually maintaining additive _row pair scores_ in this case, here score simply the minimum additive score
      scoreDelta=score-prev;
    }else{
      scoreDelta=scoreColumn(column);
      score+=scoreDelta;
    }
    if(score>maxScore){
      maxScore=score;
      maxScoreOffset=offset;
    }
    loss-=scoreDelta;
    if(loss<0)
      loss=0;
  }
  *scoreOut=maxScore;
  return maxScoreOffset;
}
