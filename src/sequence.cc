/*
Copyright (C) 2006-2008 Keio University
(Kris Popendorf) <comp@bio.keio.ac.jp> (2006)

This file is part of Murasaki.

Murasaki is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Murasaki is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Murasaki.  If not, see <http://www.gnu.org/licenses/>.
*/

//////////////
// murasaki project
// sequence.cc
// provides "sequence" class for doing operations on dna sequences
//////////////

#include "globaltypes.h"
#include "murasaki.h"
#include "sequence.h"
#include "dinkymath.h"
#include "cmultiset.h"
#include "ecohash.h"
#include "murasaki_mpi.h"
#include "seqread.h"
#include "binseq.hh"
#include "scoring.h"
#include "mingw32compat.h"
#include "cryptohasher.h"
#include "openhash.h"
#include "timing.h"
#include "hashing.h"

#include <fstream>
#include <limits.h>
#include <assert.h>
#include <set>
#include <iterator>
#include <functional>
#include <algorithm>
#include <math.h> //need log...
#include <ctype.h> //for tolower <-- im a lazy lazy man
//#include <error.h>
#include <errno.h>
#include <unistd.h>
#include <new>
#include <strings.h> //memcpy

#ifdef USE_SHM_MMAP
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/mman.h>

#include <boost/filesystem/path.hpp>
#include <boost/filesystem/operations.hpp>
namespace fs = boost::filesystem;
#endif

#ifdef USE_SHM_SYSV
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#endif

#ifdef USE_LIBCRYPTOPP
#include <cryptopp/misc.h>
#endif

//these guys are used in creating the adaptive hash stuff:
#include <ext/functional>
using namespace __gnu_cxx;

using namespace std;


int next_sequence_=0;
int _wordsize=WORDSIZE;
int hash_bits=27;
word hash_padding=0;
int max_hash_bits=27; //keeps the approx 256mb hash as default (or 515mb on 64bit)
word hash_mask=lowN(hash_bits);
int hash_max=intpow(2,max_hash_bits);
//int hash_max=67108879; //first prime over .5gb/8bytes
//524288057; //arbitrary default. first prime number past .5gb
word hash_size;

word alternating_mask;
word activeHash; //for building hash member lists
word totalSequenceMemory=0;
word sys_pagesize=getpagesize();

//static inits
map<int,int> HashMethod::popmap;

//hasher objects
//if we're scrapping the bottom of the barrel for RAM (or cache performance is suffering), we could use a smaller type here
typedef SBox<unsigned short int> SBoxT;
//typedef SBox<unsigned char> SBoxT;
SBoxT *sbox=0;
CryptoHasher *cryptoHasher=0;

//seqtable bits
Sequence** seq_table;
SeqIdx seq_table_size;
map<Sequence*,SeqIdx> seq2idx;
bool _seq_no_data=false;
double globalBaseFreq[4]={0,0,0,0};
word globalBaseCount[4]={0,0,0,0};
word globalCounted=0;

const SeqPos maxSeqPos=((unsigned)((SeqPos)-1))>>1;

void set_seqtable(vector<Sequence*> seqs){
  seq_table_size = seqs.size();

  seq_table = (Sequence**)malloc(sizeof(Sequence*)*seq_table_size);
  if(seq_table == NULL) { cerr << "set_seqtable(): malloc() failed." << endl; exit(-1); }
  for(SeqIdx i=0; i<seq_table_size; i++){
    seq_table[i] = seqs[i];
    seq2idx[seqs[i]] = i;
  }  
}

void initHashParams(int bits,BitSequence *pat){
  hash_bits=bits;
  hash_mask=lowN(bits);
  hash_size=intpow(2,bits);
  cout << "Using "<<bits<<" hashbits"<<endl;
  srandom(opt.rand_seed);
#ifndef NDEBUG
  srandom(0);
#endif

  hash_padding=random()&hash_mask;

  if(opt.quick_hash==3 && hash_bits<2){
    cerr << "Warning: adaptive hashing doesn't work with less than 2 hashbits."<<endl
	 << "         Using hashfunc 2 instead."<<endl;
    opt.quick_hash=2;
  }

  if(opt.quick_hash>100){
    opt.quick_hash=100;
  }

  switch(opt.quick_hash){
    //nothing to initialize (absurdly naive hashers)
  case 1:case 2:break;
    //adaptive hashers
  case 0:
    pat->compileHashFunc();
    sbox=new SBoxT(hash_bits);
    break;
  case 3:
    pat->compileHashFunc();
    break; 
#ifdef USE_LIBCRYPTOPP
    //cryptographic hash functions
  case 4:cryptoHasher=new MD5CryptoHasher();break;
  case 5:cryptoHasher=new SHACryptoHasher();break;
  case 6:cryptoHasher=new WhirlpoolCryptoHasher();break;
  case 7:cryptoHasher=new CRC32CryptoHasher();break;
  case 8:cryptoHasher=new Adler32CryptoHasher();break;
#endif
  case 100:
    pat->randomHashFunc(opt.randomHashInputs); //for testing only. this is absolutely crazy
    break;
  default:
    assert((cerr << "Bad quick_hash value" << endl) && 0);
    throw MurasakiDebugException("Unknown hash function");
  }
  switch(opt.hashPref){
  case EcoHash_t:
    EcoHash::init(seq_count,longestSeq,totalHashLength,bits);
    break;
  case OpenHash_t:
    OpenHash::init(seq_count,longestSeq,totalHashLength,bits);
    break;
  default: ;
  }
}

void initConstants(){
  srandom(time(0));
  alternating_mask=1;
  //make a mask like 010101010101010101
  for(int bit=2;bit<WORDSIZE;bit<<=1)
    alternating_mask|= (alternating_mask<<bit);
}

inline word revComp8bit(word t){
  switch(t & 255){
#include "revcomptable.cxx"    
  }
  throw MurasakiDebugException("revcomp8bit switch table is broken.");
}

inline word revCompWord(word t){
#if defined(USE_LIBCRYPTOPP) && WORDSIZE == 32
    //  t = ((t & 0xAAAAAAAA) >> 1) | ((t & 0x55555555) << 1);
    t = ((t & 0xCCCCCCCC) >> 2) | ((t & 0x33333333) << 2);
    t = ((t & 0xF0F0F0F0) >> 4) | ((t & 0x0F0F0F0F) << 4);
    return CryptoPP::ByteReverse((CryptoPP::word32)(~t));
#elif defined(USE_LIBCRYPTOPP) && WORDSIZE == 64
    //  t = ((t & W64LIT(0xAAAAAAAAAAAAAAAA)) >> 1) | ((t & W64LIT(0x5555555555555555)) << 1);
    t = ((t & W64LIT(0xCCCCCCCCCCCCCCCC)) >> 2) | ((t & W64LIT(0x3333333333333333)) << 2);
    t = ((t & W64LIT(0xF0F0F0F0F0F0F0F0)) >> 4) | ((t & W64LIT(0x0F0F0F0F0F0F0F0F)) << 4);
    return CryptoPP::ByteReverse((CryptoPP::word64)(~t));
#else
    //if WORDSIZE isn't 32 or 64, Murasaki will probably break...but
    //for historical raisins (and in case we don't have crypto++), I'll leave this intact:
    word res=0;
    for(int i=0;i<WORDSIZE;i+=8)
      res=(res<<8) | revComp8bit(t>>i);
    return res;
#endif
}

int highestBit(word t){
  for(int i=WORDSIZE-4;i>=0;i-=4){
    switch((t>>i) & (word)15){
    case 1:return i;
    case 2:case 3:return i+1;
    case 4:case 5:case 6:case 7:return i+2;
    case 8:case 9:case 10:case 11:case 12:case 13:case 14:case 15:
      return i+3;
      //this could be unrolled arbitrarily for performance
    }
  }
  assert(0);
  return -1;//wtf?
}

int lowestBit(word t){
  for(int i=0;i<WORDSIZE;i+=4){
    switch((t>>i) & (word)15){
    case 1:case 3:case 5:case 7: case 9: case 11: case 13: case 15:
      return i;
    case 2:case 6:case 10:case 14:return i+1;
    case 4:case 12:return i+2;
    case 8:return i+3;
      //this could be unrolled arbitrarily for performance
    }
  }
  assert(0);
  return -1;//wtf?
}

int matchCount(word t){ //count whole-base matches
  int count=0;
  for(int i=0;i<WORDSIZE;i+=4){
    switch((t>>i) & (word)15){ //4 bits at a time
    case 3:case 12:count+=1;break;
    case 15:count+=2;break;
    default:break;
    }
  }
  return count;
}

int popCount(word t){
  int count=0;
  for(int i=0;i<WORDSIZE;i+=4){
    switch((t>>i) & (word)15){
    case 1:case 2:case 4:case 8:count+=1;break;
    case 3:case 5:case 6:case 9:case 10:case 12:count+=2;break;
    case 7:case 11:case 13:case 14:count+=3;break;
    case 15:count+=4;
      //this could be unrolled arbitrarily for performance
    }
  }
  return count;
}

SeqPos Sequence::length() const{
  return fwd->length();
}

Sequence::Sequence()
  : 
  fwd(),rev(),seqID(next_sequence_++)
#ifdef USE_SHM_SYSV
  ,sysv_key(-1),sysv_shmid(-1)
#endif
{
}

Sequence::Sequence(string _filename):
  filename(_filename),name(_filename),baseFilename(_filename),
  seqID(next_sequence_++)
#ifdef USE_SHM_SYSV
  ,sysv_shmid(-1)
#endif
{
#ifdef MURASAKI_MPI
  if(mpi_capable){
    word length;
    if(opt.mpi_fileDistro){
      if(mpi_id==0){
	fwd=new BitSequence(this);
	length=(word)fwd->length();
	MPI_Bcast(&length,1,MPI_UNSIGNED_LONG,0,MPI_COMM_WORLD);
	cout << "Distributing "<<filename<<" via MPI"<<endl;
      }else{
	pair<size_t,size_t> range; //rangeSpec wants to use this
	SequenceFileReader::parseRangeSpec(baseFilename,range,opt.seqreadOptions);
	cout << "Waiting for node 0 to finish parsing "<<filename<<endl;
	MPI_Bcast(&length,1,MPI_UNSIGNED_LONG,0,MPI_COMM_WORLD);
	fwd=new BitSequence(length,this);
	cout << "Receiving "<<filename<<" from node 0"<<endl;
      }
    }else{
      fwd=new BitSequence(this);
    }
    //synch str with other procs
    fwd->mpi_distribute();

    cout << "Done with forward strand ("<<fwd->length()<<"bp). Preparing reverse complement..."<<endl;
    rev=fwd->reverseComplement();
  
    if(mpi_usingShm){
      if(!mpi_isHostLeader)
	cout << "Waiting for leader nodes to finish preparing sequence."<<endl;
      MPI_Barrier(MPI_COMM_WORLD);
      if(mpi_isHostLeader)
	cout << "Distributing to listener nodes."<<endl;
      else
	cout << (opt.use_shm_mmap ? "Mapping in shared pages from leader node.":"Finalizing shared region data.")<<endl;
      fwd->shm_distribute();
      rev->shm_distribute();
    }
  }else{
#endif
    fwd=new BitSequence(this);
    cout << "Done with forward strand ("<<fwd->length()<<"bp). Preparing reverse complement..."<<endl;
    rev=fwd->reverseComplement();
#ifdef MURASAKI_MPI
  }
#endif
  cout << "Done."<<endl;
  if(length()<100){
    cout << "Fwd contents:\n"<<*fwd<<endl;
    cout << "Rev contents:\n"<<*rev<<endl;
  }
}

Sequence::Sequence(const Sequence &a):
  filename(a.filename),
  name(a.name),
  fwd(a.fwd),
  rev(a.rev),  seqID(a.seqID)
#ifdef USE_SHM_SYSV
  ,sysv_shmid(a.sysv_shmid)
#endif
{
  assert(cerr << "Alert: Copying sequence: "<<a.name<<endl);
}

Sequence::~Sequence(){
  assert(cerr << "Alert: Destructing "<<name<<endl);
#ifdef USE_SHM_SYSV
  if(opt.use_shm_sysv){
    if(sysv_shmid==-1){//never been used?
      assert(cerr << "Alert: "<<name<<" never got assigned a shared memory region?"<<endl);
    }else{
      if(shmctl(sysv_shmid,IPC_RMID,NULL)<0)
	throw MurasakiException("Couldn't mark System V IPC shared memory region for deletion for sequence "+name+strerror(errno));
    }
  }
#endif
  delete fwd;
  delete rev;
}

//inline int Sequence::getId(){return seqID;}

//inline const vector<word>& BitSequence::words(){return _words;}
int BitSequence::wordsize(){return WORDSIZE;};

inline int BitSequence::readCode(SeqPos bitpos) const {
  word w=readWord(bitpos/WORDSIZE);
  return first2(w<<(MODWORDSIZE(bitpos)));
}

inline BASE_TYPE BitSequence::readBase(SeqPos basepos) const {
  word w=readWord(basepos*2/WORDSIZE);
  return first2(w<<(MODWORDSIZE(basepos*2)));
}

inline word BitSequence::wordAtBase(SeqPos basepos) const {
  SeqPos bitpos=basepos*2;
  word w=readWord(bitpos/WORDSIZE);
  SeqPos offset=MODWORDSIZE(bitpos);
  if(offset){
    //need to shuffle and append next word in place
    return ( (w << offset) |
	     (readWord(bitpos/WORDSIZE+1) >> (WORDSIZE-offset) )
	     );
  }
  return w;  
}

inline word BitSequence::readRawWord(SeqPos wordpos) const{
  return _words[wordpos];
}

word BitSequence::readWord(SeqPos wordpos) const{
  if(wordpos>=(SeqPos)word_count || wordpos<0)
    return 0;
  return (opt.reverseOtf && reverse) ? reverseOtf(wordpos):_words[wordpos];
}

inline word BitSequence::reverseOtf(SeqPos pos) const{
  int bitOffset=MODWORDSIZE(bit_count);
  if(bitOffset){
    if((word)pos<word_count-1){ //if it's not the very last word, it's actually composed of 2 words' data...
      return (revCompWord(_words[word_count-pos-1])<<(WORDSIZE-bitOffset)) |
	(revCompWord(_words[word_count-pos-2])>>(bitOffset));
    }
    else
      return (revCompWord(_words[word_count-pos-1])<<(WORDSIZE-bitOffset)); //last word is a lonely muffin.
  }
  return revCompWord(_words[word_count-pos-1]);
}

void BitSequence::shm_distribute(){
 //no distribution in non-mpi modes
#ifndef MURASAKI_MPI
  throw MurasakiDebugException("Distribute called without MPI support compiled??");
  return;
#else
  if(!mpi_capable)
    throw MurasakiDebugException("Distribute called without MPI enabled??");

  if(!opt.use_shm_sysv && !opt.use_shm_mmap)
    throw MurasakiDebugException("Distribute called without any shared memory modes enabled.");

#ifdef USE_SHM_SYSV
  if(opt.use_shm_sysv){
    if(reverse || !seq)
      return; //no distribution necessary. w00t.

    //on the forward strand
    //however now that we know that everyone has their sequence mapped and attached,
    //if we do the rm on the segment now, we'll prevent memory leaks in a segfaulty disaster
    if(seq->sysv_shmid==-1){//never been used?
      assert(cerr << "Alert: "<<seq->name<<" never got assigned a shared memory region?"<<endl);
    }else{
      if(opt.verbose)cout << "Tagging seq "<<seq->name<<"'s sysv region for deletion."<<endl;
      if(shmctl(seq->sysv_shmid,IPC_RMID,NULL)<0)
	throw MurasakiException("Couldn't mark System V IPC shared memory region for deletion for sequence "+seq->name+strerror(errno));
    }
    return;
  }
#endif
#ifdef USE_SHM_MMAP
  if(opt.use_shm_mmap){
    int fd;
    void *mem;
    const int NFS_Timeout=60;
    fs::path bitFile;
    switch(shmUsed){
    case SHM_MMAP_RW: //nothing, we're done.
      break;
    case SHM_MMAP_RO: //open file and mmap it.
      if(opt.reverseOtf && reverse){
	assert(seq);assert(seq->fwd);assert(seq->fwd->_words);
	_words=seq->fwd->_words; //attach to fwd seq's words, and we're done
	break;
      }
      bitFile=fs::path(genBitFilename());
      for(int trial=0;!fs::exists(bitFile);trial++){
	if(trial+1>NFS_Timeout){
	  throw MurasakiException("Gave up waiting for "+bitFile.string()+" to appear in filesystem.");
	}
	cerr << bitFile.string() << " not found. Waiting for filesystem to catch up..."<<endl;
	sleep(1);
      }
      if((fd=open(bitFile.string().c_str(),O_RDONLY))<0){
	throw MurasakiException("Couldn't open 2bit storage file for reading: "+bitFile.string()+" : "+strerror(errno));
      }
      if((mem=mmap(NULL,word_count*sizeof(word),PROT_READ,MAP_SHARED | MAP_POPULATE,fd,0))==MAP_FAILED){
	throw MurasakiException("Failed mapping listener block: "+string(strerror(errno)));
      }
      _words=(word*)mem;
      break;
    default:
      assert(cerr << "Don't have a shm mode assigned??"<<endl &&  0);
    }

    return;
  }
#endif
  throw MurasakiDebugException("BitSequence::shm_distribute called without shared memory support compiled");
  return;
#endif
}

void BitSequence::mpi_distribute(){
#ifdef MURASAKI_MPI
  if(mpi_capable){
    assert(_words);
    if(opt.mpi_fileDistro){
      word *start=_words;
      word sent=0;
      if(mpi_isHostLeader){ //leaders only!
	Stopwatch filesend;
	filesend.start();
	while(sent<word_count){
	  word block=min<word>(word_count-sent,INT_MAX>>1); //silly MPI specifies transmission count in ints...
	  cout << (mpi_id==0 ? "Sending a ":"Receiving a ")<<humanMemory(block*sizeof(word))<<" block."<<endl;
	  MPI_Bcast(start+sent,block,MPI_UNSIGNED_LONG,0,mpi_leaders_comm);
	  sent+=block;
	  cout << percent((double)sent,(double)word_count) << (mpi_id==0 ? " sent":" received")<<endl;
	}
	filesend.stop();
	if(opt.verbose)
	  cout << "Data distributed in "<<filesend<<" ("<<humanMemory(((double)(word_count)*sizeof(word))/filesend.asDouble())<<"/s)"<<endl;
      }else
	cout << "My leader's receiving a copy of the sequence for me."<<endl;
    }

    //and distribute meta-data!
    if(opt.mpi_fileDistro || mpi_usingShm){
      cout << "Synchronizing sequence meta-data:"<<endl;
      matchRegions.worldSync();
      subSeqs.worldSync();
    }
  }else
#endif
    throw MurasakiDebugException("BitSequence::mpi_distribute called without MPI support!");
}

string BitSequence::genBitFilename(){
  assert(seq);
  assert(!seq->filename.empty());
  return seq->filename+string(reverse ? ".2bit.rev":".2bit.fwd")+string(opt.repeatMask ? ".repeatmasked":"");
}

string baseToString(BASE_TYPE b){
  switch(b){
  case BASE_A:return "A";break;
  case BASE_C:return "C";break;
  case BASE_G:return "G";break;
  case BASE_T:return "T";break;
  case BASE_N:return "N";break;
  default: return "*";break;
  }
}

string bitToString(word w){
  switch(w & (word)3){
  case 0:return "A"; break;
  case 1:return "C"; break;
  case 2:return "G"; break;
  case 3:return "T"; break;
  }
  return " ";
}

string wordToString(word w,int bits){
  string ret;
  int start=bits < 0 ? bits+WORDSIZE:0;
  for(int i=start;i<WORDSIZE;i+=2)
    switch(first2(w<<i)){
    case 0:ret+="A"; break;
    case 1:ret+="C"; break;
    case 2:ret+="G"; break;
    case 3:ret+="T"; break;
    }
  return ret;
}

string wordToPattern(word w,int bits){
  string ret;
  for(int i=0;i<bits;i+=2)
    switch(first2(w<<i)){
    case 0:ret+="0"; break;
    case 1:ret+="-"; break;
    case 2:ret+="x"; break;
    case 3:ret+="1"; break;
    }
  return ret;
}

string repString(string s,int count){
  if(count<1)
    return string();
  string r=s;
  while(--count)
    r+=s;
  return r;
}

string BitSequence::asString() const {
  string ret;

  if(seq){
    SeqPos i=0;
    int li=0;
    BaseIterator b(seq,reverse ? -1:1);
    while(i<_length){
      if(li==0){
	char buf[50];
#ifdef LARGESEQ_SUPPORT
	sprintf(buf,"%4ld ",i+1);
#else
	sprintf(buf,"%4d ",i+1);
#endif
	ret+=buf;
      }
      ret+=baseToString(*b);
      ++b;
      i++;
      if(i%(WORDSIZE/2)==0)
	ret+=" ";
      li++;
      if(li>=64){
	ret+="\n";
	li=0;
      }
    }
  } else {
    const int wordsPerLine=(80/WORDSIZE*2);
    for(word i=0;i<word_count;i++){
      if(i%wordsPerLine==0){
	char buf[50];
	sprintf(buf,"%4lu ",i*WORDSIZE/2+1);
	ret+=buf;
      }
      
      string seg=wordToString(readWord(i));
      if(i>=word_count-1)
	seg.resize((((bit_count-1)%WORDSIZE)+1)/2);
      ret+=seg;
      
      
      if(i<word_count-1 && (i+1)%wordsPerLine==0)
	ret+="\n";
      else
	ret+=" ";
    }
  }
  return ret;
}

string BitSequence::asPattern() const {
  string ret;
  for(word i=0;i<word_count;i++){
    if(i%(80/WORDSIZE*2)==0){ //used to be i%4 but that looks weird on hpc
      char buf[50];
      sprintf(buf,"%4lu ",i*WORDSIZE/2+1);
      ret+=buf;
    }
    ret+=wordToPattern(readWord(i),i<word_count-1 ? WORDSIZE:
		      ((bit_count-1)%WORDSIZE)+1);
    if(i<word_count-1 && (i+1)%(80/WORDSIZE*2)==0)
	ret+="\n";
      else
	ret+=" ";
  }
  return ret;
}

int BitSequence::compHashLength(){
  int count=0;
  for(word i=0;i<word_count;i++){
    count+=popCount(readWord(i))/2;
  }
  _hashLength=count;
  return count;
}

//operator
SeqPos BitSequence::cmpRight(SeqPos base_pos,BitSequence& target,SeqPos base_targetPos){
  if(base_pos>=_length || base_targetPos>=target._length)return 0; //no can do, mack
  SeqPos pos=2*base_pos;//to bitspace
  SeqPos targetPos=2*base_targetPos;

  SeqPos myOffset=MODWORDSIZE(pos),tOffset=MODWORDSIZE(targetPos);
  word myFrame=pos/WORDSIZE,tFrame=targetPos/WORDSIZE;
  word myWord=readWord(myFrame),myNext=readWord(myFrame+1);
  word tWord=target.readWord(tFrame),tNext=target.readWord(tFrame+1);

  SeqPos max=min<SeqPos>(spaceRight(base_pos),target.spaceRight(base_targetPos));
  //  assert(cout << "local Spaceright: "<<spaceRight(base_pos)<< " target spaceright: "<< target.spaceRight(base_targetPos)<<endl);
  //  assert(cout << "Valid range: "<<(base_pos)<<","<<(base_pos+max)<<endl);
  if(!max)return 0; //if dead, die
  SeqPos matched=0;
  while(myFrame<word_count && tFrame<target.word_count){
    if(myOffset)
      myWord=(myWord<<myOffset) | ((myNext>>(WORDSIZE-myOffset)));
    if(tOffset)
      tWord=(tWord<<tOffset) | ((tNext>>(WORDSIZE-tOffset)));
    word res=(tWord ^ myWord);
    //    assert(cout << "Cmp right -> "<<wordToString(myWord)<<" to "<<wordToString(tWord) << " = "
    //	   << wordToString(res) << endl);
    if(res){ //sucky, not a whole match
      SeqPos highest=highestBit(res);
      matched+=WORDSIZE/2-1-(highest/2);
      break; //game over man, game over!
    }
    else
      matched+=WORDSIZE/2;
    
    myFrame++;tFrame++;
    myWord=myNext;myNext=readWord(myFrame+1);
    tWord=tNext;tNext=target.readWord(tFrame+1);
  }
  assert(matched>=0);
  if(matched>=max)
    matched=max;
  assert(matched>=0);
  return matched;
}

SeqPos BitSequence::cmpLeft(SeqPos base_pos,BitSequence& target,SeqPos base_targetPos){
  if(base_pos < 0 || base_targetPos < 0)return 0; //no can do, mack
  SeqPos pos=2*(base_pos);//to bitspace
  SeqPos targetPos=2*(base_targetPos);
  SeqPos myOffset=MODWORDSIZE(pos+2),tOffset=MODWORDSIZE(targetPos+2);
  SeqPos myFrame=pos/WORDSIZE,tFrame=targetPos/WORDSIZE;
  word myWord=readWord(myFrame),myNext=readWord(myFrame-1);
  word tWord=target.readWord(tFrame),tNext=target.readWord(tFrame-1);

  SeqPos max=min<SeqPos>(spaceLeft(base_pos),target.spaceLeft(base_targetPos));
  if(!max)return 0; //if dead, die
  SeqPos matched=0;
  while(myFrame>=0 && tFrame>=0){
    //    cout << "myOffset: "<<myOffset<<" tOffset: "<<tOffset<<endl;
    //    cout << "*Slice: "<<wordToString(myWord>>(WORDSIZE-myOffset)) << " | " << wordToString(myNext<<myOffset) << endl;
    //    cout << "& Dice: "<<wordToString(tWord) << " | "<< wordToString(tNext)<<endl;
    if(myOffset)
      myWord=(myWord>>(WORDSIZE-myOffset)) | ((myNext<<(myOffset)));
    if(tOffset)
      tWord=(tWord>>(WORDSIZE-tOffset)) | ((tNext<<(tOffset)));
    word res=(tWord ^ myWord);
    //    assert(cout << "Cmp Left <- "<<wordToString(myWord)<<" to "<<wordToString(tWord) << " = "
    //    	   << wordToString(res) << endl);
    if(res){ //sucky, not a whole match
      SeqPos highest=lowestBit(res);
      matched+=(highest/2);
      break; //game over man, game over!
    }
    else
      matched+=WORDSIZE/2;
    
    myFrame--;tFrame--;
    myWord=myNext;myNext=readWord(myFrame-1);
    tWord=tNext;tNext=target.readWord(tFrame-1);
  }
  assert(matched>=0);
  if(matched>=max)
    matched=max;
  assert(matched>=0);
  return matched;
}

bool BitSequence::equal(SeqPos pos,BitSequence& target,SeqPos targetPos,BitSequence& patt){
  pos*=2;//to bitspace
  targetPos*=2; 
  SeqPos myOffset=MODWORDSIZE(pos),tOffset=MODWORDSIZE(targetPos);
  word myMask=highN(myOffset),tMask=highN(tOffset);
  word myFrame=pos/WORDSIZE,tFrame=targetPos/WORDSIZE;
  word myWord=readWord(myFrame),myNext=readWord(myFrame+1);
  word tWord=target.readWord(tFrame),tNext=readWord(tFrame+1);

  word pFrame=0;
  word pWord;

  SeqPos matched=0;
  while(myFrame<word_count && tFrame<target.word_count && pFrame<patt.word_count){
    pWord=patt.readWord(pFrame%patt.word_count); //pattern just loops for now
    if(myOffset)
      myWord=(myWord<<myOffset) | (myNext<<myOffset & ~myMask);
    if(tOffset)
      tWord=(tWord<<tOffset) | (tNext<<tOffset & ~tMask);
    word res=(tWord ^ myWord) & pWord;
    if(res){ //sucky, not a whole match
      SeqPos highest=highestBit(tWord);
      if(highest>=0) //partial match
	matched+=highest/2+1;
      break; //game over man, game over!
    }
    else
      matched+=WORDSIZE/2;
    
    myFrame++;tFrame++;pFrame++;
    myWord=myNext;myNext=readWord(myFrame+1);
    tWord=tNext;tNext=readWord(tFrame+1);
  }
  return matched>=patt._length;
}

pair<SeqPos,SeqPos>* Location::localRegion(){ 
  //  cout << "Converting pos "<<pos<<" on "<<seqno<<" into a region.\n";
  return (pos>0 ? seq()->fwd->localRegion(pos-1):seq()->rev->localRegion(seq()->length()+pos));
}

SeqPosPairArray::iterator Location::localRegionIte(){ //find region local to pos
  //  cout << "Converting "<<(*this)<<" into a local region. pos is "<<pos;
  SeqPos bitseqp=(pos>0 ? pos:0-pos)-1;
  //  cout << ", but bitseqp is "<<bitseqp<<endl;
  SeqPosPairArray &regions=bitSeq()->matchRegions;
  pair<SeqPos,SeqPos> target(bitseqp,bitseqp); //for simplicity

  SeqPosPairArray::iterator
    result=lower_bound(regions.begin(),regions.end(),target,ltRegion());
  if(result==regions.end() || bitseqp<result->first){
    if(result==regions.end())assert(cout << "bitseqp "<<bitseqp << " not in any region"<<endl && 0);
    if(bitseqp<result->first)assert(cout << "bitseqp "<<bitseqp<<" falls before start of next region"<<endl && 0);
    return regions.end();
  }
  //  assert(cout << "region found: "<<result->first<<","<<result->second<<endl);
  return result;
}

pair<SeqPos,SeqPos>* BitSequence::localRegion(SeqPos pos){ //find region local to pos
  pair<SeqPos,SeqPos> target(pos,pos); //for simplicity
  SeqPosPairArray::iterator
    result=lower_bound(matchRegions.begin(),matchRegions.end(),target,ltRegion());
  if(result==matchRegions.end()){
    //    assert(cout << "pos "<<pos << " not in any region"<<endl);
    return 0;
  }
  if(pos<result->first){
    //    assert(cout << "pos "<<pos<<" falls before start of next region"<<endl);
    return 0;
  }
  //  assert(cout << "region found: "<<result->first<<","<<result->second<<endl);
  return &(*result);
}

SeqPosPairArray::iterator BitSequence::localRegionIte(SeqPos pos){ //find region local to pos
  pair<SeqPos,SeqPos> target(pos,pos); //for simplicity

  SeqPosPairArray::iterator
    result=lower_bound(matchRegions.begin(),matchRegions.end(),target,ltRegion());
  return result;
}

SeqPos BitSequence::spaceRight(SeqPos pos){ //find region local to pos
  pair<SeqPos,SeqPos> target(pos,pos); //for simplicity
  pair<SeqPos,SeqPos> *region(localRegion(pos));
  if(!region){
    return 0;
  }
  return region->second-pos+1;
}

SeqPos BitSequence::spaceLeft(SeqPos pos){ //find region local to pos
  pair<SeqPos,SeqPos> target(pos,pos); //for simplicity
  pair<SeqPos,SeqPos> *region(localRegion(pos));
  if(!region){
    return 0;
  }
  return pos-region->first+1;
}

UsedInt BitSequence::subSeqBounds(SeqPos at){
  pair<SeqPos,SeqPos> target(at,at);
  SeqPosPairArray::iterator
    result=lower_bound(subSeqs.begin(),subSeqs.end(),target,ltRegion());
  assert(result!=subSeqs.end());
  assert(result->first<=at);
  return UsedInt(result->first,result->second);
}

UsedInt Sequence::getSubSeqBounds(SeqPos at){
  UsedInt subSeqBounds((at>0 ? fwd:rev)->subSeqBounds(bitSeqCoords(at)));
  if(at<0)
    swap<SeqPos>(subSeqBounds.start,subSeqBounds.stop);
  return seqCoords(subSeqBounds,at); //coordinate system insanity!
}

UsedInt Sequence::growInBounds(const UsedInt &basis,SeqPos amount){
  UsedInt bounds(getSubSeqBounds(basis.start));
  return UsedInt(max<SeqPos>(bounds.start,basis.start-amount),min<SeqPos>(bounds.stop,basis.stop+amount));
}

BaseIterator Sequence::iterate(SeqPos at){
  return BaseIterator(this,at);
}

pair<BaseIterator,BaseIterator> Sequence::iterate(const UsedInt &at){
  pair <BaseIterator,BaseIterator> r(BaseIterator(this,at.start),BaseIterator(this,at.stop));
  if(at.start<0)
    swap(r.first,r.second);
  return r;
}

//mutator
void BitSequence::invert(){ //flips to reverse comp sequence
  for(word i=0;i<word_count;i++){
    _words[i]= ~_words[i];
  }
  //last word is tricky. all trailing elements have to be C in order
  //to not screw up dumb hashing techniques
  SeqPos last=word_count-1;
  _words[last]=_words[last] & highN(MODWORDSIZE(bit_count));
}

word* BitSequence::allocWords(){
  word *mem;
  assert(!_words); //better not already have words allocated....!
  if(opt.verbose)cout << "Allocing words...";

  if(reverse && opt.reverseOtf){
    assert(seq);assert(seq->fwd);assert(seq->fwd->_words);
    if(opt.verbose)cout << "as mirror of fwd"<<endl;
    return seq->fwd->_words; //shares the same data, and we're done.
  }

#ifdef MURASAKI_MPI
#ifdef USE_SHM_SYSV
  if(opt.use_shm_sysv && seq){
    if(opt.verbose)cout << "using sysv";
    size_t size=word_count*sizeof(word);
    if(!opt.reverseOtf)
      size*=2;//fetch double the size because we're storing both fwd and rev in there
    //sysv segments are ephemereal and local to this mpi set, so we just use the filename as our key
    shmUsed=SHM_SYSV;
    if(reverse){
      assert(seq->fwd->_words); //MUST allocate fwd before rev.
      if(opt.verbose)cout << " (continued from fwd allocation)"<<endl;
      return seq->fwd->_words+word_count; //second half of block
    }
    if(opt.verbose)cout << " (new segment)"<<endl;
    //ok, going to create a new segment
#ifdef SYSV_KEYBYFILE
    //I can't think of any reasonable reason why this method would be preferred. We'll call this "deprecated"
      if((seq->sysv_key=ftok(seq->baseFilename.c_str(),mpi_sysv_projid))==-1)
	throw MurasakiException("Error generating System V IPC key for "+string(seq->filename)+": "+strerror(errno));
    if((seq->sysv_shmid=shmget(seq->sysv_key,size,IPC_CREAT | 00600))==-1)
      throw MurasakiException("Error creating System V IPC shared memory segment (size: "+humanMemory(size)+") for "+string(seq->filename)+": "+strerror(errno));
#else
      //create a unique key by magics
      if(mpi_myLocalRank==0){
	if((seq->sysv_shmid=shmget(IPC_PRIVATE,size,IPC_CREAT | 00600))==-1)
	  throw MurasakiException("Error creating System V IPC shared memory segment (size: "+humanMemory(size)+") for "+string(seq->filename)+": "+strerror(errno));
      }
      MPI_Bcast(&seq->sysv_shmid,sizeof(key_t),MPI_BYTE,0,mpi_localhost_comm);
#endif
    if((mem=(word*)shmat(seq->sysv_shmid,NULL,mpi_isHostLeader ? O_RDWR:(O_RDONLY | SHM_RDONLY)))==(void*)-1)
      throw MurasakiException("Error attaching to System V IPC shared memory segment for "+string(seq->filename)+strerror(errno));
    cout << "Mapped a "<<humanMemory(size)<<" sysv chunk (key: "<<dhstring(seq->sysv_key)<<" shmid: "<<seq->sysv_shmid<<")"<<endl;

    if(shmctl(seq->sysv_shmid,IPC_RMID,NULL)<0)
      throw MurasakiException("Couldn't mark System V IPC shared memory region for deletion for sequence "+seq->name+strerror(errno));

    if(mpi_isHostLeader) //keep track of how much mem is lost to sequence storage
      totalSequenceMemory+=((((size-1)/sys_pagesize)+1)*sys_pagesize)/1024;
    return mem; //wee. done. much cleaner than mmap.
  }
#endif
#ifdef USE_SHM_MMAP
  if(opt.use_shm_mmap && seq){
    if(opt.verbose)cout << "using mmap"<<endl;
    if(opt.reverseOtf && reverse){
      assert(seq);assert(seq->fwd);
      shmUsed=SHM_MMAP_RO; //we'll pretend to be RO.
      return 0; 
    }
    //prepare to write my bitsequence to file if needed
    fs::path bitFile(genBitFilename());
    bool freshen=(!fs::exists(bitFile) || fs::last_write_time(bitFile)<fs::last_write_time(seq->filename));
    int fd; //actually we never need this again...
    if(freshen && ((opt.mmap_writePerHost && mpi_hostLeader[mpi_id]==mpi_id) ||
		   (mpi_id==0))){
      cerr << "Writing 2bit data out to file..."<<endl;
      fd=open(bitFile.string().c_str(),O_RDWR | O_CREAT | O_TRUNC | O_NOCTTY,S_IRUSR | S_IWUSR | S_IRGRP | S_IWGRP | S_IROTH | S_IWOTH); //create and/or trunc
      if(fd<0)
	throw MurasakiException("Couldn't create 2bit storage file: "+bitFile.string()+" : "+strerror(errno));
      //make file big enough to fit our sequence
      lseek(fd,word_count*sizeof(word)-1,SEEK_SET);
      write(fd,"",1); //write a byte.
      mem=(word*)mmap(NULL,word_count*sizeof(word),PROT_READ|PROT_WRITE,MAP_SHARED,fd,0);
      if(mem==MAP_FAILED){
	int mmap_error=errno;
	throw MurasakiException("Failed mapping initial block: "+string(strerror(mmap_error)));
      }
      totalSequenceMemory+=((word_count*sizeof(word))/(sys_pagesize-1)*sys_pagesize/1024);
      shmUsed=SHM_MMAP_RW;
    }else{
      //don't write just load from file when distribute is called.
      mem=0;
      shmUsed=SHM_MMAP_RO;
    }
    return mem;
  }
#endif
#endif
  mem=new word[word_count];
  totalSequenceMemory+=(word_count*sizeof(word));

  if(opt.verbose)cout << "directly"<<endl;
  return mem;
}

//init
void BitSequence::initPattern(const string &str){
  _hashLength=0;
  _length=str.length();
  bit_count=_length*2;
  word_count=((bit_count+WORDSIZE-1)/(WORDSIZE)); //rounds up to nearest full word
  assert(_words==0);
  _words=allocWords();
  bool writeWords=_words;
  word wordsStored=0;
  hasher=0;
  SeqPos bit=WORDSIZE;
  word w=0;
  pair<SeqPos,SeqPos> region(0,0);
  for(SeqPos i=0;i<_length;i++){
    bit-=2;
    SeqPos inpi=reverse ? (_length-i-1):i;
    switch(str[inpi]){
    case '1': w|=((word)3<<bit);isPattern=true;_hashLength++;break;
    case '0':isPattern=true;break;
    default: throw MurasakiException("Invalid pattern character (dec:"+dstring(inpi)+" char: "+str[inpi]+") encountered at position "+dstring(inpi));
    }

    if(bit==0){
      if(writeWords)
	_words[wordsStored++]=(reverse ? ~w:w);
      bit=WORDSIZE;
      w=0;
    }
  }

  if(bit<WORDSIZE)
    _words[wordsStored++]=highN(WORDSIZE-bit) & (reverse ? ~w:w);
}

BitSequence::BitSequence(Sequence* s):
  shmUsed(SHM_NONE),
  _hashLength(0),
  isPattern(false),
  seq(s),_words(0),
  reverse(false),
  counted(0)
{
  Stopwatch seqLoadTime;
  seqLoadTime.start();

  try {
    if(opt.useSeqBinary){
      try {
	cout << "Loading "<<s->filename<<endl;
	SequenceBinary binseq(s->filename);
	loadBinary(binseq);
      }catch(SeqReadException e){
	if(opt.verbose)
	  cerr << "Binary load failed ("<<e.reason()<<")."<<endl
	       <<"Parsing "<<s->filename<<" directly"<<endl;
	try {
	  SequenceFileReader reader(s->filename,opt.seqreadOptions);
	  SequenceBinary binseq(reader);
	  loadBinary(binseq);
	  if(!binseq.save())
	    cerr << "#Warning: Save of binary sequence to "<<binseq.getBinaryFilename()<<" failed";
	}catch(SeqReadException e){
	  throw MurasakiException("Error reading "+s->filename+": "+e.reason());
	}
      }
    }else{
      cout << "Parsing "<<s->filename<<" directly"<<endl;
      SequenceFileReader reader(s->filename,opt.seqreadOptions);
      loadReader(reader);
    }
  }catch(SeqReadException e){
    throw MurasakiException("Error reading "+s->filename+": "+e.reason());
  }

  seqLoadTime.stop();

  if(counted<1)
    throw MurasakiException("Murasaki can't use empty sequences ("+s->filename+")");

  cout << "Read "<<counted<<"bp ("<<humanMemory(counted)<<"p) in "<<seqLoadTime<<" ("<<humanMemory((double)counted/seqLoadTime.asDouble())<<"p/s)"<<endl;
}

void BitSequence::loadBinary(const SequenceBinary &binseq){
  hasher=0;
  _length=binseq.length();
  bit_count=_length*2;
  word_count=((bit_count+WORDSIZE-1)/(WORDSIZE)); //rounds up to nearest full word
  _words=allocWords();
  bool writeWords=_words;
#ifdef MURASAKI_MPI
  if(seq && mpi_capable && !mpi_isHostLeader)
    writeWords=false;
#endif

  if(writeWords){
    memcpy(_words,binseq.words(),binseq.getWordCount()*sizeof(word));
  }
  
  const int srcMatchRegionsIdx=opt.repeatMask ? 2:1;
  subSeqs.resize(binseq.regionSizes[0]);
  matchRegions.resize(binseq.regionSizes[srcMatchRegionsIdx]);
  assert(sizeof(SequenceBinary::Region)==sizeof(SeqPosPair));
  memcpy(&matchRegions.front(),binseq.regions[srcMatchRegionsIdx],sizeof(SequenceBinary::Region)*binseq.regionSizes[srcMatchRegionsIdx]);
  memcpy(&subSeqs.front(),binseq.regions[0],sizeof(SequenceBinary::Region)*binseq.regionSizes[0]);
  
  //grab base frequency
  for(int b=0;b<4;b++){
    word count=binseq.counters[b]-(opt.repeatMask ? binseq.counters[4+b]:0);
    globalBaseCount[b]+=count;
    counted+=count;
  }
  globalCounted+=counted;

#ifdef USE_SHM_MMAP
  if(shmUsed==SHM_MMAP_RW)
    mmap_msync();
#endif
}

void BitSequence::loadReader(SequenceFileReader& reader)
{
  hasher=0;
  _length=reader.peekLength();
  bit_count=_length*2;
  word_count=((bit_count+WORDSIZE-1)/(WORDSIZE)); //rounds up to nearest full word
  _words=allocWords();
  bool writeWords=_words;
#ifdef MURASAKI_MPI
  if(seq && mpi_capable && !mpi_isHostLeader)
    writeWords=false;
#endif
  word wordsStored=0;
  SeqPos counters[4]={0,0,0,0};
  SeqPos bit=WORDSIZE;
  word w=0;
  bool inRegion=false;
  bool matchable=false;
  pair<SeqPos,SeqPos> region(0,0),subSeqBounds(0,0);
  long prevSubSeqId=reader.getSubSeqId();
  for(SeqPos i=0;i<_length;i++){
    bit-=2;
    char c=reader.getc();
    switch(c){
    case 'A':case 'a': counters[0]++;counted++;matchable=true;break;
    case 'C':case 'c': w|=((word)1<<bit);counters[1]++;counted++;matchable=true;break;
    case 'G':case 'g': w|=((word)2<<bit);counters[2]++;counted++;matchable=true;break;
    case 'T':case 't': w|=((word)3<<bit);counters[3]++;counted++;matchable=true;break;
    case 'N':case 'n': matchable=false; break;
    case 0: //eof
      throw MurasakiDebugException("Fewer characters in stream than predicted ("+dstring(i)+" read vs "+dstring(_length)+") predicted!");
    default: throw MurasakiException("Unusable character (dec:"+dstring(c)+" char: "+c+") encountered at position "+dstring(i));
    }
    if(inRegion){ //in a matchable zone
      if(!matchable){//region end!
	region.second=i-1;
	inRegion=!inRegion;
	matchRegions.push_back(region);
      }
    }else{ //was in an N zone
      if(matchable){//region start
	region.first=i;
	inRegion=!inRegion;
      }
    }

    if(reader.getSubSeqId()!=prevSubSeqId){ //subseq changed!
      subSeqBounds.second=i-1;
      assert(subSeqBounds.second>=subSeqBounds.first);
      subSeqs.push_back(subSeqBounds);
      
      subSeqBounds.first=i+10; //this looks a bit hinky, but between every subseq there _should_ be exactly 10 Ns, so this accounts for that
      assert(c=='N'); //see? we're looking at an N!
      prevSubSeqId=reader.getSubSeqId();
    }

    if(bit==0){
      if(writeWords)
	_words[wordsStored++]=(reverse ? ~w:w);
      bit=WORDSIZE;
      w=0;
    }
  }

  if(inRegion){
    region.second=_length-1;
    matchRegions.push_back(region);
  }
  
  //finish up last subseq
  subSeqBounds.second=_length-1;
  assert(subSeqBounds.second>=subSeqBounds.first);
  subSeqs.push_back(subSeqBounds);

  if(bit<WORDSIZE)
    if(writeWords){
      _words[wordsStored++]=highN(WORDSIZE-bit) & (reverse ? ~w:w);
    }
  if(writeWords){
    assert(wordsStored==word_count);
    cout << "Collected "<<wordsStored<<" words (size: "<<word_count<<")"<<endl;
  }
  else {
    cout << "Postponed loading of "<<word_count<<" words (will fetch them directly later)"<<endl;
  }
  //set base frequency
  globalCounted+=counted;
  for(int b=0;b<4;b++){
    globalBaseCount[b]+=counters[b];
    //    globalBaseFreq[b]+=(double)counters[b]/(double)counted/2;
  }

#ifdef USE_SHM_MMAP
  if(shmUsed==SHM_MMAP_RW)
    mmap_msync();
#endif
}

BitSequence::BitSequence(const word length,Sequence *s,bool _reverse) :
  shmUsed(SHM_NONE),
  _length(length),bit_count(length*2),
  word_count((bit_count+WORDSIZE-1)/(WORDSIZE)), //rounds up to nearest full word)
  _hashLength(0),isPattern(false),
  seq(s),
  _words(0),
  reverse(_reverse),
  counted(0)
{
  _words=allocWords();
}

BitSequence* BitSequence::reverseComplement(){
  BitSequence* rev=new BitSequence(_length,seq,!reverse);
  bool writeWords=!opt.reverseOtf;
#ifdef MURASAKI_MPI
  if(seq && mpi_capable && !mpi_isHostLeader)
    writeWords=false;
#endif
  if(writeWords){
    int bitOffset=MODWORDSIZE(bit_count);
    for(word *src=_words+word_count-1,*dst=rev->_words;src>=_words;--src,++dst){
      *dst=revCompWord(*src);
      if(bitOffset){//have to shuffle bits
	if(dst!=rev->_words){ //if this wasn't my first, i have to fix the one before me too
	  *(dst-1)|=*dst>>bitOffset;
	}
	*dst<<=WORDSIZE-bitOffset;
      }
    }

    //and if we're doing the mmap grunt work, we have to write this out.
#ifdef USE_SHM_MMAP
    if(shmUsed==SHM_MMAP_RW && !opt.reverseOtf)
      rev->mmap_msync();
#endif
  }

  //have to flip regions too
#ifdef MURASAKI_MPI
  if(!(matchRegions.hasShm() && mpi_myLocalRank!=0)){
#endif
    cout << " <-> Reversing metdata." << endl;
    rev->matchRegions.reserve(matchRegions.size());
    //    cout << "(Filling "<<rev->matchRegions.size()<<" / "<<rev->matchRegions.capacity()<<" array with "<<matchRegions.size()<<" "<<sizeof(SeqPosPair)<<" elems"<<endl; 
    for(SeqPosPairArray::iterator i=matchRegions.begin();i!=matchRegions.end();++i){
      //      cout << "((Adding #"<<rev->matchRegions.size()<<" == #"<<(i-matchRegions.begin())<<" ~ "<<i->first<<","<<i->second<<endl;
      rev->matchRegions.push_back(pair<SeqPos,SeqPos>(_length-i->second-1,_length-i->first-1));
    }
    //    cout << "(Reversing array of "<<rev->matchRegions.size()<<" at "<<(void*)(&(*rev->matchRegions.begin()))<<" ending "<<(void*)(&(*rev->matchRegions.end()))<<endl;
    std::reverse(rev->matchRegions.begin(),rev->matchRegions.end()); //oops. i eclipsed std::reverse with my variable name.
    
    //and subsequences
    rev->subSeqs.reserve(subSeqs.size());
    for(SeqPosPairArray::iterator i=subSeqs.begin();i!=subSeqs.end();++i)
      rev->subSeqs.push_back(pair<SeqPos,SeqPos>(_length-i->second-1,_length-i->first-1));
    std::reverse(rev->subSeqs.begin(),rev->subSeqs.end()); //oops. i eclipsed std::reverse with my variable name.
#ifdef MURASAKI_MPI
  }
  if(matchRegions.hasShm()){
    rev->matchRegions.localSync();
    rev->subSeqs.localSync();
  }
#endif

  globalCounted+=counted; //add this to globalCounted or we'll get confused about our sequence lengths.
  return rev;
}

void BitSequence::mmap_msync(){
#ifdef USE_SHM_MMAP
  assert(shmUsed==SHM_MMAP_RW);
  cout << "Flushing mmap'd region to disk."<<endl;
  int res=msync(_words,word_count*sizeof(word),MS_SYNC);
  if(res<0){
    int err=errno;
    throw MurasakiException(string("Error synching mmap'd sequence to disk")+string(strerror(err)));
  }
#else
  throw MurasakiDebugException("mmap_msync called without mmap support??");
#endif
}

void finishGlobalFrequencies(){
  const char *forwardBases[4]={"A","C","G","T"};
  const char *bases[4]={"A/T","C/G","G","T"};
  cout << "Forward frequencies:";
  word baseTotal=0;
  for(int b=0;b<4;b++)baseTotal+=globalBaseCount[b];
  for(int b=0;b<4;b++)
    cout << "  "<<forwardBases[b]<<":"<<(double)globalBaseCount[b]/(double)baseTotal*100<<"%";
  cout << endl;
  for(int b=0;b<2;b++){
    globalBaseFreq[b]=((double)(globalBaseCount[b]+globalBaseCount[3-b]))/(double)baseTotal;
    globalBaseFreq[3-b]=globalBaseFreq[b];
  }
  cout << "Global base frequencies:";
  for(int b=0;b<2;b++){
    cout << "  "<< bases[b] << ":" << globalBaseFreq[b]*100 << "%";
  }
  cout << endl;
}

void BitSequence::init(const BitSequence& a){
  _words=a._words;
  word_count=a.word_count;
  bit_count=a.bit_count;
  _length=a._length;
  _hashLength=a._hashLength;
  hasher=a.hasher;
}

//constructors
BitSequence::BitSequence(const string &str):
  shmUsed(SHM_NONE),isPattern(false),
  seq(NULL),
  _words(0),reverse(false),
  counted(0)
{ //from 0101-ish patterns (as mask: ie double each bit)
  initPattern(str);
}

BitSequence::BitSequence(const BitSequence& a) :  //copy
  matchRegions(a.matchRegions),
  subSeqs(a.subSeqs),
  shmUsed(a.shmUsed),
  _length(a._length),
  bit_count(a.bit_count),
  word_count(a.word_count),
  _hashLength(a._hashLength),
  isPattern(a.isPattern),
  seq(a.seq),
  _words(a._words),
  reverse(a.reverse),
  counted(a.counted),
  hasher(a.hasher)
{
  assert(cerr << "Alert: Copying a bitsequence."<<endl);
}

BitSequence::BitSequence() : //uninited
  _length(0),
  bit_count(0),word_count(0),
  _hashLength(-1),isPattern(false),seq(0),_words(0),reverse(false),
  counted(0)
{}

BitSequence::~BitSequence() {
  if(_words){  //if a sequence got inited and never used, it's possible it didn't get _words
    if(reverse && opt.reverseOtf)return; //not our job to cleanup.
    switch(shmUsed){
    case SHM_NONE:
      delete[] _words;
      break;
    case SHM_MMAP_RO:case SHM_MMAP_RW:
#ifdef USE_SHM_MMAP
      munmap(_words,word_count*sizeof(word));
#endif
      break;
    case SHM_SYSV:
#ifdef USE_SHM_SYSV
      if(reverse)
	break; //nothing to do. w00t!
      //not really critical, but we can detach the sysv pages...
      if(shmdt(_words)<0)
	throw MurasakiException("Couldn't detach System V shared memory region for "+(seq ? seq->filename:string("(anon)"))+strerror(errno));
#endif
      break;
    default:
      assert(cerr << "Impossible case." << endl && 0);
    }
  }
}

int HashMethod::maxSources(){
  vector<int> members;
  vector<int> tmp;
  transform(inputs.begin(),inputs.end(),back_insert_iterator<vector<int> >(tmp),select1st<pair<int,int> >());
  sort(tmp.begin(),tmp.end());
  unique_copy(tmp.begin(),tmp.end(),back_insert_iterator<vector<int> >(members));
  int score=0;
  for(vector<int>::iterator i=members.begin();i!=members.end();i++){
    if(!popmap.count(*i)) //new to the area?
      score+=(popmap[*i]=popCount(seq->readWord(*i)));
    else
      score+=popmap[*i];
  }
  return score;
}

string HashMethod::srcSetToString(const set<int> &a){
  ostringstream r;
  for(set<int>::const_iterator i=a.begin();i!=a.end();++i)
    r << *i << ";";
  return r.str();
}

double HashMethod::fitnessCheck(){
  //calculate entrop based on pattern and inputs
  int targetBases=(hash_bits+1)/2;
  used.clear();entropy.clear(); //reset state
  entropy.resize(targetBases,0);
  used.resize(seq->length(),0);
  size_t srcCount=used.size(),sinkCount=entropy.size(),patWeight=seq->_hashLength;
  vector<set<int> > entropySrcToSink(srcCount),entropySinkToSrc(sinkCount);
  active=0;
  for(InputList::iterator i=inputs.begin();i!=inputs.end();i++){
    word w=seq->readRawWord(i->first);
    if(i->second > 0)
      w=w>>(i->second*2);
    else
      w=w<<(-i->second*2);
    w&=hash_mask;
    for(unsigned b=0;b<entropy.size();b++){
      int srcbase=i->first*WORDSIZE/2+ //word this input is selecting
	(WORDSIZE/2-1-i->second)- //b=0 would be operating on
	b;
      if(srcbase < 0 || srcbase>=(int)used.size()) {
	continue;
      }
      if(w & (word)(3L<<(b*2L))){ //damn int literals
	assert(srcbase<seq->length() && srcbase>=0);
	assert(seq->readBase(srcbase)==3);
	entropySrcToSink[srcbase].insert(b);
	entropySinkToSrc[b].insert(srcbase);
	active|=3<<(b*2);
	used[srcbase]++;
      }
    }
  }

  //if any any sink uses more than half of the sourcs, flip them so we count the UNUSED ones instead
  for(unsigned sink=0;sink<sinkCount;sink++){
    if(entropySinkToSrc[sink].size()>patWeight){ //need to flip
      set<int> realSrcs(entropySinkToSrc[sink]);
      for(set<int>::iterator src=realSrcs.begin();src!=realSrcs.end();++src){
	entropySrcToSink[*src].erase(sink);
	entropySinkToSrc[sink].erase(*src);
      }
      for(unsigned src=0;src<srcCount;src++){
	if(!realSrcs.count(src) && seq->readCode(src)){ //is in the effective set
	  entropySrcToSink[src].insert(sink);
	  entropySinkToSrc[sink].insert(src);
	}
      }
    }
  }

  int totalUsed=0;
  for(size_t i=0;i<used.size();i++)
    totalUsed+=used[i];

  /*
  cout << "Initial src->sink map: ";
  for(unsigned i=0;i<entropySrcToSink.size();++i){
    if(entropySrcToSink[i].empty())
      continue;
    cout << i << "->" << "{";
    for(set<int>::iterator sinki=entropySrcToSink[i].begin();sinki!=entropySrcToSink[i].end();++sinki)
      cout << *sinki<<",";
    cout << "} ";
  }
  cout << endl;
  */

  if(opt.hasherFairEntropy){
    totalCorrelationPenalty=0;
    for(unsigned sink=0;sink<entropySinkToSrc.size();sink++){ //foreach sink
      if(entropySinkToSrc[sink].empty())
	continue;
      
      double correlationPenalty=0;
      if(opt.hasherCorrelationAdjust && entropySinkToSrc[sink].size()>1){
	//sum of distance pair squares
	long distSum=0,pairs=0;
	for(set<int>::iterator srcA=entropySinkToSrc[sink].begin();srcA!=entropySinkToSrc[sink].end();++srcA){
	  set<int>::iterator srcB=srcA;
	  for(++srcB;srcB!=entropySinkToSrc[sink].end();++srcB){
	    distSum+=(*srcA-*srcB)*(*srcA-*srcB);
	    //	      cout << " dist @"<<sink<<" pair "<<pairs<<":  "<<(*srcA-*srcB)<<endl;
	    pairs++;
	  }
	}
	//	  correlationPenalty=1/sqrt((double)distSum/pairs); //inverse mean distance squared * C (magic penalty constant. at a distance of 1 this is the penalty)
	correlationPenalty=.2/((double)distSum/(double)pairs); //inverse mean distance squared * C (magic penalty constant. at a distance of 1 this is the penalty)
	//	  cout << "Correlation @"<<sink<<": "<<distSum<<"/"<<pairs<<"=\t"<<correlationPenalty<<endl;
	totalCorrelationPenalty+=correlationPenalty;
      }
      
      for(set<int>::iterator srcA=entropySinkToSrc[sink].begin();srcA!=entropySinkToSrc[sink].end();++srcA){
	double weight=(1.0-correlationPenalty)/(double)used[*srcA];
	entropy[sink]+=weight;
      }
    }
    //All done in time o(entropy.size()) O(used.size()*entropy.size())
  }else{
    //now that we've figured out the src->sink graph, assign each sink at least one src.
    //do low degree sinks first so we don't undercount
    vector<set<int> > sinkDegrees;
    for(unsigned i=0;i<entropy.size();i++){
      if(sinkDegrees.size()<=entropySinkToSrc[i].size())
	sinkDegrees.resize(entropySinkToSrc[i].size()+1);
      sinkDegrees[entropySinkToSrc[i].size()].insert(i);
    }
    
    for(unsigned degree=1;degree<sinkDegrees.size();degree++){
      while(!sinkDegrees[degree].empty()){
	int sink=*(sinkDegrees[degree].begin());
	//this sink must have at least one source, we arbitrarily choose the first one
	//(I'm pretty sure this is safe, but equally sure that the proof of that is complicated)
	assert(!entropySinkToSrc[sink].empty());
	int src=*(entropySinkToSrc[sink].begin());
	//      cout << "Using "<<src<<" -> "<<sink<<endl;
	//note that sink gets entropy from src
	entropy[sink]++;

	//update all the other sinks available to src (noting that they're no longer available)
	//and change the sink degree      
	for(set<int>::iterator altsinki=entropySrcToSink[src].begin();altsinki!=entropySrcToSink[src].end();){
	  set<int>::iterator nextAltsinki=altsinki; ++nextAltsinki;
	  int altsink=*altsinki;
	  assert(sinkDegrees[entropySinkToSrc[altsink].size()].count(altsink)==1);
	  sinkDegrees[entropySinkToSrc[altsink].size()].erase(altsink);
	  entropySinkToSrc[altsink].erase(src);
	  entropySrcToSink[src].erase(altsink);
	  //	cout << "Pruning "<<src<<" <-> "<<altsink<<endl;
	  if(entropySinkToSrc[altsink].size()) //don't actually use 0
	    sinkDegrees[entropySinkToSrc[altsink].size()].insert(altsink);
	  altsinki=nextAltsinki;
	}
      }
    }

    //any remaining entropy is just a bonus. distribute arbitrarily
    for(unsigned src=0;src<entropySrcToSink.size();src++){
      if(!entropySrcToSink[src].empty())
	entropy[*(entropySrcToSink[src].begin())]++;
    }
  }

  sources=0;
  for(unsigned i=0;i<used.size();i++)
    if(used[i])
      sources++;
  assert(sources<=seq->hashLength());

  //kill entropy from any redundant source sets
  set<string> usedSrcSets;
  for(int sink=0;sink<(int)entropy.size();sink++){
    string srcSet(srcSetToString(entropySinkToSrc[sink]));
    pair<set<string>::iterator, bool> res(usedSrcSets.insert(srcSet));
    if(!res.second)//already existed
      entropy[sink]=0;
  }

  //evaluate entropy
  entropyHi=entropy.front(),entropyLo=entropy.front(),entropyTotal=0;
  empties=0;
  int J=0;
  for(vector<double>::iterator i=entropy.begin();i!=entropy.end();i++,J++){
    entropyTotal+=*i;
    if(*i<.4){
      //      cout << "empty @"<<J<<": "<<(1-*i)<<endl;
      empties+=1-*i;
    }
    entropyHi=max<int>(entropyHi,*i);
    entropyLo=min<int>(entropyLo,*i);
  }
  entropyMean=(double)entropyTotal/(double)entropy.size();
  double variance=0;
  for(vector<double>::iterator i=entropy.begin();i!=entropy.end();i++)
    variance+=sqr((double)(*i)-entropyMean);
  variance/=entropy.size();
  entropyStd=sqrt(variance);

  /*  cout << *this << ": ";
  for(unsigned i=0;i<entropy.size();i++)
    cout << entropy[i]<<":";
  cout << " ("<<entropyTotal<<")"<<endl;
  cout << endl;*/

  double entropyAgro=opt.hasherEntropyAgro; //the bigger this is, the more anxiously we go after entropy
  double x=seq->_hashLength; //basically we estimate how much entropy to aim for based on how many entropy sources are available
  
  fitness=entropyTotal //overall entropy is a good thing
    -((double)inputs.size()/(double)WORDSIZE) //using more inputs is to a certain extent a bad thing, but hardly the end of the universe.
    -max<double>(1,x/WORDSIZE/2.0+entropyAgro*log(x)*sqrt(x)) //target entropy scaling function, asymtotically sqrt(x) like, weighted to be at about x/2 by 1000-ish
    -(double)(empties*entropy.size()*entropy.size()) //empties are evil
    -(entropyStd) //hopefully we're spreading entropy evenly over the whole key (or else we're wasting cycles)
    //    -(empties==0 && targetBases==seq->_hashLength ? totalUsed-seq->_hashLength:0) //special case that we have exactly as many input as outputs, if we don't have any empties prefer to use sources as few times as possible (ie: minimize reuse of sources)
    ;
  return fitness;
}

void BitSequence::randomHashFunc(int n){
  cout << "Generating random hash function with "<<n<<" hash inputs."<<endl;
  hasher=new HashMethod(this,n);
  hasher->fitnessCheck();
  cout << "Random uses "<<hasher->sources<<"/"<<(hashLength())<<" bases and has "<<hasher->empties<<" empties from "<<hasher->inputlist().size()<<" inputs."<<endl;
  cout << "Score: "<<hasher->fitness<<endl;
  cout << "Entropy: high: "<<hasher->entropyHi<<" lo: "<<hasher->entropyLo<< " total: "<<((double)hasher->entropyTotal)<<" bases (mean: "<<hasher->entropyMean<<" stddev: "<<hasher->entropyStd<<")"<<endl;
  cout << "Total correlation penalty: "<<hasher->totalCorrelationPenalty<<endl;
  //  cout << "Pre cleanup: "<<*hasher<<endl;
  cout << "Function content: "<<*hasher<<endl<<hasher->prettyPrint();
}

void BitSequence::compileHashFunc() {
  assert(isPattern);
  vector<HashMethod>  funcs;
  for(int c=0;c<100;c++){ //spawn 100 population
    funcs.push_back(HashMethod(this));
  }
  
  int cycle,lastImprovedCycle=0;
  const int targetCycles=opt.hasherTargetGACycles;
  double improved=0;
  sort(funcs.begin(),funcs.end());
  double worstScore=funcs.front().fitness;
  double bestScore=funcs.back().fitness;
  ticker.reset(targetCycles);
  for(cycle=0;
      bestScore<hash_bits && (cycle<targetCycles || (cycle-lastImprovedCycle)<(cycle/(targetCycles/10)));cycle++,ticker.tick()){ //arbitrarily large!
    double prevBestFitness=bestScore;
    //make the top N mate together, cull the bottom N
    int population=funcs.size();
    int kidCount=population/3;
    int maters=kidCount/2;
    int minMate=population-maters;
    
    for(int matei=0;matei<kidCount;matei++){
      if(random() & 1){ //actually mate with another hash function
	const HashMethod &a=funcs[randint(maters)+minMate],&b=funcs[randint(maters)+minMate];
	HashMethod c(a,b);
	funcs.push_back(c);
      }else{
	HashMethod c(funcs[randint(maters)+minMate]); //clone one
	c.mutate(); //mutate
	funcs.push_back(c);
      }
    }
    
    funcs.erase(funcs.begin(),funcs.begin()+kidCount);
    
    sort(funcs.begin(),funcs.end());
    
    bestScore=funcs.back().fitness;
    improved=bestScore-prevBestFitness;
    if(improved>0)
      lastImprovedCycle=cycle;
  }
  ticker.done();

  cout << "Acceptable hash function found after "<<cycle<<" cycles."<<endl;
  hasher=new HashMethod(funcs.back());//keep the best
  hasher->finalize();
  cout << "Best uses "<<hasher->sources<<"/"<<(hashLength())<<" bases and has "<<hasher->empties<<" empties from "<<hasher->inputlist().size()<<" inputs."<<endl;
  cout << "Score: "<<hasher->fitness<<" (cf. worst: "<<worstScore<<")"<<endl;
  cout << "Entropy: high: "<<hasher->entropyHi<<" lo: "<<hasher->entropyLo<< " total: "<<((double)hasher->entropyTotal)<<" bases (mean: "<<hasher->entropyMean<<" stddev: "<<hasher->entropyStd<<")"<<endl;
  cout << "Total correlation penalty: "<<hasher->totalCorrelationPenalty<<endl;
  //  cout << "Pre cleanup: "<<*hasher<<endl;
  cout << "Post cleanup: "<<*hasher<<endl<<hasher->prettyPrint();
}

void HashMethod::finalize(){
  removeDuplicates();
  pruneUseless();
  fitnessCheck();
}

void HashMethod::removeDuplicates(){
  InputList tmp;
  sort(inputs.begin(),inputs.end());
  unique_copy(inputs.begin(),inputs.end(),insert_iterator<InputList>(tmp,tmp.begin()));
  inputs=tmp;
}

string HashMethod::prettyPrint(){
  //each line starts with input tag
  int leaderSize=10;
  string spacer=repString(" ",leaderSize);
  string str=spacer;
  //                            |0                              |32                             |64
  static const char alphabet[]="ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/"; //the base64 alphabet
  static const int alphasize=sizeof(alphabet)-1; assert(alphasize==64);
  int lineWidth=70;
  int hashLen=((int)hash_bits+1)/2; //only show base positions
  int padLeft=(lineWidth-hashLen)/2;

  //draw hash positions
  if(hashLen>=10){ //extra tens digit line
    str+=repString(" ",padLeft);
    for(int p=hashLen-1;p>=0;p--){
      if(p>=10)
	str+=dstring((int)(p/10)%10);
      else
	str+=" ";
    }
    str+="\n";str+=spacer;
  }
  str+=repString(" ",padLeft);
  for(int p=hashLen-1;p>=0;p--)
    str+=dstring((int)p%10);
  str+="\n";str+=spacer;
  str+=repString(" ",padLeft);
  str+=repString("=",hashLen);
  str+="\n";

  //draw each input
  HashMethod::InputList::const_iterator last=inputs.end();
  last--;
  for(HashMethod::InputList::const_iterator i=inputs.begin();i!=inputs.end();i++){
    char leader[50];
    sprintf(leader,"w[%2d]%s%2d:",i->first,(i->second>0 ? ">>":"<<"),abs(i->second));
    leader[leaderSize+1]='\0';
    str+=leader;
    int linePos=0;
    int start=i->first*WORDSIZE/2+ //first base used in key
      (WORDSIZE/2-1-i->second)- 
      hashLen+1;

    for(int p=start-padLeft;p<seq->length() && linePos<lineWidth;p++,linePos++){
      if(p<0 || p/(WORDSIZE/2)!=i->first){
	str+=" ";
	continue;
      }
      bool patOn=(seq->readBase(p)==3);
      if(linePos<padLeft || linePos>=padLeft+hashLen)
	str+=patOn ? "-":"_";
      else {
	if(patOn)
	  str+=alphabet[p%alphasize];
	else
	  str+=".";
      }
    }
    str+="\n";
  }

  return str;
}

ostream& operator<<(ostream &os,const HashMethod &a){
  HashMethod::InputList::const_iterator last=a.inputs.end();
  last--;
  for(HashMethod::InputList::const_iterator i=a.inputs.begin();i!=a.inputs.end();i++){
    if(i->second > 0)
      os << "w["<<i->first<<"]>>"<<i->second;
    else if(i->second < 0)
      os << "w["<<i->first<<"]<<"<<0-i->second;
    else 
      os << "w["<<i->first<<"]";
  if(i!=last)
    os << " ^ ";
  }
  os << " {"<<a.inputs.size()<<"}";
  return os;
}

int HashMethod::align(int w){
  word pat=seq->readRawWord(w);
  //try all possible alignments
  multimap<int,int> score2shift;
  int bestscore=0;
  for(int shift=1-WORDSIZE/2;shift<WORDSIZE/2-1;shift++){
    int score;
    if(shift>0)
      score=popCount( ((pat>>shift*2)) & hash_mask);
    else
      score=popCount( ((pat<<-shift*2)) & hash_mask);
    bestscore=max<int>(score,bestscore);
    score2shift.insert(pair<int,int>(score,shift));
  }
  //choose randomly from among the non-zero scoring options
  vector<int> options;
  transform(score2shift.lower_bound(1),score2shift.end(),
	    insert_iterator<vector<int> >(options,options.begin()),
	    select2nd<map<int,int>::value_type>());
  if(options.empty())
    return randint(WORDSIZE)-WORDSIZE/2; //wtf?
  return options[randint(options.size())];
}

void HashMethod::pruneUseless(){
  vector<InputList::iterator> toErase;
  for(InputList::iterator i=inputs.begin();i!=inputs.end();i++){
    word w=seq->readRawWord(i->first);
    if(i->second>=0)
      w=w>>(i->second*2);
    else
      w=w<<(-i->second*2);
    w&=hash_mask;
    if(!popCount(w))
      toErase.push_back(i);
  }
  for(vector<InputList::iterator>::iterator i=toErase.begin();
      i!=toErase.end();i++)
    inputs.erase(*i);
}

void HashMethod::addWord(){
  int w;
  if(!unusedWords.empty()){
    int wi=randint(unusedWords.size());
    w=unusedWords[wi];
    unusedWords.erase(unusedWords.begin()+wi);
  }else{
    w=randint(seq->word_count);
  }
  inputs.push_back(pair<int,int>(w,align(w))); //add a word
}

void HashMethod::removeWord(){
  int wi=randint(inputs.size());
  //  int w=inputs[wi].first;
  inputs.erase(inputs.begin()+wi); //pull off inputs
  //pretend it never existed because we don't want force it to be pushed back on
  //  unusedWords.push_back(w);
}

void HashMethod::mutate(){
  //  cerr << "Mutating "<<id<<" "<<*this<<" ";
  int fate=randint(100); //magical 100-sided mutation die
  if(fate<50 || maxSources()<hash_bits){//randomly add another word
    addWord();
  }else if(fate<5 && inputs.size()>1){ //randomly kill a word
    removeWord();
  }else{
    //bump a random word in a random direction
    int w=randint(inputs.size());
    int shift=inputs[w].second+(random()&1) ? 1:-1;
    if(shift>=WORDSIZE/2)
      shift-=WORDSIZE; //wraparound
    else if(shift<=-WORDSIZE/2)
      shift+=WORDSIZE;
    inputs[w].second=shift;
  }
  finalize();
  //   cerr << "->" << *this<< " new score: "<<score<<endl;
}

HashMethod::HashMethod(const HashMethod &a) :
  inputs(a.inputs),seq(a.seq),
  unusedWords(a.unusedWords),
  sources(a.sources),empties(a.empties),fitness(a.fitness),
  active(a.active)
{
  //  cout << a.id << " cloned => "<<id<<" ("<<*this<<")"<<endl;
}

HashMethod::HashMethod(const HashMethod &a,const HashMethod &b) :
  inputs(a.inputs),
  seq(a.seq)
{
  inputs.insert(inputs.end(),b.inputs.begin(),b.inputs.end()); //add in inputs from b
  
  //erase a third
  int toErase=inputs.size()/3;
  for(int i=0;i<toErase;i++)
    inputs.erase(inputs.begin()+randint(inputs.size()));

  //reconstruct unused words:
  set<int> usedWords;
  transform(inputs.begin(),inputs.end(),insert_iterator<set<int> >(usedWords,usedWords.begin()),select1st<pair<int,int> >());
  for(word w=0;w<seq->word_count;w++)
    if(!usedWords.count(w))
      unusedWords.push_back(w);

  //  mutate();
  finalize();
}

HashMethod::HashMethod(BitSequence *s) :
  seq(s),active(0)
{
  cout.flush();
  for(word w=0;w<s->word_count;w++)
    unusedWords.push_back(w);
  addWord();
  fitnessCheck();
}

HashMethod::HashMethod(BitSequence *s,int n) : //random hash func for n words
  seq(s),active(0)
{
  if(n){ //craft a random hash function with exactly n inputs
    cout << "# Generating random hash function with "<<n<<" random inputs"<<endl;
    set<pair<int,int> > usedInps;
    while((int)inputs.size()<n){
      pair<int,int> inp(randint(s->wordCount()),randint(WORDSIZE-1)-WORDSIZE/2+1);
      if(!usedInps.count(inp)){
	usedInps.insert(inp);
	inputs.push_back(inp);
      }
    }
  }else{ //randomly selected hash function out of all possible hash functions
    cout << "# Selecting hash function purely at random."<<endl;
    for(int i=0;i<(int)seq->wordCount();i++)
      for(int shift=1-WORDSIZE/2;shift<=WORDSIZE/2-1;shift++)
	if(random() & 1)
	  inputs.push_back(pair<int,int>(i,shift));
  }
}

bool operator==(const BitSequence& a,const BitSequence& b){
  for(word i=0;i<a.word_count && i<b.word_count;i++)
    if(a.readWord(i)!=b.readWord(i))return false;
  return true;
}

Location::Location(Sequence *s,SeqPos p) :
  seqno(lookup_seqtable(s)),pos(p) { }

Location::Location(SeqIdx s,SeqPos p) :
  seqno(s),pos(p) { }

Location::Location() :
  seqno(255),pos(0) { } //this should only be used to create temp storage

Sequence* Location::seq() const{
  return seq_table[seqno];
}

BitSequence* Location::bitSeq() const {return pos>0 ? seq()->fwd:seq()->rev;}

word Hash::sizeAt(const HashKey key,const HashVal &a){
  //universally functional sizeAt...(slow. should be specialized for each hash)
  if(emptyAt(key))
    return 0;
  Location aloc(a);
  Window awin(aloc,hashpat);
  list<LocList> fulllist;
  mfh->getMatchingSets(key,fulllist);
  for(list<LocList>::iterator seti=fulllist.begin();seti!=fulllist.end();++seti){
    LocList &l=*seti;
    SeqIdx baseSeq=0;
    while(l[baseSeq].empty() && baseSeq<seq_count)
      baseSeq++;
    if(baseSeq>=seq_count){
      assert((cerr << "Empty loclist??") && 0);
      continue; //wtf? how'd that happen?
    }
    Location &loc=l[baseSeq].front();
    Window locwin(loc,hashpat);
    if(locwin.equals(awin)){ //this is the one we care about!
      word sum=0;
      for(SeqIdx si=0;si<seq_count;si++)
	sum+=l[si].size();
      return sum;
    }
  }
  return 0;
}

void Hash::dump(ostream &os){
  HashKey key;
  unsigned s;

  for(key=0; key<(unsigned)hash_size; key++){
    unsigned c=0;
    LocList  l(seq_count); lookup(key, l);    

    s = sizeAt(key);
    if( s==0 ) continue;

    os.write( (char*)&key, sizeof(HashKey)  );
    os.write( (char*)&s,   sizeof(unsigned) );
    //    cout << "dump: size=" << s<< endl;
    for(unsigned i=0; i<l.size(); i++){
      for(list<Location>::iterator ll=l.at(i).begin(); ll!=l.at(i).end(); ll++){
	Location lll=*ll;
	os.write( (char*)&lll, sizeof(Location) );
	c++;
      }
    }
    if (s!=c){ cerr << "Hash::dump(): incorrect size?" << endl; }
  }

  key = 0;
  s = 0;
  os.write( (char*)&key, sizeof(HashKey)  );
  os.write( (char*)&s,   sizeof(unsigned) );
}

void Hash::load(istream &is){
  HashKey key;
  unsigned s;
  Location l;
  Location *buf;

  do {
    is.read( (char*)&key, sizeof(HashKey)  );
    is.read( (char*)&s,   sizeof(unsigned) );
    if( key==0 && s==0 ) break;

    buf = (Location*)malloc(sizeof(Location) * s);
    is.read( (char*)buf, sizeof(Location) * s);
    for(unsigned i=0; i<s; i++) add(key, buf[i]);
    free(buf);
  } while(true);
}

Hash::Hash(BitSequence *pat) :
  hashpat(pat)
{
}

Hash::~Hash(){}

void Hash::writeHistogram(ostream &of){
  vector<SeqPos> histo(1,0);
  for(word base=0;base<(word)hash_size;base++){ //oh this is fun...
    if(emptyAt(base))
      continue;
    usedBuckets++;
    unsigned size=sizeAt(base);
    if(size>=histo.size())
      histo.resize(size+1);
    histo[size]++;
  }
  if(of!=cerr)
    for(unsigned i=1;i<histo.size();i++){
      of << i << "\t" << histo[i]<<endl;
    }
}

void Hash::writeDetailedHistogram(ostream &of){
  for(word base=0;base<(word)hash_size;base++){ //oh this is fun...
    if(emptyAt(base))
      continue;
    unsigned size=sizeAt(base);
    if(size){
      if(opt.histogram>3){ //super freaking detailed
	LocList ll(seq_count);
	lookup(base,ll); //fetch all locations...
	vector<Location> locs;
	back_insert_iterator<vector<Location> > ii(locs);
	for(int i=0;i<seq_count;i++) 
	  copy(ll[i].begin(),ll[i].end(),ii); //throw all into big list
	sort(locs.begin(),locs.end());
	vector<Location>::iterator i=locs.begin();
	Location last(*i);
	word count=1;
	usedPats++; //we got at least 1 new one
	do{
	  i++;
	  if(i==locs.end())
	    break;
	  Location here(*i);
	  if(last<here){ //different src pattern
	    of << base << "\t" << Window(last,hashpat).prettyString() << "\t" << count << endl;
	    if(count>worstPat)
	      worstPat=count;
	    count=1;
	    last=here;
	    usedPats++;
	  }
	  count++;
	}
	while(1);
	if(count>worstPat)
	  worstPat=count;
	of << base << "\t" << Window(last,hashpat).prettyString() << "\t" << count << endl;
      }else
	of << base<<":"<<size << endl;
    }
  }
}

void Hash::writePerformanceData(string prefix){
  //i have no performance, i'm just an abstract class =P
}

UsedMap::UsedMap():
  used(seq_count)
{ }

bool UsedMap::sanityCheck() {
  for(unsigned i=0;i<used.size();i++){
    for(usedItree::iterator s=used[i].begin();
	s!=used[i].end();s++){
      assert(*s);
      assert((*s)->sanityCheck());
    }
  }
  return 1;
}

bool AnchorSet::sanityCheck() const {
  for(unsigned i=0;i<spaces.size();i++){
    assert(UsedInt_sanityCheck(spaces[i].key()));
    assert(*spaces[i]==this);
  }
  return 1;
}

bool IntervalSet::sanityCheck() const {
  for(unsigned i=0;i<spaces.size();i++){
    assert(UsedInt_sanityCheck(spaces[i]));
  }
  return 1;
}


bool Hash::sanityCheck(){
  for(word base=0;base<(word)hash_size;base++){
    if(emptyAt(base))
      continue;
    LocList loclist;
    lookup(base,loclist);
    for(unsigned s=0;s<loclist.size();s++)
      for(LocSubList::iterator i=loclist[s].begin();
	  i!=loclist[s].end();i++){
	Window w(*i,hashpat);
	word here=w.hash();
	//      cout << "hash for "<<l<<" ("<<w<<"): "<<wordToString(here) << " (vs "<< wordToString(base)<<")"<<endl;
	if(here!=base){
	  assert(here==base);
	  return false;
	}
      }
  }
  return true;
}

bool Hash::sanityCheck(word base){
  if(emptyAt(base))
    return true;
  //  cout << "Spot checking "<<wordToString(base)<<endl;
  LocList loclist;
  lookup(base,loclist);
  for(unsigned s=0;s<loclist.size();s++)
    for(LocSubList::iterator i=loclist[s].begin();
	i!=loclist[s].end();i++){
      Window w(*i,hashpat);
      word here=w.hash();
      //    cout << "  ** hash for "<<l<<" ("<<w<<"): "<<wordToString(here)<<endl;
      if(here!=base){
	assert(here==base);
      return false;
      }
    }
  return true;
}

bool UsedMap::fetchOverlaps(int seq,const UsedInt &a,set<AnchorSet*> &out){
  bool res=false;

  for(usedItree::range_iterator i(used[seq].in_range(a));i!=used[seq].end();++i){
    out.insert(*i);
    res=true;
  }

  return res;
}

bool UsedMap::fetchNearbyOverlaps(int seq,const UsedInt &a,SeqPos maxgrow,set<AnchorSet*> &out){
  bool res=false;
  UsedInt bounds(maxgrow ? seqs[seq]->growInBounds(a,maxgrow):a);

  for(usedItree::range_iterator i(used[seq].in_range(bounds));
      i!=used[seq].end();++i){
    out.insert(*i);
    res=true;
  }

  return res;
}

bool UsedMap::alreadyExists(const IntervalSet& a){
  set<AnchorSet*> matches,temp,context;

  int i=0;
  while(isZero(a.spaces[i])){
    i++;
    assert(i<seq_count);
  }


  if(!fetchOverlaps(i,a.spaces[i],context)){
    return false;
  }

  for(i++;i<seq_count;i++){
    if(isZero(a.spaces[i]))
      continue;
    if(!fetchOverlaps(i,a.spaces[i],temp))
      return false;
    set_intersection(temp.begin(),temp.end(),
		     context.begin(),context.end(),
		     inserter(matches,matches.begin()));
    if(matches.empty())
      return false;
    context.clear();
    context.insert(matches.begin(),matches.end());
    matches.clear();
  }
  bool winner=false;
  //must find out if a is entirely inside of anything in context
  for(set<AnchorSet*>::iterator ite=context.begin();
      ite!=context.end();
      ite++){
    AnchorSet *s=*ite;
    if(s->contains(a)){
      winner=true;
      if(opt.retainMembers){ //if we're keeping track of members, this counts as a hit.
	for(HashCount::const_iterator i=a.members.begin();i!=a.members.end();i++)
	  s->members[i->first]+=i->second;
      }
    }
  }
  return winner;
}

bool UsedMap::merge(IntervalSet& a){
  //  assert(a.sanityCheck());
  set<AnchorSet*> matches,temp,context;
  SeqPos joinDist=opt.joinNeighbors ? opt.joinDist:1;

  int i=0;
  while(isZero(a.spaces[i])){
    i++;
    assert(i<seq_count);
  }

  if(!fetchNearbyOverlaps(i,a.spaces[i],joinDist,context))
    return false;

  for(i++;i<seq_count;i++){
    if(isZero(a.spaces[i]))
      continue;
    if(!fetchNearbyOverlaps(i,a.spaces[i],joinDist,temp))
      return false;
    set_intersection(temp.begin(),temp.end(),
		     context.begin(),context.end(),
		     inserter(matches,matches.begin()));
    if(matches.empty())
      return false;
    context.clear();
    context.insert(matches.begin(),matches.end());
    matches.clear();
  }
  //context now contains all anchorsets which a overlaps in each sequence individually
  //(but any single anchorset might not overlap in all sequences!!!)
  assert(a.sanityCheck());
  bool didMerge=false;
  for(set<AnchorSet*>::iterator i=context.begin();
      i!=context.end();
      i++){
    if(!a.colinear(**i,joinDist)){
      continue;
    }
    assert((*i)->sanityCheck());
    //    cout << "Merging: "<<a<<" with "<< IntervalSet(**i)<<endl;
    //    assert(sanityCheck());
    assert(a.sanityCheck());
    a.coalesce(**i);
    //    assert(sanityCheck());
    assert(a.sanityCheck());
    remove(*i);
    //    cout << "Result: "<<a<<endl;
    //    assert(sanityCheck());
    assert(a.sanityCheck());
    didMerge=true;
  }
  if(didMerge)
    add(a);
  return didMerge;
}

void UsedMap::add(const IntervalSet &a){
  //  cout << "UsedMap::Add("<<const_cast<IntervalSet&>(a)<<")"<<endl;
  new AnchorSet(a,*this); //add it
}

void UsedMap::insert(IntervalSet& a){
  //  assert(cout << "Insert: "<<a<<endl);
  assert(a.sanityCheck());
  if(alreadyExists(a)){
    //      cout << "--- Already exists."<<endl;
    return; //already complete inside some other anchor?
  }

  Score ascore=opt.scoreFilter ? a.score():0; //we only care about the score at this point if we're going to filter by it

  if(opt.fuzzyExtend)
    ascore+=a.fuzzyExtend();
  else
    a.exactExtend();

  if(opt.scoreFilter && ascore<opt.scoreFilter)
    return;

  if(merge(a)){ //try to merge it
    //    cout << "--- Merged"<<endl;
    return; 
  }

  add(a); //completely new and fresh!
  //  cout << "--- Added as a new anchor"<<endl;
}

void UsedMap::remove(AnchorSet *a){
  //  assert(sanityCheck());
  for(unsigned i=0;i<a->spaces.size();i++){
    //    assert(cerr << "Erasing seq "<<i<<" - "<<*(a->spaces[i])<<" from: "<<used[i]<<endl);
    used[i].erase(a->spaces[i]);
    //    assert(sanityCheck());
  }
  //  assert(sanityCheck());
  delete a;
  //  assert(sanityCheck());
}

bool AnchorSet::contains(const IntervalSet &a) const {
  if(opt.gappedAnchors){
    for(unsigned i=0;i<spaces.size();i++){
      if(isZero(a.spaces[i]) && isZero(spaces[i].key()))
	continue;
      if(isZero(a.spaces[i]) != isZero(spaces[i].key()))
	return false;
      if(!UsedInt_contains(spaces[i].key(),a.spaces[i]))
	return false;
    }
    return true;
  }else{
    if(!(a.overlaps(*this) & OVERLAP_AINB))
      return false;
    if(a.hasGaps(*this))
      return false;
    return true;
  }
}

bool IntervalSet::contains(const IntervalSet &a) const {
  for(unsigned i=0;i<spaces.size();i++){
    if(isZero(a.spaces[i]) && isZero(spaces[i]))
      continue;
    if(isZero(a.spaces[i]) != isZero(spaces[i]))
      return false;
    if(!UsedInt_contains(spaces[i],a.spaces[i]))
      return false;
  }
  return true;
}

SeqPos UsedInt_distance(UsedInt a,UsedInt b){
  if(isRev(a)){
    UsedInt_rawInvert(a);
    UsedInt_rawInvert(b);
  }
  if(UsedInt_overlaps(a,b))
    return 0;
  return min<SeqPos>(abs(a.stop-b.start),abs(a.start-b.stop));
}

OverlapSense UsedInt_overlaps(UsedInt a,UsedInt b){
  assert(!isZero(a));
  assert(!isZero(b));
  
  if(isRev(a)){ //work in standardized coordinate space (ugh, so crazy)
    UsedInt_rawInvert(a);
    UsedInt_rawInvert(b);
  }

  
  if(!a.overlaps(b))
    return OVERLAP_NONE; //absolutely no overlap

  if(opt.gappedAnchors) //if we relied on strict matching overlap types under gapped anchor rules the order of anchor merges would make a difference resulting in icky non-determinism
    return ~OVERLAP_NONE; //we don't know what kind of overlap, but it's definitely "not none"

  OverlapSense res=OVERLAP_NONE;
  //flush cases are both AB and BA
  if(UsedInt_contains(b,a))res|=OVERLAP_AINB;
  if(UsedInt_contains(a,b))res|=OVERLAP_BINA;

  if(a.start<=b.start)
    res|=OVERLAP_START_AB;
  if(b.start<=a.start)
    res|=OVERLAP_START_BA;
  
  if(a.stop<=b.stop)
    res|=OVERLAP_STOP_AB;
  if(b.stop<=a.stop)
    res|=OVERLAP_STOP_BA;
  
  //  assert(cout << a << " overlaps " << b << " "<< OverlapSense2str(res) << endl);

  return res;
}

string OverlapSense2str(OverlapSense o){
  ostringstream res;
  vector<string> bits;
  if(o==OVERLAP_NONE)
    bits.push_back("nowhere");
  else if(o==~OVERLAP_NONE)
    bits.push_back("somewhere");
  else {
    if(o & OVERLAP_AINB && o & OVERLAP_BINA)
      bits.push_back("perfectly");
    else {
      if(o & OVERLAP_AINB)
	bits.push_back("a in b");
      else if(o & OVERLAP_BINA)
	bits.push_back("b in a");
      else {
	if(o & OVERLAP_START_AB && o & OVERLAP_START_BA)
	  bits.push_back("start_flush");
	else{
	  if(o & OVERLAP_START_AB)
	    bits.push_back("start_ab");
	  if(o & OVERLAP_START_BA)
	    bits.push_back("start_ba");
	}

	if(o & OVERLAP_STOP_AB && o & OVERLAP_STOP_BA)
	  bits.push_back("stop_flush");
	else{
	  if(o & OVERLAP_STOP_AB)
	    bits.push_back("stop_ab");
	  if(o & OVERLAP_STOP_BA)
	    bits.push_back("stop_ba");
	}
      }
    }
  }
  if(o & COLINEAR_AB)
    bits.push_back("colinear_ab");
  if(o & COLINEAR_AB)
    bits.push_back("colinear_ba");
  assert(!bits.empty()); //i should have taken care of all everything possible...
  copy(bits.begin(),bits.end(),ostream_iterator<string>(res,","));
  return res.str();
}

OverlapSense IntervalSet::overlaps(const AnchorSet &a) const {
  OverlapSense ref=OVERLAP_NONE;
  bool first=true;
  //  cout << "Testing overlap of "<<*this<<" with "<<IntervalSet(a)<<endl;
  for(unsigned i=0;i<spaces.size();i++){
    if(isZero(a.spaces[i].key()) && isZero(spaces[i]))
      continue;
    OverlapSense here=UsedInt_overlaps(spaces[i],a.spaces[i].key());
    if(!here)
      return here;
    //ok, overlaps partially, but do they overlap the same way?
    if(first){
      ref=here;
      first=false;
    }else{
      if(ref!=here) //overlaps only count among anchorsets if they're the same sense
	return OVERLAP_NONE;
    }
  }
  return ref;
}

SeqPos UsedInt_offset(UsedInt a,UsedInt b){
  return isRev(a) ? b.stop-a.stop:a.start-b.start;
}

SeqPos IntervalSet::offset(const AnchorSet& a) const {
  assert(!gapOffset(a).first);
  return gapOffset(a).second;  
}

pair<bool,SeqPos> IntervalSet::gapOffset(const AnchorSet &a) const {
  SeqPos offset;
  uint i=0;
  while(isZero(spaces[i])){
    assert(isZero(a.spaces[i].key()));
    i++;
    assert(i<spaces.size());
  }

  //  cout << "Testing "<<*this<<" and "<<IntervalSet(a)<<" for offset/gaps: "<<endl;
  offset=UsedInt_offset(spaces[i],a.spaces[i].key());
  if(seqs[i]->getSubSeqBounds(spaces[i].start).start!=seqs[i]->getSubSeqBounds(a.spaces[i].key().start).start){
    //    cout << spaces[i]<<" and "<<a.spaces[i].key()<<" are in different subsequences of "<<i<<endl;
    return pair<bool,SeqPos>(true,maxSeqPos);
  }
  //  cout << "  Offset "<<i<<": "<<offset<<endl;
  for(i++;i<spaces.size();i++){
    if(isZero(spaces[i])){
      assert(isZero(a.spaces[i].key()));
      continue;
    }
    if(seqs[i]->getSubSeqBounds(spaces[i].start).start!=seqs[i]->getSubSeqBounds(a.spaces[i].key().start).start){
      //      cout << spaces[i]<<" and "<<a.spaces[i].key()<<" are in different subsequences of "<<i<<endl;
      return pair<bool,SeqPos>(true,maxSeqPos);
    }
    //    cout << "  Offset "<<i<<": "<< UsedInt_offset(spaces[i],a.spaces[i].key()) << endl;
    if(UsedInt_offset(spaces[i],a.spaces[i].key())!=offset)
      return pair<bool,SeqPos>(true,maxSeqPos);
  }
  return pair<bool,SeqPos>(false,offset);
}

bool IntervalSet::hasGaps(const AnchorSet &a) const {
  return gapOffset(a).first;
}

ColinearSense IntervalSet::colinear(const AnchorSet &a,SeqPos maxDist) const {
  OverlapSense res=overlaps(a);
  if(!opt.gappedAnchors && hasGaps(a)){
    return COLINEAR_NONE;
  }

  if(res){
    //amend with colinear details
    if(res & (OVERLAP_START_AB | OVERLAP_STOP_AB | OVERLAP_AINB | OVERLAP_BINA))
      res|=COLINEAR_AB;
    if(res & (OVERLAP_START_BA | OVERLAP_STOP_BA | OVERLAP_AINB | OVERLAP_BINA))
      res|=COLINEAR_BA;
    return res;
  }

  if(opt.gappedAnchors){
    res=COLINEAR_AB|COLINEAR_BA; //one of those...dunno+dont care which

    //possible non-overlapping colinearity
    for(unsigned i=0;i<spaces.size();i++){
      if(isZero(a.spaces[i].key()) && isZero(spaces[i]))
	continue;
      if(isZero(a.spaces[i].key())!=isZero(spaces[i]) || isRev(a.spaces[i].key())!=isRev(spaces[i]))
	return COLINEAR_NONE;
      
      if(seqs[i]->getSubSeqBounds(spaces[i].start).start!=seqs[i]->getSubSeqBounds(a.spaces[i].key().start).start)
	return COLINEAR_NONE;

      if(opt.gappedAnchors){ //if using gaps, everything just has to be "close enough". actual colinearity is ignored
	if(UsedInt_distance(spaces[i],a.spaces[i].key())>maxDist)
	  return COLINEAR_NONE;
      }
    }
    return res;
  }else{
    for(uint i=0;i<spaces.size();i++){
      assert(isZero(spaces[i])==isZero(a.spaces[i].key()));
      if(isZero(spaces[i]) && isZero(a.spaces[i].key()))
	continue;
      if(UsedInt_distance(spaces[i],a.spaces[i].key())>maxDist){
	return COLINEAR_NONE;
      }
      //because we're gappless, all distances must be the same
      return (COLINEAR_AB|COLINEAR_BA); //not sure which. doesn't matter though.
    }
  }
  assert(0); //control doesn't reach here. really.
  return COLINEAR_NONE; //but just to quiet the compiler...
}

bool UsedInt_contains(const UsedInt &outer,const UsedInt &inner){
  return outer.start<=inner.start && outer.stop>=inner.stop;
}

void IntervalSet::coalesce(const AnchorSet &a){
  for(size_t i=0;i<spaces.size();i++){
    if(isZero(a.spaces[i]) || isZero(spaces[i]))
      continue;
    UsedInt_coalesce(spaces[i],a.spaces[i].key());
  }
  //mmmm tasty metadata - nom nom!
  if(opt.retainMembers)
    for(HashCount::const_iterator i=a.members.begin();i!=a.members.end();i++)
      members[i->first]+=i->second;
}

void UsedInt_coalesce(UsedInt &x,const UsedInt &a){
  if(isZero(x) || isZero(a))
    return;
  x.start=x.start<=a.start ? x.start:a.start;
  x.stop=x.stop>=a.stop ? x.stop:a.stop;
}

UsedInt UsedInt_coalesced(const UsedInt &a,const UsedInt &b){
  assert(!isZero(a));  assert(!isZero(b));  //no forcing coalesces of non-intervals
  return UsedInt(a.start<=b.start ? a.start:b.start, a.stop>=b.stop ? a.stop:b.stop);
}

inline UsedInt UsedInt_grown(const UsedInt &a,const SeqPos amt){
  if(isZero(a)) //no growing non-intervals
    return a;

  if(a.start>0)
    return UsedInt(max<SeqPos>(a.start-amt,1),a.stop+amt);
  else
    return UsedInt(a.start-amt,min<SeqPos>(a.stop+amt,-1));
}

UsedInt UsedInt_inverted(const UsedInt &a,const Sequence& s){
  if(isZero(a))
    return a;
  SeqPos length=s.length();
  if(a.start>0) //ugh. that took way more reverse engineering than it should have.
    return UsedInt(0-(length-a.start+1),0-(length-a.stop+1));
  else
    return UsedInt(a.start+length+1,a.stop+length+1);
}

UsedInt& UsedInt_invert(UsedInt &a,const Sequence& s){
  if(isZero(a))
    return a;
  SeqPos length=s.length();
  if(a.start>0){ //ugh. that took way more reverse engineering than it should have.
    a.start=0-(length-a.start+1);
    a.stop=0-(length-a.stop+1);
  }else{
    a.start=a.start+length+1;
    a.stop=a.stop+length+1;
  }
  return a;
}

UsedInt& UsedInt_rawInvert(UsedInt &x){ //use with care (most of the time sequence coordinates need to be inverted with the sequence-aware form above)
  swap<SeqPos>(x.start,x.stop);
  x.start=-x.start;
  x.stop=-x.stop;
  return x;
}

bool UsedInt_sanityCheck(const UsedInt &a){
  assert(a.start <= a.stop);
  return true;
}

AnchorSet::AnchorSet(const IntervalSet &a, UsedMap &usedMap) :
  members(a.members) //if opt.retainMembers is off, this should be empty anyway
{
  spaces.reserve(seq_count);
  for(size_t i=0;i<a.spaces.size();++i)
    spaces.push_back(usedMap.used[i].insert(a.spaces[i],this));
}

void IntervalSet::add(HashVal &a,SeqPos length){
  length--;
  if(a.pos)
    spaces.push_back(UsedInt(a.pos>0 ? a.pos:a.pos-length,
			     a.pos>0 ? a.pos+length:a.pos));
  else //it's a blank in an island
    spaces.push_back(UsedInt(0,0));
  //  assert(cout << "Added "<<spaces.back()<<" to interval set "<<*this<<endl);
}

SeqPos length(const UsedInt& a){
  return a.stop-a.start+1;
}

bool operator<(const Location &a,const Location &b){
  BitSequence *as=a.bitSeq(),*bs=b.bitSeq(),*pat=mfh->hashpat;
  word wc=pat->wordCount();
  SeqPos aPos=bitSeqCoords(a.pos),bPos=bitSeqCoords(b.pos);
  for(word wi=0;wi<wc;wi++,aPos+=WORDSIZE/2,bPos+=WORDSIZE/2){
    word pw=pat->readWord(wi);
    word aw=(as->wordAtBase(aPos) & pw), bw=(bs->wordAtBase(bPos) & pw);
    if(aw < bw){
      return true;
    } else if(aw > bw){
      return false;
    }
  }
  return false;  
}

bool operator<(const Window &a,const Window &b){
  for(SeqPos frame=0;frame<a.frames;frame++){
    word pword=a.pat->readWord(frame);
    word resa=a.buffer[frame] & pword;
    word resb=b.buffer[frame] & pword;
    if(resa<resb)
      return 1;
    if(resb<resa)
      return 0;
  }
  return 0;
}

void IntervalSet::invert(){
  for(unsigned i=0;i<spaces.size();++i){
    UsedInt_invert(spaces[i],*seqs[i]);
  }
}

void IntervalSet::exactExtend(){
  //  assert(cout << "Extend: "<<*this<<endl);
  int baseSeq=0;
  while(isZero(spaces[baseSeq])){
    baseSeq++;
    assert(baseSeq<seq_count);
  }
  assert(baseSeq<seq_count-1);
  BitSequence *mySeq=spaces[baseSeq].stop>0 ? seqs[baseSeq]->fwd :
    seqs[baseSeq]->rev,*tSeq;
  SeqPos mstart=bitSeqCoords(spaces[baseSeq].start),tstart;
  SeqPos mstop=bitSeqCoords(spaces[baseSeq].stop),tstop;
  SeqPos right=mySeq->length()-mstop,
    left=mstart;
  assert(mstart>=0);
  for(unsigned i=baseSeq+1;i<spaces.size();i++){
    if(isZero(spaces[i]))
       continue;
    tSeq=spaces[i].stop>0 ? seqs[i]->fwd :
      seqs[i]->rev;
    tstart=bitSeqCoords(spaces[i].start);
    tstop=bitSeqCoords(spaces[i].stop);
    if(spaces[i].stop<0)
      swap(tstart,tstop); //sneaky devils

    assert(tstart>=0);
    
    right=min<SeqPos>(tSeq->length()-tstop-1,min<SeqPos>(right,mySeq->cmpRight(mstop+1,*tSeq,tstop+1)));
    left=min<SeqPos>(tstart,min<SeqPos>(left,mySeq->cmpLeft(mstart-1,*tSeq,tstart-1)));
  }

  if(right || left){
    //    assert(cout << *this << "-> Extending "<<mstart<<","<<mstop<<" by "<<left<<","<<right<<" to ");
    assert(sanityCheck());
    if(right)
      for(unsigned i=0;i<spaces.size();i++){
	if(isZero(spaces[i]))
	  continue;
	if(spaces[i].stop>0)
	  spaces[i].stop+=right;
	else
	  spaces[i].start-=right;
      }
    if(left)
      for(unsigned i=0;i<spaces.size();i++){
	if(isZero(spaces[i]))
	  continue;
	if(spaces[i].start>0)
	  spaces[i].start-=left;
	else
	  spaces[i].stop+=left;
      }
    //    assert(cout << *this<<"<--"<<endl);
  }else{
    //    assert(cout << *this << "\\\\// Not extending"<<endl);
  }
  assert(sanityCheck());
}

Score IntervalSet::fuzzyExtend(){
  SeqPos maxExtendLeft=maxSeqPos,maxExtendRight=maxSeqPos;
  vector<BaseIterator> leftEdges,rightEdges;
  for(size_t i=0;i<spaces.size();i++){
    if(isZero(spaces[i]))
      continue;
    UsedInt bounds(seqs[i]->getSubSeqBounds(spaces[i].start));
    SeqPos leftPos=isRev(spaces[i]) ? spaces[i].stop:spaces[i].start;
    SeqPos rightPos=isRev(spaces[i]) ? spaces[i].start:spaces[i].stop;
    leftEdges.push_back(seqs[i]->iterate(leftPos));
    rightEdges.push_back(seqs[i]->iterate(rightPos));
    maxExtendLeft=min<SeqPos>(maxExtendLeft,isRev(spaces[i]) ? bounds.stop-spaces[i].stop:spaces[i].start-bounds.start);
    maxExtendRight=min<SeqPos>(maxExtendRight,isRev(spaces[i]) ? spaces[i].start-bounds.start:bounds.stop-spaces[i].stop);
  }
  SeqPos exRight=0,exLeft=0;
  Score scoreRight=0,scoreLeft=0;
  //  assert(cout << "** Checking fuzzyExtend on "<<*this<<" up to "<<maxExtendLeft<<","<<maxExtendRight<<endl);
  if(maxExtendRight)
    exRight=fuzzyExtendUntil(rightEdges,maxExtendRight,1,&scoreRight);
  if(maxExtendLeft)
    exLeft=fuzzyExtendUntil(leftEdges,maxExtendLeft,-1,&scoreLeft);
  if(exRight || exLeft){
    //    assert(cout << *this << "-> Fuzzy Extending by "<<exLeft<<","<<exRight<<" to ");
    for(size_t i=0;i<spaces.size();i++){
      if(isZero(spaces[i]))
	continue;
      if(exRight){
	if(isRev(spaces[i]))
	  spaces[i].start-=exRight;
	else
	  spaces[i].stop+=exRight;
      }
      if(exLeft){
	if(isRev(spaces[i]))
	  spaces[i].stop+=exLeft;
	else
	  spaces[i].start-=exLeft;
      }
    }
    //    assert(cout << *this<<"<--"<<endl);
  }else{
    //    assert(cout << *this << "\\\\// Not extending"<<endl);
  }
  return scoreRight+scoreLeft; //score boosted by this much through extend.
}

string IntervalSet::contents(string delim) const {
  ostringstream os;
  for(size_t si=0;si<spaces.size();si++){
    pair<BaseIterator,BaseIterator> r(seqs[si]->iterate(spaces[si]));
    while(r.first!=r.second)
      os << baseToString(*(r.first++));
    if(si+1!=spaces.size())
      os << delim;
  }
  return os.str();
}

Score IntervalSet::score(){
  if(opt.gappedAnchors)return 0; //no scoring for gapped intervals. too crazy.
  Score baseScore=0;
  vector<BaseIterator> column;
  vector<vector<Score> > pairScores;

  SeqPos width=0;
  for(size_t i=0;i<spaces.size();i++){
    if(isZero(spaces[i]))
      continue;
    if(width==0)
      width=spaces[i].stop-spaces[i].start+1;
    else
      assert((spaces[i].stop-spaces[i].start+1)==width);
    column.push_back(seqs[i]->iterate(spaces[i]).first);
  }
  for(SeqPos i=0;i<width;i++){
    if(opt.scoreByMinimumPair){
      addColumnPairScores(column,pairScores);
    }else{
      baseScore+=scoreColumn(column);
    }
    for(vector<BaseIterator>::iterator r=column.begin();
	r!=column.end();++r){
      ++(*r);
    }
  }
  if(opt.scoreByMinimumPair)
    return minimumPairScore(pairScores);
  return baseScore;
}

bool operator==(const Location &a,const Location &b) {
  BitSequence *mySeq=a.pos>0 ? a.seq()->fwd : a.seq()->rev,
    *tSeq=b.pos>0 ? b.seq()->fwd : b.seq()->rev;
  SeqPos mstop=bitSeqCoords(a.pos),
    tstop=bitSeqCoords(b.pos);
  return mySeq->equal(mstop,*tSeq,tstop,*mfh->hashpat);
}

ostream& operator<<(ostream &os, const Window &a){
  for(SeqPos i=0;i<a.frames-1;i++){
    os << wordToString(a.buffer[i]) << " ";
  }
  os << "+" << wordToString(a.buffer[a.frames-1]);
  return os;
}

ostream& operator<<(ostream &os,const BitSequence &a){
  if(a.isPattern)
    return os<<a.asPattern();
  else
    return os << a.asString();
}

ostream& operator<<(ostream &os,const Location &a){
  return os << "Location: ("<< a.seq()->getId()<<","<<a.seq()->extCoords(a.pos)<<")";
}

void BitSequence::maskString(string &str) const{
  SeqPos j=0;
  for(word i=0;i<word_count;i++){
    word w=readWord(i);
    word mask=highN(2);
    while(mask){
      if(!(w & mask))
	str[j]='.';
      j++;
      mask>>=2;
    }
  }
}

string wordToMaskedString(word w,word p,int bits){
  string foo(wordToString(w,bits));
  assert((int)foo.length()<=bits);
  SeqPos j=0;
  word mask=highN(2);
  while(mask && j<(SeqPos)foo.length()){
    if(!(p & mask))
      foo[j]='.';
    j++;
    mask>>=2;
  }
  assert((int)foo.length()<=bits);
  return foo;
}

string Window::toRawString(){
  string ret;
  for(unsigned i=0;i<buffer.size();i++)
    ret+=wordToString(buffer[i]);
  ret.resize(pat->length());
  return ret;
}

string Window::prettyString(){
  string ret(toRawString());
  pat->maskString(ret);
  size_t len=ret.length(),patlen=min<word>(pat->length(),hash_bits/2);
  switch(opt.quick_hash){
  case 2:
    for(size_t i=patlen;i<len;i++)
      ret[i]=tolower(ret[i]);
    break;
  case 1:
    for(size_t i=WORDSIZE/2;i<len;i++)
      ret[i]=tolower(ret[i]);
    break;
  case 3:
    for(size_t i=0;i<len;i++)
      if(!pat->hasher->used[i])
	ret[i]=tolower(ret[i]);
    break;
  }
  return ret;
}

void Window::slide(){
  pos+=2;
  SeqPos i;
  for(i=0;i<frames-1;i++){
    buffer[i]=(buffer[i]<<2) | (buffer[i+1]>>(WORDSIZE-2));
  }
  buffer[i]<<=2;

  //this should be done at the end. thus frame is always correct and no buffers are ever at empty
  if(pos/WORDSIZE!=frame){
    frame=pos/WORDSIZE;
    buffer[frames-1]=seq->readWord(frame+frames-1);
  }
}

void Window::slideWord(){
  //update to new positions
  pos+=WORDSIZE;
  frame++;

  SeqPos i;
  for(i=0;i<frames-1;i++){
    buffer[i]=buffer[i+1];
  }
  //[i] is now the last buffer
  buffer[i]=seq->readWord(frame+frames-1);
  SeqPos offset=MODWORDSIZE(pos);
  if(offset) //[i-1] needs to get the offset bits that had been chopped off of the previous [i]
    buffer[i-1]|=buffer[i]>>(WORDSIZE-offset);
  //and [i] needs to not duplicate those on a subsequent slide...
  buffer[i]<<=offset;
}

void Window::slide(SeqPos dist){ //bah. this function isnt actually used so, dont care that it's not as efficient as possible
  while(dist>WORDSIZE){
    slideWord();
    dist-=WORDSIZE;
  }

  while(dist>0){
    slide();
    dist-=2;
  }
}

void Window::initslide(SeqPos dist){
  assert(dist<=WORDSIZE-(MODWORDSIZE(pos)));
  pos+=dist;
  SeqPos i;
  for(i=0;i<frames-1;i++){
    buffer[i]=(buffer[i]<<dist) | (buffer[i+1]>>(WORDSIZE-dist));
  }
  buffer[i]<<=dist;

  if(pos/WORDSIZE!=frame){
    frame=pos/WORDSIZE;
    buffer[frames-1]=seq->readWord(frame+frames-1);
  }
}

bool Window::equals(const Window &a) const {
  assert(pat==a.pat); //dont be dumb
  if(pos+pat->bit_count>seq->bit_count) //overruns end of seq
    return false;
  for(SeqPos i=0;i<frames-1;i++){
    word result=(buffer[i] & pat->readWord(i)) ^ (a.buffer[i] & pat->readWord(i));
    if(result)
      return false; //a bit's amiss!
  }
  return true;
}

inline word Window::eqbases(const Window &a) const {
  return ~(buffer[0] ^ a.buffer[0]);
}

Window::Window(const UsedInt &a,const Sequence *s,BitSequence *p) :
  seq(a.start>0 ? s->fwd:s->rev),pat(p),pos(bitSeqCoords(a.start>0 ? a.start:a.stop)*2),
  frames(p->word_count+1),buffer(frames)
{
  SeqPos stop=pos;
  frame=pos/WORDSIZE;
  pos=frame*WORDSIZE;
  for(SeqPos i=0;i<frames;i++)
    buffer[i]=seq->readWord(frame+i);
  if(pos!=stop)
    initslide(MODWORDSIZE(stop)); //scuttle right the leftover bits
}

Window::Window(BitSequence *_seq,SeqPos _pos,BitSequence *_pat):
  seq(_seq),pat(_pat),pos(_pos*2),
  frames(_pat->word_count+1),buffer(frames)
{
  frame=pos/WORDSIZE;
  pos=frame*WORDSIZE;
  for(SeqPos i=0;i<frames;i++){
    buffer[i]=seq->readWord(frame+i);
  }
  if(pos!=_pos*2)
    initslide(MODWORDSIZE(_pos*2));//scuttle right the leftover bits
}

Window::Window(Location &v,BitSequence *p):
  seq(v.bitSeq()),pat(p),pos(bitSeqCoords(v.pos)*2),
  frames(p->word_count+1),buffer(frames)
{
  SeqPos stop=pos;
  frame=pos/WORDSIZE;
  pos=frame*WORDSIZE;
  for(SeqPos i=0;i<frames;i++)
    buffer[i]=seq->readWord(frame+i);
  if(pos!=stop)
    initslide(MODWORDSIZE(stop)); //scuttle right the leftover bits
}
  
word Window::hash(){
  word hash=hash_padding;
  word patpos=0;
  word filled=0,bit=WORDSIZE-2,frame=0;

  word patword=0,input;
  SeqPos lastpati=-1;

  switch(opt.quick_hash){
  case 2: //naive hash: use first hash_bits bits
  while(filled<WORDSIZE && patpos<pat->bit_count){
    if(pat->readCode(patpos)){
      hash=hash<<2 | ((buffer[frame]>>bit) & 3);
      filled+=2;
    }
    patpos+=2;
    if(bit==0){
      frame++;
      bit=WORDSIZE-2;
    }else{
      bit-=2;
    }
  }
  break;
  case 1: //use first word only (usually very bad waste of hash)
    hash=buffer[0] & pat->readWord(0);
    break;
  case 4:case 5:case 6:case 7:case 8: //use cryptographic hash functions. seems unreasonable, but for comparison's sake, lets give it a shot
    assert(cryptoHasher);
    hash^=cryptoHasher->digest(buffer,pat);
    break;
  case 0: //adaptive hasher with s-boxes. Hopefully even smarter.
    for(HashMethod::InputList::const_iterator i=pat->hasher->inputlist().begin();
	i!=pat->hasher->inputlist().end();i++){
      if(i->first!=lastpati){
	patword=pat->readWord(i->first);
	lastpati=i->first;
      }
      input=buffer[i->first] & patword;
      if(i->second==0)
	hash^=input;
      else {
	if(i->second>=0)
	  hash^=(input>>(word)(i->second*2));
	else
	  hash^=(input<<(word)(i->second*2));
      }
    }
    return sbox->confuse(hash & hash_mask);

  case 3:case 100: //adaptive hasher. freakin' smart (usually...)
    for(HashMethod::InputList::const_iterator i=pat->hasher->inputlist().begin();
	i!=pat->hasher->inputlist().end();i++){
      if(i->first!=lastpati){
	patword=pat->readRawWord(i->first);
	lastpati=i->first;
      }
      input=(buffer[i->first] & patword);
      if(i->second==0)
	hash^=input;
      else {
	if(i->second>=0)
	  hash^=(input>>i->second*2);
	else
	  hash^=(input<<i->second*2);
      }
    }
  }

  return hash & hash_mask;
}

ostream& operator<<(ostream &os, const AnchorSet &a){
  return os << a.asString();
}

string UsedMap::asString() {
  string ret;
  for(usedItree::iterator i=used[0].begin();
      i!=used[0].end();i++){
    ret+= (*i)->asString() + "\n";
  }
  return ret;
}

ostream& operator<<(ostream &os,const pair<word,SeqPos> &p){
  return os << p.first << ":" << p.second;
}

ostream& UsedMap::writeTfidf(ostream &os){
  if(used[0].empty())
    return os;
  if(!dfCount)
    dfCount=makeDfCount();
  for(usedItree::iterator i=used[0].begin();
      i!=used[0].end();i++){
    assert((*i));
    AnchorSet *&s=*i;
    if(!opt.hitfilter || s->hitCount()>=opt.hitfilter)
      os << s->uniqueness(this) << endl;
  }
  return os;
}

ostream& UsedMap::writeScores(ostream &os){
  if(used[0].empty())
    return os;
  for(usedItree::iterator i=used[0].begin();
      i!=used[0].end();i++){
    assert((*i));
    AnchorSet *&s=*i;
    if(!opt.hitfilter || s->hitCount()>=opt.hitfilter)
      os << s->score() << endl;
  }
  return os;
}

ostream& UsedMap::saveDetails(ostream &os,ostream &bitos){
  if(used[0].empty())
    return os;
  if(opt.tfidf && !dfCount)
    dfCount=makeDfCount();
  for(usedItree::iterator i=used[0].begin();
      i!=used[0].end();i++){
    assert((*i));
    AnchorSet *&s=*i;
    if(!opt.hitfilter || s->hitCount()>=opt.hitfilter){
      os << s->asString() << "\t" << s->hitCount();

      if(opt.retainMembers){
	double score;
	if(opt.tfidf){
	  if(opt.gappedAnchors)
	    score=s->uniqueness(this);
	  else
	    score=sqrt((s->uniqueness(this))*(s->score()));
	}
	else {
	  score=s->score();
	}
	os << "\t" << score << "\t";
	HashCount::iterator last=s->members.end();
	assert(!s->members.empty());
	last--;
	for(HashCount::iterator i=s->members.begin();i!=last;i++)
	  os << i->first << ":" << i->second <<",";
	os << last->first << ":" << last->second;
      }

      if(opt.bitscore && bitos!=cerr){
	pair<SeqPos,SeqPos> scores(s->bitscore());
	bitos << scores.first << "\t" << scores.second << endl;
      }
      os << endl;
    }
  }
  return os;
}

int UsedMap::count() const {
  return used[0].size();
}

ostream& UsedMap::writeOut(ostream &os){
  if(used[0].empty())
    return os;
  for(usedItree::iterator i=used[0].begin();
      i!=used[0].end();i++){
    assert(*i);
    if(!opt.hitfilter || (*i)->hitCount()>=opt.hitfilter)
      os << (*i)->asString() << "\n";
  }
  //as these output sets can get huge, this might be a bad idea
  //  return os << asString();
  return os;
}

word* UsedMap::makeDfCount(){
  size_t size;
  word *df;
  if(opt.tfidf){
    assert(!dfCount);
#ifdef MURASAKI_MPI
    if(mpi_capable)
      size=mpi_total_hash_size;
    else
#endif
      size=hash_size;

    cout << "(Creating df table for "<<size<<" entries...)"<<endl;
    df=new (std::nothrow) word[size]; //this has a high probability of failing if we tuned hash table size to fill up memory completely...
    if(!df){
      cerr << "Warning: insufficient RAM for df table creation (disabling tfidf scoring)."<<endl;
      opt.tfidf=false;
      return df;
    }
    bzero(df,size*sizeof(word));

    if(count())
      for(usedItree::iterator i=used[0].begin();
	  i!=used[0].end();i++){
	assert(*i);
	AnchorSet *&s=*i;
	for(HashCount::iterator i=s->members.begin();i!=s->members.end();i++)
	  df[i->first]+=i->second;
      }
    return df;
  }
  return 0;
}

string AnchorSet::asString() const {
  string ret;
  for(unsigned i=0;i<spaces.size();i++){
    assert(*spaces[i]);
    ret+=asAnchor(spaces[i].key(),seqs[i]);
    if(i!=spaces.size()-1)
      ret+="\t";
  }
  return ret;
}

string asAnchor(const UsedInt& a,const Sequence *s){
  if(a.start<0) //gotta flip
    return dstring(s->extCoords(a.stop))+string("\t")+
      dstring(s->extCoords(a.start))+string("\t")+
      string(((a.start<0) != (a.stop<0)) ? "?":(a.start>=0 ? "+":"-"));
  else return dstring(s->extCoords(a.start))+string("\t")+
	 dstring(s->extCoords(a.stop))+string("\t")+
	 string(((a.start<0) != (a.stop<0)) ? "?":(a.start>=0 ? "+":"-"));
}

double AnchorSet::uniqueness(UsedMap *context) const {
  double sum=0;
#ifdef MURASAKI_MPI
  if(mpi_capable){
    if(!dfCount)dfCount=context->makeDfCount();
    if(dfCount)
      for(HashCount::const_iterator i=members.begin();i!=members.end();i++){
	sum+=i->second*log(anchors->count()/dfCount[i->first]);
      }
    return sum;
  }
#endif
  if(!opt.retainMembers){ //old way. takes into account non-anchored noise. not so cool
    for(HashCount::const_iterator i=members.begin();i!=members.end();i++){
      // wi = tfi*log(D/dfi) by the money
      if(!mfh->sizeAt(i->first)){
	throw MurasakiDebugException("What the monkey? "+dstring((long)i->first)+" isn't in hash?");
      }
      sum+=i->second*log((double)usedBuckets/(double)mfh->sizeAt(i->first));
    }
  }else{ //new way. separate counters. cleaner
    if(!dfCount)dfCount=context->makeDfCount();
    if(!dfCount)return 0;
    for(HashCount::const_iterator i=members.begin();i!=members.end();i++){
      if(!dfCount[i->first])
	throw MurasakiDebugException("dfCount missing at "+dstring((long)i->first)+" in anchor "+(this->asString()));
      sum+=i->second*log(anchors->count()/dfCount[i->first]);
    }
  }
  return sum;
}

pair<double,SeqPos> IntervalSet::entropy() const {
  double ret=0;
  vector<Window> windows;
  SeqPos shortest=0,shortlen=length(spaces[0]);
  for(int i=0;i<(int)spaces.size();i++){
    windows.push_back(Window(spaces[i],seqs[i],mfh->hashpat));
    if(shortlen>length(spaces[i])){
      shortest=i;
      shortlen=length(spaces[i]);
    }
  }

  int counters[4],offset;
  for(SeqPos pos=0;pos<shortlen;){
    for(offset=WORDSIZE-2;offset>=0 && pos<shortlen;pos++,offset-=2){
      memset(counters,0,sizeof(int)*4);
      
      for(uint s=0;s<spaces.size();s++)
	counters[(windows[s].firstWord()>>offset) & 3]++;
      
      //calculate (and add in) entropy for this column
      for(int b=0;b<4;b++)
	if(counters[b]){
	  double count=(double)counters[b];
	  ret+=count*log(count/globalBaseFreq[b]);
	}
    }
    for(vector<Window>::iterator ite=windows.begin();ite!=windows.end();ite++)
      ite->slideWord();
  }
  return pair<double,SeqPos>(ret,shortlen);
}

size_t AnchorSet::hitCount() const {
  size_t ret=0;
  for(HashCount::const_iterator i=members.begin();i!=members.end();++i)
    ret+=i->second;
  return ret;
}

Score AnchorSet::score() const {
  return IntervalSet(*this).score(); //theoretically it might be faster to have all the code here, but that's just stupid...
}

pair<SeqPos,SeqPos> AnchorSet::bitscore() const {
  vector<Window> windows;
  SeqPos shortest=0,shortlen=length(spaces[0].key());
  for(int i=0;i<(int)spaces.size();i++){
    windows.push_back(Window(spaces[i].key(),seqs[i],globalPat));
    if(shortlen>length(spaces[i].key())){
      shortest=i;
      shortlen=length(spaces[i].key());
    }
  }
  SeqPos eqbits=0;
  for(SeqPos pos=0;pos<shortlen;pos+=WORDSIZE/2){
    word ret=~0;
    //compare everything to ref seq. store in ret
    for(int comp=0;comp<(int)windows.size();comp++){
      if(comp==shortest)continue;
      word eq=windows[comp].eqbases(windows[shortest]);
      if(pos+WORDSIZE/2>length(spaces[comp].key()))
        eq=(eq & (highN(2*(length(spaces[comp].key())-pos))));
      eq=(((eq & alternating_mask)<<1) & eq);
      ret&=eq;
    }
    //count ret and add it up!
    eqbits+=popCount(ret);
    
    //shuffle everybody
    for(vector<Window>::iterator ite=windows.begin();ite!=windows.end();ite++)
      ite->slideWord();
  }
  return pair<SeqPos,SeqPos>(eqbits,shortlen);
}

string hashCacheName(string name, BitSequence *pat){
  ostringstream os;
  
  char *n = strdup(name.c_str());
  string fr;
  if ( !opt.skipFwd && !opt.skipRev ) fr = "both";
  if ( !opt.skipFwd &&  opt.skipRev ) fr = "fwd";
  if (  opt.skipFwd && !opt.skipRev ) fr = "rev";

  string rm;
  if ( opt.repeatMask ) rm="r"; else rm="nr";
					
    
  for(unsigned int i=0; i<strlen(n); i++)
    if(n[i]=='/' || n[i]=='.') n[i]='-';
    
  os << opt.cache_dir << "mc-" << n << "-";
  for(unsigned int p=0; p<pat->wordCount(); p++)
    os << hex << pat->readWord(p);
  os << "-" << fr;
  os << "-" << rm;
  os << "-" << dec << opt.rand_seed;
  os << "-" << hash_bits;
  free(n);
    
  return os.str();
}

RepeatMap::RepeatMap() : clusters() {}

void RepeatMap::add(const LocList &l){
  clusters.push_back(l);
}

void RepeatMap::writeOut(ostream &os){
  for(list<LocList>::iterator i=clusters.begin();i!=clusters.end();++i){
    LocList &l=*i;
    bool first=true;
    for(int s=0;s<seq_count;s++){
      if(l[s].empty()) //skip any seqs which aren't part of the repeat (possible with rifts)
	continue;
      if(first){ //first sequence out should tell _what_ the repeat is
	first=false;
	assert(l[s].front().seqId()<seq_count);
	os << "R: "<<Window(l[s].front(),globalPat).prettyString()<<endl;
      }
      os << s << ": ";
      LocSubList::iterator lite=l[s].begin();
      do {
	SeqPos p=lite->seq()->extCoords(lite->pos);
	os << dstring(p);
	++lite;
	if(lite!=l[s].end())
	  os << " ";
	else{
	  os << endl;
	  break;
	}
      }while(1);
    }
    os << endl; //extra new line on group end.
  }
}

IntervalSet::IntervalSet() :
  spaces() {
  spaces.reserve(seq_count);
  if(opt.retainMembers)
    members[activeHash]++;
}

IntervalSet::IntervalSet(const AnchorSet& a):
  spaces() , members(a.members)
{
  spaces.reserve(seq_count);
  for(vector<MapIte>::const_iterator i=a.spaces.begin();
      i!=a.spaces.end();++i)
    spaces.push_back(i->key());
}

IntervalSet::IntervalSet(const _IntervalIterator &intStart, const _IntervalIterator &intStop,
			 const _MemberIterator &memberStart,const _MemberIterator &memberStop){
  spaces.reserve(seq_count);
  for(_IntervalIterator i=intStart;i!=intStop;++i)
    spaces.push_back(*i);
  for(_MemberIterator i=memberStart;i!=memberStop;++i)
    members.insert(*i);
}

ostream& operator<<(ostream& os,const IntervalSet& a){
  os << "(";
  vector<UsedInt>::const_iterator i=a.spaces.begin();
  if(i==a.spaces.end())
    return os << ")";
  vector<UsedInt>::const_iterator next=i;
  int idx;
  for(next++,idx=0;next!=a.spaces.end();next++,i++,idx++)
    os << anchorCoords(*i,seqs[idx]) << ",";
  return os << anchorCoords(*i,seqs[idx]) << ")";
}

UsedInt anchorCoords(const UsedInt &b,Sequence *s){
  if(b.start<0)
    return s->length()-b;
  return b;
}

UsedInt operator-(SeqPos a,const UsedInt& b){
  return UsedInt(-1-(a+b.stop),-1-(a+b.start));
}

string SeqPosPair2string(const SeqPosPair& a){
  ostringstream os;
  os << "["<<a.first<<","<<a.second<<"]";
  return os.str();
}

BaseIterator::BaseIterator(Sequence *seq,SeqPos pos):
  rev(pos<0), src(rev ? seq->rev:seq->fwd),idx(bitSeqCoords(pos)),here(src->localRegionIte(idx)),prev(here),next(here)
{
  assert(!src->matchRegions.empty());
  if(prev!=src->matchRegions.begin())
    --prev;
  else
    prev=src->matchRegions.end();
  if(next!=src->matchRegions.end())
    ++next;
}

BaseIterator::BaseIterator(const BaseIterator& a):
  rev(a.rev),src(a.src),idx(a.idx),here(a.here),prev(a.prev),next(a.next)
{
}

BaseIterator& BaseIterator::operator++(){
  shiftFwd();
  return *this;
}

BaseIterator& BaseIterator::operator--(){
  shiftBack();
  return *this;
}

BaseIterator BaseIterator::operator++(int){
  BaseIterator old(*this);
  shiftFwd();
  return old;
}

BaseIterator BaseIterator::operator--(int){
  BaseIterator old(*this);
  shiftBack();
  return old;
}

bool BaseIterator::operator==(const BaseIterator &a) const{
  return src==a.src && idx==a.idx;
}

bool BaseIterator::operator!=(const BaseIterator &a) const{
  return !(*this==a);
}

BASE_TYPE BaseIterator::operator*() const {
  if(idx<here->first || idx>here->second)
      return BASE_N;
  return src->readBase(idx);
}

void BaseIterator::shiftFwd(){
  idx++;
  if(idx>here->second && next!=src->matchRegions.end() && idx>=next->first){
    prev=here;
    here=next;
    ++next;
  }
}

void BaseIterator::shiftBack(){
  idx--;
  if(idx<here->first && prev!=src->matchRegions.end() && idx<=prev->second){
    next=here;
    here=prev;
    if(here==src->matchRegions.begin())
      prev=src->matchRegions.end();
    else
      --prev;
  }
}

string BaseIterator::debugInfo(){
  ostringstream os;
  os <<src->genBitFilename();
  os <<"@"<<idx<<"="<<baseToString(**this)<<" in ";
  os << (prev==src->matchRegions.end() ? "??":SeqPosPair2string(*prev)) << " << ";
  os << SeqPosPair2string(*here) << " << ";
  os << (next==src->matchRegions.end() ? "??":SeqPosPair2string(*next));
  return os.str();
}
