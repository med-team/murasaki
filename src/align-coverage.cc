//compare 2 alignments
/*
Copyright (C) 2006-2008 Keio University
(Kris Popendorf) <comp@bio.keio.ac.jp> (2006)

This file is part of Murasaki.

Murasaki is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Murasaki is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Murasaki.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <iostream>
#include <fstream>
#include <memory.h>
#include <getopt.h>
#include <vector>
#include <map>
#include <list>
#include <string.h>
#include <boost/lexical_cast.hpp>
#include <err.h>
#include "dinkymath.h"
#include "itree.h"
#include "alignments.h"
#include "seqread.h"

#include <boost/filesystem/operations.hpp>

using namespace std;
namespace fs = boost::filesystem;

string program_help();
string program_version();

//function decl
string naPercent(double x,double y);
void printHeader(ostream& os);
void printResults(ostream& os,string filename,long detLength,long seqLength,long coverLength);

//globals
ProgressTicker ticker(100);

bool debug=false;
bool countNCovers=false;

int main(int argc,char** argv){
  int optc;
  uint growAnchors=0;
  ostream *os=&cout;
  string outfile;
  bool noHeader=false;

  using boost::lexical_cast;
  using boost::bad_lexical_cast;

  while(1){
    //options struct:
    // name, has_arg, store_pointer, return_value
    static struct option long_options[] = {
      {"help",0,0,'?'},
      {"version",0,0,'v'},
      {"grow",1,0,'g'},
      {"output",1,0,'o'},
      {"debug",0,0,'d'},
      {"ncover",0,0,'n'},
      {"noheader",0,0,'H'},
      {0,0,0,0}
    };
    int longindex=0;
    optc=getopt_long(argc,argv,"?vg:o:",long_options,&longindex);
    if(optc==-1)break;
    switch(optc){
    case 'v': cout << program_version();exit(-1);break;
    case '?': cout << program_help();exit(-1);break;
    case 'g': 
      if(optarg){
	string optstr(optarg);
	if(optstr==string("0") || optstr==string("false")){
	  growAnchors=0;
	  break;
	}
	try{
	  growAnchors=lexical_cast<uint>(optstr);
	}
	catch(bad_lexical_cast& e){
	  cerr << "Bad argument to --grow"<<endl;
	  exit(-2);
	}
      }
      break;
    case 'o':
      os=new ofstream(optarg);
      if(!os){
	cerr << "Couldn't write to "<<optarg<<endl;
	exit(-2);
      }
      outfile=string(optarg);
      break;
    case 'd':
      debug=true;
      break;
    case 'n':
      countNCovers=true;
      break;
    case 'H':
      noHeader=true;
    default: cout << "Unknown option."<<endl<<program_help();exit(1);break;
    }
  }

  uint alignmentCount=0;
  vector<string> files;
  for(int i=optind;i<argc;i++){
    checkSeqs(argv[i]);
    files.push_back(argv[i]);
    alignmentCount++;
  }
  
  vector<SequenceCover> covers(seqCount);
  for(uint i=0;i<alignmentCount;i++){
    int anchors=0;
    AnchorFileReader reader(files[i],growAnchors);
    for(vector<region> parts;reader.readline(parts);parts.clear()){
      anchors++;
      for(uint si=0;si<seqCount;si++)
	if(!isZero(parts[si]))
	  covers[si].merge(abs(parts[si]));
    }
    cout << "Loaded "<<anchors<<" anchors from "<<files[i]<<endl;
  }
  
  if(!noHeader)
    printHeader(*os);

  long totalDetLength=0,totalSeqLength=0,totalCoverLength=0;
  for(size_t si=0;si<seqCount;si++){
    long detLength=0,seqLength=0,coverLength=0;
    bool seqStats=true;
    try {
      SequenceFileReader reader(seqs[si]);
      bool determined;
      long idx=0;
      SequenceCover::TreeType::iterator ci=covers[si].begin();
      while(char c=reader.getc()){
	idx++;
	switch(c){
	case 'a':case 't':case 'g':case 'c':
	case 'A':case 'T':case 'G':case 'C':
	  determined=true;
	  detLength++;
	  seqLength++;
	  break;
	default:
	  seqLength++;
	  determined=false;
	}
	while(ci!=covers[si].end() && idx>ci.key().stop)
	  ++ci;
	if(ci!=covers[si].end())
	  if(idx>=ci.key().start && idx<=ci.key().stop && (countNCovers || determined)){
	    assert(c!='n' && c!='N');
	    coverLength++;
	  }
      }
      if(debug)cerr << "Finished reading "<<files[si]<<endl;

      totalDetLength+=detLength;
      totalSeqLength+=seqLength;
      totalCoverLength+=coverLength;

      printResults(*os,seqs[si],detLength,seqLength,coverLength);
    }catch(SeqReadException e){
      warn("Error while reading %s.",seqs[si].c_str());
      seqStats=false;
      printResults(*os,seqs[si],0,0,covers[si].totalLength());
    }
  }

  if(debug)cerr << "Done with all files"<<endl;
  printResults(*os,"(total)",totalDetLength,totalSeqLength,totalCoverLength);
  
  
  return 0;
}

void printResults(ostream& os,string filename,long detLength,long seqLength,long coverLength){
  os << coverLength <<"\t" << detLength <<"\t" << seqLength <<"\t" << naPercent(coverLength,countNCovers ? seqLength:detLength) << "\t" << filename << endl;
}

void printHeader(ostream& os){
  os << "coverLength" <<"\t" << "detLength" <<"\t" << "seqLength" <<"\t" << "percent" << "\t" << "filename" << endl;
}


string naPercent(double x,double y){
  if(y==0)
    return "N/A";
  ostringstream s;
  s << (x/y*(double)100) << "%";
  return s.str();
}

string program_help(){
  return string("\
Usage: align-mask [options] alignment1 [alignment2 ... ]\n\
\n\
Options\n\
*Takes an argument (like --maxres 3 or -m3)\n\
  --grow|v    = grow all anchors by some amount\n\
  --output|o  = store output to a separate file (otherwise, it's stdout)\n\
\n\
*Toggles: (just --merge or -b)\n\
  --help|h    = this message\n\
  --version|v = version string\n\
");
}

string program_version(){
  return string("align-best v0.1");
}

