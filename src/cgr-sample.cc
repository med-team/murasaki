/*
Murasaki - multiple genome global alignment program
Copyright (C) 2006-2008 Keio University
(Kris Popendorf) <comp@bio.keio.ac.jp> (2006)

This file is part of Murasaki.

Murasaki is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Murasaki is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Murasaki.  If not, see <http://www.gnu.org/licenses/>.
 */
//////////////////
// sample some cgr for statistics
// (ie: find most common/least common patterns, print)
/////////////////

#include <iostream>
#include <memory.h>
#include <getopt.h>
#include <vector>
#include <map>
#include <list>
#include <boost/lexical_cast.hpp>
#include "dinkymath.h"
#include "cgr.h"
#include "seqread.h"

using namespace std;

string program_help();
string program_version();

Cgr *cgr=0;

struct lidx {
  bool operator()(const int idx1, const int idx2){
    return cgr->get(idx1)<cgr->get(idx2);
  }
};

int main(int argc,char** argv){
  int optc;
  int high=5,low=-1,res=8;
  double highp=-1,lowp=-1;
  bool repeatmask=false,save=false;
  while(1){
    //options struct:
    // name, has_arg, store_pointer, return_value
    static struct option long_options[] = {
      {"help",0,0,'?'},
      {"version",0,0,'v'},
      {"high",1,0,'h'},
      {"low",1,0,'l'},
      {"res",1,0,'i'},
      {"highp",1,0,'H'},
      {"lowp",1,0,'L'},
      {"repeatmask",0,0,'r'},
      {"save",0,0,'s'},
      {0,0,0,0}
    };
    int longindex=0;
    optc=getopt_long(argc,argv,"?vh:l:i:H:L:rs",long_options,&longindex);
    if(optc==-1)break;
    using boost::lexical_cast;
    using boost::bad_lexical_cast;
    switch(optc){
    case 'h':
      try{
	high=lexical_cast<int>(optarg);}
      catch(bad_lexical_cast& e){
	cerr << "Bad argument to --high ("<<optarg<<")"<<endl;
	cout << program_help();exit(-1);break;
      }
      break;
    case 'l':
      try{
	low=lexical_cast<int>(optarg);}
      catch(bad_lexical_cast& e){
	cerr << "Bad argument to --low."<<endl;
	cout << program_help();exit(-1);break;
      }
      break;
    case 'H':
      try{
	highp=lexical_cast<double>(optarg);}
      catch(bad_lexical_cast& e){
	cerr << "Bad argument to --highp."<<endl;
	cout << program_help();exit(-1);break;
      }
      break;
    case 'L':
      try{
	lowp=lexical_cast<double>(optarg);}
      catch(bad_lexical_cast& e){
	cerr << "Bad argument to --lowp."<<endl;
	cout << program_help();exit(-1);break;
      }
      break;
    case 'i':
      try{
	res=lexical_cast<unsigned>(optarg);}
      catch(bad_lexical_cast& e){
	cerr << "Bad argument to --res."<<endl;
	cout << program_help();exit(-1);break;
      }
      break;
    case 'v': cout << program_version();exit(-1);break;
    case 'r': repeatmask=true; break;
    case 's': save=true;break;
    default: cout << program_help();exit(1);break;
    }
  }

  
  if(argc<2){
    cout << program_help();exit(1);
  }
  
  string inname(argv[optind]);
  if(inname.rfind(".pgm")==inname.length()){
    PgmBuffer pgm;
    cout << "Loading PGM"<<inname<<"...."<<endl;
    try {
      pgm.loadPGM(inname);
    } catch(PGMFileException e){
      cerr << e.reason() << endl;
      exit(-2);
    }
    cgr=new Cgr(pgm);
    save=false;
  }else{
    cout << "Loading "<<inname<<endl;
    string fwd;

    SequenceReader reader;
    try {
      reader.readSeqInto(fwd,inname);
    }catch(SeqReadException e){
      cerr << "SeqRead failed: "<<e.reason()<<endl;
      exit(-2);
    }
    
    if(fwd.length()<=0){
      cerr << "No data found in file. Aborting.\n";
      exit(-2);
    }
    string rev(fwd);
    cout << "Done. Generating reverse complement"<<endl;
    reverse(rev.begin(),rev.end());
    invert(rev);
    cout << "Done generating reverse compliment."<<endl;

    cgr=new Cgr(res);
    cgr->load(fwd.data(),fwd.length());
    cgr->load(rev.data(),rev.length());
  }

  cout << "Cgr loaded: rez "<<cgr->rez<<" iter "<<cgr->iter<<endl;
  /*  cout << "Sanity checking...\n";
  for(unsigned i=0;i<255*255;i++){
    string dna=itodna(i,cgr->iter,cgr->rez);
    unsigned check=dnatoi(dna.c_str(),cgr->iter,cgr->rez);
    if(check!=i){
      cerr << "Oh no! broken "<<i<<"!="<<check<<" ("<<dna<<")"<<endl;
      exit(-2);
    }
    }*/

  unsigned size=cgr->rez*cgr->rez;
  if(highp>0)
    high=(int)(size-size*highp);
  if(lowp>0)
    low=(int)(size*lowp);
  pair<pix,pix> ends=cgr->sampleHighLow(low,high);
  vector<unsigned long> lowl,highl;
  if(low>=0)
    cout << "Low "<<low<<" samples: "<<ends.first<<endl;
  if(high>=0)
    cout << "High "<<high<< " samples: "<<ends.second<<endl;
  cgr->findEdges(ends,(low>=0 ? &lowl:0),(high>=0 ? &highl:0));
  sort(lowl.begin(),lowl.end(),lidx());
  sort(highl.begin(),highl.end(),lidx());
  reverse(highl.begin(),highl.end());
  
  int rank=0,count=0;
  if(highl.size()){
    cout << "High values: "<<(highl.size())<<"\n";
    pix last=cgr->get(highl[0]);
    for(unsigned i=0;i<highl.size();i++){
      count++;
      if(cgr->get(highl[i])<last || i==0){
	rank=count;
      }
      char buf[32];
      sprintf(buf,"Rank %3d:",rank);
      cout << buf << " " << itodna(highl[i],cgr->iter,cgr->rez) << " ("<<cgr->get(highl[i])<<")"<<endl;
      last=cgr->get(highl[i]);
    }
  }

  rank=count=0;
  if(lowl.size()){
    cout << "Low values: "<<(lowl.size())<<"\n";
    pix last=cgr->get(lowl[0]);
    for(unsigned i=0;i<lowl.size();i++){
      count++;
      if(cgr->get(lowl[i])<last || i==0){
	rank=count;
      }
      char buf[32];
      sprintf(buf,"Rank %3d:",rank);
      cout << buf << " " << itodna(lowl[i],cgr->iter,cgr->rez) << " ("<<cgr->get(lowl[i])<<")"<<endl;
      last=cgr->get(lowl[i]);
    }
  }

  if(save){
    string outname=inname+".cgr";
    cout << "Saving pgm to: "<<outname<<".pgm"<<endl;
    cgr->savePGM(outname);
  }
}

string program_help(){
  return string("\
Usage: cgr-sample [options] <cgr>\n\
\n\
Options\n\
*Takes an option (like --high 3 or -h3)\n\
  --high (-h)  = find the highest frequency n patterns (default 5)\n\
  --low (-l)   = find the lowest frequency n patterns\n\
  --res (-i)   = res to use when generating CGRs from scratch (default 8)\n\
\n\
*Toggles: (just --repeatmask or -r)\n\
  --repeatmask (-r) = mask repeats\n\
  --save (-s)       = save a PGM when done\n\
");
}

string program_version(){
  return string("0.1");
}

