/*
Copyright (C) 2006-2008 Keio University
(Kris Popendorf) <comp@bio.keio.ac.jp> (2006)

This file is part of Murasaki.

Murasaki is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Murasaki is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Murasaki.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef __MURASAKI_H
#define __MURASAKI_H

#include "sequence.h"
#include "options.h"
#include "dinkymath.h"
#include "exceptions.h"

//indirection allows one to quote other #defined things (like say PROGRAMVERSION)
#define _QUOTEME(x) #x
#define QUOTEME(x) _QUOTEME(x)

#define PROGRAMVERSION "1.68.6"

//these shoudl all be class-wrapped in some "options" class
extern int seq_count;
extern word usedBuckets,usedPats,worstPat;

extern Options opt;
extern Hash* mfh; //multi-fasta-hash (right now only support one instance)
extern UsedMap* anchors;
extern RepeatMap* repeats;
extern string pattern_src;
extern int patLength;
extern BitSequence* globalPat;
extern vector<Sequence*> seqs;
extern char nodename[81];
extern ofstream *entropy_fh,*anchorProgress_fh;

void hashSeq(BitSequence *bitseq,BitSequence *pat,int sign,Sequence *s);
void extractAndMatch(BitSequence *pat);
void procLocs(LocList &locList,list<Location> &use,int skipsLeft,int level=0);
bool isFullLocList(const LocList &locList);
string program_version();
bool hashSanityCheck();
string platformInfo();
word locList2mclass(LocList &locList);
ostream& outputRegions(ostream &of);
void hashStatusCheck(int tick,SeqPos pos);
void writeHisto(string hashHisto_record,string detailed);
void writeAnchors(string anchor_record);
void writeRepeats(string record);
bool convertPattern(string &pat);
void init_tfidfCounters();
void generateCollisionHistogram(map<size_t,size_t> &collisionSizeHistogram,map<size_t,size_t> &seedHistogram);

extern word *dfCount;
extern word totalAnchoredLocs;

extern int verbose; //debug mojo
extern word longestSeq,totalSeqLength,totalHashLength; //for eco-list parameter computation

class SystemInfo {
 public:
  unsigned long wordsize,totalMemory,freeMemory,swapTotal;
  const int unit;

  SystemInfo();

  string toString() const;
  friend ostream& operator<<(ostream& of,const SystemInfo& a);
};

class MurasakiRuntime {
 protected:
  bool cleaned;
  void init(int argc,char **argv);
  void prep();
  void work();
  void success();
  void cleanup(int disaster=0);

 public:
  char mpi_tag[81];
  BitSequence *pat;
  vector<string> args;
  Timer seqLoadStart;
  int stdoe;
  char stdoename[101];

  MurasakiRuntime(int argc,char **argv);
  ~MurasakiRuntime();
};

extern SystemInfo sysinfo;
extern ProgressTicker ticker;
#endif
