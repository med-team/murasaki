/*
Copyright (C) 2006-2008 Keio University
(Kris Popendorf) <comp@bio.keio.ac.jp> (2006)

This file is part of Murasaki.

Murasaki is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Murasaki is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Murasaki.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef CGR_H
#define CGR_H

//typedef unsigned gray; //the only useful thign to come out of pam.h
//and it turns out it's wrong.... =(
#include <stdint.h> //for uint16_t
typedef uint16_t gray;
#define PGM_OVERALLMAXVAL 65535
#include <iostream>
#include <utility>

#include <vector>


using namespace std;

typedef long unsigned pix;

class PgmBuffer;

class Cgr {
 public:
  const int iter,rez;
  
  Cgr();
  Cgr(int res);
  Cgr(const Cgr&);
  Cgr(const Cgr&,bool);
  Cgr(const PgmBuffer&);
  ~Cgr();
  bool savePGM(string file,bool binary=true);
  bool savePNG(string file);
  pair<pix,pix> findNiceRange();
  pair<pix,pix> sampleHighLow(int low,int high);
  pair<pix,pix> sampleHighLow(double lowp,double highp);
  
  pix* operator[](int); //row access
  pix& operator[](const char*); //point access
  pix& operator[](const string&); //point access
  pix get(unsigned i);
  void load(const char* str,size_t size);
  void normalize();
  void merge(Cgr&);
  void findEdges(const pair<pix,pix> &p,vector<unsigned long> *low,vector<unsigned long> *high);
  
  //output
  friend ostream& operator<<(ostream&,const Cgr&);
  friend class PgmBuffer;
 protected:
  void halve(const Cgr&);
  pix *data;
  bool normalized;
};

class PgmBuffer {
 public:
  PgmBuffer();
  int loadCgr(Cgr&);
  int savePGM(string file,bool binary=true);
  int loadPGM(string file);
  int loadBinaryPGM(string file);
  ~PgmBuffer();
  friend class Cgr;
  gray average(); //junk function
  double difference(vector<PgmBuffer*> others);
  bool allSameRez(vector<PgmBuffer*> others);
  void invert();
 protected:
  vector<gray> data;
  int rez;
  gray maxval;
};

class PGMException {
 public: 
  PGMException(const string& reason):
    reason_(reason) {}
    string reason() const { return reason_; }

 private:
    string reason_;
};

class PGMFileException {
 public:
  PGMFileException( const string& reason )
    : reason_(reason) {}
    PGMFileException( const string& reason, const string& filename, 
		      unsigned int line );
    
    string reason() const { return reason_; }
    
 private:
    string reason_;
};

string itodna(unsigned long idx,const int &iter,const int &rez);
unsigned long dnatoi(const char *dna,const int &iter,const int &rez);
void invert(string& str);

#endif
