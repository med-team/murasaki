/*
Copyright (C) 2006-2008 Keio University
(Kris Popendorf) <comp@bio.keio.ac.jp> (2006)

This file is part of Murasaki.

Murasaki is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Murasaki is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Murasaki.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "ecolist.h"
#include "murasaki.h"
#include "dinkymath.h"
#include <iostream>
#include <utility>

using namespace std;

long bigrand(){
  return (long)(((unsigned long)(rand())<<32) + ((unsigned long)(rand())));
}

ostream& operator<<(ostream& os,const pair<long,long> &a){
  return os << "(" << a.first << "," << a.second << ")";
}

bool lt(const Ecolist::val_type& a,const Ecolist::val_type& b){
  //  return a.first==b.first ? a.second<b.second : a.first<b.first;
  return a.second<b.second;
}

int main(){
  const long genomesize=3221225472;
  cout << "Genome size: "<<genomesize<<" ("<<humanMemory(genomesize)<<"). "
       <<" with reverse: "<<(genomesize*2)<<" ("<<humanMemory(genomesize*2)<<")"<<endl;
  const long seqs=3;
  Ecolist::init(seqs,genomesize,genomesize*2,24,&lt,false);
  Ecolist bob;
  cout << bob;
  cout << "Capacity: "<<bob.capacity()<<endl;
  
  const long vals=50;
  const long repeats=10;
  const long samples=vals*repeats;
  cout << "Adding lots... ("<<samples<<" vals * "<<repeats<<" times)"<<endl;
  vector<pair<long,long> > v;
  pair<long,long> keep;
  srand(0);
  for(long i=0;i<vals;++i){
    pair<long,long> entry(bigrand()%seqs,(bigrand()%(genomesize*2))-genomesize);
    while(entry.second==0){
      entry.second=(bigrand()%(genomesize*2))-genomesize;
    }
    cout << "Adding: "<<entry<<"...";
    cout.flush();
    for(long j=0;j<repeats;j++){
      v.push_back(entry);
      bob.push(entry.first,entry.second);
    }
    cout << bob <<endl;
    pair<long,long> check(bob.getValAt(bob.getSize()-1));
    if(entry!=check)
      cout << (entry == check ? "=":"*")<< "Compare "<<entry<< " vs " << check<<endl;
    keep=entry;
  }

  if(1){
    cout << "Rechecking all..."<<endl;
    for(long i=0;i<samples;i++){
      pair<long,long> check(bob.getValAt(i));
      if(v[i]!=check){
	cout << ">>>Entry: "<<i<<" anomaly! "<< v[i] << " vs "<<check<<endl;
      }
      //else
      //      cout << "   Entry: "<<i<<" ok  --- "<<check<<endl;
    }
    
    cout << "Testing iterators..."<<endl;
    cout << "Forwards..."<<endl;
    Ecolist::iterator ei=bob.begin(),stop=bob.end();
    for(long i=0;ei!=stop;++i){
      if(!(i<samples)){
	cout << "!!!!!!!!!!!!!!!!!!!!!Ran over the end!"<<endl;
	exit(-2000);
	break;
      }
      pair<long,long> check(*ei);
      if(v[i]!=check){
	cout << ">>>Entry: "<<i<<" anomaly! "<< v[i] << " vs "<<check<<endl;
      }
      //    else
      //      cout << "   Entry: "<<i<<" ok  --- "<<check<<endl;
      ++ei;
    }
    cout << "Reverse..."<<endl;
    Ecolist::bi_iterator bei=bob.bi_end();
    Ecolist::bi_iterator bstop=bob.bi_begin();
    for(long i=samples-1;bei!=bstop;--i){
      if(!(i>=0)){
	cout << "!!!!!!!!!Ran over the end!"<<endl;
      }
      pair<long,long> check(*--bei);
      if(v[i]!=check){
	cout << ">>>Entry: "<<i<<" anomaly! "<< v[i] << " vs "<<check<<endl;
      }
      //    else
      //      cout << "   Entry: "<<i<<" ok  --- "<<check<<endl;
    }
  }

  cout << "Let's try a sort!"<<endl;
  bob.sort();
  cout << "Done!"<<endl;

  cout << ">>>Searching for last inserted entry..."<<endl;
  pair<Ecolist::iterator,Ecolist::iterator> range(bob.equal_range(keep));
  cout << "<<<Bits equal to "<<keep<<endl;
  for(Ecolist::iterator i=range.first;i!=range.second;++i){
    cout << i.to_i() << ": "<<*i<<endl;
  }

  cout << ">>>Search for first entry..."<<endl;
  Ecolist::iterator ei=bob.begin();
  pair<long,long> first=*ei;
  range=bob.equal_range(first);
  cout << "<<<Bits equal to "<<first<<endl;
  for(Ecolist::iterator i=range.first;i!=range.second;++i){
    cout << i.to_i() << ": "<<*i<<endl;
  }

  cout << ">>>Search for something smaller than first entry..."<<endl;
  first.second-=100;
  range=bob.equal_range(first);
  cout << "<<<Bits equal to "<<first<<endl;
  for(Ecolist::iterator i=range.first;i!=range.second;++i){
    cout << i.to_i() << ": "<<*i<<endl;
  }

  cout << ">>>Search for last entry..."<<endl;
  Ecolist::bi_iterator bei=bob.bi_end();
  --bei;
  pair<long,long> final=*bei;
  range=bob.equal_range(final);
  cout << "<<<Bits equal to "<<final<<endl;
  for(Ecolist::iterator i=range.first;i!=range.second;++i){
    cout << i.to_i() << ": "<<*i<<endl;
  }

  cout << ">>>Search for something bigger than any..."<<endl;
  final.second+=100;
  range=bob.equal_range(final);
  cout << "<<<Bits equal to "<<final<<endl;
  for(Ecolist::iterator i=range.first;i!=range.second;++i){
    cout << i.to_i() << ": "<<*i<<endl;
  }

  return 0;
}

