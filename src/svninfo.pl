#!/usr/bin/perl

use strict;
local $\="\n";
local $,=",";

my @svn=`svn info -rHEAD 2>&1`;
exit 1 if $?; #svn not happy? don't return anything.
my %svn=kvsplit(@svn);
my ($date)=split(/ /,$svn{"Last Changed Date"});
my ($branch)=$svn{URL};
$branch=$1 if $svn{URL}=~m!((?:branches|trunk|svn)/[a-zA-Z/]*)src$!;
$branch=~s!/$!!;
my @svnstatus=`svn status`;
my @modified=grep {/^M/ and !/Makefile$/ } @svnstatus;
my $modified="*".join("~",map {m!([^/ \t]+?)\s*$!; "$1"} @modified);
print join(":",$branch,"r$svn{'Last Changed Rev'}",$date,(@modified ? ($modified):()));

sub kvsplit {
  my %r;
  foreach my $l (@_){
    chomp $l;
    next unless $l=~m/^([^:]+): (.*)/;
    $r{$1}=$2;
  }
  return %r;
}


