/*
Copyright (C) 2006-2008 Keio University
(Kris Popendorf) <comp@bio.keio.ac.jp> (2006)

This file is part of Murasaki.

Murasaki is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Murasaki is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Murasaki.  If not, see <http://www.gnu.org/licenses/>.
*/

//////////////
// murasaki project
// mbfa.cc
// offline MBFA generator
//////////////

#include <iostream>
#include <fstream>
#include <assert.h>
#include "seqread.h"
#include "binseq.hh"
#include "genopts.h"
#include <getopt.h>
#include "timing.h"

#include <sys/types.h>
#include <sys/stat.h>

using namespace std;

string program_version();
string program_help();
double sizeRatio(string a,string b);

int main(int argc,char **argv){
  SequenceReader reader;
  bool verbose=false,forceCreate=false,fatalErrors=false,info=false,asFasta=false;

  int optc;
  while(1){
    //options struct:
    // name, has_arg, store_pointer, return_value
    static struct option long_options[] = {
      {"help",0,0,'h'},
      {"version",0,0,'V'},
      {"verbose",0,0,'v'},
      {"info",2,0,'i'},
      {"force",2,0,'f'},
      {"fatal",2,0,'F'},
      {"fasta",2,0,'A'},
      {0,0,0,0}
    };
    int longindex=0;
    string prefreq;
    optc=getopt_long(argc,argv,"hVvf::i::F::A::",long_options,&longindex);
    if(optc==-1)break;
    switch(optc){
    case 'h': cout << program_help();exit(0);break;
    case 'V': cout << program_version()<<endl;exit(0);break;
    case 'v': verbose=true;break;
    case 'f': toggleYesNo(optarg,forceCreate,"force");break;
    case 'F': toggleYesNo(optarg,fatalErrors,"fatal");break;
    case 'i': toggleYesNo(optarg,info,"info");break;
    case 'A': toggleYesNo(optarg,asFasta,"fasta");break;
    default: cerr << "Unknown option: "<<(char)optc<<endl; assert(0);break;
    }
  }

  if(verbose)
    reader.verbose=true;

  size_t fails=0;

  for(int i=optind;i<argc;i++){
    Stopwatch conversionTime;
    SequenceBinary *mbfa=NULL;
    if(!forceCreate && SequenceBinary::exists(argv[i])) //try to load it
      try {
	mbfa=new SequenceBinary(argv[i],asFasta); //if we want to see fasta, we need full data
      }catch(SeqReadException e){
	if(verbose)
	  cout << "Binary load of "<<argv[i]<<" failed."<<endl;
      }
    if(!mbfa) //don't have one yet
      try {
	conversionTime.start();
	SequenceFileReader seq(argv[i],reader.options());
	mbfa=new SequenceBinary(seq);
	if(!mbfa->save()){
	  cout << "*Write of "<<mbfa->getBinaryFilename()<<" failed."<<endl;
	  fails++;
	  if(fatalErrors)
	    exit(1);
	}
	conversionTime.stop();
	cerr << "# Conversion of "<<argv[i]<<" to "<<mbfa->getBinaryFilename()<<" completed in "<<conversionTime<<endl;
      }catch(SeqReadException e){
	cout << "Conversion of "<<argv[i]<<" failed."<<endl;
	fails++;
	if(fatalErrors)
	  exit(2);
      }
    if(mbfa){
      if(info){
      cout << argv[i] << " -> " << mbfa->getBinaryFilename()<<endl
	   << " Compression ratio: "<<sizeRatio(mbfa->getBinaryFilename(),argv[i])<<endl
	   << " Length: "<<mbfa->length()<<endl
	   << " Base frequencies: ";
      for(size_t i=0;i<sizeof(mbfa->counters)/sizeof(mbfa->counters[0]);i++)
	cout << mbfa->counters[i] << " ";
      cout << endl;
	
      cout << " Subsequences: "<<mbfa->regionSizes[0]<<endl
	   << " Readable regions: "<<mbfa->regionSizes[1]<<endl
	   << " Unmasked regions: "<<mbfa->regionSizes[2]<<endl;
      }
      if(asFasta){
	mbfa->asFasta(cout);
      }
      delete mbfa;
    }
  }
  return fails;
}

size_t getSize(string file){
  struct stat buf;
  if(stat(file.c_str(),&buf))
    return 0;
  //if it's a stitch file it's not really fair unless we compare against the full component length
  try{
    SequenceFileReader reader(file);
    if(reader.format()==SEQFILE_STITCH)
      return reader.peekLength() + buf.st_size;
  }catch(SeqReadException e){
    //if it fails, so what, who cares
  }
  return buf.st_size;
}

double sizeRatio(string a,string b){
  return (double)getSize(a)/(double)getSize(b);
}

string program_version(){
  SequenceBinaryMetadata meta;
  return string("1.0 MBFA format ")+dstring((long)meta.formatVersion)+" ("+meta.suffix+")";
}

string program_help(){
  return string("\
Usage: mbfa [options...] [input] [input2 ...]\n\
\n\
Generate Murasaki Binary FASTA (mbfa) files for corresponding\n\
input files.\n\
\n\
Options:\n\
  --help|-h        - this message\n\
  --version|-V     - show version\n\
  --verbose|-v     - increase verbosity\n\
  --info|-i        - show info on each MBFA specified\n\
  --force|-f       - force (re)creation of MBFA even if it already existed\n\
  --fatal|-F       - make errors fatal\n\
");
}
