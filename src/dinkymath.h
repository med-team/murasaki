/*
Copyright (C) 2006-2008 Keio University
(Kris Popendorf) <comp@bio.keio.ac.jp> (2006)

This file is part of Murasaki.

Murasaki is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Murasaki is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Murasaki.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef __DINKYMATH_H
#define __DINKYMATH_H

#include <iostream>
#include <fstream>
#include <vector>
#include <set>
#include <map>
#include <iterator>
#include <stdlib.h> //need random()
#include "timing.h"

using namespace std;

//int log2(int x);

inline long ceil_log2_loop(long b){
  int l=0;
  while(b>0){
    b>>=1;
    l++;
  }
  return l;
}

inline long intlog(long x,long b){
  int l=0;
  while(x>=b){
    x/=b;
    l++;
  }
  return l;
}

inline unsigned long intpow(unsigned long base,unsigned long pow){
  unsigned long b=base;
  for(unsigned long i=1;i<pow;i++)
    base*=b;
  return base;
}

template <class T>
inline T sqr(const T& a){
  return a*a;
}

//int intpow(int base,int pow);
inline int abs(int a) throw () {return a<0 ? -a:a;}

string littleEndianStr(unsigned int n,int digits);
string littleEndianPat(unsigned int n,int digits);
string littleEndianStr(unsigned int n);

template <class T>
int writeOutVector(string filename,const vector<T> &v,string delim,int,int,int skipZeros=0);

string dstring(int);
string dstring(long);
string fstring(float);
string dhstring(int i);
string humanMemory(unsigned long bytes);
string humanMemory(unsigned int bytes);
string humanMemory(int bytes);
string humanMemory(double bytes);
string humanMemory(long bytes);
string humanScale(double f);
string percent(double,double);
size_t progressTick(size_t i,size_t max);
void writeOut(string filename,string content,bool append=false);

class RandomTraverse{
 public:
  int next();
  int remaining();
  RandomTraverse(int min,int max);
 protected:
  int min,max;
  set<int> hit;
};

class ProgressTicker {
 public:
  ProgressTicker(size_t _max);
  size_t tick(size_t i);
  inline size_t tick(){return tick(lastCall+1);}
  void reset(size_t _max);
  void done();
  Timer started;
 protected:
  size_t max;
  size_t lineDelay,dotDelay,lineCount,dotCount,lastCall;
};

const int bercent=1024;

class EtaBar {
 public:
  EtaBar(int _max);
  inline int tick();
  inline int tick(int to);
 protected:
  int max;
  int updateDelay,untilNextUpdate,lastCall;
  Timer started;
};

unsigned gcd(unsigned a,unsigned b);
unsigned lcm(unsigned a,unsigned b);

template<class T>
T sum(const vector<T> &v){
  T ret=0;
  for(typename vector<T>::const_iterator i=v.begin();i!=v.end();++i)
    ret+=*i;
  return ret;
}

inline int randint(int max){
  double x=((double)random())/(double)RAND_MAX;
  return (int)(x*(double)max);
}

template<typename T>
string joinStrings(const T &l,string d){
  string r;
  for(typename T::const_iterator i(l.begin()); i!=l.end() ;++i){
    typename T::const_iterator next(i);
    ++next;
    r+=*i;
    if(next!=l.end())
      r+=d;
  }
  return r;
}

template<typename T>
bool writeMap(map<T,T> m,string filename,string description){
  ofstream of(filename.c_str());
  if(of){
    for(typename map<T,T>::iterator i=m.begin();i!=m.end();++i)
      of << i->first << "\t" << i->second << endl;
  }else{
    cerr << "Couldn't write "<<description<<" to "<<filename<<endl;
    return false;
  }
  return true;
}

#endif

