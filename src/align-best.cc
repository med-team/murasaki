//compare 2 alignments
/*
Copyright (C) 2006-2008 Keio University
(Kris Popendorf) <comp@bio.keio.ac.jp> (2006)

This file is part of Murasaki.

Murasaki is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Murasaki is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Murasaki.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <iostream>
#include <fstream>
#include <memory.h>
#include <getopt.h>
#include <vector>
#include <map>
#include <list>
#include <string.h>
#include <boost/lexical_cast.hpp>
#include <err.h>
#include "dinkymath.h"
#include "itree.h"
#include "alignments.h"

#include <boost/filesystem/operations.hpp>

using namespace std;
namespace fs = boost::filesystem;

string program_help();
string program_version();

//function decl
double compareAlignment(Alignment &ref,Alignment &test);
void printResults(vector<vector<double> > &results);

//globals
ProgressTicker ticker(100);

bool debug=false;

int main(int argc,char** argv){
  int optc;
  uint growAnchors=0;
  ostream *os=&cout;
  string outfile;

  string statTarget="ungappedscore";

  using boost::lexical_cast;
  using boost::bad_lexical_cast;

  while(1){
    //options struct:
    // name, has_arg, store_pointer, return_value
    static struct option long_options[] = {
      {"help",0,0,'?'},
      {"version",0,0,'v'},
      {"grow",1,0,'g'},
      {"output",1,0,'o'},
      {"debug",0,0,'d'},
      {"stat",1,0,'s'},
      {0,0,0,0}
    };
    int longindex=0;
    optc=getopt_long(argc,argv,"?vg:o:",long_options,&longindex);
    if(optc==-1)break;
    switch(optc){
    case 'v': cout << program_version();exit(-1);break;
    case '?': cout << program_help();exit(-1);break;
    case 'g': 
      if(optarg){
	string optstr(optarg);
	if(optstr==string("0") || optstr==string("false")){
	  growAnchors=0;
	  break;
	}
	try{
	  growAnchors=lexical_cast<uint>(optstr);
	}
	catch(bad_lexical_cast& e){
	  cerr << "Bad argument to --grow"<<endl;
	  exit(-2);
	}
      }
      break;
    case 'o':
      os=new ofstream(optarg);
      if(!os){
	cerr << "Couldn't write to "<<optarg<<endl;
	exit(-2);
      }
      outfile=string(optarg);
      break;
    case 'd':
      debug=true;
      break;
    case 's':
      statTarget=optarg;
      break;
    default: cout << "Unknown option."<<endl<<program_help();exit(1);break;
    }
  }

  uint alignmentCount=0;
  vector<string> files;
  for(int i=optind;i<argc;i++){
    checkSeqs(argv[i]);
    files.push_back(argv[i]);
    alignmentCount++;
  }
  
  Alignment result;
  vector<Alignment> inputs(alignmentCount);
  typedef multimap<double,pair<int,list<Anchor>::iterator> > BestAnchorMap;
  BestAnchorMap bestAnchors;
  for(uint i=0;i<alignmentCount;i++){
    string statfile=files[i]+string(".stats.")+statTarget;
    ifstream statfh(statfile.c_str());
    if(!statfh)
      err(-1,"Couldn't open stat file: %s",statfile.c_str());

    int anchors=0;
    AnchorFileReader reader(files[i],growAnchors);
    for(vector<region> parts;reader.readline(parts);parts.clear()){
      Anchor& a=inputs[i].add(parts);
      double stat;
      statfh >> stat;
      if(!statfh)
	err(-2,"Stat file (%s) ended before anchor file (%s)",statfile.c_str(),files[i].c_str());

      bestAnchors.insert(BestAnchorMap::value_type(stat,BestAnchorMap::mapped_type(i,a.backref)));
      anchors++;
    }
    cout << "Loaded "<<anchors<<" anchors."<<endl;
  }
  
  cerr << "Selecting best anchors from "<<bestAnchors.size()<<" anchors."<<endl;

  size_t selectedAnchors=0;

  ProgressTicker *progress=debug ? 0:new ProgressTicker(bestAnchors.size());
  for(BestAnchorMap::reverse_iterator bami=bestAnchors.rbegin();bami!=bestAnchors.rend();++bami){
    set<Anchor*> overlaps;
    list<Anchor>::iterator ai=bami->second.second;
    
    vector<region> parts;
    if(debug)cout << "Confirming "<< *ai;
    for(size_t s=0;s<ai->parts.size();s++){
      region r=ai->parts[s].key();
      if(debug)cout << s << " "<< r << " = ";
      if(isZero(r)){
	if(debug)cout << "zero (skip)"<<endl;
	continue;
      }
      
      set<Anchor*> overlaps;
      if(result.fetchAnchors(s,r,overlaps)){
	if(debug)cout << "overlaps "<<overlaps.size()<<" anchors "<<endl;
	goto AnchorEnd;
      }
      if(debug)cout << parts.back()<<endl;
    }
    result.add(*ai);
    selectedAnchors++;

  AnchorEnd: ;
    if(progress)
      progress->tick();
  }     

  cerr << "Done. Writing "<<result.anchors.size()<<" anchors."<<endl;
  *os << result;

  if(outfile.length()){
    cerr << "Adding seqs file to output."<<endl;
    writeSeqs(outfile.c_str());
    os->flush();
    delete os; // i dont like this, but it'll do...
  }

  
  return 0;
}


string program_help(){
  return string("\
Usage: align-best [options] alignment1 [alignment2 ... ]\n\
\n\
Options\n\
*Takes an argument (like --maxres 3 or -m3)\n\
  --grow|v    = grow all anchors by some amount\n\
  --output|o  = store output to a separate file (otherwise, it's stdout)\n\
  --stat|s    = select anchors based on stat X\n\
\n\
*Toggles: (just --merge or -b)\n\
  --help|h    = this message\n\
  --version|v = version string\n\
");
}

string program_version(){
  return string("align-best v0.1");
}

