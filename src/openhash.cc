/*
Copyright (C) 2006-2008 Keio University
(Kris Popendorf) <comp@bio.keio.ac.jp> (2006)

This file is part of Murasaki.

Murasaki is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Murasaki is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Murasaki.  If not, see <http://www.gnu.org/licenses/>.
*/

////////
// super-memory-miserly hash list storage (def.)
// Kris Popendorf
///////

#include "openhash.h"
#include "ecolist.h"
#include "murasaki.h"
#include "exceptions.h"
#include "options.h"

//for performance measurement purposes...
#include <iostream>
#include <fstream>

using namespace std;

OpenHash::OpenHash(BitSequence *pat) :
  Hash(pat),
  keysFree(hash_size),
  keysFreeWarning(hash_size/2) //if we're over 50% full, we're probably in big trouble
#ifdef HASHPROFILE
  ,perfProbeCount(0),perfFindAddrCount(0)
#endif
{
  fasta=new Ecolist[hash_size];
}

OpenHash::~OpenHash(){
  delete[] fasta;
}

void OpenHash::clear(){
  for(word i=0;i<hash_size;i++)
    fasta[i].clear();
}

bool OpenHash::emptyAt(const HashKey key){
  return !fasta[key].getSize();
}

word OpenHash::sizeAt(const HashKey key){
  return fasta[key].getSize();
}

word OpenHash::sizeAt(const HashKey key,const HashVal &val){
  return fasta[findAddr(key,val)].getSize();
}

void OpenHash::add(const HashKey key,const HashVal &val){
  fasta[findAddr(key,val)].push(val.seqno,val.pos);
}

void OpenHash::lookup(HashKey key,LocList &locList){
  Ecolist::iterator start(fasta[key].begin()),stop(fasta[key].end());
  for(Ecolist::iterator i=start;i!=stop;++i){
    val_type l(*i);
    locList[l.first].push_back(val2loc(l));
  }
}

void OpenHash::getMatchingSets(HashKey key,list<LocList> &sets){
  if(emptyAt(key))
    return;
  sets.push_back(LocList(seq_count));
  //we don't have any non-matching entries in each key, so, throw em all in!
  lookup(key,sets.back());
}

bool OpenHash::lessthan(const val_type& a,const val_type& b){
  Location la(val2loc(a)),lb(val2loc(b));
  Window wina(lb,mfh->hashpat),winb(la,mfh->hashpat);
  return wina<winb;
}

void OpenHash::init(SeqPos _seq_count,word _longestSeq,word _totalHashLength,SeqPos _hash_bits){
  Ecolist::init(_seq_count,_longestSeq,_totalHashLength,_hash_bits,&(OpenHash::lessthan),opt.ecolistFrugal);
}

HashKey OpenHash::findAddr(const HashKey start, const HashVal &val){
#ifdef HASHPROFILE
  perfFindAddrCount++;
#endif
  if(emptyAt(start)){
#ifdef HASHPROFILE
    perfProbeHisto[0]++;
#endif
    return start;
  }

  //must search for an empty/matching address
  size_t probeCount=0;
  HashKey probe=start;
  HashVal refval(val);
  Window startWin(refval,mfh->hashpat);
  static bool warningIssued=false;
  while(1){
    if(emptyAt(probe)){
      keysFree--;
      if(keysFree<keysFreeWarning && !warningIssued){
	cerr << "OpenHash: Warning: Hash table is over 50% full resulting in very poor performance."<<endl
	     << "OpenHash: I'll keep trying, but this will probably either crash or not end in your lifetime."<<endl
	     << "OpenHash: It's probably better to restart Murasaki with either more hashbits or use a hashtable that supports chaining (eg. EcoHash)."<<endl;
	warningIssued=true;
      }
      goto FOUNDADDR;
    }
    Location pl(val2loc(*(fasta[probe].begin())));
    Window probeWin(pl,mfh->hashpat);
    if(probeWin.equals(startWin))
      goto FOUNDADDR;

    switch(opt.probingMethod){
    case 1: //quadratic
      probe+=1+(probeCount*probeCount);
      break;
      
    default: //linear
      probe++;
    }

    while(probe>=hash_size)
      probe-=hash_size;

    probeCount++;
    if(probeCount>hash_size && !keysFree){
      throw MurasakiException("Out of hash keys. Rerun Murasaki using either more hashbits, or a different hash table that supports chaining (eg. EcoHash)");
    }
  }
 FOUNDADDR:
#ifdef HASHPROFILE
  perfProbeHisto[probeCount]++;
  perfProbeCount+=probeCount;
#endif
  return probe;
}

void OpenHash::writePerformanceData(string prefix){
#ifdef HASHPROFILE
  {
    cout << "Openhash: Writing probe histogram..."<<endl;
    string histfile(prefix+string(".probe.histogram"));
    ofstream hfh(histfile.c_str());
    if(hfh){
      for(map<size_t,size_t>::iterator i=perfProbeHisto.begin();i!=perfProbeHisto.end();++i)
	hfh << i->first << "\t" << i->second <<endl;
    }else
      cerr << "Warning: Couldn't write OpenHash probe histogram to "<<histfile<<endl;
    cout << "OpenHash: address seeks: "<<perfFindAddrCount<<endl;
    cout << "OpenHash: total probes: "<<perfProbeCount<<endl;
    if(perfFindAddrCount)
    cout << "OpenHash: average probes per seek: "<<((double)perfProbeCount/(double)perfFindAddrCount)<<endl;
  }

  {
    cout << "Openhash: Writing address distance histogram..."<<endl;
    string histfile(prefix+string(".address.dist.histogram"));
    ofstream hfh(histfile.c_str());
    if(hfh){
      size_t first=0;
      while(emptyAt(first) && first<hash_size)
	first++;
      if(first<hash_size){
	//build histogram
	map<size_t,size_t> disthist;
	
	for(size_t dist=1,i=first+1;i!=first;i=(i+1>=hash_size ? 0:i+1),dist++){
	  if(!emptyAt(i)){
	    disthist[dist]++;
	    dist=0;
	  }
	}
	
	for(map<size_t,size_t>::iterator i=disthist.begin();i!=disthist.end();++i)
	  hfh << i->first << "\t" << i->second <<endl;
      }
    }else
    cerr << "Warning: Couldn't write OpenHash distance histogram to "<<histfile<<endl;
  }
#endif
}
