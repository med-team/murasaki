/*
Copyright (C) 2006-2008 Keio University
(Kris Popendorf) <comp@bio.keio.ac.jp> (2006)

This file is part of Murasaki.

Murasaki is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Murasaki is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Murasaki.  If not, see <http://www.gnu.org/licenses/>.
*/

//////////////
// murasaki project
// seqread.h
// c++ support for reading various sequence formats
//////////////

#ifndef __SEQREAD_H_
#define __SEQREAD_H_

#include <fstream>
#include <iostream>
#include <list>
#include <boost/regex.hpp>
#include <boost/iostreams/filtering_streambuf.hpp>
#include <boost/iostreams/copy.hpp>
#include <boost/iostreams/filter/bzip2.hpp>
#include <boost/iostreams/filter/gzip.hpp>
#include <boost/iostreams/filter/zlib.hpp>

using namespace std;
namespace io = boost::iostreams;

class SeqReadException {
 public: 
  SeqReadException(const string& reason):
    reason_(reason) {}
    string reason() const { return reason_; }

 private:
    string reason_;
};

enum SeqReadOptions {SEQO_RMASK=1,SEQO_LEN=2,SEQO_NOREC=4,SEQO_SILENT=8,SEQO_VERBOSE=16};

class SequenceReaderGlobalOptions { //singleton
 public:
  static SequenceReaderGlobalOptions* options();

  bool ignoreBogusChars;
 protected:
  SequenceReaderGlobalOptions();
 private:
  static SequenceReaderGlobalOptions* _instance;
};

class SequenceReader {
 public:
  size_t readSeqInto(string& dst,istream& is,const string filename = string());
  size_t readSeqInto(string& dst,const string &filename);
  
  bool repeatMask,lengthOnly,recordLength,silent,verbose;

  inline int options(){return repeatMask | lengthOnly<<1 | (!recordLength)<<2 | silent<<3 | verbose<<4;}
  SequenceReader();
};

enum FileFormats {SEQFILE_RAW=0,SEQFILE_FASTA,SEQFILE_STITCH,SEQFILE_GBK,SEQFILE_END};

class SequenceFileReader;
class SequenceByteReader {
 public:
  SequenceByteReader(istream &is,const string filename=string(),int options=0);
  char getc();
  inline size_t size(){return count;}
  bool eof();
  bool repeatMask,lengthOnly,recordLength,silent,verbose;
  inline int options(){return repeatMask | lengthOnly<<1 | (!recordLength)<<2 | silent<<3 | verbose<<4;}
  inline FileFormats format(){return filetype;}
  inline long getSubSeqId(){return subSeqId;}
  inline string getSubSeqName(){return subSeqName;}
 protected:
  //no-init needed
  const int bufsize;
  std::list<string> buffers;
  string subSeqName;
  size_t linenum;
    
  //initialized data
  bool finished;
  FileFormats filetype;
  string filename;
  istream& is;
  SequenceFileReader *redirect;
  size_t count,subcount;
  long subSeqId;    
  bool bogusCharWarned;

  void procBuf(string& dst,const char *buf,const int &bufsize);
  void addRedirect(boost::cmatch results);
  bool readMore();
  bool writeBackSize();
};

class SequenceFileReader { //provides the sugar to run with filenames and subsequences
 protected:

  
 public:
  SequenceFileReader(istream &is,const string filename=string(),int options=0);
  SequenceFileReader(const string filename,int options=0);
  static bool parseRangeSpec(string &filename,pair<size_t,size_t> &range,int options);
  bool useRangeSpec(int noOpen=0);
  char getc();
  size_t size();
  bool eof();
  inline const string& get_filename(){return filename;}
  static size_t getLength(string filename,int options=0);
  size_t peekLength() const;
  size_t readLength(); //actually read the file and compute length
  inline FileFormats format(){return byteReader->format();}
  string formatString();
  ~SequenceFileReader();
  
  //vars
  int options;
  
 protected:
  string filename;
  istream* setupInputFilters(istream &file);
  io::filtering_streambuf<io::input> in;
  ifstream ifs;
  istream *is;
  SequenceByteReader *byteReader;
  bool rangeOnly;
  pair<size_t,size_t> range;
  size_t outCount;

 public:
  inline long getSubSeqId(){return byteReader->getSubSeqId();}
  inline string getSubSeqName(){return byteReader->getSubSeqName();}
};

#endif
