/*
Murasaki - multiple genome global alignment program
Copyright (C) 2006-2008 Keio University
(Kris Popendorf) <comp@bio.keio.ac.jp> (2006)

This file is part of Murasaki.

Murasaki is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Murasaki is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Murasaki.  If not, see <http://www.gnu.org/licenses/>.
*/

//////////////
// murasaki project
// arrayhash.cc
// provides arrayhash class
//////////////

#include "murasaki.h"
#include "sequence.h"
#include "dinkymath.h"
#include "cmultiset.h"
#include "arrayhash.h"

bool ArrayHash::emptyAt(const HashKey key){
  return fasta[key] == NULL;
}

word ArrayHash::sizeAt(const HashKey key){
    if(fasta[key] == NULL) return 0;
    else return fasta[key]->size();
}

word ArrayHash::sizeAt(const HashKey key,const HashVal &val){
  pair<HashVal*,HashVal*> range=fasta[key]->equal_range(val);
  return range.second-range.first;
}

void ArrayHash::add(const HashKey key, const HashVal &a){
  if(fasta[key]==NULL) fasta[key] = new cmultiset<HashVal>;
  fasta[key]->insert(a);
}

ArrayHash::ArrayHash(BitSequence *pat) :  Hash(pat),  //holy crap C++ is cool
  activeRange(0,0)
{
  fasta=new MHash[hash_size];
  memset(fasta,0,sizeof(MHash)*hash_size);
}

void ArrayHash::clear(){
  /* // this is too slow!
  for(int i=0; i<hash_size; i++) delete fasta[i];
  memset(fasta,0,sizeof(MHash)*hash_size);
  */

  for(word i=0; i<hash_size; i++)
    if(fasta[i]!=NULL) fasta[i]->clear();
}

void ArrayHash::pickup(LocList &locList, pair<HashIte,HashIte> range){
  for(HashIte i=range.first; i!=range.second; i++)
    locList[i->seqId()].push_back(*i);
}

void ArrayHash::getMatchingSets(HashKey key,list<LocList> &sets){
  if(!fasta[key])
    return;
  
  fasta[key]->sort();

  HashIte start(fasta[key]->begin()),stop(fasta[key]->end());
  for(HashIte si=start;si!=stop;){
    sets.push_back(LocList(seq_count));
    HashIte ref=si;
    (sets.back())[si->seqId()].push_back(*si);
    for(++si;si!=stop && !(*ref<*si);++si){
      (sets.back())[si->seqId()].push_back(*si);
    }
  }
}

void ArrayHash::lookup(HashKey key,LocList &locList){
  if(!fasta[key])
    return;
  fasta[key]->sort(); //the list returned by this _MUST_ be sorted.
  pair<HashIte,HashIte> range(fasta[key]->begin(), fasta[key]->end());
  pickup(locList, range);
}

void ArrayHash::dump(ostream &os){
  HashKey key;
  unsigned s;

  for(key=0; key<(unsigned)hash_size; key++){
    s = sizeAt(key);
    //    cout << "dump: size=" << s<< endl;
    if( s==0 ) continue;
    
    os.write( (char*)&key, sizeof(HashKey)  );
    os.write( (char*)&s,   sizeof(unsigned) );

    pair <int, HashVal*> p = fasta[key]->block_read();
    HashVal* l = p.second;
    os.write( (char*)l, sizeof(HashVal) * s);
  }

  key = 0;
  s = 0;
  os.write( (char*)&key, sizeof(HashKey)  );
  os.write( (char*)&s,   sizeof(unsigned) );
}

void ArrayHash::load(istream &is){
  HashKey key;
  unsigned s;
  HashVal *buf;
  int bufsize;

  bufsize = 1024*1024;
  buf = (HashVal*)malloc(bufsize);

  do{
    is.read( (char*)&key, sizeof(HashKey) );
    is.read( (char*)&s,   sizeof(unsigned));
    if (key==0 && s==0) break;

    if(fasta[key]==NULL) fasta[key] = new cmultiset<HashVal>;
    
    int readbytes = sizeof(HashVal)*s;

    if(bufsize > readbytes){
      bufsize = readbytes;
      buf = (HashVal*)realloc(buf, bufsize);
    }
    is.read( (char*)buf, sizeof(HashVal)*s );
    fasta[key]->block_append(buf, (int)s);
  } while(true);

  free(buf);
}

bool ArrayHash::rawSanityCheck(){
  for(word base=0;base<(word)hash_size;base++){
    if(emptyAt(base))
      continue;
    for(HashIte i=(*fasta[base]).begin();i!=(*fasta[base]).end();i++){
      Location l(*i);
      Window w(l,hashpat);
      word here=w.hash();
      //      cout << "hash for "<<l<<" ("<<w<<"): "<<wordToString(here) << " (vs "<< wordToString(base)<<")"<<endl;
      if(here!=base){
	assert(here==base);
        return false;
      }
    }
  }
  return true;
}

bool ArrayHash::rawSanityCheck(word base){
  if((*fasta[base]).empty())
    return true;
  //  cout << "Spot checking "<<wordToString(base)<<endl;
  for(HashIte i=(*fasta[base]).begin();i!=(*fasta[base]).end();i++){
    Location l(*i);
    Window w(l,hashpat);
    word here=w.hash();
    //    cout << "  ** hash for "<<l<<" ("<<w<<"): "<<wordToString(here)<<endl;
    if(here!=base){
      assert(here==base);
      return false;
    }
  }
  return true;
}

