/*
Copyright (C) 2006-2008 Keio University
(Kris Popendorf) <comp@bio.keio.ac.jp> (2006)

This file is part of Murasaki.

Murasaki is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Murasaki is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Murasaki.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef _SCORING_H_
#define _SCORING_H_

#include "globaltypes.h"
#include "sequence.h"
#include <limits.h>
#include <vector>

typedef long Score; //basic additive logodds-like score
const Score MAXSCORE=LONG_MAX;

extern Score ScoreMatrix[5][5]; //standard ACGT + N


Score scoreColumn(const vector<BaseIterator> &column);
void addColumnPairScores(const vector<BaseIterator> &column,vector<vector<Score> > &output);
Score minimumPairScore(const vector<vector<Score> > &output);
SeqPos fuzzyExtendUntil(vector<BaseIterator> column,SeqPos max,int direction,long* scoreOut);

#endif
