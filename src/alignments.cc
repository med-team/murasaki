//post-processing alignment management stuff
/*
Copyright (C) 2006-2008 Keio University
(Kris Popendorf) <comp@bio.keio.ac.jp> (2006)

This file is part of Murasaki.

Murasaki is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Murasaki is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Murasaki.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "alignments.h"
#include <iostream>
#include <fstream>

#include <boost/regex.hpp>

//multiple versions of spirit floating around there. annoying.
#include <boost/version.hpp>
#if BOOST_VERSION >= 103800
//use spirit "classic"
#if !defined(BOOST_SPIRIT_USE_OLD_NAMESPACE)
#define BOOST_SPIRIT_USE_OLD_NAMESPACE
#endif

#include <boost/spirit/include/classic_core.hpp>
#include <boost/spirit/include/classic_attribute.hpp>
#include <boost/spirit/include/classic_symbols.hpp>
#include <boost/spirit/include/classic_chset.hpp>
#include <boost/spirit/include/classic_escape_char.hpp>
#include <boost/spirit/include/classic_confix.hpp>
#include <boost/spirit/include/classic_iterator.hpp>
#include <boost/spirit/include/classic_exceptions.hpp>
#else
//current is "classic"
#include <boost/spirit/core.hpp>
#include <boost/spirit/attribute.hpp>
#include <boost/spirit/symbols/symbols.hpp>
#include <boost/spirit/utility/chset.hpp>
#include <boost/spirit/utility/escape_char.hpp>
#include <boost/spirit/utility/confix.hpp>
#include <boost/spirit/iterator.hpp>
#include <boost/spirit/error_handling/exceptions.hpp>
#endif

using namespace std;

int Alignment::nextId=0;
int Anchor::nextId=0;

uint seqCount=0;
vector<string> seqs;
bool seqinit=false;

bool verify(const region& r){
  if(r.stop<r.start)
    return false;
  if((r.start<0) != (r.stop<0))
    return false;
  return true;
}

void loadAnchors(const char* filename,Alignment& alignment,ulong growAnchors)
{
  int anchors=0;
  AnchorFileReader reader(filename,growAnchors);
  for(vector<region> parts;reader.readline(parts);parts.clear()){
    if(growAnchors)
      alignment.mergeAdd(parts);
    else
      alignment.add(parts);
    anchors++;
  }
}

void writeSeqs(const char* alignfile){
  const boost::regex prefix("^(.*)\\.anchors$");
  boost::cmatch m;
  string seqfile;
  if(boost::regex_match(alignfile,m,prefix)){
    assert(m.size()>=1);
    string prefix=m[1];
    seqfile=prefix+string(".seqs");
  }else{ //assume we've been handed a prefix
    seqfile=string(alignfile)+string(".seqs");
  }
  ofstream seqsfh(seqfile.c_str());
  if(!seqsfh.good()){
    cerr << "Error opening "<<seqfile<<endl;
    exit(-1);
  }
  for(uint s=0;s<seqCount;s++){
    seqsfh << seqs[s] << endl;
  }
}

string getAnchorPrefix(string str){
  string ret;
  const boost::regex prefix("^(.*)\\.anchors$");
  /*  if(!fs::exists(alignfile)){
    cerr << "Alignment file: "<<alignfile<<" not found.\n";
    }*/
  boost::smatch m;
  if(boost::regex_match(str,m,prefix)){
    assert(m.size()>=1);
    ret=m[1];
  }
  return ret;
}

void checkSeqs(const char* alignfile){
  string prefix=getAnchorPrefix(string(alignfile));
  if(!prefix.empty()){
    string seqfile=prefix+string(".seqs");
    ifstream seqsfh(seqfile.c_str());
    if(!seqsfh.good()){
      cerr << "Error opening "<<seqfile<<endl;
      exit(-1);
    }
    for(uint i=0;!seqsfh.eof();i++){
      string here;
      getline(seqsfh,here);
      if(here.length()<1)break;
      if(seqinit){
	if(i>=seqCount){
	  cerr << "Alignments have a different number of members!"<<endl;
	  exit(5);
	}
	if(seqs[i]!=here)
	  cerr << "Warning: Alignments appear to be of different sequences! ("<<seqs[i]<<"!="<<here<<")"<<endl;
      }else{
	cerr << "Seq "<<i<<" refers to "<<here<<endl;
	seqs.push_back(here);
	seqCount++;
      }
    }
  }else{
    cerr << "Alignment file must be an anchors file.\n";
    exit(5);
  }
  seqinit=true;
}

ostream& operator<<(ostream& os,Anchor& a){
  for(uint s=0;s<seqCount;s++){
    os << (a.parts[s]).key();
    if(s<seqCount-1)
      os << ",";
  }
  return os;
}

ostream& operator<<(ostream& os,const RegionSet& a){
  for(uint s=0;s<seqCount;s++){
    os << (a.parts[s]);
    if(s<seqCount-1)
      os << ",";
  }
  return os;
}

ostream& operator<<(ostream& os,Alignment& a){
  for(list<Anchor>::iterator i=a.anchors.begin();i!=a.anchors.end();++i){
    Anchor& anc=*i;
    for(uint s=0;s<seqCount;s++){
      os << (anc.parts[s]).key().start << "\t" <<(anc.parts[s]).key().stop << "\t" 
	 << ((anc.parts[s]).key().start < 0 ? "-":"+");
      if(s==seqCount-1)
	os << "\n";
      else
	os << "\t";
    }
  }
  return os;
}

bool Alignment::findOverlaps(const Anchor& a,set<Anchor*> &context){
  set<Anchor*> matches,temp;

  uint i=0;
  while(isZero(a.parts[i])){
    i++;
    assert(i<seqCount);
  }

  if(!fetchAnchors(i,a.parts[i].key(),context))
    return false;
  
  for(i++;i<seqCount;i++){
    if(isZero(a.parts[i]))
      continue;
    if(!fetchAnchors(i,a.parts[i].key(),temp))
      return false;
    set_intersection(temp.begin(),temp.end(),
		     context.begin(),context.end(),
		     inserter(matches,matches.begin()));
    if(matches.empty())
      return false;
    context.clear();
    context.insert(matches.begin(),matches.end());
    matches.clear();
  }
  //context now contains only ids which overlap in each sequence (however where a isZero, all bets are off!)
  return !context.empty();
}

bool Alignment::findOverlaps(const vector<region> parts,set<Anchor*> &context){
  set<Anchor*> matches,temp;

  uint i=0;
  while(isZero(parts[i])){
    i++;
    assert(i<seqCount);
  }

  if(!fetchAnchors(i,parts[i],context))
    return false;
  
  for(i++;i<seqCount;i++){
    if(isZero(parts[i]))
      continue;
    if(!fetchAnchors(i,parts[i],temp))
      return false;
    set_intersection(temp.begin(),temp.end(),
		     context.begin(),context.end(),
		     inserter(matches,matches.begin()));
    if(matches.empty())
      return false;
    context.clear();
    context.insert(matches.begin(),matches.end());
    matches.clear();
  }
  //context now contains only ids which overlap in all the sets
  return !context.empty();
}

bool Anchor::operator==(const Anchor& a){
  return backref==a.backref;
}

bool Anchor::overlaps(const vector<region> &regions) const {
  uint s=0;
  for(s=0;s<seqCount;s++){
    const region& a=regions[s];
    region b(parts[s].key());
    if(isZero(a) && isZero(b))
      continue;
    else
      if(!regionsOverlap(a,b))
	return false;
  }
  return true;
}

bool regionsOverlap(const region &a,const region &b){
  if(isZero(a) || isZero(b))
    return false;
  return a.overlaps(b);
}


void Alignment::mergeAdd(const Anchor& a){
  vector<region> parts(seqCount);
  for(uint s=0;s<seqCount;s++)
    parts[s]=a.parts[s].key();
  mergeAdd(parts);
}

void Alignment::mergeAdd(const vector<region> &parts){
  set<Anchor*> overlaps;
  vector<region> newparts(parts);

  if(findOverlaps(parts,overlaps)){
    for(set<Anchor*>::iterator o=overlaps.begin();o!=overlaps.end();++o){
      if(!(*o)->overlaps(parts)) //might not overlaps in _all_ sequences
	continue;
      //      cerr << "Overlap in: "<<endl;
      for(uint s=0;s<seqCount;s++){
	//	cerr << s << " "<<newparts[s]<<" with "<<(*o)->parts[s].key() << " ->";
	assert(verify(newparts[s]));
	assert(verify((*o)->parts[s].key()));
	coalesce(newparts[s],(*o)->parts[s].key());
	//	cerr << newparts[s]<<endl;
	assert(verify(newparts[s]));
      }
      remove(*o);
    }
  }

  add(newparts);
}

void Alignment::remove(Anchor *a){
  for(uint s=0;s<seqCount;s++)
    trees[s].erase(a->parts[s]);
  anchors.erase(a->backref);
}

bool canonicalize(vector<region> &vr){
  size_t si=0;
  assert(!vr.empty());
  while(si<vr.size()){
    if(!isZero(vr[si]))
      break;
    si++;
  }
  if(isRev(vr[si])){ //need to flip everything
    while(si<vr.size()){
      vr[si]=invert(vr[si]);
      si++;
    }
    return true;
  }else{
    return false;
  }
}

AnchorFileReader::AnchorFileReader(string _filename,ulong _growAnchors) :
  filename(_filename),
  growAnchors(_growAnchors),
  inf(_filename.c_str())
{
}

bool AnchorFileReader::readline(vector<region> &parts){
  using namespace boost::spirit;
  const int maxline=1024;
  char line[maxline];
  parse_info<char const*> info;
  rule<> sign_p(ch_p('+')|'-');

  linenum++;
  inf.getline(line,maxline);
  if(inf.bad() || strlen(line)<1)
    return false;
  const char *parse_start=line;
  long start,stop;
  char sign='*'; //mmm invalid sign
  int_parser<long,10,1,-1> long_p;
  for(uint seqi=0;seqi<seqCount;seqi++){
    info = parse(parse_start, long_p[assign(start)] >> long_p[assign(stop)] >> (ch_p('+')[assign(sign)]|ch_p('-')[assign(sign)])
		 ,space_p);
    parse_start=info.stop;
    if(!info.hit){
      cerr << "Failed to extract a whole anchor!!!! ("<<filename<<":"<<linenum<<" (seq "<<seqi<<", here: "<<info.stop<<")"<<endl;
      exit(10);
    }
    assert(sign=='-' || sign=='+');
    if(sign=='-' && start>0){
      swap(start,stop);
      start=-start;
      stop=-stop;
    }
    if(start>stop){
      cerr << filename << ":"<<linenum<<" Backwards coordinates!\n";
      swap<long>(start,stop);
    }
    assert(verify(region(start,stop)));
    parts.push_back(growAnchors ? grow(region(start,stop),growAnchors):region(start,stop));
    assert(verify(parts.back()));
  }
  
  canonicalize(parts);
  return true;
}

SequenceCover::SequenceCover(){
}

SequenceCover::TreeType::iterator SequenceCover::merge(region in){
  size_t count=1;
  for(TreeType::range_iterator i(cover.in_range(in)),next;i!=cover.end();i=next){
    next=i;
    ++next;
    assert(in.overlaps(i.key()));
    count++;
    in.start=min(in.start,i.key().start);
    in.stop=max(in.stop,i.key().stop);
    TreeType::iterator ite(i);
    cover.erase(ite);
  }
  return cover.insert(in,count);
}

long SequenceCover::totalLength() {
  long total=0;
  for(TreeType::iterator i=cover.begin();i!=cover.end();++i){
    total+=length(i.key());
  }
  return total;
}

RegionSet::RegionSet(const Anchor &a){
  parts.reserve(a.parts.size());
  for(size_t i=0;i<a.parts.size();i++)
    parts.push_back(a.parts[i].key());
  assert(parts.size());
}

double RegionSet::avgLength() const {
  long len=0,count=0;
  for(vector<region>::const_iterator ite=parts.begin();
      ite!=parts.end();++ite)
    if(!isZero(*ite)){
      count++;
      len+=length(*ite);
    }
  return (double)len/(double)count;
}

bool RegionSet::isGapped() const {
  vector<region>::const_iterator ite=parts.begin();
  while(isZero(*ite) && ++ite!=parts.end())
    ;
  long len=length(*ite);
  while(++ite!=parts.end()){
    if(isZero(*ite))continue;
    if(length(*ite)!=len)
      return true;
  }
  return false;
}

long RegionSet::riftCount() const {
  vector<region>::const_iterator ite=parts.begin();
  long count=0;
  while(++ite!=parts.end())
    if(isZero(*ite))
      count++;
  return count;
}

RegionSet::RegionSet() {
}

RegionSet RegionSet::getIslands() const {
  RegionSet r;
  vector<region>::const_iterator ite=parts.begin();
  while(++ite!=parts.end())
    if(!isZero(*ite))
      r.parts.push_back(*ite);
  assert(!r.parts.empty());
  return r;
}

RegionSet RegionSet::setAbs() const {
  RegionSet r;
  for(vector<region>::const_iterator ite=parts.begin();
      ite!=parts.end();
      ++ite){
    r.parts.push_back(abs(*ite));
  }
  return r;
}
