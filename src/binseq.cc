/*
Copyright (C) 2006-2008 Keio University
(Kris Popendorf) <comp@bio.keio.ac.jp> (2006)

This file is part of Murasaki.

Murasaki is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Murasaki is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Murasaki.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "binseq.hh"
#include <iostream>
#include <fstream>
#include <sys/mman.h>
#include <string.h> //memcpy
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <errno.h>
#include <err.h>

using namespace std;

SequenceBinaryMetadata::SequenceBinaryMetadata() :
  formatVersion(1),
  suffix(string(".mbfa")+dstring((int)sizeof(word))+dstring((int)sizeof(SeqPos)))
{}

bool SequenceBinary::save(){
  return save(binaryFilename);
}

bool SequenceBinary::save(string filename){
  //sadly ostream::write sucks for even moderately large values of N on 64bit debian 5.0,
  //so I'm just going to say: suck it ostream, I'm using C
  ofstream ofh(filename.c_str(),ios::out | ios::binary);
  try {
    ofh.exceptions(ofstream::failbit | ofstream::badbit);
    char wordsize=sizeof(word),seqposSize=sizeof(SeqPos);
    ofh.write(BINSEQ_MAGICNUMBER,sizeof(BINSEQ_MAGICNUMBER)-1);
    ofh.write(CHARCAST(formatMetaData.formatVersion),1);
    ofh.write(CHARCAST(wordsize),1);
    ofh.write(CHARCAST(seqposSize),1);
    ofh.write(CHARCAST(_length),sizeof(_length));
    ofh.write(CHARCAST(counters),sizeof(counters));
    ofh.write(CHARCAST(regionSizes),sizeof(regionSizes));
    assert(sizeof(streamsize)>=sizeof(size_t)); //otherwise this next line is in trouble
    ofh.write((char*)_words,sizeof(word)*word_count); //the big one... hopefully sizeof(streamsize)>=sizeof(size_t)
    for(size_t ri=0;ri<sizeof(regions)/sizeof(regions[0]);ri++){
      ofh.write(CHARCAST(regions[ri][0]),((streamsize)sizeof(Region))*((streamsize)regionSizes[ri]));
    }

    for(vector<string>::iterator si=subSeqNames.begin();si!=subSeqNames.end();++si){
      word len=si->length()+1; //include null terminator
      ofh.write(CHARCAST(len),sizeof(len));
      ofh.write(si->c_str(),len);
    }
    ofh.close();
  }catch(ofstream::failure e){
    warn("SequenceBinary::save(%s) failure",filename.c_str());
    ofh.close();
    unlink(filename.c_str());
    return false;
  }

  return true;
}

string SequenceBinary::deriveLocalBinaryFilename(string filename){
  filename+=formatMetaData.suffix; //eg. on a 64bit machine with LONGSEQ on these are .mbfa88 files
  return filename;
}

string SequenceBinary::deriveBinaryFilename(string filename){
  SequenceBinaryMetadata def;
  filename+=def.suffix; //eg. on a 64bit machine with LONGSEQ on these are .mbfa88 files
  return filename;
}

bool SequenceBinary::exists(string filename){
  try {
    SequenceBinary test(filename,false);
    return true;
  }catch(SeqReadException e){
    return false;
  }
}

SequenceBinary::SequenceBinary(string filename,bool completeLoad) :
  formatMetaData(),
  baseFilename(filename),
  binaryFilename(deriveLocalBinaryFilename())
{
  init();

  if((fd=open(binaryFilename.c_str(),O_RDONLY))<0){
    throw SeqReadException("Couldn't open MBFA file for reading: "+binaryFilename+" : "+strerror(errno));
  }
  struct stat fdstat;
  fstat(fd,&fdstat);
  struct stat srcStat;
  if(!stat(baseFilename.c_str(),&srcStat) && srcStat.st_mtime>fdstat.st_mtime)
    throw SeqReadException("MBFA is older than source file. Cowardly refusing to use outdated MBFA.");

  fdmem_len=fdstat.st_size;
  if((fdmem=mmap(NULL,fdmem_len,PROT_READ,MAP_PRIVATE,fd,0))==MAP_FAILED){
    throw SeqReadException("Failed mapping listener block: "+string(strerror(errno)));
  }
  if(strncmp((const char*)fdmem,BINSEQ_MAGICNUMBER,sizeof(BINSEQ_MAGICNUMBER)-1))
    throw SeqReadException("Not a Murasaki Binary FASTA file: "+filename);
  char fileversion;
  char wordsize=sizeof(word),seqposSize=sizeof(SeqPos);
  DeSerial parser(fdmem,3);
  parser.get(fileversion);
  if(fileversion!=formatMetaData.formatVersion)
    throw SeqReadException("Wrong MBFA format version. Expected "+dstring(formatMetaData.formatVersion)+", found "+dstring(fileversion));
  parser.get(wordsize);
  if(wordsize!=sizeof(word))
    throw SeqReadException("Wrong MBFA system architecture. Expected "+dstring((long)sizeof(word))+", found "+dstring((long)wordsize));
  parser.get(seqposSize);
  if(seqposSize!=sizeof(SeqPos))
    throw SeqReadException("Wrong MBFA murasaki architecture. Expected "+dstring((long)sizeof(SeqPos))+", found "+dstring((long)seqposSize));
  parser.get(_length);
  parser.get(counters,sizeof(counters)/sizeof(counters[0]));
  parser.get(regionSizes,sizeof(regionSizes)/sizeof(regionSizes[0]));

  bit_count=_length*2;
  word_count=((bit_count+WORDSIZE-1)/(WORDSIZE)); //rounds up to nearest full word
  parser.map(_words,word_count);
  for(size_t ri=0;ri<sizeof(regions)/sizeof(regions[0]);ri++)
    parser.map(regions[ri],regionSizes[ri]);

  if(completeLoad){    
    for(word si=0;si<regionSizes[0];++si){
      word len;
      parser.get(len);
      char *buf;
      parser.map(buf,len);
      subSeqNamesP.push_back(buf);
    }
    assert((off_t)parser.offset==fdstat.st_size); //should have read the whole file now
  }
}

void SequenceBinary::fillCopyVectors(){ //makes it safe to munmap the file if necessary
  subSeqs.resize(regionSizes[0]);
  memcpy(&subSeqs.front(),regions[0],regionSizes[0]*sizeof(Region));
  readableRegions.resize(regionSizes[1]);
  memcpy(&readableRegions.front(),regions[1],regionSizes[1]*sizeof(Region));
  unmaskedRegions.resize(regionSizes[2]);
  memcpy(&unmaskedRegions.front(),regions[2],regionSizes[2]*sizeof(Region));
  for(word si=0;si<regionSizes[0];++si)
    subSeqNames.push_back(string(subSeqNamesP[si]));
}

DeSerial::DeSerial(void *_m,size_t _offset):
     m(_m),offset(_offset)
{ }

void SequenceBinary::init(){
  for(size_t i=0;i<sizeof(counters)/sizeof(counters[0]);i++)
    counters[i]=0;
  seqdataMalloc=0;
  fd=0;
  fdmem=NULL;
}

SequenceBinary::SequenceBinary(SequenceFileReader &reader) :
  formatMetaData(),
  baseFilename(reader.get_filename()),
  binaryFilename(deriveLocalBinaryFilename())
{
  init();
  _length=reader.peekLength();
  bit_count=_length*2;
  word_count=((bit_count+WORDSIZE-1)/(WORDSIZE)); //rounds up to nearest full word
  _words=allocWords();

  word wordsStored=0;
  SeqPos bit=WORDSIZE;
  word w=0;
  bool inReadableRegion=false,inUnmaskedRegion=false;
  bool readable=false,unmasked=false;
  pair<SeqPos,SeqPos> unmaskedRegion(0,0),readableRegion(0,0),subSeqBounds(0,0);
  long prevSubSeqId=reader.getSubSeqId();
  string prevSubSeqName=reader.getSubSeqName();

  for(SeqPos i=0;i<(SeqPos)_length;i++){
    bit-=2;
    char c=reader.getc();
    unmasked=true;
    switch(c){
    case 'a': unmasked=false;counters[0+4]++;
    case 'A': counters[0]++;readable=true;break;
    case 'c': unmasked=false;counters[1+4]++;
    case 'C': w|=((word)1<<bit);counters[1]++;readable=true;break;
    case 'g': unmasked=false;counters[2+4]++;
    case 'G': w|=((word)2<<bit);counters[2]++;readable=true;break;
    case 't': unmasked=false;counters[3+4]++;
    case 'T': w|=((word)3<<bit);counters[3]++;readable=true;break;
    case 'N':case 'n': unmasked=false; readable=false; break;
    case 0: //eof
      throw SeqReadException("Fewer characters in stream than predicted ("+dstring(i)+" read vs "+dstring((SeqPos)_length)+") predicted!");
    default: throw SeqReadException("Unusable character (dec:"+dstring(c)+" char: "+c+") encountered at position "+dstring(i));
    }

    if(inReadableRegion){ //in a readable zone
      if(!readable){//region end!
	readableRegion.second=i-1;
	inReadableRegion=!inReadableRegion;
	readableRegions.push_back(readableRegion);
      }
    }else{ //was in an N zone
      if(readable){//region start
	readableRegion.first=i;
	inReadableRegion=!inReadableRegion;
      }
    }

    if(inUnmaskedRegion){ //in a unmasked zone
      if(!unmasked){//region end!
	unmaskedRegion.second=i-1;
	inUnmaskedRegion=!inUnmaskedRegion;
	unmaskedRegions.push_back(unmaskedRegion);
      }
    }else{ //was in an N zone
      if(unmasked){//region start
	unmaskedRegion.first=i;
	inUnmaskedRegion=!inUnmaskedRegion;
      }
    }

    if(reader.getSubSeqId()!=prevSubSeqId){ //subseq changed!
      subSeqBounds.second=i-1;
      assert(subSeqBounds.second>=subSeqBounds.first);
      subSeqs.push_back(subSeqBounds);
      subSeqNames.push_back(prevSubSeqName);
      subSeqNamesP.push_back(subSeqNames.back().c_str());
      
      subSeqBounds.first=i+10; //this looks a bit hinky, but between every subseq there _should_ be exactly 10 Ns, so this accounts for that
      assert(c=='N'); //see? we're looking at an N!
      prevSubSeqId=reader.getSubSeqId();
      prevSubSeqName=reader.getSubSeqName();
    }

    if(bit==0){
      assert(wordsStored<word_count);
      _words[wordsStored++]=w;
      bit=WORDSIZE;
      w=0;
    }
  }


  if(inReadableRegion){
    readableRegion.second=_length-1;
    readableRegions.push_back(readableRegion);
  }
  if(inUnmaskedRegion){
    unmaskedRegion.second=_length-1;
    unmaskedRegions.push_back(unmaskedRegion);
  }

  //finish up last subseq
  assert(wordsStored<word_count);
  if(bit<WORDSIZE)
    _words[wordsStored++]=w;
  subSeqBounds.second=_length-1;
  assert(subSeqBounds.second>=subSeqBounds.first);
  subSeqs.push_back(subSeqBounds);
  subSeqNames.push_back(prevSubSeqName);
  subSeqNamesP.push_back(subSeqNames.back().c_str());

  regions[0]=&(subSeqs.front());
  regions[1]=&(readableRegions.front());
  regions[2]=&(unmaskedRegions.front());
  regionSizes[0]=subSeqs.size();
  regionSizes[1]=readableRegions.size();
  regionSizes[2]=unmaskedRegions.size();
}

word* SequenceBinary::allocWords(){
  if(!word_count)
    return NULL;
  //  cerr << "Allocing "<<word_count<<" words"<<endl;
  seqdataMalloc=1;
  return new word[word_count];
}

SequenceBinary::~SequenceBinary(){
  if(fdmem)
    if(munmap(fdmem,fdmem_len))
      warn("Problem unmapping mmap'd region.");
  if(fd)
    close(fd);
  if(seqdataMalloc)
    delete[] _words;
}

word SequenceBinary::rawBaseBits(size_t n){
  assert(n<_length);
  size_t widx=n*2/WORDSIZE;
  int offset=(n*2)%WORDSIZE;
  assert(widx<word_count);
  word w=_words[widx];
  word r=(w>>(WORDSIZE-2-offset) & 3);
  return r;
}

char SequenceBinary::bitToBase(word w){
  switch(w & 3){
  case 0: return 'A';
  case 1: return 'C';
  case 2: return 'G';
  case 3: return 'T';
  }
  assert("Oh noes. n & 3 > 3???");
  throw SeqBinException("n & 3 > 3. Check your CPU's math.");
}

char SequenceBinary::rawBase(size_t n){
  return bitToBase(rawBaseBits(n));
}

ostream& SequenceBinary::asFasta(ostream &os,int nmaskR){
  Region *ri[3],*rEnd[3];
  vector<const char*>::const_iterator ni=subSeqNamesP.begin();
  for(int i=0;i<3;i++){
    ri[i]=regions[i];
    rEnd[i]=regions[i]+regionSizes[i];
  }
  
  const word lineWrap=75;
  word wrapCount=0;
  for(;ri[0]!=rEnd[0];++ri[0],++ni){
    if(ri[0]!=regions[0]) //not the very first one?
      os << endl; //then we need a linebreak
    assert(ni!=subSeqNamesP.end());
    os << ">"<<*ni<<endl;
    wrapCount=0;
    for(size_t n=ri[0]->first;n<=ri[0]->second;n++){
      //advance regions if necessary
      for(int i=1;i<3;i++)
	while(ri[i]!=rEnd[i] && n>ri[i]->second)
	  ri[i]++;
      char c='?';

      assert(ri[0]!=rEnd[0] && n>=ri[0]->first && n<=ri[0]->second);
      assert(ri[nmaskR]==rEnd[nmaskR] || n<=ri[nmaskR]->second);
      if(ri[nmaskR]==rEnd[nmaskR] || n<ri[nmaskR]->first)
	c='N';
      else if(nmaskR<2){ //show repeatmask bases, 
	c=rawBase(n); 
	assert(ri[2]==rEnd[2] || n<=ri[2]->second);
	if(ri[2]==rEnd[2] || n<ri[2]->first)
	  c-='A'-'a';
      }
      if(wrapCount>lineWrap){
	os << endl;
	wrapCount=0;
      }
      os << c;
      wrapCount++;
    }
  }
  os << endl;
  return os;
}

