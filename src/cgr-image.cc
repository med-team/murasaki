/*
Murasaki - multiple genome global alignment program
Copyright (C) 2006-2008 Keio University
(Kris Popendorf) <comp@bio.keio.ac.jp> (2006)

This file is part of Murasaki.

Murasaki is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Murasaki is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Murasaki.  If not, see <http://www.gnu.org/licenses/>.
*/

//////////////////////////
// krisp - CGR image builder. from raw sequence
//////////////////////

#include <iostream>
#include <memory.h>
#include <getopt.h>
#include <vector>
#include "dinkymath.h"
#include "cgr-image.h"
#include "cgr.h"
#include "seqread.h"

using namespace std;

int maxiter=8,startiter=1;
pix junk; //used as a throw-away pixel for N-containing chunks
int repeatmask=0,interskip=0;
bool domerge=false;
extern unsigned long seedcount;
bool binary=true;
extern bool fullWidthBinary;

int main(int argc,char** argv){
  int optc;
  while(1){
    //options struct:
    // name, has_arg, store_pointer, return_value
    static struct option long_options[] = {
      {"help",0,0,'?'},
      {"version",0,0,'v'},
      {"maxiter",1,0,'m'},
      {"start",1,0,'s'},
      {"merge",0,0,'b'},
      {"interskip",0,0,'i'},
      {"repeatmask",0,0,'r'},
      {"only",1,0,'1'},
      {"plain",0,0,'p'},
      {"8bit",0,0,'8'},
      {0,0,0,0}
    };
    int longindex=0;
    optc=getopt_long(argc,argv,"m:?pvris:1:b8",long_options,&longindex);
    if(optc==-1)break;
    switch(optc){
    case '1':
      interskip=1;
      if(!optarg || !sscanf(optarg,"%d",&maxiter)){
	cerr << "Could not parse resolution.\n";
	cerr << program_help();
	exit(-1);
      }
      maxiter=max<int>(1,maxiter);
      startiter=maxiter;
      startiter=max<int>(1,startiter);
      cout << "Making only 1 CGR-resolution:"<<maxiter<<endl;
      break;
    case '8': fullWidthBinary=false;break;
    case 'i':
      interskip=1;break;
    case 'b':
      domerge=true;break;
    case 'r':
      repeatmask=1;break;
    case 'm':
      if(!optarg || !sscanf(optarg,"%d",&maxiter)){
	cerr << "Could not parse resolution.\n";
	cerr << program_help();
	exit(-1);
      }
      maxiter=max<int>(1,maxiter);
      cout << "Set maxiter to "<<maxiter<<endl;
      break;
    case 's':
      if(!optarg || !sscanf(optarg,"%d",&startiter)){
	cerr << "Could not parse resolution.\n";
	cerr << program_help();
	exit(-1);
      }
      startiter=max<int>(1,startiter);
      cout << "Set maxiter to "<<startiter<<endl;
      break;
    case 'p': binary=0;break;
    case 'v': cout << program_version();exit(-1);break;
    default: cout << program_help();exit(1);break;
    }
  }

  for(int seqi=optind;seqi<argc;seqi++){
    string inname(argv[seqi]);
    string outname(inname+".cgr");
    string cmd("./geneparse.pl");
    vector<string> argv(1);
    if(repeatmask)
      argv.push_back(string("-r"));
    argv.push_back(string("-c"));
    argv.push_back(inname);

    cout << "Loading "<<inname<<endl;
    string fwd;
    SequenceReader reader;
    try {
      reader.readSeqInto(fwd,inname);
    }catch(SeqReadException e){
      cerr << "SeqRead failed: "<<e.reason()<<endl;
      exit(-2);
    }
    if(fwd.length()<=0){
      cerr << "No data found in file. Aborting.\n";
      exit(-2);
    }
    string rev(fwd);
    cout << "Done. Generating reverse complement"<<endl;
    reverse(rev.begin(),rev.end());
    invert(rev);
    cout << "Done."<<endl;

    cout << "Looping for "<<(1+maxiter-startiter)<<" iterations..."<<endl;
        
    Cgr *cgr=new Cgr(startiter); //aww, cute little feller
    Cgr *old=0;
    seedcount=fwd.length()*2;
    while(cgr->iter<=maxiter){
      cgr->load(fwd.data(),fwd.length());
      cgr->load(rev.data(),rev.length());
      if(old){
	if(domerge)
	  cgr->merge(*old);
	delete old;
      }
      if(!interskip)
	cgr->savePGM(outname+"."+dstring(cgr->iter),binary);
      if(cgr->iter<maxiter){ //it can grow!
	old=cgr;
	cgr=new Cgr(*old);
	delete old;
	old=0;
      }else
      if(cgr->iter==maxiter)
	break;
    }
    cgr->savePGM(outname,binary);
    if(old)delete old;
    if(cgr)delete cgr;
  }
  cout << "All done!\n";
}

string program_help(){
  return string("\
Usage: cgr-image [options] <input-sequence1> [<input-sequence2> ...]\n\
\n\
Options\n\
*Takes an option (like --maxres 3 or -m3)\n\
  --maxres|-m  = specify max number of subsamples\n\
  --startiter|-s = start from iteration n\n\
  --only|-1 = only do 1 iteration\n\
\n\
*Toggles: (just --merge or -b)\n\
  --merge|-b = merge between layers\n\
  --interskip|-i = skip intermediate output\n\
  --repeatmask|-r = mask repeats (ie: lowercase letters)\n\
  --plain|-p = save as plain PGMs (not binary)\n\
  --8bit|-8 = use 8bit binary PGMs (some programs (like gimp) can't use 16bit)\n\
");
}

string program_version(){
  return string("0.1");
}
