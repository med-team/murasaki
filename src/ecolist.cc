/*
Copyright (C) 2006-2008 Keio University
(Kris Popendorf) <comp@bio.keio.ac.jp> (2006)

This file is part of Murasaki.

Murasaki is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Murasaki is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Murasaki.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "ecolist.h"
#include "exceptions.h"
#include "dinkymath.h"
#include <assert.h>
#include <algorithm>

//a "chunk" is a "location seqbit/idxbit combo"
//a "block" is word (in words) that is used to store either chunks (which may straddle up to 2 blocks)
//blocks are allocated in "blobs" of initBlocks<<ite blocks
//each blob holds minChunk<<ite chunks, and finally pointer to the next block
bool Ecolist::inited=false;
SeqPos Ecolist::minBlocks,Ecolist::minChunks;
unsigned Ecolist::seqbits,Ecolist::idxbits,Ecolist::chunkSize;
word Ecolist::idxLongest;
SeqPos Ecolist::initBlocks;
SeqPos Ecolist::externalSortLimit=1<<28; //this is about 2.68435e8
bool (*Ecolist::lessthan)(const pair<SeqPos,SeqPos>&,const pair<SeqPos,SeqPos>&)=0;

Ecolist::Ecolist() :
  size(0),
  blocks(NULL)
{
  assert(inited);
}

void Ecolist::clear(){
  eraseAll(blocks,0);
  size=0;
}

ostream& operator<<(ostream& os,Ecolist& a){
  return os << "(Ecolist: "<<a.getSize()<<"/"<<a.capacity()<<")";
}

void Ecolist::push(SeqPos seq,SeqPos idx){
  SeqPos loc=size;
  block* root=findBlockFor(loc);
  size++; //we've _now_ grown
  writeBits(root,loc,0,seqbits,seq);
  writeBits(root,loc,seqbits,idxbits,idx+idxLongest);
}

std::pair<SeqPos,SeqPos> Ecolist::getValAt(SeqPos loc){
  assert(loc<size);
  block* root=findBlockFor(loc);
  SeqPos seq=readBits(root,loc,0,seqbits);
  SeqPos idx=readBits(root,loc,seqbits,idxbits)-idxLongest;
  return std::pair<SeqPos,SeqPos>(seq,idx);
}

SeqPos Ecolist::capacity(){
  SeqPos cap=0;
  SeqPos ite=0;
  for(block *root=blocks;root;root=root[initBlocks<<ite++].p)
    cap+=minChunks<<ite;
  return cap;
}

Ecolist::iterator Ecolist::begin(){
  return iterator(blocks,0,0);
}

Ecolist::iterator Ecolist::end(){
  SeqPos ite=0;
  block *root;
  SeqPos pos=size;
  for(root=blocks;root;){
    block *tmp=root[initBlocks<<ite].p;
    if(tmp){
      pos-=minChunks<<ite++;
      root=tmp;
    }
    else
      break;
  }
  return iterator(root,pos,ite);
}

Ecolist::iterator Ecolist::at(SeqPos remain){
  if(remain>=size)
    return end();
  SeqPos ite=0;
  block* root=blocks;
  while(remain>=minChunks<<ite){
    assert(root[initBlocks<<ite].p);
    root=root[initBlocks<<ite].p;
    remain-=minChunks<<ite;
    ite++;
  }
  return iterator(root,remain,ite);
}

size_t operator-(const Ecolist::iterator &a,const Ecolist::iterator &b){
  return a.to_i()-b.to_i();
}

//bidirectional versions
Ecolist::bi_iterator Ecolist::bi_begin(){
  return bi_iterator(blocks,list<block*>(),0,0);
}

Ecolist::bi_iterator Ecolist::bi_end(){
  SeqPos ite=0;
  list<block*> prev;
  block *root;
  SeqPos pos=size;
  for(root=blocks;root;){
    block *tmp=root[initBlocks<<ite].p;
    if(tmp){
      prev.push_back(root);
      pos-=minChunks<<ite++;
    }else
      break;
    root=tmp;
  }
  return bi_iterator(root,prev,pos,ite);
}

Ecolist::bi_iterator Ecolist::bi_at(SeqPos remain){
  if(remain>=size)
    return bi_end();
  SeqPos ite=0;
  list<block*> prev;
  block* root=blocks;
  while(remain>=minChunks<<ite){
    prev.push_back(root);
    assert(root[initBlocks<<ite].p);
    root=root[initBlocks<<ite].p;
    remain-=minChunks<<ite;
    ite++;
  }
  return bi_iterator(root,prev,remain,ite);
}

size_t operator-(const Ecolist::bi_iterator &a,const Ecolist::bi_iterator &b){
  return a.to_i()-b.to_i();
}

//static mojo
void Ecolist::init(SeqPos seqs,word longest,word total,SeqPos weight,bool (*_lt)(const val_type&,const val_type&),bool frugal)
{
  lessthan=_lt;
  idxLongest=longest;
  seqbits=ceil_log2_loop(seqs);
  idxbits=ceil_log2_loop((longest+1)*2);
  chunkSize=seqbits+idxbits;
  if(chunkSize>WORDSIZE)
    throw MurasakiException("Can't deal with chunks larger than wordsize. Please use an environment with bigger words, or reduce input size, or use a different hash table");

  //now we have about allocation choice. We can vow to never waste a single bit (we'll call this "frugal") or
  //we can make the most out of a block while assuming that each list has zero entries (we'll call this "optimistic")
  SeqPos expected=ceil_log2_loop(total)-weight;
  if(frugal){
    minBlocks=lcm(ceil_log2_loop(seqs)+ceil_log2_loop((longest+1)*2),WORDSIZE)/WORDSIZE;
    for(initBlocks=minBlocks;initBlocks<expected;initBlocks+=minBlocks);
  }else{
    minBlocks=1;
    initBlocks=minBlocks;
  }
  minChunks=WORDSIZE*initBlocks/(seqbits+idxbits);
  string header(frugal ? "Ecolist (frugal): ":"Ecolist (optimistic): ");
  std::cout << header << "(seqbits: "<<seqbits<<" idxbits: "<<idxbits<<" minBlocks: "<<minBlocks<<" initblocks: "<<initBlocks<<")"<<endl
	    << header << "Expcted entries per list: "<<expected<<endl
	    << header << "Minimum chunks per list: "<<minChunks<<endl;
  inited=true;
}

size_t Ecolist::cost(size_t chunks){
  assert(inited);
  size_t cost=sizeof(Ecolist);
  if(chunks<1)
    return cost;
  SeqPos ite=0;
  do{
    cost+=sizeof(block)*((initBlocks<<ite)+1);
    if(chunks>=(size_t)(minChunks<<ite)) //don't want this to go negative as we don't have signed chunks
      chunks-=(size_t)(minChunks<<ite);
    ite++;
  }while(chunks>=(size_t)(minChunks<<ite));
  return cost;
}

//protected mojo
Ecolist::~Ecolist(){
  eraseAll(blocks,0);
}

void Ecolist::writeBits(block* root,SeqPos loc,SeqPos offset,SeqPos size,SeqPos val){
  SeqPos bitloc=loc*chunkSize+offset;
  SeqPos wloc=bitloc/WORDSIZE;
  offset=MODWORDSIZE(bitloc); //offset now means "location inside word to start writing from"
  word mask=lowN(size)<<offset;
  root[wloc].w=(root[wloc].w & (~mask)) | ((word)(val & lowN(size))<<offset);
  if(offset && MODWORDSIZE(bitloc+size-1)<MODWORDSIZE(bitloc-1)){ //overlaps SeqPoso next word
    SeqPos valoff=WORDSIZE-offset; //first bit of val not yet written
    size-=valoff; //size is now the remainder. had better fit in 1 word!
    assert(size>0);
    assert(size<WORDSIZE);
    bitloc+=valoff;
    assert(MODWORDSIZE(bitloc)==0); //bitloc should now be exactly at the word-boundary
    assert(bitloc/WORDSIZE==wloc+1); //and that word should be the one right after wloc
    wloc++;
    //offset=0; //we can simplify the operation just by leaving this out
    mask=lowN(size);
    root[wloc].w=(root[wloc].w & (~mask)) | ((word)(val & (mask<<valoff))>>valoff);
  }
}

SeqPos Ecolist::readBits(block* root,SeqPos loc,SeqPos offset,SeqPos size){
  SeqPos bitloc=loc*chunkSize+offset;
  SeqPos wloc=bitloc/WORDSIZE;
  offset=MODWORDSIZE(bitloc); //offset now means "location inside word to start reading from"
  word mask=lowN(size)<<offset;
  word res=(root[wloc].w & mask)>>offset;
  if(offset && MODWORDSIZE(bitloc+size-1)<MODWORDSIZE(bitloc-1)){ //overlaps into next word
    SeqPos valoff=WORDSIZE-offset; //first bit of val not yet written
    size-=valoff; //size is now the remainder. had better fit in 1 word!
    assert(size>0);
    assert(size<WORDSIZE);
    bitloc+=valoff;
    assert(MODWORDSIZE(bitloc)==0); //bitloc should now be exactly at the word-boundary
    assert(bitloc/WORDSIZE==wloc+1); //and that word should be the one right after wloc
    wloc++;
    //offset=0; //we can simplify the operation just by leaving this out
    mask=lowN(size);
    res=res | (word)(root[wloc].w & mask)<<valoff;
  }
  return (SeqPos)res;
}

void Ecolist::eraseAll(block* root,SeqPos ite){
  block* next=root[initBlocks<<ite].p;
  delete root;
  if(next)
    eraseAll(next,ite+1);
}

block* Ecolist::findBlockFor(SeqPos &remain){
  SeqPos ite=0;
  if(!blocks){ //first access on a clean list
    assert(!size);
    blocks=allocBlob(0);
  }
  block* root=blocks;
  while(remain>=minChunks<<ite){
    //get next root (create if necessary), and dec remain
    block*& next=root[initBlocks<<ite].p;
    if(!next)
      next=allocBlob(ite+1);
    root=next;
    remain-=minChunks<<ite;
    ite++;
  }
  return root;
}

block* Ecolist::allocBlob(SeqPos ite){
  //  std::cerr << "Allocing new blob for level "<<ite<<endl;
  SeqPos last=initBlocks<<ite;
  block* n=new block[last+1];
  n[last].p=0;
  return n;
}

//iterator mojo
Ecolist::EcolistIterator::EcolistIterator(block *_start,SeqPos _p,SeqPos _ite) :
  current(_start),pos(_p),ite(_ite),end(Ecolist::minChunks<<_ite)
{ }

Ecolist::EcolistIterator::EcolistIterator(const Ecolist::bi_iterator &a) :
  current(a.current),pos(a.pos),ite(a.ite),end(a.end)
{ }

std::pair<SeqPos,SeqPos> Ecolist::EcolistIterator::operator*() const {
  SeqPos seq=readBits(current,pos,0,Ecolist::seqbits);
  SeqPos idx=readBits(current,pos,Ecolist::seqbits,Ecolist::idxbits)-Ecolist::idxLongest;
  return std::pair<SeqPos,SeqPos>(seq,idx);
}

std::pair<SeqPos,SeqPos> Ecolist::EcolistIterator::get() const {
  SeqPos seq=readBits(current,pos,0,Ecolist::seqbits);
  SeqPos idx=readBits(current,pos,Ecolist::seqbits,Ecolist::idxbits)-Ecolist::idxLongest;
  return std::pair<SeqPos,SeqPos>(seq,idx);
}

Ecolist::EcolistIterator::_Self& Ecolist::EcolistIterator::operator++(){
  pos++;
  if(pos==end && current[initBlocks<<ite].p){ //roll into next block
    assert(current[initBlocks<<ite].p); //better not be null!
    current=current[initBlocks<<ite].p;
    pos=0;
    end=Ecolist::minChunks<<++ite;
  }
  return *this;
}

Ecolist::EcolistIterator::_Self Ecolist::EcolistIterator::operator++(int a){
  _Self __tmp=*this;
  pos++;
  if(pos==end && current[initBlocks<<ite].p){ //roll into next block
    assert(current[initBlocks<<ite].p); //better not be null!
    current=current[initBlocks<<ite].p;
    pos=0;
    end=Ecolist::minChunks<<++ite;
  }
  return __tmp;
}

bool Ecolist::EcolistIterator::operator!=(const _Self& a) const {
  return current!=a.current || pos!=a.pos;
}

bool Ecolist::EcolistIterator::operator==(const _Self& a) const {
  return current==a.current && pos==a.pos;
}

void Ecolist::EcolistIterator::put(SeqPos seq,SeqPos idx) const {
  writeBits(current,pos,0,Ecolist::seqbits,seq);
  writeBits(current,pos,Ecolist::seqbits,Ecolist::idxbits,idx+Ecolist::idxLongest);
}

void Ecolist::EcolistIterator::put(const pair<SeqPos,SeqPos> &v) const {
  writeBits(current,pos,0,Ecolist::seqbits,v.first);
  writeBits(current,pos,Ecolist::seqbits,Ecolist::idxbits,v.second+Ecolist::idxLongest);
}

//bidirectional-mojo
Ecolist::BidirectionalEcolistIterator::BidirectionalEcolistIterator(block *_start,list<block*> _prev,SeqPos _p,SeqPos _ite) :
  current(_start),prev(_prev),pos(_p),ite(_ite),end(Ecolist::minChunks<<_ite)
{ }

std::pair<SeqPos,SeqPos> Ecolist::BidirectionalEcolistIterator::operator*() const {
  SeqPos seq=readBits(current,pos,0,Ecolist::seqbits);
  SeqPos idx=readBits(current,pos,Ecolist::seqbits,Ecolist::idxbits)-Ecolist::idxLongest;
  return std::pair<SeqPos,SeqPos>(seq,idx);
}

std::pair<SeqPos,SeqPos> Ecolist::BidirectionalEcolistIterator::get() const {
  SeqPos seq=readBits(current,pos,0,Ecolist::seqbits);
  SeqPos idx=readBits(current,pos,Ecolist::seqbits,Ecolist::idxbits)-Ecolist::idxLongest;
  return std::pair<SeqPos,SeqPos>(seq,idx);
}

Ecolist::BidirectionalEcolistIterator::_Self& Ecolist::BidirectionalEcolistIterator::operator++(){
  pos++;
  if(pos==end && current[initBlocks<<ite].p){ //roll into next block
    assert(current[initBlocks<<ite].p); //better not be null!
    prev.push_back(current);
    current=current[initBlocks<<ite].p;
    pos=0;
    end=Ecolist::minChunks<<++ite;
  }
  return *this;
}

Ecolist::BidirectionalEcolistIterator::_Self Ecolist::BidirectionalEcolistIterator::operator++(int a){
  _Self __tmp=*this;
  pos++;
  if(pos==end && current[initBlocks<<ite].p){ //roll into next block
    assert(current[initBlocks<<ite].p); //better not be null!
    prev.push_back(current);
    current=current[initBlocks<<ite].p;
    pos=0;
    end=Ecolist::minChunks<<++ite;
  }
  return __tmp;
}

Ecolist::BidirectionalEcolistIterator::_Self& Ecolist::BidirectionalEcolistIterator::operator--(){
  if(pos==0){ //roll into prev block
    assert(!prev.empty()); //better not be null!
    current=prev.back();
    prev.pop_back();
    pos=end=Ecolist::minChunks<<--ite;
  }
  pos--;
  return *this;
}

Ecolist::BidirectionalEcolistIterator::_Self Ecolist::BidirectionalEcolistIterator::operator--(int a){
  _Self __tmp=*this;
  if(pos==0){ //roll into prev block
    assert(!prev.empty()); //better not be null!
    current=prev.back();
    prev.pop_back();
    pos=end=Ecolist::minChunks<<--ite;
  }
  pos--;
  return __tmp;
}

bool Ecolist::BidirectionalEcolistIterator::operator!=(const _Self& a) const {
  return current!=a.current || pos!=a.pos;
}

bool Ecolist::BidirectionalEcolistIterator::operator==(const _Self& a) const {
  return current==a.current && pos==a.pos;
}

void Ecolist::BidirectionalEcolistIterator::put(SeqPos seq,SeqPos idx) const {
  writeBits(current,pos,0,Ecolist::seqbits,seq);
  writeBits(current,pos,Ecolist::seqbits,Ecolist::idxbits,idx+Ecolist::idxLongest);
}

void Ecolist::BidirectionalEcolistIterator::put(const pair<SeqPos,SeqPos> &v) const {
  writeBits(current,pos,0,Ecolist::seqbits,v.first);
  writeBits(current,pos,Ecolist::seqbits,Ecolist::idxbits,v.second+Ecolist::idxLongest);
}

//sort mojo
void Ecolist::inPlaceSort(){
  assert(lessthan);
  qsort(0,size);
}

SeqPos Ecolist::partition(SeqPos left,SeqPos right,SeqPos pivoti){
  iterator pi(at(pivoti)),li(at(left)),ri(at(right));
  val_type pv=*pi;
  swap(pi,ri);
  iterator store(li);SeqPos si=left; //using a store lets us get away with forward-only iterators
  for(iterator i(li),stop(ri);i!=stop;++i){
    if(lessthan(*i,pv)){
      swap(store,i);
      ++store;++si;
    }
  }
  swap(ri,store);
  return si;
}

void Ecolist::qsort(SeqPos left,SeqPos right){
  if(right<=left)
    return;
  //left edge. theoretically no reason to believe that middle would be better. (unless it's already been sorted. then we want middle) as a (slight) bonus left edge is guaranteed to be the fastest one to look up.
  //  SeqPos pivoti=left; //Actually, using the seedfilter, we might get a mostly ordered list, which would make us sad, so we'll take middle (which would be optimal in that case anyway)
  SeqPos pivoti=(left+right)/2;
  pivoti=partition(left,right,pivoti);
  qsort(left,pivoti-1);
  qsort(pivoti+1,right);
}

void Ecolist::swap(iterator &a,iterator &b){
  val_type temp=*a;
  a.put(*b);
  b.put(temp);
}

struct less_map : public binary_function<const Ecolist::val_type&,const Ecolist::val_type&,bool> {
  bool operator()(const Ecolist::val_type& a,const Ecolist::val_type& b) { return Ecolist::lessthan(a,b); }
};

void Ecolist::externalSort(){
  vector<val_type> v;
  v.reserve(size);
  iterator ite(begin()),stop(end());
  for(;ite!=stop;++ite)
    v.push_back(*ite);
  std::sort(v.begin(),v.end(),less_map());
  ite=begin();
  for(SeqPos i=0;i<size;i++,ite++)
    ite.put(v[i]);
}

SeqPos Ecolist::int_lower_bound(const val_type &m){ //first elem not <m
  SeqPos low=0,high=size;
  SeqPos len=high-low;
  while(len>0){
    SeqPos half=len/2;
    SeqPos mid=low+half;
    high=low+len;
    val_type here=getValAt(mid);
    if(lessthan(here,m)){
      low=mid+1;
      len=len-half-1;
    }
    else
      len=half;
  }
  return low;
}

SeqPos Ecolist::int_upper_bound(const val_type &m){ //first elem not <m
  SeqPos low=0,high=size;
  SeqPos len=high-low;
  while(len>0){
    SeqPos half=len/2;
    SeqPos mid=low+half;
    high=low+len;
    val_type here=getValAt(mid);
    if(lessthan(m,here))
      len=half;
    else{
      low=mid+1;
      len=len-half-1;
    }
  }
  return low;
}
