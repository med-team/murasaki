#!/usr/bin/perl

#Copyright (C) 2006-2008 Keio University
#(Kris Popendorf) <comp@bio.keio.ac.jp> (2006)
#
#This file is part of Murasaki.
#
#Murasaki is free software: you can redistribute it and/or modify
#it under the terms of the GNU General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.
#
#Murasaki is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU General Public License for more details.
#
#You should have received a copy of the GNU General Public License
#along with Murasaki.  If not, see <http://www.gnu.org/licenses/>.


use File::Basename;
use Pod::Usage;
use Sys::Hostname;
use Cwd;

use strict;
BEGIN {
  unshift(@INC,(fileparse($0))[1].'perlmodules');
}
use Murasaki;

our %mca;
our $mem=$ENV{QLOB_MEM} ? $ENV{QLOB_MEM}:"4gb";

our $dir="output";
our $localroot=cwd;
chomp $localroot;

if($#ARGV<0 or grep(/--help/,@ARGV)){
 pod2usage(1);
  exit;
}

our ($name,$target,$local,$kogfile,$mpinodes,$myrinet,$username,$cm);
our $ppn=$ENV{QLOB_PPN} ? $ENV{QLOB_PPN}:4;

my @seqs=grep(/^[^-]/,@ARGV);
@ARGV=map {$_="-p11111100100001111100000100101111" if $_ eq "-P";
	 $_} @ARGV;

@ARGV=grep {my $res=0;
	    if(m/^(-n|--name=)(.*)/){
	      $username=$2;
	      $res=1;
	    }

	    if(m/^(--ppn=)(\d+)/){
 	      $ppn=$2;
	      $res=1;
	    }

	    if(m/^(--cm)/){
 	      $cm=1;
	      $myrinet=1;
	      $res=1;
	    }

	    if(m/^(--target=)(.*)/){
 	      $target=$2;
	      $res=1;
	    }
	    if(m/^(-m|--memo?r?y?=)(.*)/){
	      $mem=$2;
	      $res=1;
	    }
	    if(m/^--?l(ocal)?$/){
	      $local=1;
	      $res=1;
	    }
	    if(m/^(--kogfile=)(.*)/){
	      $kogfile=$2;
	      $res=1;
	    }
	    if(m/^(--mpi(?:nodes)?=)(\d+)/){
	      $mpinodes=$2;
	      $res=1;
	    }
	    if(m/^(--myrinet)/){
	      $myrinet=1;
	      $res=1;
	    }
	    if(m/^(--dir=)(.*)/){
	      $dir=$2;
	      $res=1;
	    }
	    !$res
	  } @ARGV;

my $runstring=join(" ",@ARGV);

print "Got Sequences: ".join(",",@seqs)."\n";
if(!$username){
    $name=join("-",getName(@seqs));
    $name=quotemeta($name);
    $name=~s!\\[^\\\-/]!_!g;
    $name=~s!\\-!-!g;
    foreach my $seq (@seqs){
	die "sequence file not found: $seq" unless -e $seq;
    }
    my $basename=$name;
    my $round;
    while(-e "$dir/$name.job"){
	$round++;
	$name=$basename."_$round";
    }
}else{
    $name=$username;
}

print "Using name: $name\n";
my $prefix="$dir/$name";
print "Storing output to: $prefix\n";
die "No name?" unless $name;
#print "Murasaki Args: $runstring";

my %requirements;
my @cmds;
push(@cmds,$ENV{QLOB_PROLOG}) if $ENV{QLOB_PROLOG};
#$requirements{mem}=$mem;
$requirements{vmem}=$mem;
#$requirements{pmem}=$mem;
#$requirements{nodes}="1:bigmem";

$requirements{nodes}="$target" if $target;
$requirements{nodes}=join(":",($mpinodes ? ($mpinodes,"ppn=$ppn"):($target ? $target:())),($myrinet ? "myrinet":()));
if($ENV{QLOB_BADMX}){
  $mca{btl}='^mx';
}else{
  $mca{pml}='cm' if($myrinet and $cm);
  if($ENV{MX_CSUM}){ #use debugging library
    my $debuglib='/usr/local/lib64/debug';
    $debuglib=$ENV{QLOB_DEBUG_LD_LIBRARY_PATH} if $ENV{QLOB_DEBUG_LD_LIBRARY_PATH};
    unshift(@cmds,
	    'export LD_LIBRARY_PATH='.$debuglib.':$LD_LIBRARY_PATH',
	    "export MX_CSUM=$ENV{MX_CSUM}"
	   );
  }
}
my $reqs=join(",",map {"$_=$requirements{$_}"} (grep {$requirements{$_}} keys(%requirements)));
my $mpiopts=join(" ",map {"-mca $_ $mca{$_}"} keys %mca);

my $qsub_args=qq!-d $localroot -mae -N "murasaki:$name" -e $prefix.stderr -o $prefix.stdout -l $reqs!;

open(JOB,">$prefix.job");
print JOB "#!/bin/sh\n\n";
if(`which qsub` and !$local){
  open(QSUB,"|qsub $qsub_args");
#  print JOB "qsub $qsub_args\n";
  print "Launching via qsub\n";# $qsub_args\n";
  print QSUB "echo \$HOSTNAME > $prefix.host\n"; #quite annoying not to know that
}else{
  open(QSUB,">-");
  $local=1;
}

my $perl="";
if($local){
#  chdir($root);
  $perl=`which perl`;
  chomp $perl;
}
foreach my $cmd ("set -e",
		 @cmds,
		 "time ".($mpinodes ? "mpirun $mpiopts ":"")."$root/murasaki -n$name $runstring | tee $prefix.murasaki",
		 "time $perl $root/simplegraph.pl $prefix.anchors",
		 "time $perl $root/histoplot.pl $prefix.histogram",
		 "time $perl $root/histoplot.pl $prefix.histogram.details",
		 ($kogfile ? ("time $perl $root/filter.pl --kogfile $kogfile $prefix.anchors"):()),
		){
  my ($file)=$cmd=~m/(\S*)$/;
  do {print "Necessary file not found for histogram graph.\n"; next;} if $local and $file=~m/histogram/ and !($file=~m/murasaki$/) and !-e "$file";
  $cmd=~s/^time // if $local; #local does not like timing perl programs
  print QSUB "$cmd\n";
  print JOB "$cmd\n";
  print "Local run. Exit result: ".system($cmd)."\n" if $local;
}

close QSUB;
close JOB;

sub getName {
  my @ret=map {
#    m!.*?/?([^./]+)\..*?! or print "Eek. Couldn't extract name from $_\n";
# $1
    my ($name,$path,$suffix) = fileparse($_,qr{\.[^.]*(?:\.gz)?});
    $name
    } @_;
  return @ret if $#_;
  return $ret[0];
}

__END__

=head1 NAME

qlob.pl: run quick mursaki compare

=head1 SYNOPSIS

qlob.pl [options] {genome list...}

qlob is a frontend to murasaki (through qsub if it's available).
It automatically generates an output name for you based on the input files,
sets a more reasonable memory limit, runs the job, and generates dot-plot
outputs.

In addition to the standard Murasaki options (see murasaki --help),
qlob supports some some options of its own:

 Options:
  -P           uses an old pattern of 11111100100001111100000100101111
  --target=<hostname>  requests the job be run on hostname
  -n|--name    name for the job (allows overwriting of old output)
  -m|--mem     specifies a memory requirement to pass to qsub (default 4gb)
  --mpi=N      use MPI across N machines (sets ppn=4 also)
  --myrinet    require myrinet nodes
  --kogfile=<file> Run filter.pl to score alignment using <file>

=head1 DESCRIPTION

B<This program> runs several sequences through murasaki.
When run on an hpc machine it submits the job via qsub.

=cut
