#!/usr/bin/perl

#Copyright (C) 2006-2008 Keio University
#(Kris Popendorf) <comp@bio.keio.ac.jp> (2006)
#
#This file is part of Murasaki.
#
#Murasaki is free software: you can redistribute it and/or modify
#it under the terms of the GNU General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.
#
#Murasaki is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU General Public License for more details.
#
#You should have received a copy of the GNU General Public License
#along with Murasaki.  If not, see <http://www.gnu.org/licenses/>.

###############
## convert Pattern Hunter alignment to Murasaki Anchors
# by krisp

use File::Basename;
use Getopt::Long;
use Pod::Usage;
#use Data::Dump qw{dump};

BEGIN {
  unshift(@INC,(fileparse($0))[1].'perlmodules');
}
use Murasaki;

use strict;
my ($help,$man,$align_type);
our $flexible=0;
our $signed=1;
my $useAlignment;
GetOptions('help|?' => \$help, man => \$man);
pod2usage(1) if $help or $#ARGV<0;
pod2usage(-exitstatus => 0, -verbose => 2) if $man;

foreach my $file (@ARGV){
  local $\="\n";

  my ($basename,$path,$suffix) = fileparse($file);
  die "Input file not found: $file" unless -e $file;

  open(my $infh,$file) or die "Couldn't open $file";

  my $prefix="$path$basename.aligned";
  my $outf="$prefix.anchors";

  open(my $otfh,">$outf") or die "Couldn't write to $outf";
  my ($state,$strand,$length,$qi,$si,$links,$in,$qinc,$starts,$startq,$qx,$sx);
  $state='findStrand';
  my $count=0;
  while(<$infh>){
    if($state eq 'findStrand'){
      next unless m!Strand = (\w+) / (\w+)!;
      $strand=$1;
      $qinc=($strand=~m/Minus/i ? -1:1);
      $state='readQuery';
      $in=0;
    }elsif($state eq 'readQuery'){
      chomp;
#      print "Query:$_" if $_=~m/\S/;
      if(m/Score =/i){ #ended a stanza
#	print "Hit the end!\n";
	if($in){#were we midway through an anchor?
#	    print "Making a final anchor: ".join(" ",$starts,$sx,'+',$startq,$qx,($strand=~m/Minus/i ? '-':'+'))."\n";
	    print $otfh join("\t",$startq,$qx,($strand=~m/Minus/i ? '-':'+'),$starts,$sx,'+');
	    $count++;
	}
	$state='findStrand';
	next;
      }
      next unless m!Query:\s+(\d+)\s+(\S+)!;
      my ($qdat,$sdat)=($2);
      $qi=$1;
      $links=<$infh>;
      $_=<$infh>;
      m!Sbjct:\s+(\d+)\s+(\S+)! or die "Wtf? $_";
      $si=$1;
      $sdat=$2;

#      print "Query $qi: $qdat\n",
#	    "Subjt $si: $sdat";

      #parse link bits
      chomp $links;
      $links=~s/^              //; #stupid white space based header
      my $inre=qr/[^\-]/;
      foreach my $i (0..(length($qdat)-1)){
	my ($sc,$lc,$qc)=map {substr($_,$i,1)} ($sdat,$links,$qdat);
	$qx=$qi+$i*$qinc if $qc=~m/$inre/;
	$sx=$si+$i if $sc=~m/$inre/;
	if($qc=~m/$inre/ and $sc=~m/$inre/){
	  if($in){
	    #nothin to see here...
	  }else{ # start a new anchor
	    $startq=$qx;
	    $starts=$sx;
	    $in=1;
	  }
	}else{
	  if($in){ #end of an anchor
	    my $stopq=$qx;
	    my $stops=$sx;
#	    print "Making an anchor midway ($i -> '$qc,$lc,$sc): ".join(" ",$starts,$stops,'+',$startq,$stopq,($strand=~m/Minus/i ? '-':'+'))."\n";
	    print $otfh join("\t",$startq,$stopq,($strand=~m/Minus/i ? '-':'+'),$starts,$stops,'+');
	    $count++;
	    $in=0;
	  }else{
	  } #move along...
	}
      }
    }else{
      die "Uh oh. Broken FSM!";
    }
  }
  print "Done with $outf ($count anchors)";
}

__END__

=head1 NAME

phAlignment2anchors - converts a PatternHunter alignment into Murasaki anchor format

=head1 SYNOPSIS

mauveAlignment2anchors <input1> [input2 ...]

=head1 OPTIONS

Nothing. This is a very simple program.
