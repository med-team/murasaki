#!/usr/bin/perl -w

#Copyright (C) 2006-2008 Keio University
#(Kris Popendorf) <comp@bio.keio.ac.jp> (2006)
#
#This file is part of Murasaki.
#
#Murasaki is free software: you can redistribute it and/or modify
#it under the terms of the GNU General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.
#
#Murasaki is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU General Public License for more details.
#
#You should have received a copy of the GNU General Public License
#along with Murasaki.  If not, see <http://www.gnu.org/licenses/>.

##################
## convert mauve alignments to Murasaki anchor files -- krisp
##################

use File::Basename;
use Getopt::Long;
use Pod::Usage;
#use Data::Dump qw{dump};

BEGIN {
  unshift(@INC,(fileparse($0))[1].'perlmodules');
}
use Murasaki;

use strict;
my ($help,$man,$align_type);
my $autoout=1;
our $flexible=0;
our $signed=1;
my $useAlignment;
GetOptions('help|?' => \$help, man => \$man, 'autoout!'=>\$autoout);
pod2usage(1) if $help or $#ARGV<0;
pod2usage(-exitstatus => 0, -verbose => 2) if $man;

my ($filename,$outfile)=@ARGV;

my ($basename,$path,$suffix) = fileparse($filename);
die "Input file not found: $filename" unless -e $filename;

$outfile="$path/$basename.anchors" if $autoout;
print "Outfile is $outfile\n" if $autoout;

if($outfile){
  open(OUTF,">$outfile");
} else {
  open(OUTF,">-");
}

my %mauvedat=%{loadMauveAlignment($filename)};
foreach my $lcb (@{$mauvedat{LCBs}}){
  print OUTF (join("\t",map {($_->{start},$_->{stop},($_->{start}<0 ? "-":"+"))} (@$lcb))."\n");
}

if($basename){ #we can also make the seqs file
  open(my $seqfh,">$basename.seqs");
  local $,="\n";
  local $\="\n";
  print $seqfh (map {$_->{seqFile}} @{$mauvedat{seqs}});
}

sub loadMauveAlignment {
  my $alignment=shift;
  open(MAUVE,"<$alignment");
  <MAUVE>=~m/FormatVersion\s+(\d+)/ or die "Not a mauve file: $alignment";
  my $version=$1;
  my @seqs=();
  do {print "This program is written for Mauve Format Version 4.\n
This file is version $version. Weird stuff may happen.\n"; $flexible=1;} if $version!=4;
  <MAUVE>=~m/SequenceCount\s+(\d+)/ or die "Unknown sequence count\n";
  my $seqCount=$1;
  while(<MAUVE>){
    next unless m/Sequence(\d+)File\s+(\S.*)/;
    my ($seqId,$seqFile)=($1,$2);
    $_=<MAUVE>;
    m/Sequence${seqId}Length\s+(\d+)/ or $flexible or die "Input file is weird: $_";
    my $seqLength=$1;
    $seqs[$seqId]={'seqId' => $seqId,'seqFile' => $seqFile,'seqLength'=>$seqLength, 'seqName' => getName($seqFile) };
    last if $seqId==$seqCount-1;
  }

  my @LCBs=();
  $_=<MAUVE>;
  m/IntervalCount\s(\d+)/ or $flexible or die "Interval Count line weird: $_";
  my $LCBCount=$1;
  while(<MAUVE>){
    m/Interval\s(\d+)/ or next;
    my $LCBId=$1;
    $_=<MAUVE>;
    chomp;
    my ($length,@start)=split(/\s+/);
    my @stop=map {$_+$length} @start;
#    print "Start is at     : ".join("\t",@start)."\n";
#    print "Updating stop to: ".join("\t",@stop)."\n";
    my @segs;
    while(<MAUVE>){
      chomp;
      last if $_ eq '';
      next if $_ eq 'GappedAlignment' or m/^[A-Z-]+$/; #skip gapped lines
      ($length,@segs)=split(/\s+/);
      @stop=map {$_+$length} @segs;
    }
    next if (grep {$_==0} @start)>0;
    my @LCB=map {
      { start => $start[$_], stop => $stop[$_], LCBId => $LCBId }} 0..$#stop;
    
    push(@LCBs,\@LCB);
  }
  return {'seqs' => \@seqs, 'LCBs' => \@LCBs};
}

sub getName {
  my @ret=map {
    my ($name,$path,$suffix) = fileparse($_, qr{\.[^.]*});
    $name
    } @_;
  return @ret if $#_;
  return $ret[0];
}

__END__

=head1 NAME

mauve2anchors - converts a Mauve alignment into Murasaki anchor format

=head1 SYNOPSIS

mauve2anchors <input> [output]

=head1 OPTIONS

--autoout -- automatically pick an output name (otherwise output goes to stdout)

=over 8

=back
