#!/usr/bin/perl

use strict;
use File::Basename;

my ($foo,$base)=fileparse($0);

my @files=@ARGV;
unless(@files){
  @files=<$base/*.pod>;
}

foreach my $file (@files){
  my ($name)=fileparse($file,qr/\.pod/);
  use Pod::Man;
  my $podman=Pod::Man->new();
  $podman->parse_from_file($file,"$name.1");
  use Pod::Text;
  my $podtext=Pod::Text->new();
  $podtext->parse_from_file($file,"$name.txt");
  use Pod::Html;
  pod2html("--infile=$file","--outfile=$name.html","--title=$name");
  foreach my $tmpfile (<pod2htm?.tmp>){unlink $tmpfile}
}
