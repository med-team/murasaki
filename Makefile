# Handy Makefile
# Kris Popendorf 2005/10/14
# Murasaki project

#one Makefile to rule them all...

all: 
	$(MAKE) -C src all

#one Makefile to bring them all...

clean:
	-rm -rf *.o *~ .*~ core
	$(MAKE) -C src clean

#and in the darkness...

depend:
	$(MAKE) -C src depend

#bind them