#!/usr/bin/perl

#Copyright (C) 2006-2008 Keio University
#(Kris Popendorf) <comp@bio.keio.ac.jp> (2006)
#
#This file is part of Murasaki.
#
#Murasaki is free software: you can redistribute it and/or modify
#it under the terms of the GNU General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.
#
#Murasaki is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU General Public License for more details.
#
#You should have received a copy of the GNU General Public License
#along with Murasaki.  If not, see <http://www.gnu.org/licenses/>.

use Getopt::Long;
use Pod::Usage;
use File::Basename;
#use Data::Dump qw {dump};

use strict;

BEGIN {
  unshift(@INC,(fileparse($0))[1].'perlmodules');
}

use Murasaki;
use Murasaki::KOG;

my ($man,$help,$makecds);
my (@kogmap,@seqnames);
my $opterr=
  GetOptions('help|?' => \$help, man => \$man,makecds=>\$makecds,
	     'kog=s%' => sub {pod2usage({-msg=>"Need values for --kog $_[1]=$_[2] (lhs and rhs!)"})
				unless(defined $_[1] and $_[2]);
			      pod2usage({-msg=>"Bad --kog $_[1]= (lhs)"})
				unless(defined $_[1] and $_[2] and $_[1]>=0);
			      pod2usage({-msg=>"Bad --kog $_[1]=$_[2] (rhs)"})
				unless(defined $_[1] and $_[2] and $_[1]>=0 and $_[2]);
			      my $newname=scalar(Murasaki::KOG->guessKogMember($_[2]));
			      $kogmap[$_[1]]=$newname ? $newname:$_[2]}
	    );
pod2usage(1) if $help;
pod2usage(-exitstatus => 0, -verbose => 2) if $man;

pod2usage({-verbose=>1,-exitval=>2,-message=>'Need some input file...'}) if $#ARGV<0;


my ($inf,$output)=@ARGV;
my ($name,$path,$suffix) = fileparse($inf, qr{\.[^.]*});
$output="$path/$name.kog" if !$output;

warn "Warning: going to overwrite $output" if -f $output;
open(our $outf,">$output") or die "Couldn't open $output for writing.";

makeNames("$path/$name.seqs");

print "Using these kognames: @kogmap\n";

open(my $infh,$inf) or die "Couldn't open $inf";
my $line=0;
while(<$infh>){
  chomp;
  my @dats=split(/\t/);
  print $outf "[?] $line Alignment-derived kog\n";
  foreach my $seqid (0..$#kogmap){
    my ($start,$stop,$strand)=@dats[map {$seqid*3+$_} (0..2)];
    ($start,$stop)=(0-$stop,0-$start) if $start<0;
    $strand=($strand eq '+' ? 1:-1);
    my $locii="$name:$seqid:$line";
    my $spec=$kogmap[$seqid];
    print $outf "  $spec:  $locii\n";
  }
  print $outf "\n";
}continue{$line++}

close $outf;

our $root;
if($makecds){
  my $seqf=repSuffix($inf,".seqs");
  open(my $seqfh,$seqf);
  foreach my $file (<$seqfh>){
    chomp $file;
    print "Making CDS for $file...\n";
    system("$root/getcds.pl --redirect murasaki_synth=$inf $file");
  }
}

exit;

sub makeNames {
  my $seqdata=shift;
  open(my $seqfh,$seqdata) or die "Couldn't read sequence file";
  my $kogre=join("|",(Murasaki::KOG->knownKogs,Murasaki::KOG->knownCogs));
  my $i=0;
  my %aliases=Murasaki::KOG->commonAliases;
  foreach(<$seqfh>){
    chomp;
    push(@seqnames,getName($_));
    next if $kogmap[$i];
    my ($id)=m/($kogre)/;
    unless($id){ #try harder...
      foreach my $alias (keys(%aliases)){
	$id=$aliases{$alias} if (m/$alias/i);
      }
    }
    print "Identified $_ as KOG member $id\n" if $id;
    $id="a$i" unless $id;
    $kogmap[$i]=$id;
  }continue{$i++}
  @kogmap=@kogmap[0..($i-1)] unless scalar(@kogmap)==$i;
}

sub getName {
  my @ret=map {
    my ($name,$path,$suffix) = fileparse($_, qr{\.[^.]*});
    $name
  } @_;
  return @ret if $#_;
  return $ret[0];
}

__END__

=head1 NAME

align2kog.pl - converts an alignment into a kog file

=head1 SYNOPSIS

align2kog.pl [options] <input1> [output file]

(make sure to use the murasaki_synth redirect in getcds to generate the corresponding cds)

=head1 OPTIONS

[Output file] defaults to input.kog (for an input of input.anchors).

No options exist yet.
