#!/usr/bin/perl

#Copyright (C) 2006-2008 Keio University
#(Kris Popendorf) <comp@bio.keio.ac.jp> (2006)
#
#This file is part of Murasaki.
#
#Murasaki is free software: you can redistribute it and/or modify
#it under the terms of the GNU General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.
#
#Murasaki is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU General Public License for more details.
#
#You should have received a copy of the GNU General Public License
#along with Murasaki.  If not, see <http://www.gnu.org/licenses/>.

use File::Basename;
use Getopt::Long;
use Pod::Usage;

GetOptions('help|?' => \$help, man => \$man);
pod2usage(1) if $help or $#ARGV<0;
pod2usage(-exitstatus => 0, -verbose => 1) if $man;

foreach $file (@ARGV){
  my ($name,$path,$suffix) = fileparse($file, qr{\.[^.]*$});
  system("mkdir $name") unless -d $name;
  open(INF,"<$file");
  while(<INF>){
    if(m/^>(.+)/){
      $filename=$1;
      $filename.='.fa' unless $filename=~m/\.fa$/;
      close(OF) if OF;
      open(OF,">$name/$filename");
    }
    print OF $_ if OF;
  }
}

__END__

=head1 NAME

mfasplit.pl -- splits mfa (MultiFASTA) files into individual fa files

=head1 SYNOPSIS

mfasplit.pl <file1.mfa> [file2.mfa ...]

=head1 OPTIONS

=over 8


=back
