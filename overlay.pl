#!/usr/bin/perl -w

#Copyright (C) 2006-2008 Keio University
#(Kris Popendorf) <comp@bio.keio.ac.jp> (2006)
#
#This file is part of Murasaki.
#
#Murasaki is free software: you can redistribute it and/or modify
#it under the terms of the GNU General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.
#
#Murasaki is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU General Public License for more details.
#
#You should have received a copy of the GNU General Public License
#along with Murasaki.  If not, see <http://www.gnu.org/licenses/>.

use File::Basename;
use Getopt::Long;
use Pod::Usage;
#use Data::Dump qw{dump};
use POSIX qw{floor};

use strict;
my ($help,$man,$opt_prefix);

my ($opt_output);
my @validModes=qw{clear src dst src-over dst-over src-in dst-in src-out dst-out src-atop dst-atop xor plus multiply screen overlay darken lighten color-dodge color-burn hard-light soft-light difference exclusion change-mask};
my $mode="multiply";
GetOptions('help|?' => \$help, man => \$man, 'o|output=s'=>\$opt_output, 'mode=s'=>\$mode);
pod2usage(1) if $help or $#ARGV<0;
pod2usage(-exitstatus => 0, -verbose => 2) if $man or scalar(@ARGV)<2 or !grep {$mode ne $_} @validModes;

my @bad=grep {!-r $_} @ARGV;
die "File(s) not found: @bad" if @bad;

my (@paths,@names,@extensions);
foreach my $inf (@ARGV){
  my @dat=fileparse($inf,qr/\..+/);
  push(@names,$dat[0]);
  push(@paths,$dat[1]);
  push(@extensions,$dat[2]);
}

my $path=$paths[0]; #use first path.
my $outf=($opt_output ? $opt_output:$path."overlay-".join("-",@names).".png");
print "Writing output to $outf\n";

my @hues=(scalar(@names)>2 ? (map {$_/$#names*200} (0..$#names)):(100,30));
my @bits=map {"\\( $ARGV[$_] -modulate 100,100,$hues[$_] \\)"} (0..$#names);

my $cmd="convert -compose $mode @bits -composite $outf";

print "Running: $cmd\n";
print "Merging using $mode\n";
system($cmd);

__END__

=head1 NAME

overlay.pl - overlay a couple of images, offsetting the hue in each

=head1 SYNOPSIS

overlay.pl [file1] [file2 [file3 ... ]]

=head1 OPTIONS

--output - force output to go to some particular file (otherwise it's automatically derived from the input filenames)

--mode - specify a mode to use for compose. Valid modes are: clear src dst src-over dst-over src-in dst-in src-out dst-out src-atop dst-atop xor plus multiply screen overlay darken lighten color-dodge color-burn hard-light soft-light difference exclusion change-mask

For information on each mode, consult http://www.imagemagick.org/script/command-line-options.php#compose
